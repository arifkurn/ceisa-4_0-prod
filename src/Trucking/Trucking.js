import React,{Component, Fragment} from 'react';
import {NavLink, HashRouter} from 'react-router-dom';
import {withGoogleMap,withScriptjs,GoogleMap,DirectionsRenderer} from 'react-google-maps'
import {Map, GoogleApiWrapper,Marker,InfoWindow} from 'google-maps-react';
import {Button, Tabs, Tab, Modal} from 'react-bootstrap';
import DataTable, { memoize } from 'react-data-table-component';
import * as Constants from '../Constants.js';
import * as Helper from '../Helper.js';
import axios, { post } from 'axios';

const perPage = 10;
const paginationOptions = { noRowsPerPage: true };
const defaultMapCenter = { lat:'-6.1066044', lng:'106.8942988' };
var directionsService, directionsRenderer, renderedMap, googlemap;


class Trucking extends Component{
  constructor(props){
    super(props)
    this.state = {
      activeTabs: 'activeTrucks',
      loading_active_trucks: false, totalActiveTrucks: 10, activeTrucksData: [],
      loading_tracking_history: false, totalTrackingHistory: 10, trackingHistoryData: [],
      loading_booking_data: false, totalBookingData: 10, bookingData: [],
      activeIndexTrucks: null,
      activeIndexBooking: null,
      showDetailActiveTrucks: false,
      showDetailBooking: false,
      activePerPageBooking: perPage,
      activePerPageActiveTrucks: perPage,
      activePerPageTrackingHistory: perPage,
      activeTrucksBookedDate: '',
      activeTrucksFilterID: '',
      trackingHistoryBookedDate: '',
      trackingHistoryFilterID: '',
      bookingDataBookedDate: '',
      bookingDataFilterID: '',
      npwp: this.props.dataUserPortal.npwp,
      mapCenterLat: defaultMapCenter.lat,
      mapCenterLng: defaultMapCenter.lng,
      showingInfoWindow:false,
      tempListMarkers: [],
      listMarkers: [],
      activeMarker:null,
      selectedPlace:null,
      distance:0,
      intervalLiveTracking: false
    };
    this.detailActiveTrucks = this.detailActiveTrucks.bind(this);
  }

  fetchingDataActiveTrucks(page, limit, keyword, bookedDate) {
    page = page > 0 ? page-1: page;
    this.setState({ loading_active_trucks: true });
    if ( typeof keyword === "undefined" || keyword === null ) {
      keyword = "";
    } 
    if ( typeof bookedDate === "undefined" || bookedDate === null ) {
      bookedDate = "";
    } 
    const url = Constants.API_TRUCKING_LOG_STATUS;

    fetch(`${url}?page=${page}&size=${limit}&bookedDate=${bookedDate}&npwp=${this.state.npwp}&idRequestBooking=${keyword}`)
      .then(res => res.json())
      .then(parsedJSON => {
        let markers = [];
        this.setState({
          activeTrucksData: parsedJSON.content,
          totalActiveTrucks: parsedJSON.totalElements,
          loading_active_trucks: false,
        })
      })
      .catch(error => {
        this.setState({
          activeTrucksData: [],
          totalActiveTrucks: 0,
          loading_active_trucks: false
        });
        console.log(`parsing data from ${url} failed`, error);
      });
  }
  doSearchTrucks(event) {
    const {activePerPageActiveTrucks, activeTrucksBookedDate, activeTrucksFilterID} = this.state;
    this.fetchingDataActiveTrucks(1, activePerPageActiveTrucks, activeTrucksFilterID, activeTrucksBookedDate);
    event.preventDefault();
  }
  handleChangeTrucksFilterID = (event) => {
    this.setState({activeTrucksFilterID: event.target.value});
  }
  handleChangeTrucksBookedDate = (event) => {
    this.setState({activeTrucksBookedDate: event.target.value});
  }
  fetchingDataTrackingHistory(page, limit, keyword, bookedDate) {
    page = page > 0 ? page-1: page;
    this.setState({ loading_tracking_history: true });
    if ( typeof keyword === "undefined" || keyword === null ) {
      keyword = "";
    } 
    if ( typeof bookedDate === "undefined" || bookedDate === null ) {
      bookedDate = "";
    } 
    const url = Constants.API_TRUCKING_HISTORY;

    fetch(`${url}?page=${page}&size=${limit}&bookedDate=${bookedDate}&npwp=${this.state.npwp}&idRequestBooking=${keyword}`)
      .then(res => res.json())
      .then(parsedJSON => {
        let markers = [];
        this.setState({
          trackingHistoryData: parsedJSON.content,
          totalTrackingHistory: parsedJSON.totalElements,
          loading_tracking_history: false,
        })
      })
      .catch(error => {
        this.setState({
          trackingHistoryData: [],
          totalTrackingHistory: 0,
          loading_tracking_history: false
        });
        console.log(`parsing data from ${url} failed`, error);
      });
  }
  doSearchTracking(event) {
    const {activePerPageTrackingHistory, trackingHistoryBookedDate, trackingHistoryFilterID} = this.state;
    this.fetchingDataTrackingHistory(1, activePerPageTrackingHistory, trackingHistoryFilterID, trackingHistoryBookedDate);
    event.preventDefault();
  }
  handleChangeTrackingFilterID = (event) => {
    this.setState({trackingHistoryFilterID: event.target.value})
  }
  handleChangeTrackingBookedDate = (event) => {
    this.setState({trackingHistoryBookedDate: event.target.value});
  }
  fetchingDataBooking(page, limit, keyword, bookedDate) {
    page = page > 0 ? page-1: page;
    this.setState({ loading_booking_data: true });
    if ( typeof keyword === "undefined" || keyword === null ) {
      keyword = "";
    } 
    if ( typeof bookedDate === "undefined" || bookedDate === null ) {
      bookedDate = "";
    } 
    const url = Constants.API_TRUCKING_BOOKING_LIST;

    fetch(`${url}?page=${page}&size=${limit}&bookedDate=${bookedDate}&npwp=${this.state.npwp}&idRequestBooking=${keyword}`)
      .then(res => res.json())
      .then(parsedJSON => {
        let arrResults = [];
        parsedJSON.content.map((item) => {
          const objectResult = {
            idRequestBooking: item.idRequestBooking,
            booked_date: item.booked_date,
            bl_no: item.booking.bl_no,
            bl_date: Helper.formatDate(item.booking.bl_date),
            plan_date: Helper.formatDate(item.booking.plan_date),
            sp2valid_date: Helper.formatDate(item.booking.sp2valid_date),
            spcvalid_date: Helper.formatDate(item.booking.spcvalid_date),
            pod: item.booking.pod,
            destination: item.booking.destination,
            depo: item.booking.depo,
            total_distance: item.booking.total_distance,
            hargaPenawaran: "Rp "+ Helper.convertToRupiah(item.hargaPenawaran) +" "+(item.paidStatus === 1 ? "(Paid)": item.paidStatus === 0 && item.dtransporter? item.dtransporter.transporterDetil.length > 0? "(Unpaid)" : "" : ""),
            status: item.status,
            nama_platform: item.booking.platform.nama_platform,
            company_platform: item.booking.platform.company,
            platform_payment_url: item.booking.platform.paymentUrl,
            container: item.booking.container,
            paidStatus: item.paidStatus,
            dtransporter: item.dtransporter,
            other_destination: item.booking.other_destination,
          };
          arrResults.push(objectResult);
        });
        this.setState({
          bookingData: arrResults,
          totalBookingData: parsedJSON.totalElements,
          loading_booking_data: false
        }, () => {
          const getIdFromUrl = Helper.getUrlParameter('idRequestBooking');
          if ( getIdFromUrl !== '' && getIdFromUrl !== null && arrResults.length ) {
            this.setState({activeIndexBooking: 0});
          }
        })
      })
      .catch(error => {
        this.setState({
          bookingData: [],
          totalBookingData: 0,
          loading_booking_data: false
        });
        console.log(`parsing data from ${url} failed`, error);
      });
  }
  doSearchBooking(event) {
    const {activePerPageBooking, bookingDataFilterID, bookingDataBookedDate} = this.state;
    this.fetchingDataBooking(1, activePerPageBooking, bookingDataFilterID, bookingDataBookedDate);
    event.preventDefault();
  }
  handleChangeBookingFilterID = (event) => {
    this.setState({bookingDataFilterID: event.target.value});
  }
  handleChangeBookingBookedDate = (event) => {
    this.setState({bookingDataBookedDate: event.target.value});
  }
  doPayment(idRequestBooking, urlPayment) {
    const ENC_CODE = encodeURIComponent(Constants.ENC_ID)
    urlPayment = urlPayment.replace("[ENC_CODE]", ENC_CODE).replace("[ID_REQUEST_BOOKING]", idRequestBooking)
    Helper.popupWindow(urlPayment, 'Payment', window, 800, 400);
  }
  async componentDidMount() {
    const getIdFromUrl = Helper.getUrlParameter('idRequestBooking');
    this.fetchingDataActiveTrucks(1, perPage);
    this.fetchingDataTrackingHistory(1, perPage);
    if ( getIdFromUrl !== '' ) {
      this.setState({bookingDataFilterID: getIdFromUrl, showDetailBooking: true, activeTabs: 'bookingData'}, () => {
        this.fetchingDataBooking(1, perPage, getIdFromUrl);
      })
    } else {
      this.fetchingDataBooking(1, perPage);
    }
  }
  truckingLiveTracking = (data, originalMarkers) => {
    const {listMarkers, intervalLiveTracking} = this.state;
    if ( intervalLiveTracking )
      clearInterval(intervalLiveTracking);
    let apiLiveTracking = data.booking.platform.liveTrackingUrl || null;
    if ( apiLiveTracking !== null ) {
      this.setState({
        intervalLiveTracking: setInterval(() => {
          fetch(`${apiLiveTracking}?idRequestBooking=${data.idRequestBooking}&container_no=${data.container_no}`)
            .then(res => res.text())
                  .then(response => {
                    const jsonResult = JSON.parse(response);
                    var markerlist = listMarkers;
                    if ( typeof jsonResult.result !== "undefined" ) {
                      if ( jsonResult.result === "true" ) {
                        let objectTruck = {
                          title: "Truck",
                          lat: jsonResult.datacontainertracking[0].truck_lat,
                          lng: jsonResult.datacontainertracking[0].truck_lon,
                          icon: 'assets/images/truck-icon.png',
                          address: data.truckPlateNo,
                          driver: data.namaDriver,
                          driver_hp: data.hpDriver
                        };
                        if ( markerlist.length < 4 ) {
                          markerlist.push(objectTruck);
                          this.setState({listMarkers: markerlist});
                        }
                      }
                    }
                  })
                  .catch(error => {
                    this.setState({listMarkers: originalMarkers});
                    console.log(`parsing data failed`, error);
                  });
            }, 2000)
      })
    }
    
  }
  handlePageActiveTrucksClicked = (rowData) => {
    this.mapClickHandler();
    const id = rowData.idDataTransporterDetil;
    const { activeTabs, activeTrucksData, trackingHistoryData } = this.state;
    let index = -1;
    const dataTrucks = activeTabs === "trackingHistory" ? trackingHistoryData: activeTrucksData;
    for(var i = 0; i < dataTrucks.length; i++) {
        if(dataTrucks[i]['idDataTransporterDetil'] === id) {
            index = i;
        }
    }
    if ( index !== -1 ) {
      const truckDetail = dataTrucks[index];
      const destinationList = truckDetail.booking.other_destination
      const destinationListMarker = destinationList.map((item, index) => {
        return {
            lat: item.latitude,
            lng: item.longitude,
            title: `Destination ${index+1}`,
            address: item.destination,
            icon: "assets/images/destination-icon-marker.png"
          
        }
      })
      let markers = [
        {
          lat: truckDetail.pod_lat,
          lng: truckDetail.pod_lon,
          title: "POD/Origin",
          address: truckDetail.pod,
          icon: "assets/images/from-icon-marker.png"
        },
        ...destinationListMarker
      ];
      this.setState({activeIndexTrucks: index, showDetailActiveTrucks: true, showDetailBooking: false, listMarkers: [], tempListMarkers: markers}, () => {
        this.setMapDirections();
      });
    } else {
      this.setState({activeIndexTrucks: null, listMarkers: []});
    }
  }
  handlePerRowsActiveTrucksChange = async (perPage, page) => {
    this.setState({activePerPageActiveTrucks: perPage});
    this.fetchingDataActiveTrucks(page, perPage);
  }
  handlePageActiveTrucksChange  = async (page, totalRows) => {
    const {activePerPageActiveTrucks} = this.state;
    this.fetchingDataActiveTrucks(page, activePerPageActiveTrucks);
  }
  handlePerRowsTrackingHistoryChange = async (perPage, page) => {
    this.setState({activePerPageTrackingHistory: perPage});
    this.fetchingDataTrackingHistory(page, perPage);
  }
  handlePageTrackingHistoryChange  = async (page, totalRows) => {
    const {activePerPageTrackingHistory} = this.state;
    this.fetchingDataTrackingHistory(page, activePerPageTrackingHistory);
  }

  handleCloseActiveTruck = () => {
    console.log('Close');
    this.setState({showDetailActiveTrucks : false});
  }

  markerClickHandler = (props, marker, e) =>{
    this.setState({
      selectedPlace: props.info,
      activeMarker: marker,
      showingInfoWindow: true,
    });
  }

  mapClickHandler = props => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        selectedPlace:null,
        activeMarker: null
      });
    }
  };

  activeTrucks() {
    const { loading_active_trucks, activeTrucksData, totalActiveTrucks } = this.state;
    const columns = [
      {
        name: 'Provider',
        selector: 'nama_platform',
        sortable: false
      },
      {
        name: 'Truck',
        selector: 'truckPlateNo',
        sortable: false
      },
      {
        name: 'Origin',
        selector: 'pod',
        sortable: false,
        hide: "sm"
      },
      {
        name: 'Destination',
        cell: row => row.booking.other_destination.length > 0 ? <ul
        style={{margin: 0, padding: 0}}>
          {row.booking.other_destination.map((item, i) => {
        return <li key={i}><div 
                            className="destination-list"
                            >
                            {item.destination}</div></li>
        })}</ul> : row.destination,
        center:true,
        sortable: false,
        hide: "sm",
      },
    ];
    return (
      <DataTable
        title="Active Trucks"
        columns={columns}
        data={activeTrucksData}
        progressPending={loading_active_trucks}
        pagination
        paginationServer
        paginationTotalRows={totalActiveTrucks}
        paginationComponentOptions={paginationOptions}
        onRowClicked={this.handlePageActiveTrucksClicked}
        onChangeRowsPerPage={this.handlePerRowsActiveTrucksChange}
        onChangePage={this.handlePageActiveTrucksChange}
        className="customDataTable"
        subHeader
        subHeaderComponent={
          (
            <form onSubmit={this.doSearchTrucks.bind(this)} className="form-inline">
              <div className="form-row">
                <div className="col-auto">
                  <input type="text" className="form-control" placeholder="BOOKING ID" aria-describedby="input-group-search" 
                    onChange={(ev) => this.handleChangeTrucksFilterID(ev)}/>
                </div>
                <div className="col-auto">
                  <input type="date" className="form-control" placeholder="BOOKED DATE" aria-describedby="input-group-search" 
                    onChange={(ev) => this.handleChangeTrucksBookedDate(ev)}/>
                </div>
                <div className="col-auto">
                  <button className="btn btn-default">Search <i className="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          )
        }/>
    );
  } 
  trackingHistory() {
    const { loading_tracking_history, trackingHistoryData, totalTrackingHistory } = this.state;
    const columns = [
      {
        name: 'Provider',
        selector: 'nama_platform',
        sortable: false
      },
      {
        name: 'Truck',
        selector: 'truckPlateNo',
        sortable: false
      },
      {
        name: 'Origin',
        selector: 'pod',
        sortable: false,
        hide: "sm"
      },
      {
        name: 'Destination',
         cell: row => row.booking.other_destination.length > 0 ? <ul
        style={{margin: 0, padding: 0}}>
          {row.booking.other_destination.map((item, i) => {
        return <li key={i}><div 
                            className="destination-list"
                            >
                            {item.destination}</div></li>
        })}</ul> : row.destination,
        sortable: false,
        hide: "sm",
      },
    ];
    return (
      <DataTable
        title="Trucking History"
        columns={columns}
        data={trackingHistoryData}
        progressPending={loading_tracking_history}
        pagination
        paginationServer
        paginationTotalRows={totalTrackingHistory}
        paginationComponentOptions={paginationOptions}
        onRowClicked={this.handlePageActiveTrucksClicked}
        onChangeRowsPerPage={this.handlePerRowsTrackingHistoryChange}
        onChangePage={this.handlePageTrackingHistoryChange}
        className="customDataTable"
        subHeader
        subHeaderComponent={
          (
            <form onSubmit={this.doSearchTracking.bind(this)} className="form-inline">
              <div className="form-row">
                <div className="col-auto">
                  <input type="text" className="form-control" placeholder="BOOKING ID" aria-describedby="input-group-search" 
                    onChange={(ev) => this.handleChangeTrackingFilterID(ev)}/>
                </div>
                <div className="col-auto">
                  <input type="date" className="form-control" placeholder="BOOKED DATE" aria-describedby="input-group-search" 
                    onChange={(ev) => this.handleChangeTrackingBookedDate(ev)}/>
                </div>
                <div className="col-auto">
                  <button className="btn btn-default">Search <i className="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          )
        }/>
    );
  }

  setMapDirections = () => {
      const {activeIndexTrucks, activeTrucksData, tempListMarkers, trackingHistoryData, activeTabs} = this.state;
      const dataTrucks = activeTabs === "trackingHistory" ? trackingHistoryData: activeTrucksData;   
      const truckDetail = activeIndexTrucks === null ? false: dataTrucks[activeIndexTrucks];
      const destinationList = truckDetail.booking.other_destination
      const destinationLen = destinationList.length
      const checkPosition = (destination) => {
        return destination.latitude !== null && destination.longitude !== null
      }
      if (truckDetail && directionsService) {
        directionsRenderer.setMap(null);
        directionsService = new googlemap.maps.DirectionsService();
        directionsRenderer = new googlemap.maps.DirectionsRenderer();
        directionsRenderer.setMap(renderedMap);
        if (Array.isArray(destinationList) && destinationLen > 0) {
          if (truckDetail.pod_lat !== null && truckDetail.pod_lon !== null && destinationList.every(checkPosition)) {
            const start = new this.props.google.maps.LatLng(parseFloat(truckDetail.pod_lat), parseFloat(truckDetail.pod_lon));
            let waypoints
            if (destinationLen > 1 && destinationLen !== 0) {
              waypoints = destinationList
                          .filter((item, i) => i !== destinationLen-1)
                          .map(item => {
                            return {
                                    location: new this.props.google.maps.LatLng(parseFloat(item.latitude), parseFloat(item.longitude)),
                                    stopover: true
                                  }
                          })
            } else {
              waypoints = []
            }
            const end = new this.props.google.maps.LatLng(parseFloat(destinationList[destinationLen -1].latitude), parseFloat(destinationList[destinationLen - 1].longitude));
            const requestRoutes = {
              origin: start,
              destination: end,
              waypoints,
              provideRouteAlternatives: false,
              travelMode: 'DRIVING',
              unitSystem: this.props.google.maps.UnitSystem.IMPERIAL
            };
            directionsService.route(requestRoutes, (result, status) => {
              if (status === 'OK') {
                directionsRenderer.setDirections(result);
                const intervalGoogleMaps = setInterval(() => {
                  this.setState({listMarkers: tempListMarkers}, () => {
                    this.truckingLiveTracking(truckDetail, tempListMarkers)
                  });
                  clearInterval(intervalGoogleMaps)
                }, 100)
              } else {
                this.setState({listMarkers: []});
              }
            });
          }
        }
      }
  }

  destinationList (dataList, dataLen) {
    return(
    Array.isArray(dataList) && dataLen  > 0 ? 
    dataList.map((item, i) => {
      return (
        <tr className="dot-border" key={i}>
          <td className="keyfield">DESTINATION {dataLen == 1? "": i+1}</td>
          <td>{item.destination}</td>
        </tr>
      )
    })
    :
    <tr>
      <td style={{width: "150px"}} className="keyfield">DESTINATION</td>
      <td></td>
    </tr>
    )}
  
  containerTableComponent = (containerList) => {
    return(
      <table 
      className="table table-bordered table1"
      >
        <thead>
          <tr>
            <th className="keyfield">NUMBER</th>
            <th className="keyfield">TYPE</th>
            <th className="keyfield">SIZE</th>
          </tr>    
        </thead>
        <tbody>
        {
          containerList.map((item, i) => {
            return (
              <tr key={i}>
                <td className="size">{item.container_no ? item.container_no: " " }</td>
                <td>
                  <span className="keyfield">Type</span><br/>
                  {item.container_type !== "null"? <span>{item.container_type}</span>: <span>-</span>}<br/>
                  {item.over_height? (<div><span className="keyfield">Over Height</span><br/><span>{item.over_height}</span></div>): (null)}
                  {item.over_width? (<div><span className="keyfield">Over Width</span><br/><span>{item.over_width}</span></div>): (null)}
                  {item.over_length? (<div><span className="keyfield">Over Length</span><br/><span>{item.over_length}</span></div>): (null)}
                  {item.over_weight? (<div><span className="keyfield">Over Weight</span><br/><span>{item.over_weight}</span></div>): (null)}
                  {item.over_temperature? (<div><span className="keyfield">Temperature</span><br/><span>{item.over_temperature}</span></div>): (null)}
                  <span className="keyfield">Dangerous Type</span><br/>
                  {item.dangerous_type? <span>{item.dangerous_type}</span>: <span>-</span> }<br/>
                  {item.dangerous_material? (<div><span className="keyfield">Dangerous Material</span><br/>{item.dangerous_material}</div>): (null)}
                </td>
                <td className="size">{item.container_size ? <span>{item.container_size}</span>: <span>-</span> }</td> 
              </tr>
            )
          })
        }
        </tbody>
      </table>
    )
  }
  
  detailActiveTrucks(){
    const {activeIndexTrucks, listMarkers, activeTabs, trackingHistoryData, activeTrucksData,mapCenterLat, mapCenterLng} = this.state;
    const dataTrucks = activeTabs === "trackingHistory" ? trackingHistoryData: activeTrucksData;    
    const truckDetail = activeIndexTrucks === null ? false: dataTrucks[activeIndexTrucks];
    if ( truckDetail ) {
      const destinationList = truckDetail.booking.other_destination
      const formattedDestinationList = destinationList.map((item, index) => {
        return {
            lat: parseFloat(item.latitude),
            lng: parseFloat(item.longitude),         
        }
      })
      var points = [
        { 
          lat: parseFloat(truckDetail.pod_lat), 
          lng: parseFloat(truckDetail.pod_lon)
        },
        ...formattedDestinationList,
      ]
    } else {
      var points = [];
    }
    var bounds = new this.props.google.maps.LatLngBounds();
    for (var i = 0; i < points.length; i++) {
      bounds.extend(points[i]);
    }

    const initMap = (mapProps, map) => {
      const {google} = mapProps;
      googlemap = google;
      renderedMap = map;
      directionsService = new google.maps.DirectionsService();
      directionsRenderer = new google.maps.DirectionsRenderer();
      directionsRenderer.setMap(map);

      const {activeIndexTrucks, activeTrucksData, tempListMarkers, trackingHistoryData, activeTabs} = this.state;
       const dataTrucks = activeTabs === "trackingHistory" ? trackingHistoryData: activeTrucksData;   
      const truckDetail = activeIndexTrucks === null ? false: dataTrucks[activeIndexTrucks];
      const destinationList = truckDetail.booking.other_destination
      const destinationLen = destinationList.length
      const checkPosition = (destination) => {
        return destination.latitude !== null && destination.longitude !== null
      }
      if ( truckDetail ) {
        if (Array.isArray(destinationList) && destinationLen > 0) {
          if (truckDetail.pod_lat !== null && truckDetail.pod_lon !== null && destinationList.every(checkPosition)) {
            const start = new this.props.google.maps.LatLng(parseFloat(truckDetail.pod_lat), parseFloat(truckDetail.pod_lon));
            let waypoints
            if (destinationLen > 1 && destinationLen !== 0) {
              waypoints = destinationList
                          .filter((item, i) => i !== destinationLen-1)
                          .map(item => {
                            return {
                                    location: new this.props.google.maps.LatLng(parseFloat(item.latitude), parseFloat(item.longitude)),
                                    stopover: true
                                  }
                          })
            } else {
              waypoints = []
            }
            const end = new this.props.google.maps.LatLng(parseFloat(destinationList[destinationLen -1].latitude), parseFloat(destinationList[destinationLen - 1].longitude));
            const requestRoutes = {
              origin: start,
              destination: end,
              waypoints,
              provideRouteAlternatives: false,
              travelMode: 'DRIVING',
              unitSystem: this.props.google.maps.UnitSystem.IMPERIAL
            };
            directionsService.route(requestRoutes, (result, status) => {
              if (status === 'OK') {
                directionsRenderer.setDirections(result);
                const intervalGoogleMaps = setInterval(() => {
                  this.setState({listMarkers: tempListMarkers}, () => {
                    this.truckingLiveTracking(truckDetail, tempListMarkers)
                  });
                  clearInterval(intervalGoogleMaps)
                }, 100)
                
              } else {
                this.setState({listMarkers: []});
              }
            });
          }

        }
      }
    }
    const truckDesList = truckDetail? truckDetail.booking.other_destination : null;
    const truckDesLen = truckDesList? truckDesList.length: 0;
    return (
      <div className="col-sm-12 col-md-4 col-lg-6 order-lg-3 order-xl-2">
        <div className="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
          
          <div className="kt-portlet__head">
            <div className="kt-portlet__head-label">
                <h4 className="kt-font-primary kt-font-bold">
                    Trucking Details
                </h4>
            </div>    
            <div className="kt-portlet__head-toolbar">
                <div className="dropdown dropdown-inline" style={{marginTop:"10px"}}>      
                   <button className="btn btn-primary-outline" onClick={(e) => this.handleCloseActiveTruck()}><i className="fa fa-times"></i></button>
                </div>
            </div>
          </div>

          <div className="kt-portlet__body">
              {
                truckDetail ? (
                  <>
                  <div>
                    <div className="kt-widget15__map">
                      <div style={{height: "450px", position: "relative", overflow: "hidden"}}>
                        <div style={{height: "100%", width: "100%", position: "absolute",
                            top: "0px", left: "0px"}}>
                          <div className="gm-err-container">
                            <Map
                              onReady={initMap}
                              onClick={this.mapClickHandler.bind(this)}
                              google={this.props.google} 
                              zoom={11} 
                              initialCenter={{lat:parseFloat(mapCenterLat), lng: parseFloat(mapCenterLng)}}
                              bounds={bounds}
                            >
                            {
                              listMarkers.length ? listMarkers.map((item, index) => {
                                return (
                                  <Marker
                                    defaultZIndex={999999}
                                    position={{lat:parseFloat(item.lat),lng:parseFloat(item.lng)}}
                                    icon = {item.icon}
                                    onClick={this.markerClickHandler}
                                    key={index}
                                    info={item}
                                  />
                                )
                              })
                              : null
                            }
                            {
                              this.state.activeMarker !== null ? (
                                <InfoWindow
                                  marker={this.state.activeMarker}
                                  visible={this.state.showingInfoWindow}
                                  >
                                  {
                                      this.state.selectedPlace === null ? null
                                      : (
                                        <div>
                                          <span className="d-block mb-1"><b>{this.state.selectedPlace.title}</b></span>
                                          <span className="d-block">{this.state.selectedPlace.address}</span>
                                          {
                                            this.state.selectedPlace.title === "Truck" ? (
                                              <>
                                              <span className="d-block mb-1"><b>Driver</b></span>
                                              <span className="d-block">{this.state.selectedPlace.driver}</span>
                                              <span className="d-block mb-1"><b>Driver HP</b></span>
                                              <span className="d-block">{this.state.selectedPlace.driver_hp}</span>
                                              </>
                                            ): (null)
                                          }
                                        </div>
                                      )
                                    }
                                  </InfoWindow>
                              ): (null)
                            }
                            </Map>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  </>
                ): null
              }  
              {
                truckDetail ? (
                  <>
                    <table className="detailTable">
                      <tbody>
                        <tr>
                          <td colSpan="2">
                            <ul className="list-ic horizontal single" style={{marginTop: "20px", marginBottom: "0", paddingLeft: "0px"}}>
                              <li>
                                <img className="img-fluid" src={'/media/ceisa/document-3.jpg'} alt={'Document 3'} />
                                <strong>Tracking History</strong>
                                <ul className="list-ic vertical single" style={{marginBottom: "0"}}>
                                  {
                                    truckDetail.logtruck.map((item, idx) => {
                                      return (
                                        <li key={idx}>
                                          <span/>
                                          <div>
                                            <p className="kt-font-bold">{item.status}</p>
                                            <p className="kt-font-info">{item.create_date}</p>
                                          </div>
                                        </li>
                                      )
                                    })
                                  }
                                </ul>
                              </li>
                            </ul>
                            
                          </td>
                        </tr>
                        <tr>
                          <td style={{width: "150px"}} className="keyfield">ORDER-ID</td>
                          <td>{truckDetail.idRequestBooking}</td>
                        </tr>
                        <tr>
                          <td className="keyfield">TRUCK PROVIDER</td>
                          <td>{truckDetail.nama_platform}</td>
                        </tr>
                        <tr>
                          <td className="keyfield">ORIGIN</td>
                          <td>{truckDetail.pod}</td>
                        </tr>
                        { 
                          this.destinationList(truckDesList, truckDesLen)
                        }
                        <tr>
                          <td className="keyfield">TRUCK PLATE NO</td>
                          <td>{truckDetail.truckPlateNo}</td>
                        </tr>
                        <tr>
                          <td colSpan={2} style={{width: "150px"}} className="keyfield">CONTAINER</td>
                        </tr>
                        <tr>
                          <td  colSpan={2}>
                            {this.containerTableComponent(truckDetail.booking.container)}
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </>
                ):null
              }
          </div>
        </div>
      </div>
    );
  }
  handlePageBookingClicked = (rowData) => {
    const id = rowData.idRequestBooking;
    const { bookingData } = this.state;
    let index = -1;
    for(var i = 0; i < bookingData.length; i++) {
        if(bookingData[i]['idRequestBooking'] === id) {
            index = i;
        }
    }
    if ( index !== -1 ) {
      this.setState({activeIndexBooking: index, showDetailBooking: true, showDetailActiveTrucks: false});
    } else {
      this.setState({activeIndexBooking: null});
    }
  }
  handlePerRowsBookingChange = async (currentRowsPerPage, currentPage) => {
    this.setState({activePerPageBooking: currentRowsPerPage});
    this.fetchingDataBooking(currentPage, currentRowsPerPage);
  }
  handlePageBookingChange  = async (page, totalRows) => {
    const {activePerPageBooking} = this.state;
    this.fetchingDataBooking(page, activePerPageBooking);
  }

  handleCloseBooking = () => {
    console.log('Close');
    this.setState({showDetailBooking : false});
  }


  detailBooking() {
    const {activeIndexBooking, bookingData} = this.state;
    const bookingDetail = activeIndexBooking === null ? false: bookingData[activeIndexBooking];
    const bookDesList = bookingDetail? bookingDetail.other_destination : null;
    const bookDesLen = bookDesList? bookDesList.length: 0;

    return (
      <div className="col-sm-12 col-md-4 col-lg-6 order-lg-3 order-xl-2">
        <div className="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
          
          <div className="kt-portlet__head">
            <div className="kt-portlet__head-label">
                <h4 className="kt-font-primary kt-font-bold">
                    Booking Details
                </h4>
            </div>    
            <div className="kt-portlet__head-toolbar">
                <div className="dropdown dropdown-inline" style={{marginTop:"10px"}}>      
                   <button className="btn btn-primary-outline" onClick={(e) => this.handleCloseBooking()}><i className="fa fa-times"></i></button>
                </div>
            </div>
          </div>

          <div className="kt-portlet__body">
            {
              bookingDetail ? (
                <table 
                className="detailTable"
                >
                  <tbody>
                    <tr>
                      <td style={{width: "150px"}} className="keyfield">BOOKING-ID</td>
                      <td>{bookingDetail.idRequestBooking}</td>
                    </tr>
                    <tr>
                      <td style={{width: "150px"}} className="keyfield">BOOKED DATE</td>
                      <td>{bookingDetail.booked_date}</td>
                    </tr>
                    <tr>
                      <td style={{width: "150px"}} className="keyfield">BL NUMBER</td>
                      <td>{bookingDetail.bl_no}</td>
                    </tr>
                    <tr>
                      <td style={{width: "150px"}} className="keyfield">BL DATE</td>
                      <td>{Helper.formatPrintDate(bookingDetail.bl_date)}</td>
                    </tr>
                    <tr>
                      <td colSpan={2} style={{width: "150px"}} className="keyfield">CONTAINER</td>
                    </tr>
                    <tr>
                      <td  colSpan={2}>
                        {this.containerTableComponent(bookingDetail.container)}
                      </td>
                    </tr>
                    <tr>
                      <td style={{width: "150px"}} className="keyfield">SP2 VALID DATE</td>
                      <td>{Helper.formatPrintDate(bookingDetail.sp2valid_date)}</td>
                    </tr>
                    <tr>
                      <td style={{width: "150px"}} className="keyfield">SPC Valid Date</td>
                      <td>{Helper.formatPrintDate(bookingDetail.spcvalid_date)}</td>
                    </tr>
                    <tr>
                      <td colSpan={2} style={{padding: 0}}>
                        <table className="table2">                        
                          <tbody>
                          <tr className="dot-border">
                            <td style={{width: "150px"}} className="keyfield">FROM</td>
                            <td>{bookingDetail.pod}</td>
                          </tr>
                          {this.destinationList(bookDesList, bookDesLen)}
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td style={{width: "150px"}} className="keyfield">TOTAL DISTANCE</td>
                      <td>{bookingDetail.total_distance+" km"}</td>
                    </tr>
                    <tr>
                      <td style={{width: "150px"}} className="keyfield">PROVIDER</td>
                      <td>{bookingDetail.nama_platform}<br/>{bookingDetail.company_platform}</td>
                    </tr>
                    <tr>
                      <td style={{width: "150px"}} className="keyfield">PRICE</td>
                      <td>{bookingDetail.hargaPenawaran}</td>
                    </tr>
                    <tr>
                      <td style={{width: "150px"}} className="keyfield">STATUS</td>
                      <td>{bookingDetail.status}</td>
                    </tr>
                    {
                      bookingDetail.dtransporter !== null && bookingDetail.paidStatus === 0 ? (
                        <tr>
                          <td colSpan="2" className="text-center">
                            <button type="button" className="btn btn-warning btn-sm" onClick={() => this.doPayment(bookingDetail.idRequestBooking, bookingDetail.platform_payment_url)}>
                              Payment
                            </button>
                          </td>
                        </tr>
                      ): null
                    }
                  </tbody>
                </table>
              )
              : null
            }
          </div>
        </div>
      </div>
    );
  }
  bookingData() {
    const { loading_booking_data, bookingData, totalBookingData, bookingDataFilterID } = this.state;
    const columns = [
      {
        name: 'Booking-ID',
        selector: 'idRequestBooking',
        sortable: false
      },
      {
        name: 'Booked Date',
        selector: 'booked_date',
        sortable: false
      },
      {
        name: 'Price',
        selector: 'hargaPenawaran',
        sortable: false,
      },
      {
        name: 'Status',
        selector: 'status',
        sortable: false,
        hide: "sm"
      }
    ];
    return (
      <DataTable
        title="Booking List"
        columns={columns}
        data={bookingData}
        progressPending={loading_booking_data}
        pagination
        paginationServer
        paginationTotalRows={totalBookingData}
        paginationComponentOptions={paginationOptions}
        onRowClicked={this.handlePageBookingClicked}
        onChangeRowsPerPage={this.handlePerRowsBookingChange}
        onChangePage={this.handlePageBookingChange}
        className="customDataTable"
        subHeader
        subHeaderComponent={
          (
            <form onSubmit={this.doSearchBooking.bind(this)} className="form-inline">
              <div className="form-row">
                <div className="col-auto">
                  <input type="text" className="form-control" placeholder="BOOKING ID" aria-describedby="input-group-search" 
                    onChange={(ev) => this.handleChangeBookingFilterID(ev)}
                    value={bookingDataFilterID}/>
                </div>
                <div className="col-auto">
                  <input type="date" className="form-control" placeholder="BOOKED DATE" aria-describedby="input-group-search" 
                    onChange={(ev) => this.handleChangeBookingBookedDate(ev)}/>
                </div>
                <div className="col-auto">
                  <button className="btn btn-default">Search <i className="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          )
        }/>
    );
  }
  contentTruckingHeader = (breadcrump) =>{
    return(
      <Fragment>
        <div className="kt-portlet__head">

          <div className="kt-portlet__head-label">
              <h3 className="kt-portlet__head-title">
              {
                breadcrump.map((bcp, i) => {
                  return (
                    <span key={i}>
                    {
                      bcp.link === null 
                      ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                      : (<a href="/#/trucking/" className="kt-link kt-font-transform-u" onClick={()=>{this.setActiveView(bcp.link)}} key={i}>{bcp.label}</a>)
                    } 
                    {
                      i !== (breadcrump.length-1)
                      ? " > "
                      : null
                    }
                    </span>
                  )
                })
              }
              </h3>
          </div>    
    
          <div className="kt-portlet__head-toolbar">
              <div className="dropdown dropdown-inline">      
                  <NavLink to="/trucking/booktruck" className="btn btn-outline-brand ">
                    <i className="la la-file-text"></i>
                    <span className="kt-menu__link-text">Book New Truck</span>
                  </NavLink>
              </div>
          </div>
        </div>
      </Fragment>
    )
  } 
  render(){
    const {showDetailBooking, showDetailActiveTrucks, npwp, activeTabs} = this.state;
    const breadcrump = [
        {
          label: 'Trucking',
          link: null
        }
      ];
      return(
        <HashRouter>
          <div className="row">
            <div className={showDetailBooking === true || showDetailActiveTrucks === true ? "kt-hidden-mobile col-lg-6 col-md-8 order-lg-3 order-xl-1": "col-lg-12 order-lg-3 order-xl-1"}>
              <div className="kt-portlet kt-portlet--height-fluid">
                {this.contentTruckingHeader(breadcrump)}
                <Fragment>
                  <div className="kt-portlet__body">
                    <Tabs defaultActiveKey="activeTrucks" activeKey={activeTabs} id="truck_tabs" onSelect={k => this.setState({activeTabs: k})}>
                      <Tab eventKey="activeTrucks" title="Active Trucks">
                        {this.activeTrucks()}
                      </Tab>
                      <Tab eventKey="trackingHistory" title="Trucking History">
                        {this.trackingHistory()}
                      </Tab>
                      <Tab eventKey="bookingData" title="Booking">
                        {this.bookingData()}
                      </Tab>
                    </Tabs>
                  </div>
                </Fragment>
              </div>
            </div>
            {showDetailActiveTrucks === true ? this.detailActiveTrucks(): null}
            {showDetailBooking === true ? this.detailBooking(): null}
          </div>
        </HashRouter>
      )
  }

}


export default GoogleApiWrapper({
  apiKey: (Constants.googleMapAPI)
})(Trucking);