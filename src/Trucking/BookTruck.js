import React,{Component} from 'react';
import {NavLink} from 'react-router-dom';
import {Map, GoogleApiWrapper,Marker,InfoWindow} from 'google-maps-react';
import axios from 'axios'
import {Modal,Button} from 'react-bootstrap'
import {Input} from 'reactstrap'
import 'whatwg-fetch';
import uuid from "uuid";
import './booktruck.css';
import Select from 'react-select';
import DataTable, { memoize } from 'react-data-table-component';
import { createSkeletonProvider } from '@trainline/react-skeletor';
import { createSkeletonElement } from '@trainline/react-skeletor';
import * as Constants from '../Constants.js';
import * as Helper from '../Helper.js';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'
import '../assets/css/demo7/pages/general/wizard/wizard-1.css';
import TextField from '@material-ui/core/TextField';

function items(dict, fn) {
  return Object.keys(dict).map((key, i) => {
    return fn(key, dict[key], i)
  })
}

const defaultMapCenter = { lat:'-6.1066044', lng:'106.8942988' };
var directionsService, directionsRenderer;

const termOfPayment = [
  {
    label: 'Cash',
    value: 0
  },
  {
    label: 15,
    value: 15
  },
  {
    label: 30,
    value: 30
  },
  {
    label: 60,
    value: 60
  }
]

const formatPlanDate = date => {
  let day = date.getDate();
  let month = date.getMonth() + 1;
  let year = date.getFullYear();
  let hour = date.getHours();
  let minutes = date.getMinutes()
  let seconds = date.getSeconds()
  if(day<10){
    day='0'+day
  } 
  if(month<10){
    month='0'+month
  }
  if (hour < 10) (hour = '0' + hour)
  if (minutes < 10) (minutes = '0' + minutes)
  if (seconds < 10) (seconds = '0' + seconds)
  return year + '-' + month + '-' + day + ' ' + hour + ':' + minutes + ':' + seconds;
}

class BookTruck extends Component{
  constructor(props){
    super(props);
    this.refSearchResult = React.createRef();
    this.refBody = React.createRef();
    this.state={
      ws: null,
      idRequestBooking:[],
      idRequestBookingPayment:'',
      booking_date : '',
      POD :'',
      destination:'', destinationLatLng: null,
      selectedDocumentType: null,
      depo:'', selectedDepo: null, selectedDepoLatLng: null,
      selectedPayment: '', payment:'',channel:'',
      plan_date: new Date(),
      bl_no:'',bl_date:'',
      selectedBl: "",
      loadingBL: false,
      sp2valid_date:'',spcvalid_date:'',
      container_no: '', container_size:'', container_type:'', gross_weight:'',id_User:this.props.dataUserPortal.npwp,
      statusRequest:'', id_platform:'PL001',nama_platform:'',desc_platform:'',kat_platform:'',
      dbooking:[], currentIndexDataBooking: null,
      documentTypeOptions: [
        {
          value: 'BC 2.0', 
          label: 'BC 2.0',
        },
        {
          value: 'BC 1.6', 
          label: 'BC 1.6',
        },
        {
          value: 'SP3B', 
          label: 'SP3B',
        },
        {
          value: 'BC 2.3', 
          label: 'BC 2.3',
        },
        {
          value: 'BC 2.7', 
          label: 'BC 2.7',
        },
        {
          value: 'BC 3.0', 
          label: 'BC 3.0',
        },
      ],
      blNo:[],blOptions:[],
      from:[], fromOptions:[], 
      Tdepo:[],depoOptions:[],
      paymentMethodOptions:[],paymentChannelOptions:[],
      container:[], dataTableContainer: [],
      loading_searching: false, loading_container: false, toggledClearCheckedContainers: false, loading_truck_list: true,
      checkedContainers: [],checkedContainersAPI01l:[],
      dBookingId:[], bookingProcess: false,
      isChecked:true, showDetail:false,showBook:false, showTakeOrder:false,
      currentPageNumber:1,
      itemsPerPage: 5,
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: null,
      selectedFromLocation: null,
      selectedFromLatLng: null,
      lat_lng_map: [],
      lat_lng_map_length: 0,
      loading_map: false,
      mapCenter: defaultMapCenter,
      mapCenterLat: defaultMapCenter.lat,
      mapCenterLng: defaultMapCenter.lng,
      mapZoom: 11,
      mapBounds: new this.props.google.maps.LatLngBounds(),
      markers: [],
      distance: 0,
      nearTrucks: [],
      npwp: this.props.dataUserPortal.npwp,
      nama_perusahaan: this.props.dataUserPortal.nama_perusahaan,
      alamat_perusahaan: this.props.dataUserPortal.alamat,
      email_perusahaan: this.props.dataUserPortal.email,
      telepon_perusahaan: this.props.dataUserPortal.telp,
      apiBlNumber: Constants.GET_LIST_BL2_0,
      checkedTermAndCondition: false,
      currentStep: 1,
      otherDestinationLatLng: [{
        lat: null,
        lng: null,
        info: {
          Destination: null
        },
        icon: null
      }], 
      otherDestinationArray: [null], otherDestination: '',
      numInput: 0,
      // container type
      containerType: null, containerTypeOption: null, relatedFields: null, dangerousOption: null, dangerousType: null, dangerousRelatedFields: null,
      activeContainerNo: '',
      showContainerTypeDetail: false,
      selectedContainerType: 'General/ Dry Cargo',
      selectedDangerousType: 'Non Dangerous',
      modalType: 'container',
      selectedDangerousMaterial: '',
      offers: [],
      errorOffers: null,
      searchTrucksAndDrivers: false,
      bookedIdRequestBooking: null,
      cancelProcess: false,
      getTakeOrderProcess: [],
      arrayBookedId:[],
      truckTakeOrder: [],
      truckMessage: [],
      currentSubscribe: [],
      showDetailBooking: false, idBookingData: 0,
      suggestions: [], fromField: '',
      term_of_payment: 0,
      booking_note: ''
    };
    this.postAPI01 = this.postAPI01.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onChangeBcSebelas = this.onChangeBcSebelas.bind(this);
    this.checkBox = this.checkBox.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleCloseBook = this.handleCloseBook.bind(this);
    this.handleShowTakeOrder = this.handleShowTakeOrder.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.handleShowBook = this.handleShowBook.bind(this);
    this.autoCompleteMaps = this.autoCompleteMaps.bind(this);
    this.autoCompleteDepo = this.autoCompleteDepo.bind(this);
    this.autoCompleteDocumentType = this.autoCompleteDocumentType.bind(this);
    this.autoCompleteBl = this.autoCompleteBl.bind(this);
    this.handleChangePaymentMethod = this.handleChangePaymentMethod.bind(this);
    this.handleChangeOtherPlacesAutoComplete = this.handleChangeOtherPlacesAutoComplete.bind(this)
  }

  count_distance(from, others) {
    let last_end_obj = from;
    var R = 3958.8; // Radius of the Earth in miles
    var d = 0;

    if ( others.length > 0 ) {
      others.map((other, i) => {
        var rlat1 = parseFloat(last_end_obj.lat) * (Math.PI/180); // Convert degrees to radians
        var rlat2 = parseFloat(other.lat) * (Math.PI/180); // Convert degrees to radians
        var difflat = rlat2-rlat1; // Radian difference (latitudes)
        var difflon = (parseFloat(other.lng)-parseFloat(last_end_obj.lng)) * (Math.PI/180); // Radian difference (longitudes)

        var ditem = 2 * R * Math.asin(Math.sqrt(Math.sin(difflat/2)*Math.sin(difflat/2)+Math.cos(rlat1)*Math.cos(rlat2)*Math.sin(difflon/2)*Math.sin(difflon/2)));
        d = d+ditem;
        last_end_obj = other;
      })
    }
    
    //Convert to KM
    d = d*1.60934;
    d = d.toFixed(2);
    return d;
  }

  handleChangePlacesAutocomplete = destination => {
    let currentOtherArray = this.state.otherDestinationArray
    currentOtherArray[0] = destination
    this.setState({ 
      otherDestinationArray: currentOtherArray
    });
  };

  handleChangeOtherPlacesAutoComplete = (otherdestination, index) => {
    const newotherDestinationArray = this.state.otherDestinationArray
    newotherDestinationArray[index] = otherdestination
    this.setState({
      // otherDestinationArray: newDestination,
      otherDestinationArray: newotherDestinationArray
    })
  }

  handleSelectOtherPlacesAutoComplete = (otherdestination, index) => {
    const newotherDestinationArray = this.state.otherDestinationArray
    const newotherDestinationLatLng = this.state.otherDestinationLatLng
    geocodeByAddress(otherdestination)
    .then(results => getLatLng(results[0]))
    .then(latLng => {
      const datamap = {
        lat: latLng.lat,
        lng: latLng.lng,
        info: {
          Destination: otherdestination
        },
        icon: 'assets/images/destination-icon-marker.png'
      }
      newotherDestinationLatLng[index] = datamap
      newotherDestinationArray[index] = otherdestination
      this.setState({otherDestinationLatLng: newotherDestinationLatLng, otherDestinationArray: newotherDestinationArray});
    })
    .catch(error => console.error('Error', error))
  }

  handleAddOtherDestination =  () => {
    const {otherDestinationArray, otherDestinationLatLng} = this.state;
    let currentDestination = otherDestinationArray
    let currentDestinationLatLng = otherDestinationLatLng
    let newDestination = {
      lat: null,
      lng: null,
      info: {
        Destination: null
      },
      icon: null
    }
    currentDestinationLatLng.push(newDestination)
    currentDestination.push(null);
    this.setState({
      otherDestinationArray: currentDestination,
      otherDestinationLatLng: currentDestinationLatLng
    })
  }

   
  handleSelectPlacesAutocomplete = destination => {
    const newotherDestinationArray = this.state.otherDestinationArray
    const newotherDestinationLatLng = this.state.otherDestinationLatLng
    geocodeByAddress(destination)
    .then(results => getLatLng(results[0]))
    .then(latLng => {
      const datamap = {
        lat: latLng.lat,
        lng: latLng.lng,
        info: {
          Destination: destination
        },
        icon: 'assets/images/destination-icon-marker.png'
      }
      newotherDestinationLatLng[0] = datamap
      newotherDestinationArray[0] = destination
      this.setState({otherDestinationLatLng: newotherDestinationLatLng, otherDestinationArray: newotherDestinationArray});
    })
    .catch(error => console.error('Error', error))
  };
  
  getDetailBookingByUser(id) {    
    this.setState({loading_truck_list: true}, () => {
      let urlGet = Constants.API_TRUCKING_GET_SEARCH_RESULT;
      fetch(urlGet + id)
        .then(res => res.json())
        .then((result) => {
          if ( result.length > 0 ) {
            let currentOffers = this.state.offers
            for ( var co in result ) {
              let checkIdRequest = this.checkIdRequestBookingOnOffers(result[co].idRequestBooking)
              if ( Helper.in_array(result[co].idRequestBooking, this.state.idRequestBooking) && !checkIdRequest ) {
                currentOffers.push(result[co])
              }
            }
            this.setState({
              offers: currentOffers,
              loading_searching: false, 
              errorOffers: currentOffers.length > 0 ? null: 'Trucking saat ini sedang tidak tersedia, cobalah beberapa saat lagi.'
            });
          } else {
            this.setState({errorOffers: "Trucking saat ini sedang tidak tersedia, cobalah beberapa saat lagi.", loading_searching: false})
          }
        },
        (error) => {
          this.setState({errorOffers: "Truck not found, please try again later.", loading_searching: false})
          console.log(error)
        }
        )
    });
  }
  getNearTrucks(datamap, radius) {
    this.setState({
      nearTrucks: [], 
      lat_lng_map: [],
      loading_map: true
    }, () => {
      if ( datamap.lat.length && datamap.lng.length && radius ) {
        let dataPOST = new FormData();
        
        dataPOST.append("f_lat", parseFloat(datamap.lat));
        dataPOST.append("f_lon", parseFloat(datamap.lng));
        dataPOST.append("f_radius", parseInt(radius));
        dataPOST.append("f_limit", 100);

        const urlGet = '//toms-id.com/toms_server/api/getNearTrucks';
        fetch(urlGet, {
          method:"POST", 
          body:dataPOST
        })
        .then(Response => Response.json())
        .then((result) => {
            const dataResult = result.data;
            let nearTruckResults = []
            let newArrayNearTruck = []
            if ( dataResult ) {
              dataResult.map((item, i) => {
                const datamap = {
                  lat: item.tracking_lat,
                  lng: item.tracking_lng,
                  icon: 'assets/images/truck-icon.png',
                  info: {
                    Plate: item.truck,
                    Address: item.tracking_address
                  }
                }
                if ( i < 100 )
                  newArrayNearTruck.push(datamap);
              })
              nearTruckResults = dataResult.data;
            }
            this.setState({
              nearTrucks: nearTruckResults, 
              lat_lng_map: newArrayNearTruck,
              lat_lng_map_length: newArrayNearTruck.length,
              // mapCenterLat: datamap.lat,
              // mapCenterLng: datamap.lng,
              // mapZoom: 16,
              loading_map: false
            }, () => { this.setMapBounds() });
        },(error) => {
            this.setState({
              nearTrucks: [], 
              lat_lng_map: [],
              selectedFromLatLng: datamap,
              // mapCenterLat: datamap.lat,
              // mapCenterLng: datamap.lng,
              // mapZoom: 16,
              loading_map: false
            }, () => { this.setMapBounds() });
          }
        )
      }
      else {
        this.setState({
          nearTrucks: [], 
          lat_lng_map: [],
          selectedFromLatLng: null,
          // mapCenterLat: defaultMapCenter.lat,
          // mapCenterLng: defaultMapCenter.lng,
          // mapZoom: 13,
          loading_map: false
        });
      }
    });
  }

  fetchContainerByNoBl(noBl){
    return fetch(Constants.API_BOOKING_CONTAINER+noBl)
    .then(Response => Response.json());
  }
  getContainerByBl(noBl, tglBl) {
    const {selectedDocumentType} = this.state;
    let apiContainer = null;
    let resultName = null;
    if ( selectedDocumentType === "BC 2.0" ) {
      apiContainer = Constants.GET_CONTAINER1_2_0
      resultName = "Data Kontainer BC20"
    } else if ( selectedDocumentType === "BC 1.6" ) {
      apiContainer = Constants.GET_CONTAINER1_1_6
      resultName = "Data Kontainer BC16"
    } else if ( selectedDocumentType === "SP3B" ) {
      apiContainer = Constants.GET_CONTAINER1_SP3B
      resultName = "Data Kontainer SP3B"
    } else if ( selectedDocumentType === "BC 2.3" ) {
      apiContainer = Constants.GET_CONTAINER1_2_3
      resultName = "Data Kontainer BC16"
    } else if ( selectedDocumentType === "BC 2.7" ) {
      apiContainer = Constants.GET_CONTAINER1_2_7
      resultName = "data status impor"
    } else if ( selectedDocumentType === "BC 3.0" ) {
      apiContainer = Constants.GET_CONTAINER1_3_0
      resultName = "Data Kontainer SP3B"
    }
    if ( apiContainer !== null ) {
      this.setState({loading_container: true, container: [], dataTableContainer: [], checkedContainers: []}, () => {
        // Get Container
          noBl = noBl
          if (selectedDocumentType === "BC 2.7"){
            tglBl = ''
          } else {
            tglBl = tglBl.replace(/-/g, "")
          }
          fetch(apiContainer + noBl +'/'+tglBl)
          .then(Response => Response.json())
          .then((result) => {
            let containerResults = [];
            let dataTableContainerResults = result[resultName];
            this.fetchContainerByNoBl(noBl).then((result2) => {
              let containerNos = result2.map(container=> container.container_no );

              result[resultName].map((containerData, indexContainer) => {
                let objectContainer = containerData;
                objectContainer.selectedContainerType = 'General/ Dry Cargo';
                objectContainer.over_weight = null;
                objectContainer.over_height = null;
                objectContainer.over_length = null;
                objectContainer.over_width = null;
                objectContainer.temperatur = null;
                objectContainer.dangerous_type = 'Non Dangerous';
                objectContainer.dangerous_material = null

                if( ! Helper.in_array(containerData.container_no,containerNos)){
                  containerResults[containerData.container_no] = objectContainer
                  containerResults.length=1;
                }

              });

              dataTableContainerResults = dataTableContainerResults.filter(row=>!Helper.in_array(row.container_no,containerNos));
              
              this.setState({
                loading_container: false,
                container: containerResults,
                dataTableContainer: dataTableContainerResults
              });
            });
          },(error) => {
            this.setState({loading_container: false, container:[], dataTableContainer: []});
            }
          )
          
      })
    } else {
      this.setState({
        loading_container: false,
        container:[],
        dataTableContainer: [],
        checkedContainers: []
      });
    }
  }
  componentDidMount(){
          // Get From
          const apiUrlport = Constants.API_TRUCKING_GET_PORT_LIST
          fetch(apiUrlport)
            .then(Response => Response.json())
            .then((result) => {
              let newFromOptions = [];
              result.map((item, index) => {
                let fromdata = {value: index, label: item.name}
                newFromOptions.push(fromdata)
              });
              this.setState({from: result, fromOptions: newFromOptions})
            }, 
            (error) => {
              this.setState({ error })
            })
          // Get Depo
          // const apiUrldepo = Constants.API_TRUCKING_GET_DEPO_LIST
          // fetch(apiUrldepo)
          //   .then(res => res.json())
          //   .then((result) => {
          //     let newTdepoOptions = [];
          //     result.map((item,index)=>{
          //       let depodata = {value:index, label : item.name}
          //       newTdepoOptions.push(depodata)
          //     });
          //     this.setState({Tdepo: result, depoOptions:newTdepoOptions})
          //   },
          //     (error) => {this.setState({ error })}
          //   )
          // Get DetilBooking
          // let id = this.state.id_User;
          // this.getDetailBookingByUser(id)
          this.fetchDataContainerType()
          this.connectWebsocket();
  }

  timeout = 250;
  checkIdRequestBookingOnOffers = (idRequestBooking) => {
    const {offers} = this.state
    for ( var i in offers ) {
      if ( offers[i].idRequestBooking == idRequestBooking ) {
        return true;
      }
      return false;
    }
  }
  checkIdRequestBookingOnTrucks = (idRequestBooking) => {
    const {truckTakeOrder} = this.state
    for ( var i in truckTakeOrder ) {
      if ( truckTakeOrder[i].idRequestBooking == idRequestBooking ) {
        return true;
      }
      return false;
    }
  }
  getDetilFromOffers = (idRequestBooking) => {
    const {offers} = this.state
    for ( var x in offers ) {
      if ( offers[x].idRequestBooking === idRequestBooking ) 
        return offers[x]
    }
    return false
  }
  getTruckMessage = (idRequestBooking) => {
    const {truckMessage} = this.state
    for ( var x in truckMessage ) {
      if ( truckMessage[x].idRequestBooking === idRequestBooking ) 
        return truckMessage[x].message
    }
    return null
  }
  getTrucksFromTakeOrder = (idRequestBooking) => {
    const {truckTakeOrder} = this.state
    for ( var x in truckTakeOrder ) {
      if ( truckTakeOrder[x].idRequestBooking === idRequestBooking ) 
        return truckTakeOrder[x].transporterDetil
    }
    return []
  }
  connectWebsocket = () => {
    const {currentSubscribe} = this.state;
    var ws = new WebSocket(Constants.SOCKET_SERVER, Constants.SOCKET_TOKEN);
    let that = this; // cache the this
    var connectInterval;

    // websocket onopen event listener
    ws.onopen = () => { 
      console.log("Connected to Socket");
      this.setState({ ws: ws }, () => {
        for ( var is in currentSubscribe ) {
          ws.send(JSON.stringify(currentSubscribe[is]));
        }
        // For Test API-02 dan API-09
        // this.setState({idRequestBooking:["60ba9fc2-52ea-427c-996c-2292042bff4a0"]}, () => { 
        //   this.subscribeSocketConsumer("subscribe", this.state.id_User, 'nle-offer', this.state.id_User)
        // });
      });

      that.timeout = 250; // reset timer to 250 on open of websocket connection 
      clearTimeout(connectInterval); // clear Interval on on open of websocket connection
    };
    ws.onmessage = evt => {
      const message = JSON.parse(evt.data)
      const status = message.status
      if ( status === "ok" ) {
        const topic = message.topic
        const key = message.key
        const data = message.data
        if ( topic === "nle-offer" ) {
          let currentOffers = this.state.offers
          let checkIdRequest = this.checkIdRequestBookingOnOffers(data.idRequestBooking)
          if ( Helper.in_array(data.idRequestBooking, this.state.idRequestBooking) && !checkIdRequest ) {
            currentOffers.push(data)
          }
          this.setState({offers:currentOffers, loading_searching: false, errorOffers: null})
        } else if ( topic === "nle-truck" ) {
          let currentTruck = this.state.truckTakeOrder
          let checkIdRequest = this.checkIdRequestBookingOnTrucks(data.idRequestBooking)
          if ( !checkIdRequest ) {
            currentTruck.push(data)
          }
          this.setState({truckTakeOrder: currentTruck})
        }
      }
    }
    // websocket onclose event listener
    ws.onclose = e => {
      console.log(
        `Socket is closed. Reconnect will be attempted in ${Math.min(
          10000 / 1000,
          (that.timeout + that.timeout) / 1000
        )} second.`,
        e.reason
      );
      that.timeout = that.timeout + that.timeout; //increment retry interval
      connectInterval = setTimeout(this.checkWebsocket, Math.min(10000, that.timeout)); //call check function after timeout
    };

    ws.onerror = err => {
      console.error(
        "Socket encountered error: ",
        err.message,
        "Closing socket"
      );

      ws.close();
    };
  }
  checkWebsocket = () => {
    const { ws } = this.state;
    if (!ws || ws.readyState == WebSocket.CLOSED) this.connectWebsocket(); //check if websocket instance is closed, if so call `connect` function.
  }
  refreshSearchResult() {
    // Get DetilBooking
    let id = this.state.id_User;
    this.getDetailBookingByUser(id)
  }
  setMapDirections = () => {
    const {selectedFromLatLng, destinationLatLng, selectedDepoLatLng, otherDestinationLatLng, lat_lng_map, lat_lng_map_length} = this.state;
    
    let newArrayLatLng = [];
    let newMarkerArray = [];
    lat_lng_map.map((item, i) => {
      if ( i < lat_lng_map_length ) {
        let obj_latlng = {
          lat: parseFloat(item.lat),
          lng: parseFloat(item.lng)
        }
        newArrayLatLng.push(obj_latlng);
        newMarkerArray.push(item);
      }
    });
    if ( selectedFromLatLng !== null ) {
      newArrayLatLng.push({lat:parseFloat(selectedFromLatLng.lat), lng:parseFloat(selectedFromLatLng.lng)});
      newMarkerArray.push(selectedFromLatLng);
      const start = new this.props.google.maps.LatLng(parseFloat(selectedFromLatLng.lat), parseFloat(selectedFromLatLng.lng));
      let waypoints = [];
      let end;
      otherDestinationLatLng.map((valOther, indexOther) => {
        if ( indexOther < (otherDestinationLatLng.length-1) ) {
          let objPoint = new this.props.google.maps.LatLng(parseFloat(valOther.lat), parseFloat(valOther.lng));
          waypoints.push({location: objPoint, stopover: true});
        } else {
          end = new this.props.google.maps.LatLng(parseFloat(valOther.lat), parseFloat(valOther.lng));
        }
        if ( valOther.lat !== null && valOther.lng !== null ) {
          newArrayLatLng.push({lat:parseFloat(valOther.lat), lng:parseFloat(valOther.lng)});
          newMarkerArray.push(valOther);
        }
      })

      const requestRoutes = {
        origin: start,
        destination: end,
        waypoints: waypoints,
        provideRouteAlternatives: false,
        travelMode: 'DRIVING',
        unitSystem: this.props.google.maps.UnitSystem.IMPERIAL
      };
      directionsService.route(requestRoutes, (result, status) => {
        if (status == 'OK') {
          directionsRenderer.setDirections(result);
          const intervalGoogleMaps = setInterval(() => {
            this.setState({markers: newMarkerArray});
            clearInterval(intervalGoogleMaps)
          }, 100)
        }
      });
      const distance_total = this.count_distance(selectedFromLatLng, otherDestinationLatLng);
      this.setState({distance: distance_total});
    }
  }

  setMapBounds() {
    const {lat_lng_map, lat_lng_map_length, selectedFromLatLng, destinationLatLng, selectedDepoLatLng, otherDestinationLatLng} = this.state;
    let newArrayLatLng = [];
    let newMarkerArray = [];
    lat_lng_map.map((item, i) => {
      if ( i < lat_lng_map_length ) {
        var obj_latlng = {
          lat: parseFloat(item.lat),
          lng: parseFloat(item.lng)
        }
        newArrayLatLng.push(obj_latlng);
        newMarkerArray.push(item);
      }
    });
    if ( selectedFromLatLng !== null ) {
      newArrayLatLng.push({lat:parseFloat(selectedFromLatLng.lat), lng:parseFloat(selectedFromLatLng.lng)});
      newMarkerArray.push(selectedFromLatLng);
    }
    if ( destinationLatLng !== null ) {
      newArrayLatLng.push({lat:parseFloat(destinationLatLng.lat), lng:parseFloat(destinationLatLng.lng)});
      newMarkerArray.push(destinationLatLng);
    }
    if ( otherDestinationLatLng.length ) {
      otherDestinationLatLng.map((itemOther, iOther) => {
        if ( itemOther.lat !== null && itemOther.lng !== null ) {
          newArrayLatLng.push({lat:parseFloat(itemOther.lat), lng:parseFloat(itemOther.lng)});
          newMarkerArray.push(itemOther);
        }
      })
    }
    // if ( selectedDepoLatLng !== null ) {
    //   newArrayLatLng.push({lat:parseFloat(selectedDepoLatLng.lat), lng:parseFloat(selectedDepoLatLng.lng)});
    //   newMarkerArray.push(selectedDepoLatLng);
    // }
   
    var bounds = new this.props.google.maps.LatLngBounds();
    for (var i = 0; i < newArrayLatLng.length; i++) {
      bounds.extend(newArrayLatLng[i]);
    }
    this.setState({mapBounds: bounds});
  }
  displayMarkers = () => { 
    const {markers} = this.state;
    return (
      markers.map((store, index) => {
        return <Marker key={index} id={index}
          icon={store.icon ? store.icon: null}
          position={{lat: parseFloat(store.lat),lng: parseFloat(store.lng)}}
          info={store}
          onClick={this.markerClickHandler} />
      })
    )
  }
  markerClickHandler = (props, marker, e) => {
    this.setState({
      selectedPlace: props.info.info,
      activeMarker: marker,
      showingInfoWindow: true
    });
  }
  mapClickHandler = (props) => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        selectedPlace: null,
        activeMarker: null
      });
    }
  }

  handleClick(page){
    let pageAfter = this.state.currentPageNumber;
    if ( page === 'next' ) {
      pageAfter = this.state.currentPageNumber+1
    } else {
      pageAfter = this.state.currentPageNumber-1;
    }
    this.setState({
      currentPageNumber: Number(pageAfter)
    });
  }

  postAPI01(e){
    const {ws} = this.state;
    e.preventDefault();
    var truckingPlatform = Constants.TRUCKING_PLATFORM;
    var API01 = [];
    
    let defaultIdRequestBooking = uuid();
    let idRequestBookingList = [];
    var indexBooking = 1;
    for ( let i in truckingPlatform ) {
      let idRequestBooking = defaultIdRequestBooking+indexBooking;
      idRequestBookingList.push(idRequestBooking);
      let newCheckedContainersAPI01l = [];
      this.state.checkedContainers.map((itemContainer, ic) => {
        let dataContainerAPI01l ={
          idRequestBooking: idRequestBooking,
          container_no: itemContainer.container_no,
          container_size: itemContainer.container_size,
          container_type: itemContainer.container_type,
          dangerous_type: itemContainer.dangerous_type,
          dangerous_material: itemContainer.dangerous_material,
          over_height: itemContainer.over_height,
          over_weight: itemContainer.over_weight,
          over_length: itemContainer.over_length,
          over_width: itemContainer.over_width,
          temperatur: itemContainer.temperatur
        };
        newCheckedContainersAPI01l.push(dataContainerAPI01l);
      });
      let otherDestinationList = [];
      let urutanDestination = 1;
      this.state.otherDestinationLatLng.map((itemDes, a) => {
        let objOtherDes = {
          idRequestBooking: idRequestBooking,
          urutan: urutanDestination,
          destination: itemDes.info.Destination,
          latitude: itemDes.lat,
          longitude: itemDes.lng
        }
        otherDestinationList.push(objOtherDes)
        urutanDestination++
      });
      var objAPI01 = {
        idRequestBooking : idRequestBooking,
        booking_date: Helper.getDateTimeNow(),
        pod: this.state.POD,
        pod_lat: this.state.selectedFromLatLng === null ? null: this.state.selectedFromLatLng.lat,
        pod_lon: this.state.selectedFromLatLng === null ? null: this.state.selectedFromLatLng.lng,
        total_distance: this.state.distance,
        party: this.state.checkedContainers.length || 0,
        // plan_date: this.state.plan_date+' '+Helper.getTimeNow(),
        plan_date: formatPlanDate(this.state.plan_date),
        bl_no:this.state.bl_no,
        bl_date:Helper.formatDate(this.state.bl_date),
        sp2valid_date:this.state.sp2valid_date,
        spcvalid_date:this.state.spcvalid_date,
        container:newCheckedContainersAPI01l,
        gross_weight:this.state.gross_weight,
        id_User:this.state.id_User,
        npwp: this.state.id_User,
        statusRequest: this.state.statusRequest,
        id_platform:truckingPlatform[i],
        other_destination:otherDestinationList,
        company: this.state.nama_perusahaan, 
        address: this.state.alamat_perusahaan, 
        email: this.state.email_perusahaan, 
        mobile: this.state.telepon_perusahaan,
        doc_type: this.state.selectedDocumentType,
        term_of_payment: this.state.term_of_payment,
      };
      API01.push(objAPI01);
      indexBooking++;
    }
    this.setState({offers: [], truckTakeOrder: [], searchTrucksAndDrivers: false, loading_searching: true, errorOffers: null, loading_truck_list: true, idRequestBooking:idRequestBookingList}, () => {
      var headers = new Headers();
      headers.append('Content-Type','application/json');
      headers.append('Accept','application/json');

      let url01 = Constants.API_TRUCKING_MULTIPLE_BOOKING;
      fetch(url01,{
        method:"POST",
        headers:headers,
        body:JSON.stringify(API01),
      })
      .then(Response =>{
        if(Response.status === 200){
          this.setState({
            loading_truck_list: false,
            currentStep: 4
          }, () => {
            if ( ws && ws.readyState !== WebSocket.CLOSED ) {
              this.subscribeSocketConsumer("subscribe", this.state.id_User, 'nle-offer', this.state.id_User);
            } else {
              setTimeout(() => {
                this.getDetailBookingByUser(this.state.id_User);
              }, 3000);
            }
          });
        }
      })
      .catch(function(error){
        console.log(error.toString())
      })
    });
    return false
  }

  subscribeSocketConsumer = (type, groupId, topic, key) => {
    const {ws, currentSubscribe} = this.state
    let subscribeList = currentSubscribe;
    if ( ws && ws.readyState !== WebSocket.CLOSED ) {
      let subscribeMessage = {
        type: type,
        topic: topic,
        key: key,
        groupId: groupId
      };
      ws.send(JSON.stringify(subscribeMessage));
      if ( type === "unsubscribe" ) {
        let subscribeIndex = Helper.array_search(subscribeMessage, subscribeList);
        if ( subscribeIndex ) {
          subscribeList.splice(subscribeIndex, 1);
          this.setState({currentSubscribe: subscribeList});
        }
      } else {
        if ( !Helper.in_array(subscribeMessage, subscribeList) ) {
          subscribeList.push(subscribeMessage);
        }
        this.setState({currentSubscribe: subscribeList});
        setTimeout(() => {
          if ( topic === "nle-offer" && this.state.offers.length < 1 ) {
            this.getDetailBookingByUser(this.state.id_User);
          }
        }, 10000);
        setTimeout(() => {
          let checkTakeOrder = this.checkIdRequestBookingOnTrucks(this.state.bookedIdRequestBooking);
          if ( topic === "nle-truck" && !checkTakeOrder ) {
            this.getTakeOrderFromAPI(this.state.bookedIdRequestBooking, true);
          }
        }, 60000);
      }
    }
  }
  getTakeOrderFromAPI(idRequestBooking, showError) {
    var currentTruck = this.state.truckTakeOrder
    var newObjTruckMessage = {
      idRequestBooking: idRequestBooking,
      message: ""
    }
    var currentTruckMessage = this.state.truckMessage
    var checkTruckMessage = false
    var indexTruckMessage = null
    for ( let ct in currentTruckMessage ) {
      if ( currentTruckMessage[ct].idRequestBooking === idRequestBooking && !checkTruckMessage ) {
        checkTruckMessage = true
        indexTruckMessage = ct
      }
    }
    axios.get(Constants.API_GET_TRUCK_BY_ID+idRequestBooking)
    .then(res => {
      if ( res.status === 200 ) {
        var result = res.data
        if ( result !== null && result !== "" ) {
          let checkIdRequest = this.checkIdRequestBookingOnTrucks(idRequestBooking)
          if ( !checkIdRequest ) {
            currentTruck.push(result)
          }
          newObjTruckMessage.message = ""
          if ( !checkTruckMessage ) {
            currentTruckMessage.push(newObjTruckMessage)
          } else if ( indexTruckMessage !== null ) {
            currentTruckMessage[indexTruckMessage] = newObjTruckMessage
          }
          this.setState({truckTakeOrder: currentTruck, truckMessage: currentTruckMessage});
        } else {
          if ( showError )
            newObjTruckMessage.message = "Belum ada Truck dan Driver yang mengambil pesanan anda untuk saat ini. Silahkan cek kembali pesanan anda nanti melalui Tab Booking pada menu Trucking."
          if ( !checkTruckMessage ) {
            currentTruckMessage.push(newObjTruckMessage)
          } else if ( indexTruckMessage !== null ) {
            currentTruckMessage[indexTruckMessage] = newObjTruckMessage
          }
          this.setState({truckMessage: currentTruckMessage})
        }
      } else {
        if ( showError )
          newObjTruckMessage.message = "Maaf, sedang terjadi kendala dalam sistem. Silahkan cek kembali pesanan anda nanti melalui Tab Booking pada menu Trucking."
        if ( !checkTruckMessage ) {
          currentTruckMessage.push(newObjTruckMessage)
        } else if ( indexTruckMessage !== null ) {
          currentTruckMessage[indexTruckMessage] = newObjTruckMessage
        }
        this.setState({truckMessage: currentTruckMessage})
      }
    })
    .catch(error => {
      console.log(error)
    })
  }
  getTakeOrder = (idRequestBooking, isSocket) => {
    const {getTakeOrderProcess, ws, id_User, currentSubscribe} = this.state
    if ( ws && ws.readyState !== WebSocket.CLOSED && isSocket ) {
      let subscribeMessage = {
        type: "subscribe",
        topic: "nle-truck",
        key: id_User,
        groupId: id_User
      };
      let checkSubscribeExists = Helper.array_search(subscribeMessage, currentSubscribe);
      if ( !checkSubscribeExists ) {
        ws.send(JSON.stringify(subscribeMessage));
        let subscribeList = currentSubscribe;
        subscribeList.push(subscribeMessage);
        this.setState({currentSubscribe: subscribeList});  
        setTimeout(() => {
          let checkTakeOrder = this.checkIdRequestBookingOnTrucks(idRequestBooking)
          if ( !checkTakeOrder ) {
            this.getTakeOrderFromAPI(idRequestBooking, true);
          }
        }, 60000);
      }
    } else {
      setTimeout(() => {
        var totalMinutes = 0;
        const intervalTakeOrder = setInterval(() => {
            let checkTakeOrder = this.checkIdRequestBookingOnTrucks(idRequestBooking)
            var showError = totalMinutes === 58000 ? true: false
            if ( !checkTakeOrder ) {
              this.getTakeOrderFromAPI(idRequestBooking, showError);
            }
            totalMinutes = totalMinutes+2000
            if ( checkTakeOrder || totalMinutes > 58000 )
              clearInterval(intervalTakeOrder)
          }, 2000)
      }, 15000)
    }
  }
  getOfferFromConsumer = (groupId, topic, key) => {
    let urlConsumer = Constants.KAFKA_CONSUMER+"?token="+Constants.SOCKET_TOKEN+"&groupId="+groupId+"&topic="+topic+"&key="+key;
    fetch(urlConsumer)
      .then(res => res.json())
      .then((result) => {
        this.setState({
          offers: result,
          errorOffers: null,
          loading_searching: false
        });
      },
      (error) => {
        this.setState({
          offers: [],
          truckTakeOrder: [], searchTrucksAndDrivers: false,
          errorOffers: "Something error, please try again or contact administrator.",
          loading_searching: false
        });
      })
  }

  postAPI04(id){
    const {currentSubscribe, id_User, ws} = this.state
    this.setState({bookingProcess: true});
    var headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept','application/json');
    headers.append('Access-Control-Allow-Origin',true);

    var API02 ={
      isBooked : 1,
      booking_note: this.state.booking_note
    }
    // console.log("Post API04",API04)

    let urlDetail = Constants.API_TRUCKING_SET_BOOKED
    
    fetch(urlDetail + id,{
          method:"PUT",
          headers:headers,
          body:JSON.stringify(API02)
        })
        .then(Response => {
          let currentArrayBooked = this.state.arrayBookedId
          let checkArrayBooked = Helper.array_search(id, currentArrayBooked)
          if ( !checkArrayBooked ) 
            currentArrayBooked.push(id)
          
          this.setState({searchTrucksAndDrivers: true, bookedIdRequestBooking: id, arrayBookedId: currentArrayBooked, showBook: false, showDetail: false, bookingProcess: false}, () => {
            this.handleShowTakeOrder(id)
          });
          //this.props.history.push('/trucking')
        })
        .catch(error => {
          alert("Order failed, please try again later.")
          this.setState({bookingProcess: false});
          console.log(error.toString())
        })
  }

  handleClose(){
    this.setState({showDetail : false})
  }
  handleCloseBook(){
    this.setState({showBook : false, showDetail: false})
  }
  handleShowTakeOrder(id) {
    this.setState({showTakeOrder: !this.state.showTakeOrder, bookedIdRequestBooking: id}, () => {
      if ( this.state.showTakeOrder ) {
        this.getTakeOrder(id, true)
      } else {
        this.setState({bookedIdRequestBooking: null})
      }
    })
  }
  handleShow(currentIndex, param) {
    this.setState({
      showDetail : true, 
      currentIndexDataBooking: currentIndex,
      isLoading: false
    });
  }
  doPayment(idRequestBooking, urlPayment) {
    const ENC_CODE = encodeURIComponent(Constants.ENC_ID)
    urlPayment = urlPayment.replace("[ENC_CODE]", ENC_CODE).replace("[ID_REQUEST_BOOKING]", idRequestBooking)
    Helper.popupWindow(urlPayment, 'Payment', window, 800, 400);
  }
  openTermAndCondition(url) {
    Helper.popupWindow(url, 'Term & Condition', window, 800, 600);
  }
  handleClickedTermAndCondition(e) {
    const {checkedTermAndCondition} = this.state
    this.setState({checkedTermAndCondition: !checkedTermAndCondition})
  }
  handleShowBook(indexDataBooking, id){
    let index = 0;
    let currentMethodOptions = [];
    let newPaymentOptions = [];
    let mappingMethodChannel = [];
    this.setState({
      currentIndexDataBooking: indexDataBooking,
      idRequestBookingPayment: id,
      showBook: true,
      checkedTermAndCondition: false
    });
  }
  onChange = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]:val}, () => {
      if ( nam === 'bl_date' && this.state.bl_no ) {
        this.getContainerByBl(this.state.bl_no, val);
      }
    });
  }

  dateTimePicker = plan_date => {
    this.setState({plan_date})
  }

  checkBox = (state) => {
    const checkedContainers = []
    const checkedContainersAPI01l = []
    state.selectedRows.map((selectedData,i)=>{
      const dataContainer = {
        container_no: selectedData.container_no,
        container_size: selectedData.container_size,
        container_type: selectedData.selectedContainerType,
        dangerous_type: selectedData.dangerous_type,
        dangerous_material: selectedData.dangerous_material,
        over_height: selectedData.over_height,
        over_weight: selectedData.over_weight,
        over_length: selectedData.over_length,
        over_width: selectedData.over_width,
        temperatur: selectedData.temperatur,
      }
      const dataContainerAPI01l ={
        container_no: selectedData.container_no,
        container_size: selectedData.container_size,
        container_type: selectedData.selectedContainerType,
        dangerous_type: selectedData.dangerous_type,
        dangerous_material: selectedData.dangerous_material,
        over_height: selectedData.over_height,
        over_weight: selectedData.over_weight,
        over_length: selectedData.over_length,
        over_width: selectedData.over_width,
        temperatur: selectedData.temperatur
      }
      checkedContainers.push(dataContainer)
      checkedContainersAPI01l.push(dataContainerAPI01l)
    })
  this.setState({checkedContainers: checkedContainers, checkedContainersAPI01l:checkedContainersAPI01l}, () => {
    // console.log("Select Container",checkedContainers,checkedContainersAPI01l)
  });
  /*var container = {container_no:param1,container_size:param2,container_type:param3}
  var newArray = this.state.checkedContainers;
  if ( event.target.checked ) {
    newArray.push(container);
    this.setState({checkedContainers: newArray}, () => {
      console.log(newArray)
    });
  } else {
    let index = -1;
    for(var i = 0; i < newArray.length; i++) {
        if(JSON.stringify(newArray[i]) === JSON.stringify(container)) {
            index = i;
        }
    }
    if ( index !== -1 ) {
      newArray.splice(index, 1);
      this.setState({checkedContainers: newArray}, () => {
        console.log(newArray)
      });
    }
  }*/
  };
  handleClearCheckedContainers = () => {
    this.setState({ 
      toggledClearCheckedContainers: !this.state.toggledClearCheckedContainers,
      checkedContainers: []
    }, () => {
      console.log('Clear Containers');
    })
  }

   handleChangeFrom = pod => {
    this.setState({ 
      POD: pod
    });
  };

  autoCompleteMaps = (selectedPOD) => {
    geocodeByAddress(selectedPOD)
    .then(results => getLatLng(results[0]))
    .then(latLng => {
      const datamap = {
        lat: latLng.lat.toString(),
        lng: latLng.lng.toString(),
        info: {
          From: selectedPOD
        },
        icon: 'assets/images/destination-icon-marker.png'
      }
      this.setState({POD: selectedPOD, selectedFromLatLng: datamap}, () => {this.getNearTrucks(datamap, 3)});
    })
    .catch(err => console.log(err))
  }

  autoCompleteDepo(selected){
    const index = selected.value;
    const selectedDepoData = this.state.Tdepo[index];
    const datamap = {
      lat: selectedDepoData.lat,
      lng: selectedDepoData.lon,
      info: {
        Name: selectedDepoData.name,
        Address: selectedDepoData.address
      },
      icon: 'assets/images/depo-icon-marker.png'
    }
    this.setState({
      selectedDepo:index, 
      depo:selectedDepoData.name,
      selectedDepoLatLng: datamap
    }, () => {
      this.setMapBounds();
    })
  }

  handleChangePaymentMethod = (selected) => {
    const {paymentMethodOptions, paymentChannelOptions} = this.state;
    const index = selected.target.value;
    const dataPaymentMethod = paymentMethodOptions[index];
    const newChannelOptions = dataPaymentMethod.channel;
    const selectedChannel = newChannelOptions.length ? newChannelOptions[0]: null;
    this.setState({
      selectedPayment: index,
      payment: dataPaymentMethod.label,
      channel: selectedChannel,
      paymentChannelOptions: newChannelOptions
    });
  }

  removeDestination = (otherdestination, index) => {
    const {otherDestinationArray, otherDestinationLatLng, markers} = this.state;
    var result = otherDestinationArray
    var removeLatLng = otherDestinationLatLng
    removeLatLng.splice(index, 1)
    result.splice(index,1)  
    this.setState({
      otherDestinationArray: result,
      otherDestinationLatLng: removeLatLng
    })
    
    
  }

  otherDestinationInput() {
    const searchOptions = {
      componentRestrictions: { country: ['id'] }
    };

      return(
        <React.Fragment>
        {this.state.otherDestinationArray.map((item, index) => 
          {
            return index > 0 ? (
              <PlacesAutocomplete
                key={index}
                value={item || ''}
                onChange={(e) => this.handleChangeOtherPlacesAutoComplete(e, index)}
                onSelect={(other_des) =>this.handleSelectOtherPlacesAutoComplete(other_des, index)}
                searchOptions={searchOptions}
                >
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                <div className="places__autocomplete-wrap" style={{marginTop: "10px"}}>
                  <div className="input-group">
                    <input
                      {...getInputProps({
                        placeholder: 'Search Places ...',
                         className: 'form-control',
                      })}
                      />
                    <div className='input-group-append'>
                      <span className='input-group-text'><i className='fas fa-trash' onClick={(remove)=>this.removeDestination(remove,index)}></i></span>
                    </div>
                  </div>
                
                  <div className="places__autocomplete-container">
                    {loading && <div style={{padding: "8px", zIndex: "999999", backgroundColor: "#FFFFFF"}}>Loading...</div>}
                    {suggestions.map(suggestion => {
                      const className = suggestion.active
                        ? 'places__suggestion-item--active'
                        : 'places__suggestion-item';
                      // inline style for demonstration purpose
                      const style = suggestion.active
                        ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                        : { backgroundColor: '#ffffff', cursor: 'pointer' };
                      return (
                        <div
                          {...getSuggestionItemProps(suggestion, {
                            className,
                             style,
                    })}
                    >
                          <span>{suggestion.description}</span>
                        </div>
                      );
                    })}
                  </div>
                </div>
                )}
              </PlacesAutocomplete>
            ): (null)
          }    
        )}
        </React.Fragment>
      )
    
  }

  onChangeBcSebelas = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]:val}, () => {
      if (this.state.selectedDocumentType === 'BC 2.7') {
        if (nam === 'bl_no'){
          this.getContainerByBl(val, null)
        }
      }
      else{
        if ( nam === 'bl_date' && this.state.bl_no.length ) {
          this.getContainerByBl(this.state.bl_no, val);
        } else if ( nam === 'bl_no' && this.state.bl_date.length ) {
          this.getContainerByBl(val, this.state.bl_date);
        }
      }
    });
  }
  autoCompleteDocumentType(selected){
    const docType = selected.value;
    this.setState({
      selectedDocumentType: docType,
      blNo: [], 
      bl_no: '',
      bl_date: '',
      blOptions: [],
      loadingBL: true,
      selectedBl: "",
      container: [],
      dataTableContainer: [],
      checkedContainers: []
    }, () => {
      // GET B/L Nomer NPWP
      let apiBl = null;
      let resultName = null;
      if ( docType === "BC 2.0" ) {
        apiBl = Constants.GET_LIST_BL2_0
        resultName = "Data BL BC 2.0";
      } else if ( docType === "BC 1.6" ) {
        apiBl = Constants.GET_LIST_BL1_6
        resultName = "Data BL BC 1.6"
      } else if ( docType === "BC 2.3" ) {
        apiBl = Constants.GET_LIST_BL2_3
        resultName = "Data BL BC23"
      } else {
        apiBl = null
        resultName = null
      }
      const bl_pelimpahan = Constants.API_DATA_PELIMPAHAN_DOCTYPE + "?ppjk=" + this.state.npwp + "&doctype=" + escape(docType)
      if ( apiBl !== null ) {
        fetch(apiBl + this.state.npwp)
          .then(Response =>Response.json())
          .then((result) => {
            let newBLOptions = [];
            let blResults = [];
            result[resultName].map((item, index) => {
              let bldata = {value: index, label: item.bl_no}
              newBLOptions.push(bldata)
              blResults.push(item)
            });
            fetch(bl_pelimpahan)
              .then(ResponseP =>ResponseP.json())
              .then((resultP) => {
                resultP.map((itemP, indexP) => {
                  let bldata = {value: indexP, label: itemP.bl_no}
                  newBLOptions.push(bldata)
                  blResults.push(itemP)
                })
                this.setState({blNo: blResults, blOptions: newBLOptions, loadingBL: false, container: [], dataTableContainer: []})
              },
              (error) => {
                this.setState({blNo: [], blOptions: [], loadingBL: false, container: [], dataTableContainer: []})
                console.log("Err", error);
              })
          }, 
          (error) => {
            this.setState({blNo: [], blOptions: [], loadingBL: false, container: [], dataTableContainer:[]})
            console.log("Err", error);
          })
      } else {
        this.setState({blNo: [], blOptions: [], loadingBL: false, container: [], dataTableContainer: []})
      }
    });
  }
  autoCompleteBl(selected){
    const index = selected.value;
    const selectedBlData = this.state.blNo[index];
    const selectedDate = Helper.formatDate(selectedBlData.bl_date);
    const {bl_date} = this.state;
   
    this.setState({selectedBl:index, bl_no:selectedBlData.bl_no,bl_date:selectedDate}, () => {
      this.getContainerByBl(selectedBlData.bl_no, selectedDate);
    })
  }
  state = {
    modal20: false
  }

  load = () => {
    var hasil = document.getElementById("pilihBooking").value;
    // console.log(hasil);
    document.getElementById("bilangan").value = hasil;
  }

  contentInputHeader = (breadcrump) => {
    return(
      <div className="kt-portlet__head">
        <div className="kt-portlet__head-label">
            <h3 className="kt-portlet__head-title">
              {
                breadcrump.map((bcp, i) => {
                  return (
                    <span key={i}>
                    {
                      bcp.link === null 
                      ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                      : (<NavLink className="kt-link kt-font-transform-u" to={bcp.link} key={i}>{bcp.label}</NavLink>)
                    } 
                    {
                      i !== (breadcrump.length-1)
                      ? " > "
                      : null
                    }
                    </span>
                  )
                })
              }
            </h3>
        </div>                   
      </div>
    )
  }
  stepValidation = (step) => {
    const {otherDestinationLatLng, otherDestinationArray, checkedContainers, bl_no, bl_date, POD, destination, sp2valid_date, spcvalid_date, plan_date, selectedContainerType, selectedDangerousType} = this.state
    if ( step === 1 ) {
      if (this.state.selectedDocumentType === 'BC 2.7') {
        if ( checkedContainers.length && bl_no.length  && sp2valid_date.length && spcvalid_date.length )
          return true
      }
      else {
        if ( checkedContainers.length && bl_no.length && bl_date.length && sp2valid_date.length && spcvalid_date.length )
          return true
      }
    } else if ( step === 2 ) {
      if ( POD.length && otherDestinationLatLng.length && plan_date.toString().length ) 
        return true
    } else if ( step === 3 ) {
      return true
    }
    return false
  }
  onStepClicked = step => {
    const {currentStep, searchTrucksAndDrivers} = this.state
    const nextStep = step

    if ( (currentStep < step && this.stepValidation(currentStep)) || currentStep > step ) {
      this.setState({currentStep: step}, () => {
        if ( step === 4 ) {
          this.subscribeSocketConsumer("subscribe", this.state.id_User, 'nle-offer', this.state.id_User)
          if ( searchTrucksAndDrivers ) {
            this.subscribeSocketConsumer("subscribe", this.state.id_User, 'nle-truck', this.state.id_User)
          }
        } else {
          // this.subscribeSocketConsumer("unsubscribe", this.state.id_User, 'nle-offer', this.state.id_User)
          // if ( searchTrucksAndDrivers ) {
          //   this.subscribeSocketConsumer("unsubscribe", this.state.id_User, 'nle-truck', this.state.id_User)
          // }
        }
      })
      window.scrollTo(0, this.refBody.current.offsetTop)
    }
  }
  onPrevStepClicked = () => {
    const {currentStep, searchTrucksAndDrivers} = this.state
    const nextStep = (currentStep-1)
    this.setState({currentStep: nextStep}, () => {
        if ( nextStep === 4 ) {
          this.subscribeSocketConsumer("subscribe", this.state.id_User, 'nle-offer', this.state.id_User)
          if ( searchTrucksAndDrivers ) {
            this.subscribeSocketConsumer("subscribe", this.state.id_User, 'nle-truck', this.state.id_User)
          }
        } else {
          // this.subscribeSocketConsumer("unsubscribe", this.state.id_User, 'nle-offer', this.state.id_User)
          // if ( searchTrucksAndDrivers ) {
          //   this.subscribeSocketConsumer("unsubscribe", this.state.id_User, 'nle-truck', this.state.id_User)
          // }
        }
      })
    window.scrollTo(0, this.refBody.current.offsetTop)
  }
  onNextStepClicked = () => {
    const {currentStep, searchTrucksAndDrivers} = this.state
    const nextStep = (currentStep+1)
    this.setState({currentStep: nextStep}, () => {
        if ( nextStep === 4 ) {
          this.subscribeSocketConsumer("subscribe", this.state.id_User, 'nle-offer', this.state.id_User)
          if ( searchTrucksAndDrivers ) {
            this.subscribeSocketConsumer("subscribe", this.state.id_User, 'nle-truck', this.state.id_User)
          }
        } else if (nextStep === 3) {
          this.setMapBounds()
          this.setMapDirections()
          // this.subscribeSocketConsumer("unsubscribe", this.state.id_User, 'nle-offer', this.state.id_User)
          // if ( searchTrucksAndDrivers ) {
          //   this.subscribeSocketConsumer("unsubscribe", this.state.id_User, 'nle-truck', this.state.id_User)
          // }
        }
        else {
          // this.subscribeSocketConsumer("unsubscribe", this.state.id_User, 'nle-offer', this.state.id_User)
          // if ( searchTrucksAndDrivers ) {
          //   this.subscribeSocketConsumer("unsubscribe", this.state.id_User, 'nle-truck', this.state.id_User)
          // }
        }
      })
    window.scrollTo(0, this.refBody.current.offsetTop)
  }
  fetchDataContainerType(){
    let containerTypeUrl = axios.get(Constants.API_CONTAINER_TYPE)
    let dangerousTypeUrl = axios.get(Constants.API_DANGEROUS_TYPE)

    axios.all([containerTypeUrl, dangerousTypeUrl])
    .then(axios.spread((...response) => {
      var responseContainerType = response[0].data
      var responseDangerousType = response[1].data
      
      let containerOption = []
      responseContainerType.map(item => {
        const data = {
          value: item.type,
          label: item.type
        }
        containerOption.push(data)
      })

      let dangerousTypeOption =[]
      responseDangerousType.map(item => {
        const data = {
          value: item.type,
          label: item.type
        }
        dangerousTypeOption.push(data)
      })
      
      this.setState({
        containerType: responseContainerType,
        containerTypeOption: containerOption,
        dangerousOption: dangerousTypeOption,
        dangerousType: responseDangerousType
      })
    }))
    .catch(errors => {
      console.log(errors, 'errors')
    })
  }
  openContainerType = (container_no) => {
    this.setState({
      showContainerTypeDetail: true,
      selectedContainerType: null,
      activeContainerNo: container_no,
      modalType: 'container'
    })
  }
  closeContainerType = () => {
    const currentCheckedContainers = this.state.checkedContainers
    currentCheckedContainers.map(item => {
      if (item.container_no === this.state.activeContainerNo){
        return(
          item.container_type = this.state.container[this.state.activeContainerNo].selectedContainerType,
          item.over_height = this.state.container[this.state.activeContainerNo].over_height,
          item.over_weight = this.state.container[this.state.activeContainerNo].over_weight,
          item.over_length = this.state.container[this.state.activeContainerNo].over_length,
          item.over_width = this.state.container[this.state.activeContainerNo].over_width,
          item.temperatur = this.state.container[this.state.activeContainerNo].temperatur
        )
      }
    })
    this.setState({
      showContainerTypeDetail: false,
      checkedContainers: currentCheckedContainers,
    })
  }
  openDangerousType = (container_no) => {
    this.setState({
      showContainerTypeDetail: true,
      modalType: 'dangerous',
      activeContainerNo: container_no,
      selectedDangerousType: null,
      selectedDangerousMaterial: null,
    })
    
  }
  closeDangerousType = () => {
    const currentCheckedContainers = this.state.checkedContainers
    currentCheckedContainers.map(item => {
      if (item.container_no === this.state.activeContainerNo){
        return (
          item.dangerous_type = this.state.container[this.state.activeContainerNo].dangerous_type,
          item.dangerous_material = this.state.container[this.state.activeContainerNo].dangerous_material
        )
      }
    })
    this.setState({
      showContainerTypeDetail: false,
    })

  }
  selectContainerType = (selected) => {
    const {containerType, activeContainerNo} = this.state
    
    var currentContainer = this.state.container
    currentContainer[activeContainerNo].selectedContainerType = selected.value
    currentContainer[activeContainerNo].over_height = ''
    currentContainer[activeContainerNo].over_weight = ''
    currentContainer[activeContainerNo].over_length = ''
    currentContainer[activeContainerNo].over_width = ''
    currentContainer[activeContainerNo].temperatur = ''

    let id = null
    for (let index in containerType) {
      if (containerType[index].type === selected.value){
        id = index
      }
    }
    let related_fields = containerType[id].related_fields
    
    if (related_fields.length > 0){
      this.setState({
        relatedFields: JSON.parse(related_fields),
        idSelectedContainerType: id,
        selectedContainerType: selected.value,
        container: currentContainer,
      })
    }
    else{
      this.setState({
        relatedFields: null,
        idSelectedContainerType: id,
        selectedContainerType: selected.value,
        container: currentContainer
      })
    }
  }
  selectDangerousType = (selected) =>{
    const {container, activeContainerNo, dangerousType} = this.state;
    const currentContainer = this.state.container
    currentContainer[activeContainerNo].dangerous_type = selected.value
    currentContainer[activeContainerNo].dangerous_material = null
    

    let id = null
    for (let index in dangerousType) {
      if (dangerousType[index].type === selected.value){
        id = index
      }
    }
    let related_fields = dangerousType[id].related_fields

    if (related_fields.length > 0) {
      this.setState({
        selectedDangerousType: selected.value,
        container: currentContainer,
        dangerousRelatedFields: JSON.parse(related_fields),
      })
    }
    else{
      this.setState({
        selectedDangerousType: selected.value,
        container: currentContainer,
        dangerousRelatedFields: null,
      })
    }
  }
  selectDangerousMaterial = (selected) => {
    const currentContainer = this.state.container;
    currentContainer[this.state.activeContainerNo].dangerous_material = selected.value;
  
    this.setState({
      selectedDangerousMaterial: selected.value,
      container: currentContainer,
    })
  }
  handleFormContainerType = (event) => {
    const {container, activeContainerNo} = this.state
    
    let currentContainer = this.state.container
    currentContainer[activeContainerNo][event.target.name] = event.target.value
    
    this.setState({
      container : currentContainer
    })
  }
  containerTypeModal(){
    const {containerType, idSelectedContainerType, relatedFields, dangerousOption, selectedContainerType,
      containerTypeOption, selectedDangerousType, container, activeContainerNo} = this.state
    return(
      <React.Fragment>
        <Modal show={this.state.showContainerTypeDetail} onHide={this.closeContainerType}>
          <Modal.Header>
            {this.state.modalType === 'container' ? <h2>Detail Container Type</h2> : <h2>Dangerous Type</h2>}
          </Modal.Header>
          <Modal.Body>
          {this.state.modalType === 'container' ? 
            <React.Fragment>
              <label className='mb-3'>Select Container Type</label>
              <Select
              name="container_type"
              options={containerTypeOption}
              onChange={this.selectContainerType}
              defaultValue={selectedContainerType}
              innerRef={selectedContainerType}
              />
              {relatedFields === null ? null : (
                relatedFields.map((item, index) => {
                  if(selectedContainerType === null){
                    return null
                  }
                  else{
                    return(
                      <React.Fragment key={item.column_name}>
                        <label className='my-3'>{item.label}</label>
                        <input
                        className='form-control'
                        name={item.column_name}
                        value={this.state.container[activeContainerNo][item.column_name]}
                        type={item.type}
                        maxLength={item.maxlength}
                        required={item.required}
                        onChange={(ev) => this.handleFormContainerType(ev)}
                        
                        />
                      </React.Fragment>
                    )
                  }
                })
                )}
            </React.Fragment>
          : 
            <React.Fragment>
              <label className='mb-3'>Select Dangerous Type</label>
              <Select
              name="dangerous_type"
              options={dangerousOption}
              onChange={this.selectDangerousType}
              defaultValue={selectedDangerousType}
              innerRef={selectedDangerousType}
              />
              {this.state.dangerousRelatedFields === null ? null : (
                this.state.selectedDangerousType === null ? null : (
                  <React.Fragment>
                    <label className='mt-3'>Select Dangerous Material</label>
                    <Select
                    name="dangerous_material"
                    options={this.state.dangerousRelatedFields[0].options}
                    onChange={this.selectDangerousMaterial}
                    defaultValue={this.state.selectedDangerousMaterial}
                    innerRef={this.state.selectedDangerousMaterial}
                    />
                  </React.Fragment>
                  )
                
                
                )}
            </React.Fragment>
          }
          </Modal.Body>
          <Modal.Footer>
          {this.state.modalType === 'container' ? <Button onClick={this.closeContainerType}>Save</Button> : <Button onClick={this.closeDangerousType}>Save</Button>}
          </Modal.Footer>
        </Modal>
      </React.Fragment>
    )
  }
  cancelOrder = (idRequestBooking) => {
    this.subscribeSocketConsumer("unsubscribe", this.state.id_User, 'nle-offer', this.state.id_User)
    this.setState({
      searchTrucksAndDrivers: false,
      bookedIdRequestBooking: null,
      showTakeOrder: false,
      cancelProcess: true
    }, () => {
      var API04 ={
        isBooked : 0
      }
      let urlDetail = Constants.API_TRUCKING_SET_BOOKED
      var headers = new Headers();
      headers.append('Content-Type','application/json');

      fetch(urlDetail + idRequestBooking,{
        method:"PUT",
        headers:headers,
        body:JSON.stringify(API04)
      })
      .then(Response => {
        let currentArrayBooked = this.state.arrayBookedId
        let indexArrayBooked = Helper.array_search(idRequestBooking, currentArrayBooked)
        if ( indexArrayBooked ) 
          currentArrayBooked.splice(indexArrayBooked, 1)

        this.setState({arrayBookedId: currentArrayBooked, cancelProcess: false})
      })
      .catch(function(error){
        this.setState({cancelProcess: false})
        console.log(error.toString())
      })
    });
  }
  searchPlatformTruckAndDriver = () => {
    const {truckTakeOrder, bookedIdRequestBooking, truckMessage} = this.state
    const dataBooking = this.getDetilFromOffers(bookedIdRequestBooking)
    const listTruck = this.getTrucksFromTakeOrder(bookedIdRequestBooking)
    const truckErrorMessage = this.getTruckMessage(bookedIdRequestBooking)
    if ( dataBooking ) {
      return (
        <Modal show={this.state.showTakeOrder} size="lg" onHide={() => this.handleShowTakeOrder(bookedIdRequestBooking)} animation={false}>
          <Modal.Header closeButton className="modal-header">
            <Modal.Title className="modal-title">{dataBooking.booking.platform.nama_platform}</Modal.Title>
          </Modal.Header>
          <Modal.Body className="modal-body">
            <div className="kt-form__section kt-form__section--first">
            {
              listTruck.length > 0 ? (
                <table className="table table-striped">
                  <thead>
                    <tr>
                      <th>Container No</th>
                      <th>Truck Plate No</th>
                      <th>Driver</th>
                      <th>HP</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      listTruck.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td>{item.container_no}</td>
                            <td>{item.truckPlateNo}</td>
                            <td>{item.namaDriver}</td>
                            <td>{item.hpDriver}</td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colSpan="3">Price: Rp {Helper.convertToRupiah(dataBooking.hargaPenawaran)}</td>
                      <td>
                        &nbsp;<button type="button" className="btn btn-success" onClick={() => this.doPayment(bookedIdRequestBooking, dataBooking.booking.platform.paymentUrl)}>Payment</button>
                        </td>
                    </tr>
                  </tfoot>
                </table>
              ): (
                <div className="kt-wizard-v1__form text-center">
                  {
                    truckErrorMessage !== null && truckErrorMessage !== "" ? (
                      <p>{truckErrorMessage}</p>
                    ): (
                      <React.Fragment>
                        <p><img src="/assets/images/signal.gif" /></p>
                        <p>Sedang mencari Truk dan Pengemudi, silahkan tunggu atau anda dapat mengecek pesanan anda kembali nanti melalui <strong>Tab Booking</strong> pada <strong>Menu Trucking</strong>.</p>
                      </React.Fragment>
                    )
                  }
                </div>
              )
            }
          </div>
          </Modal.Body>
          <Modal.Footer className="modal-footer">
            <Button color="secondary" onClick={() => this.handleShowTakeOrder(bookedIdRequestBooking)}>Close</Button>
          </Modal.Footer>
        </Modal>
      )
    } else {
      return (null)
    }
  }
  
  contentInputBody = () => {
    const {
      container,loading_container,loading_searching, errorOffers, documentTypeOptions, currentStep,
      loading_map, mapCenterLat, mapCenterLng, mapZoom, mapBounds,
      lat_lng_map, selectedFromLatLng, destinationLatLng, selectedDepoLatLng, otherDestinationLatLng, dataTableContainer,
      searchTrucksAndDrivers, markers, lat_lng_map_length
    } = this.state;
    /* Begining Map Bounds*/
    let newArrayLatLng = [];
    let newMarkerArray = [];
    lat_lng_map.map((item, i) => {
      if ( i < lat_lng_map_length ) {
        var obj_latlng = {
          lat: parseFloat(item.lat),
          lng: parseFloat(item.lng)
        }
        newArrayLatLng.push(obj_latlng);
        newMarkerArray.push(item);
      }
    });
    if ( selectedFromLatLng !== null ) {
      newArrayLatLng.push({lat:parseFloat(selectedFromLatLng.lat), lng:parseFloat(selectedFromLatLng.lng)});
      newMarkerArray.push(selectedFromLatLng);
    }
    if ( otherDestinationLatLng.length ) {
      otherDestinationLatLng.map((itemOther, iOther) => {
        if ( itemOther.lat !== null && itemOther.lng !== null ) {
          newArrayLatLng.push({lat:parseFloat(itemOther.lat), lng:parseFloat(itemOther.lng)});
          newMarkerArray.push(itemOther);
        }
      })
    }
    // if ( selectedDepoLatLng !== null ) {
    //   newArrayLatLng.push({lat:parseFloat(selectedDepoLatLng.lat), lng:parseFloat(selectedDepoLatLng.lng)});
    //   newMarkerArray.push(selectedDepoLatLng);
    // }
    var bounds = new this.props.google.maps.LatLngBounds();
    for (var i = 0; i < newArrayLatLng.length; i++) {
      bounds.extend(newArrayLatLng[i]);
    }
    
    const container_columns = [
      {
        name: 'Container Number',
        selector: 'container_no',
        sortable: false,
        width: '150px',  
        style: {display: "table-cell", paddingTop: "14px"}    
      },
      {
        name: 'Container Type',
        cell: row => {
          return(
            <React.Fragment>
              {this.state.container[row.container_no].selectedContainerType === null ?
                <span onClick={()=>this.openContainerType(row.container_no)} style={{cursor: 'pointer'}} className='text-primary'>Choose Type</span> :
                <div className='text-left'>
                  <div><strong>Type</strong><p> {this.state.container[row.container_no].selectedContainerType}</p></div>
                  <div>{this.state.container[row.container_no].over_height === '' || this.state.container[row.container_no].over_height === null ? null : <span><strong>Over Height</strong><p>{this.state.container[row.container_no].over_height}</p></span>}</div>
                  <div>{this.state.container[row.container_no].over_weight === '' || this.state.container[row.container_no].over_weight === null ? null :  <span><strong>Over Weight</strong><p>{this.state.container[row.container_no].over_weight}</p></span>}</div>
                  <div>{this.state.container[row.container_no].over_width === '' || this.state.container[row.container_no].over_width === null ? null : <span><strong>Over Width</strong><p>{this.state.container[row.container_no].over_width}</p></span>}</div>
                  <div>{this.state.container[row.container_no].over_length === '' || this.state.container[row.container_no].over_length === null ? null : <span><strong>Over Length</strong><p>{this.state.container[row.container_no].over_length}</p></span>}</div>
                  <div>{this.state.container[row.container_no].temperatur === '' || this.state.container[row.container_no].temperatur === null ? null : <span><strong>Temperatur</strong><p>{this.state.container[row.container_no].temperatur}</p></span>}</div>
                  <p style={{cursor: 'pointer'}} className='text-primary' onClick={() => this.openContainerType(row.container_no)}>Edit Type</p>
                </div>
              }
            </React.Fragment>
          )
        },
        sortable: false,
        width: '180px',  
        style: {display: "table-cell", paddingTop: "14px"}   
      },
      {
        name: 'Dangerous Type',
        cell: row => {
          return(
            <React.Fragment>
              {this.state.container[row.container_no].dangerous_type === null ?
              <span onClick={() => this.openDangerousType(row.container_no)} style={{cursor: 'pointer'}} className='text-primary'>Choose Type</span> : 
              <div className='text-left'>
                <div><strong>Type</strong><p>{this.state.container[row.container_no].dangerous_type}</p></div>
                <div>{this.state.container[row.container_no].dangerous_material === '' || this.state.container[row.container_no].dangerous_material === null ? null : <span><strong>Material</strong> <p>{this.state.container[row.container_no].dangerous_material}</p></span>}</div>
                <p style={{cursor: 'pointer'}} className='text-primary' onClick={() => this.openDangerousType(row.container_no)}>Edit Type</p>
              </div>
              }
            </React.Fragment>
          )
        },
        sortable: false,
        width: '310px',  
        style: {display: "table-cell", paddingTop: "14px"}   
      },
      {
        name: 'Container Size',
        selector: 'container_size',
        sortable: false,
        width: '120px',  
        style: {display: "table-cell", paddingTop: "14px"}
      },
      {
        name: 'Container Seal',
        selector: 'noSeal',
        sortable: false,
        width: '120px',  
        style: {display: "table-cell", paddingTop: "14px"}
      },
      {
        name: 'Container Kind',
        selector: 'jenisMuat',
        sortable: false,
        width: '120px',  
        style: {display: "table-cell", paddingTop: "14px"}
      }
    ];
    const total_containers = container.length;
    const searchOptions = {
      componentRestrictions: { country: ['id'] }
    };
    return (
      <div className="kt-portlet__body kt-portlet__body--fit">
        <div className="kt-grid kt-wizard-v1 kt-wizard-v1--white" id="kt_wizard_v1" data-ktwizard-state="first">
          <div className="kt-grid__item">
            <div className="kt-wizard-v1__nav">
              <div className="kt-wizard-v1__nav-items kt-wizard-v1__nav-items--clickable">
                <div className="kt-wizard-v1__nav-item" data-ktwizard-type="step" data-ktwizard-state={currentStep === 1 ? "current": currentStep > 1 ? "done": "pending"} onClick={(e) => this.onStepClicked(1)}>
                  <div className="kt-wizard-v1__nav-body">
                    <div className="kt-wizard-v1__nav-icon">
                      <i className="flaticon2-box-1"></i>
                    </div>
                    <div className="kt-wizard-v1__nav-label">1. Select Containers</div>
                  </div>
                </div>
                <div className="kt-wizard-v1__nav-item" data-ktwizard-type="step" data-ktwizard-state={currentStep === 2 ? "current": currentStep > 2 ? "done": "pending"} onClick={(e) => this.onStepClicked(2)}>
                  <div className="kt-wizard-v1__nav-body">
                    <div className="kt-wizard-v1__nav-icon">
                      <i className="flaticon-map-location"></i>
                    </div>
                    
                    <div className="kt-wizard-v1__nav-label">
                      2. Setup Location
                    </div>
                  </div>
                </div>
                <div className="kt-wizard-v1__nav-item" data-ktwizard-type="step" data-ktwizard-state={currentStep === 3 ? "current": currentStep > 3 ? "done": "pending"} onClick={(e) => this.onStepClicked(3)}>
                  <div className="kt-wizard-v1__nav-body">
                    <div className="kt-wizard-v1__nav-icon">
                      <i className="flaticon-list-3"></i>
                    </div>
                    <div className="kt-wizard-v1__nav-label">
                      3. Preview
                    </div>
                  </div>
                </div>
                <div className="kt-wizard-v1__nav-item" data-ktwizard-type="step" data-ktwizard-state={currentStep === 4 ? "current": currentStep > 4 ? "done": "pending"} onClick={(e) => this.onStepClicked(4)}>
                  <div className="kt-wizard-v1__nav-body">
                    <div className="kt-wizard-v1__nav-icon">
                      <i className="flaticon-cart"></i>
                    </div>
                    <div className="kt-wizard-v1__nav-label">
                      4. Booking
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="kt-grid__item kt-grid__item--fluid kt-wizard-v1__wrapper">
            <form className="kt-form" onSubmit={this.postAPI01}>
              <input type="hidden" name="idRequestBooking" value={this.state.idRequestBooking}/>
              <div className="kt-wizard-v1__content" data-ktwizard-type="step-content" data-ktwizard-state={currentStep === 1 ? "current": currentStep > 1 ? "done": "pending"}>
                <div className="kt-heading kt-heading--md">Select Your Containers</div>
                <div className="kt-form__section kt-form__section--first">
                  <div className="kt-wizard-v1__form">
                    <div className="form-group">
                      <label>Document Type</label>
                      <Select
                        name="selectedDocumentType"
                        defaultValue={this.state.selectedDocumentType}
                        options={documentTypeOptions}
                        onChange={(ev) => this.autoCompleteDocumentType(ev)}
                        innerRef={this.state.selectedDocumentType}
                      />
                    </div>
                    <div className="row">
                      <div className={this.state.selectedDocumentType === 'BC 2.7' ? 'col-sm-12' : 'col-sm-6'}>
                        <div className="form-group">
                          {
                            this.state.selectedDocumentType === "SP3B"  || this.state.selectedDocumentType === "BC 3.0" || this.state.selectedDocumentType === "BC 2.7" 
                            ? (
                              <React.Fragment>
                                <label>{this.state.selectedDocumentType === "BC 2.7" ? "Document Number (nomor AJU)" : 'BC11 Number'}</label>
                                <input type="text" className="form-control" name="bl_no" value={this.state.bl_no} onChange={(ev) => this.onChangeBcSebelas(ev)} required/>
                              </React.Fragment>
                            ): (
                              <React.Fragment>
                                <label>B/L Number</label>
                                <Select
                                  name="blLocation"
                                  defaultValue={this.state.selectedBl}
                                  isLoading={this.state.loadingBL}
                                  options={this.state.blOptions}
                                  onChange={this.autoCompleteBl}
                                  innerRef={this.state.selectedBl}
                                />
                              </React.Fragment>
                            )
                          }
                        </div>
                      </div>
                      <div className="col-sm-6">
                        {
                          this.state.selectedDocumentType === "SP3B"  || this.state.selectedDocumentType === "BC 3.0" || this.state.selectedDocumentType === "BC 2.7"
                          ? (
                            this.state.selectedDocumentType === 'BC 2.7' ? null : (
                              <React.Fragment>
                                <label>BC11 Date</label>
                                <i className="flaticon-calendar-2" style={{float:"right"}}></i>                        
                                <input type="date" step="1" className="form-control" name="bl_date" value={this.state.bl_date} onChange={(ev) => this.onChangeBcSebelas(ev)} required/>
                              </React.Fragment>
                            )
                          ): (
                            <React.Fragment>
                              <label>BL Date</label>
                              <i className="flaticon-calendar-2" style={{float:"right"}}></i>                        
                                <input type="datetime" readOnly={true} step="1" className="form-control" name="bl_date"
                                  value={this.state.bl_date} onChange={(ev) => this.onChange(ev)}/>
                            </React.Fragment>
                          )
                        }
                      </div>
                    </div>
                    {
                      (this.state.bl_no) ? (
                        <div className="form-group">
                          <div className="table-responsive">
                            <DataTable
                              title={"Container List"}
                              columns={container_columns}
                              data={dataTableContainer}
                              keyField="noContainer"
                              isLoading={loading_container}
                              paginationPerPage={10}
                              pagination
                              paginationTotalRows={total_containers}
                              selectableRows
                              onSelectedRowsChange={(state, index) => this.checkBox(state, index)}
                              clearSelectedRows={this.state.toggledClearCheckedContainers}
                              />
                            {this.containerTypeModal()}
                          </div>
                        </div>
                      ): (null)
                    }
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="form-group">
                          <label>SP2 Valid Date</label>
                          <input type="date" className="form-control" name="sp2valid_date"
                            min={Helper.getDateNow()} value={this.state.sp2valid_date} onChange={(ev) => this.onChange(ev)} required/>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="form-group">
                          <label>SPC Valid Date</label>
                          <input type="date" className="form-control" name="spcvalid_date"
                            min={Helper.getDateNow()} value={this.state.spcvalid_date} onChange={(ev) => this.onChange(ev)} required/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="kt-wizard-v1__content" data-ktwizard-type="step-content" data-ktwizard-state={currentStep === 2 ? "current": currentStep > 2 ? "done": "pending"}>
                <div className="kt-heading kt-heading--md">Setup Origin and Destination</div>
                <div className="kt-form__section kt-form__section--first">
                  <div className="kt-wizard-v1__form">
                    <div className="form-group">
                      <label>From</label>
                      <PlacesAutocomplete
                        value={this.state.POD}
                        onChange={this.handleChangeFrom}
                        onSelect={this.autoCompleteMaps}
                        searchOptions={searchOptions}
                        >
                        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                        <div className="places__autocomplete-wrap">
                          <input
                            {...getInputProps({
                              placeholder: 'Search Places ...',
                              className: 'form-control',
                            })}/>
                          <div className="places__autocomplete-container">
                            {loading && <div style={{padding: "8px"}}>Loading...</div>}
                            {suggestions.map(suggestion => {
                              const className = suggestion.active
                                ? 'places__suggestion-item--active'
                                : 'places__suggestion-item';
                              // inline style for demonstration purpose
                              const style = suggestion.active
                                ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                : { backgroundColor: '#ffffff', cursor: 'pointer' };
                              return (
                                <div
                                  {...getSuggestionItemProps(suggestion, {
                                    className,
                                    style,
                                  })}
                                >
                                  <span>{suggestion.description}</span>
                                </div>
                              );
                            })}
                          </div>
                        </div>
                        )}
                      </PlacesAutocomplete>
                    </div>
                    <div className="form-group">
                      <label>To</label>
                      <PlacesAutocomplete
                        value={this.state.otherDestinationArray[0] || ''}
                        onChange={this.handleChangePlacesAutocomplete}
                        onSelect={this.handleSelectPlacesAutocomplete}
                        searchOptions={searchOptions}
                        >
                        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                        <div className="places__autocomplete-wrap">
                          <input
                            {...getInputProps({
                              placeholder: 'Search Places ...',
                              className: 'form-control',
                            })}/>
                          <div className="places__autocomplete-container">
                            {loading && <div style={{padding: "8px"}}>Loading...</div>}
                            {suggestions.map(suggestion => {
                              const className = suggestion.active
                                ? 'places__suggestion-item--active'
                                : 'places__suggestion-item';
                              // inline style for demonstration purpose
                              const style = suggestion.active
                                ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                : { backgroundColor: '#ffffff', cursor: 'pointer' };
                              return (
                                <div
                                  {...getSuggestionItemProps(suggestion, {
                                    className,
                                    style,
                                  })}
                                >
                                  <span>{suggestion.description}</span>
                                </div>
                              );
                            })}
                          </div>
                        </div>
                        )}
                      </PlacesAutocomplete>
                      {this.otherDestinationInput()}   
                    </div>
                    <div className="form-group">
                      <span onClick={this.handleAddOtherDestination}>
                        <i className="fas fa-plus fa-lg" ></i>
                        Add Other Destination
                      </span>
                    </div>
                    <div className='form-group'>
                      <label>Term of Payment</label>
                      <div className='input-group'>
                        <Input type="select" className='custom-select' name='term_of_payment' value={this.state.term_of_payment} onChange={(ev) => this.onChange(ev)}>
                          {
                            termOfPayment.map((term, i) => {
                              return (
                                <option value={term.value} key={i}>{term.label}</option>
                              )
                            })
                          }
                        </Input> 
                        <div className="input-group-append">
                          <span className="input-group-text">Days</span>
                        </div>
                      </div>
                    </div>
                    {/*<div className="form-group">
                                                              <label>Depo</label>
                                                              <Select
                                                                name="depoLocation"
                                                                defaultValue={this.state.selectedDepo}
                                                                options={this.state.depoOptions}
                                                                onChange={this.autoCompleteDepo}
                                                                innerRef={this.state.selectedDepo}
                                                                />
                                                            </div>*/}
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="form-group">
                          <label>Plan Date</label>
                          <div className="input-group date">
                            <DatePicker
                              selected={this.state.plan_date}
                              onChange={this.dateTimePicker}
                              showTimeSelect
                              timeFormat="HH:mm:ss"
                              timeIntervals={60}
                              timeCaption="Time"
                              dateFormat="yyyy-MM-dd hh:mm:ss aa"
                              className='form-control'
                              minDate={new Date()}
                              name='plan_date'
                              placeholderText="Select date and time"
                              required
                            /> 
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="form-group">
                          <label>Booking Date</label>
                          <input type="text" readOnly={true} step="1" className="form-control" name="booking_date"
                            value={Helper.getDateTimeNow()} onChange={(ev) => this.onChange(ev)}/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="kt-wizard-v1__content" data-ktwizard-type="step-content" data-ktwizard-state={currentStep === 3 ? "current": currentStep > 3 ? "done": "pending"}>
                <div className="kt-heading kt-heading--md">Review Your Data</div>
                <div className="kt-form__section kt-form__section--first">
                  <div className="kt-wizard-v1__review">
                    <div className="kt-wizard-v1__review-item">
                      <div className="kt-wizard-v1__review-title">
                        Route
                      </div>
                      <div className="kt-wizard-v1__review-content">
                        <div style={{height: "500px", position: "relative", overflow: "hidden"}}>
                          {
                            loading_map
                            ? (
                                <div className="global_overlay text-center">
                                  <p><img src="/assets/images/signal.gif" /></p>
                                  <p>Searching Nearest Trucks ...</p>
                                </div>
                              )
                            : null
                          }
                          <div style={{height: "100%", width: "100%", position: "absolute",
                              top: "0px", left: "0px"}}>
                            <div className="gm-err-container">
                                <Map 
                                  // key={maps.id_tplaces_port}
                                  onReady={this.initMap}
                                  onClick={this.mapClickHandler.bind(this)}
                                  google={this.props.google} 
                                  zoom={mapZoom} 
                                  initialCenter={{lat: mapCenterLat, lng: mapCenterLng}}
                                  bounds={bounds}
                                  >
                                {
                                  markers.map((store, index) => {
                                    return <Marker key={index} id={index}
                                      icon={store.icon ? store.icon: null}
                                      position={{lat: parseFloat(store.lat),lng: parseFloat(store.lng)}}
                                      info={store}
                                      onClick={this.markerClickHandler} />
                                  })
                                }

                                <InfoWindow
                                  marker = { this.state.activeMarker }
                                  visible = { this.state.showingInfoWindow }
                                >
                                  <div>
                                    <table>
                                      {
                                        this.state.selectedPlace === null ? null
                                        : items(this.state.selectedPlace, (key, value, i) => (
                                            <tr key={i}>
                                              <td>{key}</td>
                                              <td width="10px">:</td>
                                              <td>{value}</td>
                                            </tr>
                                          ))
                                      }
                                    </table>
                                  </div>
                                </InfoWindow>
                              </Map>
                            </div>
                          </div>
                        </div>
                        <table className="detailTable">
                          <tbody>
                            <tr style={{width:"120px"}}>
                              <td className="keyfield">DISTANCES</td>
                              <td>{this.state.distance} km</td>
                            </tr>
                            <tr style={{width:"120px"}}>
                              <td className="keyfield">ORIGIN</td>
                              <td>{this.state.POD}</td>
                            </tr>
                            {this.state.otherDestinationArray.map((item, index) =>
                        
                              <tr key={index}>
                                <td className="keyfield">{index === 0 ? "DESTINATION": (null)}</td>
                                <td >{item}</td>
                              </tr>
                              

                            )}
                            
                            {/*<tr>
                              <td className="keyfield">DEPO</td>
                              <td>{this.state.depo}</td>
                            </tr>
                            <tr>
                              <td className="keyfield">Total Distance</td>
                              <td>{this.state.distance} Km</td>
                            </tr>*/}
                            <tr>
                              <td className="keyfield">Plan Date</td>
                              <td>{formatPlanDate(this.state.plan_date)}</td>
                            </tr>
                            <tr>
                              <td className="keyfield">Term of Payment</td>
                              <td>{this.state.term_of_payment === '0' || this.state.term_of_payment === 0  ? 'Cash' : `${this.state.term_of_payment} Days`}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="kt-wizard-v1__review-item">
                      <div className="kt-wizard-v1__review-title">
                        Containers
                      </div>
                      <div className="kt-wizard-v1__review-content">
                        <table className="detailTable">
                          <tbody>
                            <tr>
                              <td className="keyfield">Document Type</td>
                              <td>{this.state.selectedDocumentType}</td>
                            </tr>
                            <tr>
                              <td className="keyfield" style={{width:"120px"}}>
                                {this.state.selectedDocumentType === 'BC 3.0' || this.state.selectedDocumentType === 'SP3B'  ? 'BC11 Number' : (
                                  this.state.selectedDocumentType === 'BC 2.7' ? 'Nomor AJU' : 'BL Number'
                                ) }
                              </td>
                              <td>{this.state.bl_no}</td>
                            </tr>
                            <tr>
                              <td className="keyfield">
                                {this.state.selectedDocumentType === 'BC 3.0' || this.state.selectedDocumentType === 'SP3B'  ? 'BC11 Date' : (
                                  this.state.selectedDocumentType === 'BC 2.7' ? null : 'BL Date'
                                ) }
                              </td>
                              <td>{Helper.formatPrintDate(this.state.bl_date)}</td>
                            </tr>
                            <tr>
                              <td className="keyfield">Containers</td>
                              <td>
                                <table className="table table-bordered">
                                  <thead>
                                    <tr>
                                      <th>NUMBER</th>
                                      <th>TYPE</th>
                                      <th>SIZE</th>
                                      <th>DANGEROUS TYPE</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {
                                      this.state.checkedContainers.map((item, i) => {
                                        return (
                                          <tr key={i}>
                                            <td>{item.container_no === null ? "-": item.container_no}</td>
                                            <td>
                                              <p>{item.container_type === null ? "-": item.container_type }</p>
                                              <div>{item.over_length === '' || item.over_length === null ? null : <div> <strong>Over Length</strong> <p>{item.over_length}</p></div>}</div>
                                              <div>{item.over_width === '' || item.over_width === null ? null : <div><strong>Over Width</strong><p>{item.over_width}</p></div>}</div>
                                              <div>{item.over_weight === '' || item.over_weight === null ? null : <div><strong>Over Weight</strong><p>{item.over_weight}</p></div>}</div>
                                              <div>{item.over_height === '' || item.over_height === null ? null : <div><strong>Over Height</strong><p>{item.over_height}</p></div>}</div>
                                              <div>{item.temperatur === '' || item.temperatur === null ? null : <div><strong>Temperatur</strong><p>{item.temperatur}</p></div>}</div>
                                            </td>
                                            <td>{item.container_size === "null" ? "-": item.container_size}</td>
                                            <td>
                                              <p>{item.dangerous_type === null ? null: item.dangerous_type}</p>
                                              {item.dangerous_type === 'Dangerous' ? item.dangerous_material : null}
                                            </td>
                                          </tr>
                                        )
                                      })
                                    }
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td className="keyfield">SP2 Valid Date</td>
                              <td>{Helper.formatPrintDate(this.state.sp2valid_date)}</td>
                            </tr>
                            <tr>
                              <td className="keyfield">SPC Valid Date</td>
                              <td>{Helper.formatPrintDate(this.state.spcvalid_date)}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="kt-wizard-v1__content" data-ktwizard-type="step-content" data-ktwizard-state={currentStep === 4 ? "current": currentStep > 4 ? "done": "pending"}>
                <div className="kt-heading kt-heading--md">Booking Trucks</div>
                        <div className="kt-form__section kt-form__section--first">
                          <div className="kt-wizard-v1__form">
                            {loading_searching ? (<span>Waiting offers from Trucking Platform ...</span>): (null)}
                            {errorOffers !== null ? (<span>{errorOffers}</span>): (null)}
                          {/**/}
                            {
                              !loading_searching ? (
                                <div className="form-group">
                                  <div className='kt-portlet__body'>
                                    <div className="tab-content">
                                      <div className="kt-section">
                                        <div className='row'>
                                        {this.state.offers.map((item, index) =>
                                          <div className='col-lg-4 col-md-4 col-xs-12 mb-3' key={index}>
                                            <div className="kt-widget4">
                                              <div className="kt-section__content kt-section__content--border kt-section__content--fit booktruck-card">
                                                  <div className="text-center mt-5">
                                                    <h4 style={{color: '#5d78ff'}}>{item.booking.platform.nama_platform}</h4>
                                                    <p>{item.booking.platform.company}</p>
                                                  </div>
                                                  <div className="text-center truck">
                                                    <i className="fa fa-truck-moving fa-4x" style={{marginTop: '50px', color: '#5d78ff'}}></i>
                                                    <hr className='mt-3 truck-line'/>
                                                  </div>
                                                  <div className="text-center mt-5">
                                                    <h5>Rp. {Helper.convertToRupiah(item.hargaPenawaran)}</h5>
                                                  </div>
                                                  <div className="text-center mt-3">
                                                    <p><em>for {item.booking.total_distance} km distances</em></p>
                                                    {
                                                      Helper.in_array(item.idRequestBooking, this.state.arrayBookedId) ? (<i>Booked</i>): (<p>&nbsp;</p>)
                                                    }
                                                  </div>
                                                  <div className="text-center mb-5">
                                                    {
                                                      Helper.in_array(item.idRequestBooking, this.state.arrayBookedId) ? (
                                                        <button type="button" className="btn btn-warning" onClick={() => this.handleShowTakeOrder(item.idRequestBooking)}>View Order</button>
                                                      ): (
                                                        <button type="button" className="btn btn-success btn-sm" style={{marginTop: '5px', width: '100px', letterSpacing: '2px'}} onClick={() => this.handleShowBook(index, item.idRequestBooking)}>
                                                          BOOK
                                                        </button>
                                                      )
                                                    }
                                                  </div>
                                              </div>
                                            </div>
                                          </div>
                                          )}
                                        </div>                           
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              ): (null)
                            }
                            
                            {/**/}
                          </div>
                        </div>
                  {this.searchPlatformTruckAndDriver()}
              </div>
              <div className="kt-form__actions">
                {
                  currentStep > 1 && currentStep <= 4 ? (
                    <button type="button" className="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" onClick={() => this.onPrevStepClicked()}>
                      Previous
                    </button>
                  ): (null)
                }
                {
                  currentStep === 3 ? (
                    <button type="submit" className="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                      Search {loading_searching ? (<span className="fa fa-circle-notch fa-spin"></span>): (null)}
                    </button>
                  ): (null)
                }
                {
                  currentStep >= 1 && currentStep < 4 && currentStep !== 3 ? (
                    <button type="button" className="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" onClick={() => this.onNextStepClicked()} disabled={!this.stepValidation(currentStep)}>
                      Next Step
                    </button>
                  ): (null)
                }
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }

  initMap(mapProps, map) {
    const {google} = mapProps;
    directionsService = new google.maps.DirectionsService();
    directionsRenderer = new google.maps.DirectionsRenderer();
    directionsRenderer.setMap(map);
  }
  
  contentDetail = () => {
    const {dbooking, bookingProcess, currentPageNumber, itemsPerPage, loading_truck_list} =this.state;
    //Page
    const indexOfLastPage = currentPageNumber * itemsPerPage;
    const indexOfFirstPage = indexOfLastPage - itemsPerPage;
    const currentPage = loading_truck_list 
                      ? [undefined, undefined, undefined, undefined, undefined]
                      : dbooking.slice(indexOfFirstPage,indexOfLastPage);
    const lastPage = Math.ceil(dbooking.length / itemsPerPage);

    const DivList = createSkeletonElement('div', 'pending_truck');
    const DivLine = createSkeletonElement('div', 'pending_line');
    const P = createSkeletonElement('p', 'pending_text');
    const Span = createSkeletonElement('span', 'pending_text');
    const H5 = createSkeletonElement('h5', 'pending_text');
    let currentIndex = indexOfFirstPage;
    return(
      <div className="col-xl-6 col-lg-6 order-lg-3 order-xl-1">
        <div className="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
            <div className="kt-portlet__head" style={{minHeight: "30px", padding: "10px 25px"}}>
              <span className="link" onClick={() => this.refreshSearchResult()}><i className="flaticon2-reload"></i> Refresh Search Result</span>
            </div>
            <div className="kt-portlet__body">
              <div className="tab-content">                  
                <div className="kt-section" ref={this.refSearchResult}>
                  {
                    currentPage.length ? currentPage.map((detail, i) => {
                      const currentIndex = ((currentPageNumber*itemsPerPage)-itemsPerPage)+i;
                      let payment_methods = [];
                      if ( detail !== undefined ) {
                        detail.buttonView = 'VIEW';
                        detail.buttonBook = 'BOOK';
                        detail.buttonPayment = 'PAYMENT';
                        detail.waktuPenawaran = (
                          <span>{detail.waktuPenawaran}</span>
                        );
                        payment_methods = detail.payment_method;
                      }
                      const DetailItem = (props) => (
                      <DivList className="kt-section__content kt-section__content--border kt-section__content--fit" key={`skeleton-${i}`}>
                        <div className="kt-widget4" >
                          {
                            props.detail.status !== null && props.detail.isBooked === 1 && !loading_truck_list ? (
                              <DivLine className="kt-widget4__item text-left" style={{padding:"5px 5px 5px 5px", textAlign:"left", backgroundColor:"azure"}}>
                                <strong>Status: {props.detail.status}</strong>
                              </DivLine>
                            ): null
                          }
                          {/*
                            !loading_truck_list
                            ? (
                              <DivLine className="kt-widget4__item text-left" style={{padding:"5px 5px 5px 5px", textAlign:"left", backgroundColor:"azure"}}>
                            <table style={{border: "0", width: "100%"}}>
                              <tbody>
                                <tr>
                                  <td style={{width: "70px"}}><Span>From</Span></td>
                                  <td style={{width: "10px"}}><Span>:</Span></td>
                                  <td><Span>{props.detail.booking.pod}</Span></td>
                                </tr>
                                <tr>
                                  <td style={{width: "70px"}}><Span>To</Span></td>
                                  <td style={{width: "10px"}}><Span>:</Span></td>
                                  <td><Span>{props.detail.booking.destination}</Span></td>
                                </tr>
                                <tr>
                                  <td style={{width: "70px"}}><Span>Depo</Span></td>
                                  <td style={{width: "10px"}}><Span>:</Span></td>
                                  <td><Span>{props.detail.booking.depo}</Span></td>
                                </tr>
                              </tbody>
                            </table>
                          </DivLine>
                            )
                            : null
                            */
                          }
                          
                          <div className="kt-widget4__item">
                            
                            <DivLine className="kt-widget4__pic kt-widget4__pic--pic col-lg-1">
                              <i className="fa fa-truck-moving" style={{marginLeft:"5px"}}></i>
                            </DivLine>
                            <DivLine className="kt-widget4__info col-lg-4">
                              <P className="kt-widget4__text">PROVIDER</P>
                                <H5 className="kt-font-primary kt-font-bold">{props.detail.booking.platform.nama_platform}</H5>
                                {props.detail.waktuPenawaran}
                            </DivLine>
                            <DivLine className="kt-widget4__info col-lg-4" style={{paddingLeft:"0px",paddingRight:"0px", paddingTop: "5px", paddingBottom: "5px"}}>
                              <P className="kt-widget4__text" style={{color:"black"}}>Distance: {props.detail.booking.total_distance + ' km'}</P>
                                <H5 className="kt-font-danger kt-font-bold">
                                  Rp. {Helper.convertToRupiah(props.detail.hargaPenawaran)} ,-
                                </H5>
                                {
                                  props.detail.isBooked === 1
                                  ? (<P><i>{props.detail.paidStatus === 1 ? (<span>(Paid)</span>): (<span>(Unpaid)</span>)}</i></P>)
                                  : null
                                }
                            </DivLine>
                            <div className="kt-widget4__info col-lg-3 col-md-3 col-sm-3 col-xs-12">
                              {
                                loading_truck_list
                                ? (
                                  <React.Fragment>
                                  <DivLine style={{ marginTop:"5px", height:"32px"}}>____</DivLine>
                                  <DivLine style={{ marginTop:"5px", marginBottom:"5px", height:"32px"}}>____</DivLine>
                                  </React.Fragment>
                                  )
                                : (
                                    <React.Fragment>
                                    <button type="button" className="btn btn-primary btn-sm" style={{ marginTop:"5px"}}
                                    onClick={()=>this.handleShow(currentIndex, props.detail.idRequestBooking)}>
                                      {props.detail.buttonView}
                                    </button>
                                    {
                                      props.detail.isBooked === 1
                                      ? detail.dtransporter !== null && detail.paidStatus === 0 ? (
                                          <button type="button" className="btn btn-warning btn-sm" style={{ marginTop:"5px"}}
                                          onClick={() => this.doPayment(props.detail.idRequestBooking, props.detail.booking.platform.paymentUrl)}>
                                            {props.detail.buttonPayment}
                                          </button>
                                        ): null
                                      : (
                                        <React.Fragment>
                                        <button type="button" className="btn btn-success btn-sm" style={{marginTop:"5px", marginBottom:"5px"}}
                                        onClick={()=>this.handleShowBook(currentIndex, props.detail.idRequestBooking, payment_methods)}>{props.detail.buttonBook}</button>
                                        </React.Fragment>
                                      )
                                    }
                                    </React.Fragment>
                                  )
                              }
                            </div>
                          </div>
                          
                        </div>
                      </DivList>
                      );
                      const dummyData = {
                        detail: {
                          idDetilBooking: '__',
                          booking: {
                            platform: {
                              nama_platform: '_____ _____',
                              paymentUrl: '',
                            },
                            container_no: '__________ ______'
                          },
                          hargaPenawaran: '__ __.__.__',
                          status: '______',
                          paidStatus: '____',
                          isBooked: '',
                          buttonView: '____',
                          buttonBook: '____',
                          buttonPayment: '____',
                          waktuPenawaran: '____'
                        }
                      };
                      const pendingDetailBooking = (props) => props.detail === undefined;
                      const ViewDetailBooking = createSkeletonProvider(
                        dummyData, 
                        pendingDetailBooking,
                        () => ({
                          color: '#ddd'
                        })
                      )(DetailItem);
                      
                      return <ViewDetailBooking detail={detail} key={i} />;
                    })
                    : (<p className="text-center">Data is not available</p>)
                  }
                </div>
                {
                  this.state.loading_truck_list || !currentPage.length
                  ? null
                  : (
                    <nav aria-label="Booking Navigation" className="text-center">
                      Page {currentPageNumber} of {lastPage}
                      <ul className="pagination pagination-lg kt-pull-right">
                        {
                          currentPageNumber === 1
                          ? (
                              <li className="page-item disabled">
                                <span className="page-link">Previous</span>
                              </li>
                            )
                          : (
                              <li className="page-item">
                                <a className="page-link" onClick={() => this.handleClick('prev')}>Previous</a>
                              </li>
                            )
                        }
                        {
                          currentPageNumber === lastPage
                          ? (
                              <li className="page-item disabled">
                                <span className="page-link">Next</span>
                              </li>
                            )
                          : (
                              <li className="page-item">
                                <a className="page-link" onClick={() => this.handleClick('next')}>Next</a>
                              </li>
                            )
                        }
                      </ul>
                    </nav>
                  )
                }
              </div>
            </div>
        </div>             
        {this.contenModalTruck()}
        {this.contenModalBook()}  
      </div> 
    )
  }
  contenModalTruck = () =>{
    const {currentIndexDataBooking, offers} = this.state;
    const dataBooking = offers.length ? offers[currentIndexDataBooking]: false;
    return(
      <Modal show={this.state.showDetail} onHide={this.handleClose} animation={false}>
        <Modal.Header closeButton className="modal-header">
          <Modal.Title className="modal-title">Detail Booking</Modal.Title>
        </Modal.Header>
        <Modal.Body className="modal-body">
          <div className="kt-widget12">
              <div className="kt-widget12__content">
                <div style={{marginBottom:"10px",borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                  <div className="kt-widget12__info">
                    <span className="kt-font-boldest">ID</span>
                  </div>
                  <div className="kt-widget12__info">
                    <span>{dataBooking ? dataBooking.booking.idRequestBooking: null}</span>
                  </div>
                </div>
                <div style={{marginBottom:"10px",borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                  <div className="kt-widget12__info">
                    <span className="kt-font-boldest">BOOKED DATE</span>
                  </div>
                  <div className="kt-widget12__info">
                    <span>{dataBooking ? dataBooking.booked_date: null}</span>
                  </div>
                </div>
                <div style={{marginBottom:"10px",borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                  <div className="kt-widget12__info">
                    <span className="kt-font-boldest">B/L Nomer</span>
                  </div>
                  <div className="kt-widget12__info">
                    <span>{dataBooking ? dataBooking.booking.bl_no: null}</span>
                  </div>
                </div>
                <div style={{marginBottom:"10px" ,borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                  <div className="kt-widget12__info">
                    <span className="kt-font-boldest">From</span>
                  </div>
                  <div className="kt-widget12__info">
                    <span>{dataBooking ? dataBooking.booking.pod: null}</span>
                  </div>
                </div>
                {
                  dataBooking ? dataBooking.booking.other_destination.map((destination, ides) => {
                    return (
                      <div style={{marginBottom:"10px" ,borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item" key={ides}>
                        <div className="kt-widget12__info">
                          <span className="kt-font-boldest">Destination {ides+1}</span>
                        </div>
                        <div className="kt-widget12__info">
                          <span>{destination.destination}</span>
                        </div>
                      </div>
                    )
                  }): (null)
                }
                <div style={{marginBottom:"10px" ,borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                  <div className="kt-widget12__info">
                    <span className="kt-font-boldest">Price</span>
                  </div>
                  <div className="kt-widget12__info">
                    <span>Rp.{Helper.convertToRupiah((dataBooking ? dataBooking.hargaPenawaran: ''))} ,-</span>
                  </div>
                </div>
                {
                  dataBooking && dataBooking.isBooked === 1 ? (
                    <React.Fragment>
                      <div style={{marginBottom:"10px" ,borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                        <div className="kt-widget12__info">
                          <span className="kt-font-boldest">Booking Date</span>
                        </div>
                        <div className="kt-widget12__info">
                          <span>{dataBooking.booked_date}</span>
                        </div>
                      </div>
                      <div style={{marginBottom:"10px" ,borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                        <div className="kt-widget12__info">
                          <span className="kt-font-boldest">{dataBooking.paidStatus === 1 ? 'Order': 'Booking'} Status</span>
                        </div>
                        <div className="kt-widget12__info">
                          <span>{dataBooking.status}</span>
                        </div>
                      </div>
                      {
                        dataBooking.dtransporter !== null ? (
                          <div style={{marginBottom:"10px" ,borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                            <div className="kt-widget12__info">
                              <span className="kt-font-boldest">Billing Code</span>
                            </div>
                            <div className="kt-widget12__info">
                              <span>{dataBooking.dtransporter.billingCode}</span>
                            </div>
                          </div>
                        ): null
                      }
                      <div style={{marginBottom:"10px" ,borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                        <div className="kt-widget12__info">
                          <span className="kt-font-boldest">Payment Status</span>
                        </div>
                        <div className="kt-widget12__info">
                          <span>{dataBooking.paidStatus === 1 ? 'Paid': 'Unpaid'}</span>
                        </div>
                      </div>
                    </React.Fragment>
                  ): null
                }
              </div>
            </div>
        </Modal.Body>
        <Modal.Footer className="modal-footer">
          <Button color="secondary" onClick={this.handleClose}>Close</Button>
        </Modal.Footer>
      </Modal>
    )
  };
  contenModalBook = () =>{
    const {currentIndexDataBooking, offers, checkedTermAndCondition} = this.state;
    const dataBooking = offers.length ? offers[currentIndexDataBooking]: null;
    return(
      <Modal show={this.state.showBook} onHide={this.handleCloseBook} animation={false}>
        <Modal.Header closeButton className="modal-header">
          <Modal.Title className="modal-title">Method Payment</Modal.Title>
        </Modal.Header>
        <Modal.Body className="modal-body">
          <div className="kt-widget12">
              <div className="kt-widget12__content">
                <div style={{marginBottom:"10px",borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                  <div className="kt-widget12__info">
                    <span className="kt-font-boldest">B/L Nomer</span>
                  </div>
                  <div className="kt-widget12__info">
                    <span>{dataBooking ? dataBooking.booking.bl_no: null}</span>
                  </div>
                </div>
                <div style={{marginBottom:"10px" ,borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                  <div className="kt-widget12__info">
                    <span className="kt-font-boldest">From</span>
                  </div>
                  <div className="kt-widget12__info">
                    <span>{dataBooking ? dataBooking.booking.pod: null}</span>
                  </div>
                </div>
                {
                  dataBooking ? dataBooking.booking.other_destination.map((destination, ides) => {
                      return (
                        <div style={{marginBottom:"10px" ,borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item" key={ides}>
                          <div className="kt-widget12__info">
                            <span className="kt-font-boldest">Destination {ides+1}</span>
                          </div>
                          <div className="kt-widget12__info">
                            <span>{destination.destination}</span>
                          </div>
                        </div>
                      )
                  }): (null)
                }
                <div style={{marginBottom:"10px" ,borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                  <div className="kt-widget12__info">
                    <span className="kt-font-boldest">Price</span>
                  </div>
                  <div className="kt-widget12__info">
                    <span>Rp.{Helper.convertToRupiah((dataBooking ? dataBooking.hargaPenawaran: ''))} ,-</span>
                  </div>
                </div>
                <div style={{marginBottom:"10px", paddingBottom: "5px", borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                  <div className="kt-widget12__info">
                    <span className="kt-font-boldest">Telepon</span>
                  </div>
                  <div className="kt-widget12__info">
                    <input type="text" className="form-control" name="telepon_perusahaan"
                            value={this.state.telepon_perusahaan} onChange={(ev) => this.onChange(ev)} placeholder="Input Nomor Telepon"/>   
                  </div>
                </div>
                <div style={{marginBottom:"10px", paddingBottom: "5px", borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                  <div className="kt-widget12__info">
                    <span className="kt-font-boldest">Catatan</span>
                  </div>
                  <div className="kt-widget12__info">
                    <textarea type="text" className="form-control" name="booking_note"
                      value={this.state.booking_note} onChange={(ev) => this.onChange(ev)}></textarea>   
                  </div>
                </div>
                <div style={{marginBottom:"10px", paddingBottom: "5px", borderBottom:"1px dashed #c9cace"}} className="kt-widget12__item">
                  <div className="kt-widget12__info">
                    <div className="kt-checkbox-inline">
                      <label className="kt-checkbox">
                        <input type="checkbox" onChange={() => this.handleClickedTermAndCondition()} checked={checkedTermAndCondition} />
                        {'I agree to the'} <a style={{color: "blue"}} onClick={() => this.openTermAndCondition((dataBooking ? dataBooking.booking.platform.tcUrl: null))}>{dataBooking ? dataBooking.booking.platform.nama_platform: null} {'term & condition'}</a>
                        <span></span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </Modal.Body>
        <Modal.Footer className="modal-footer">
          <Button className="btn btn-success" onClick={()=> this.postAPI04(dataBooking.idRequestBooking)} disabled={this.state.bookingProcess || !checkedTermAndCondition}>
          {this.state.bookingProcess ? (<React.Fragment><span className="fa fa-circle-notch fa-spin"></span> Book</React.Fragment>): 'Book'}
          </Button>
          <Button className="btn btn-secondary btn-hover-brand" onClick={this.handleCloseBook}>Close</Button>
        </Modal.Footer>
      </Modal>
    ) 
  }
  checkoutView() {
    const breadcrump = [
      {
        label: 'Trucking',
        link: '/trucking'
      },
      {
        label:'Book Truck',
        link: null
      },
      {
        label:'Checkout',
        link: null
      }
    ];
    return (
      <div className="row">
        <div className="col-xl-8 col-lg-12 order-lg-3 order-xl-1">
          <div className="kt-portlet kt-portlet--height-fluid">
            {this.contentInputHeader(breadcrump)}
          </div>
        </div>
      </div>
    );
  }
  searchView() {
    const breadcrump = [
      {
        label: 'Trucking',
        link: '/trucking'
      },
      {
        label:'Book Truck',
        link: null
      }
    ];
    return (
      <div className="row">
        <div className="col-xl-12 col-lg-12 order-lg-3 order-xl-1">
          <div className="kt-portlet kt-portlet--height-fluid" ref={this.refBody}>
            {this.contentInputHeader(breadcrump)}
            {this.contentInputBody()}
            {this.contenModalTruck()}
            {this.contenModalBook()}  
          </div>
        </div>
      </div>
    );
  }
  render(){
    const {currentStep, npwp} = this.state;
    if ( currentStep === 'checkout' ) {
      return this.checkoutView()
    } else {
      return this.searchView()
    }
  }
}

export default GoogleApiWrapper({
  apiKey: (Constants.googleMapAPI)
})(BookTruck);