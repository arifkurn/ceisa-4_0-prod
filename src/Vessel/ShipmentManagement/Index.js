import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../BookingRequest/dashforge.module.css'
import { UncontrolledCollapse, Button, CardBody, Card } from 'reactstrap';
import { withRouter } from 'react-router-dom'
import axios from 'axios'
import Swal from 'sweetalert2'
import Spinner from 'react-bootstrap/Spinner'
import { useToasts } from 'react-toast-notifications'
import * as ApiClickargo from '../../Clickargo/ApiList.js'

function Index(props) {

    const { addToast } = useToasts()

    const detailShipment = (job_number, date_convert) => {
        //country list
        axios
            .get(ApiClickargo.CLICKARGO_COUNTRY_LIST)
            .then(response => {
                localStorage.setItem('ckSmCountryList', JSON.stringify(response.data.data, null, 2))
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response.data.message)
                }
            })

            localStorage.setItem('ckSmJobNumber', job_number)
            localStorage.setItem('ckSmDateConvert', date_convert)
            props.history.push('/shipment_management_detail')
    }

    const [arrVesselList, setArrVesselList] = useState([]);

    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        var month_name = function (dt) {
            var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            return mlist[dt.getMonth()];
        }

        axios
            .post(ApiClickargo.CLICKARGO_ORDER_LIST_BY_NPWP, {
                // tax_number: '541267799887999'
                tax_number: props.dataUserPortal.npwp
            })
            .then(response => {
                console.log('success get job vessel list')
                const values = [...arrVesselList]
                response.data.data.filter(opt => opt.orders[0].transaction === 'freight' && opt.orders[0].type === 'export').map((inputOrder, indexOrder) => {
                    const dateOrder = new Date(inputOrder.created_at)
                    values.push({
                        reference_number: inputOrder.value,
                        created_at: dateOrder.getDate() + ' ' + month_name(dateOrder) + ' ' + dateOrder.getFullYear(),
                        created_by: JSON.parse(inputOrder.orders[0].attribute).user_name,
                        orders:
                            inputOrder.orders.map((orderDetails, indexOrderDetails) => (
                                {
                                    type: orderDetails.type,
                                    move_type: orderDetails.move_type,
                                    created_at: dateOrder.getDate() + ' ' + month_name(dateOrder) + ' ' + dateOrder.getFullYear(),
                                    job_number: orderDetails.job_number,
                                    product_type: JSON.parse(orderDetails.attribute).product_type,
                                    freight_mode: orderDetails.freight_mode,
                                    transaction: orderDetails.transaction,
                                    user_status: orderDetails.user_status
                                }
                            ))
                    })
                    setArrVesselList(values)
                })
                setIsLoading(false)
            })
            .catch(error => {
                console.log('failed get job vessel list')
                if (!error.response) {
                    addToast('Please check your internet connection!', {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                } else {
                    addToast(error.response.data.message + `. Failed to load vessel transaction list.`, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }
                setIsLoading(false)
            })
    }, [])

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
                <Fragment>
                    <div style={{ marginBottom: "10px" }}>
                        <h3 style={{ display: "inline" }} className={dashforgeCss.txBlack}>My Vessel Transaction</h3>
                    </div>

                    {arrVesselList.map((inputOrder, indexOrder) => (
                        <Fragment key={indexOrder}>
                            <div className="kt-portlet" id={`toggler` + indexOrder}>
                                <div className="kt-portlet__body" style={{ cursor: 'pointer', paddingLeft: '25px', paddingTop: '0px', paddingRight: '0px', paddingBottom: '0px' }}>
                                    <div className="kt-widget15">
                                        <div className="row">
                                            <div className={dashforgeCss.alignSelfCenter + ` col-md-3 ` + dashforgeCss.pd15And20}>
                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Reference No</h4>
                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{inputOrder.reference_number}</h5>
                                            </div>
                                            <div className={dashforgeCss.alignSelfCenter + ` col-md-3 ` + dashforgeCss.pd15And20}>
                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Created At</h4>
                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{inputOrder.created_at}</h5>
                                            </div>
                                            <div className={dashforgeCss.alignSelfCenter + ` col-md-3 ` + dashforgeCss.pd15And20}>
                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Created By</h4>
                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{inputOrder.created_by}</h5>
                                            </div>
                                            <div className={dashforgeCss.statusBox + ` ` + dashforgeCss.alignSelfCenter + ` col-md-3 ` + dashforgeCss.pd60And20}>
                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txWhite}>Click to See Transaction</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {inputOrder.orders.map((orderDetails, indexOrderDetails) => (
                                <Fragment key={indexOrderDetails}>
                                    <UncontrolledCollapse toggler={`#toggler` + indexOrder} style={{ paddingLeft: '10px', paddingRight: '5px' }}>
                                        <div className="kt-portlet">
                                            <div className="kt-portlet__body" onClick={() => detailShipment(orderDetails.job_number, orderDetails.created_at)} style={{ cursor: 'pointer', paddingLeft: '25px', paddingTop: '0px', paddingRight: '0px', paddingBottom: '0px' }}>
                                                <div className="kt-widget15">
                                                    <div className={`col-12`}>
                                                        <div className="row">
                                                            <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20}>
                                                                <h3 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails.type}</h3>
                                                                <span className={dashforgeCss.upperTitle + ` ` + dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderDetails.move_type}</span>
                                                                <span className={dashforgeCss.upperTitle + ` ` + dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderDetails.created_at}</span>
                                                                <h5 className={dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderDetails.job_number}</h5>
                                                            </div>
                                                            <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Product Type</h4>
                                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails.product_type}</h5>
                                                            </div>
                                                            <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Freight Mode</h4>
                                                                <h4 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails.freight_mode}</h4>
                                                            </div>
                                                            <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Transaction</h4>
                                                                <h4 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails.transaction}</h4>
                                                            </div>
                                                            <div className={dashforgeCss.statusBox + ` ` + dashforgeCss.alignSelfCenter + ` col-md-4 ` + dashforgeCss.pd60And20}>
                                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Status</h4>
                                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txWhite}>{orderDetails.user_status}</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </UncontrolledCollapse>
                                </Fragment>
                            ))}
                        </Fragment>
                    ))}

                    {/* <br />
                    <pre>
                        {JSON.stringify(arrVesselList, null, 2)}
                    </pre> */}
                </Fragment>
            )
    )
}

export default withRouter(Index)
