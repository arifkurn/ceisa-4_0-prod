import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../BookingRequest/dashforge.module.css'
import Spinner from 'react-bootstrap/Spinner'
import axios from 'axios'
import * as ApiClickargo from '../../../Clickargo/ApiList.js'

function Status() {

    const [orderDetails, setOrderDetails] = useState([])

    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            axios
                .post(ApiClickargo.CLICKARGO_FIND_ORDER_BY_JOB_NUMBER, {
                    job_number: localStorage.getItem('ckSmJobNumber')
                })
                .then(response => {
                    console.log('success get detail vessel')
                    const dataVessel = response.data.data
                    const values = [...orderDetails]

                    dataVessel.tracking_status.freight[0].clickargo.subStatus.map((input, index) => {
                        values.push({
                            trackingStatus: input.name,
                            trackingStart: input.value.start,
                            trackingEnd: input.value.end,
                        })
                        setOrderDetails(values)
                    })

                    //spinner false after all data is updated
                    setIsLoading(false)
                })
                .catch(error => {
                    console.log(error.response.data.message)
                })
        }, 1000);
    }, [])

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
                <Fragment>
                    <div className={dashforgeCss.dSmFlex + ` ` + dashforgeCss.alignItemsCenter + ` ` + dashforgeCss.justifyContentCenter}>
                        <div className="row">
                            <ul className="list-ic horizontal">
                                <li className="active">
                                    <img
                                        className="img-fluid"
                                        src={window.location.origin + '/assets/images/clickargo/trucking/vessel.svg'}
                                        style={{ width: '70px', height: '70px' }}
                                        alt={'Document 4'} />
                                    <ul className="list-ic vertical">
                                        {orderDetails.map((input, index) => (
                                            <Fragment key={index}>
                                                <li>
                                                    <span />
                                                    <div>
                                                        <p className="kt-font-bold">{input.trackingStatus}</p>
                                                        <p className="kt-font-info">{input.trackingStart} <label className={dashforgeCss.txDanger}>-</label> {input.trackingEnd}</p>
                                                    </div>
                                                </li>
                                            </Fragment>
                                        ))}
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {/* <br />
                <pre>
                    {JSON.stringify(orderDetails, null, 2)}
                </pre> */}
                </Fragment>
            )
    )
}

export default Status
