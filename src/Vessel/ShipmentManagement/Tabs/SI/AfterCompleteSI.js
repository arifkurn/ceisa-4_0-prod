import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../../BookingRequest/dashforge.module.css'
import axios from 'axios'
import * as ApiClickargo from '../../../../Clickargo/ApiList.js'

const AfterCompleteSI = params => {

    const [isRenderingComponent, setIsRenderingComponent] = useState(true)

    const [isHazmat, setIsHazmat] = useState(false);

    const [formSI, setFormSI] = useState([])

    const [inputFieldsContainer, setInputFieldsContainer] = useState([])

    const [inputBlTypeOriginal, setinputBlTypeOriginal] = useState([])

    const [inputBlTypeSeaway, setinputBlTypeSeaway] = useState([])

    const [inputBlTypeHouseBill, setinputBlTypeHouseBill] = useState([])

    useEffect(() => {
        setTimeout(() => {
            axios
                .post(ApiClickargo.CLICKARGO_FIND_ORDER_BY_JOB_NUMBER, {
                    job_number: localStorage.getItem('ckSmJobNumber')
                })
                .then(response => {
                    console.log('success get detail vessel')
                    const dataVessel = response.data.data

                    let SI = dataVessel.shipping_instructions
                    let SILastIndex = SI.sort((a, b) => b.version - a.version)[0].details

                    var splitData = function (data) {
                        return data.split(',')
                    }

                    setFormSI([...formSI,
                    {
                        //general details
                        //shipper
                        shipperName: SILastIndex.shipper_name,
                        shipperTaxNumber: SILastIndex.shipper_npwp,
                        shipperAddress1: SILastIndex.shipper_address_1,
                        shipperAddress2: SILastIndex.shipper_address_2,
                        shipperCity: SILastIndex.shipper_city,
                        shipperPostalCode: SILastIndex.shipper_postal_code,
                        shipperCountry: SILastIndex.shipper_country_name,
                        //forwarder
                        forwarderName: SILastIndex.forwarder_name,
                        forwarderAddress1: SILastIndex.forwarder_address_1,
                        forwarderAddress2: SILastIndex.forwarder_address_2,
                        forwarderCity: SILastIndex.forwarder_city,
                        forwarderPostalCode: SILastIndex.forwarder_postal_code,
                        forwarderCountry: SILastIndex.forwarder_country_name,
                        //carrier
                        carrierName: SILastIndex.carrier_name,
                        //consignee
                        consigneeName: SILastIndex.consignee_name,
                        consigneeAddress1: SILastIndex.consignee_address_1,
                        consigneeAddress2: SILastIndex.consignee_address_2,
                        consigneeCity: SILastIndex.consignee_city,
                        consigneePostalCode: SILastIndex.consignee_postal_code,
                        consigneeCountry: SILastIndex.consignee_country_name,
                        //additional party
                        //contract party
                        contractPartyName: SILastIndex.contract_party_name,
                        contractPartyAddress1: SILastIndex.contract_party_address_1,
                        contractPartyAddress2: SILastIndex.contract_party_address_2,
                        contractPartyCity: SILastIndex.contract_party_city,
                        contractPartyPostalCode: SILastIndex.contract_party_postal_code,
                        contractPartyCountry: SILastIndex.contract_party_country_code,
                        contractPartyCountryLabel: SILastIndex.contract_party_country_name,
                        //notify party
                        notifyPartyName: SILastIndex.notify_party_name,
                        notifyPartyAddress1: SILastIndex.notify_party_address_1,
                        notifyPartyAddress2: SILastIndex.notify_party_address_2,
                        notifyPartyCity: SILastIndex.notify_party_city,
                        notifyPartyPostalCode: SILastIndex.notify_party_postal_code,
                        notifyPartyCountry: SILastIndex.notify_party_country_code,
                        notifyPartyCountryLabel: SILastIndex.notify_party_country_name,
                        //first additional notify party
                        firstAdditionalPartyName: SILastIndex.first_additional_party,
                        firstAdditionalPartyAddress1: SILastIndex.first_additional_party_address_1,
                        firstAdditionalPartyAddress2: SILastIndex.first_additional_party_address_2,
                        firstAdditionalPartyCity: SILastIndex.first_additional_party_city,
                        firstAdditionalPartyPostalCode: SILastIndex.first_additional_party_postal_code,
                        firstAdditionalPartyCountry: SILastIndex.first_additional_party_country_code,
                        firstAdditionalPartyCountryLabel: SILastIndex.first_additional_party_country_name,
                        //second additional notify party
                        secondAdditionalPartyName: SILastIndex.second_additional_party,
                        secondAdditionalPartyAddress1: SILastIndex.second_additional_party_address_1,
                        secondAdditionalPartyAddress2: SILastIndex.second_additional_party_address_2,
                        secondAdditionalPartyCity: SILastIndex.second_additional_party_city,
                        secondAdditionalPartyPostalCode: SILastIndex.second_additional_party_postal_code,
                        secondAdditionalPartyCountry: SILastIndex.second_additional_party_country_code,
                        secondAdditionalPartyCountryLabel: SILastIndex.second_additional_party_country_name,
                        //transport
                        vessel: SILastIndex.vessel,
                        voyage: SILastIndex.voyage,
                        originOfGoods: SILastIndex.port_of_load,
                        originOfGoodsPrint: SILastIndex.bl_origin,
                        placeOfReceipt: SILastIndex.port_of_load,
                        placeOfReceiptPrint: SILastIndex.bl_receipts,
                        portOfLoad: SILastIndex.port_of_load,
                        portOfLoadPrint: SILastIndex.bl_pol,
                        portOfDischarge: SILastIndex.port_of_discharge,
                        portOfDischargePrint: SILastIndex.bl_pod,
                        destination: SILastIndex.port_of_discharge,
                        destinationPrint: SILastIndex.bl_destination,
                        moveType: SILastIndex.move_type,
                        shipmentType: SILastIndex.shipment_type,
                        //Customs Compliance (Government Tax IDs)
                        shipperTaxId: SILastIndex.shipper_govt_id,
                        consigneeTaxId: SILastIndex.consignee_govt_id,
                        //Documentation and Freight Charges
                        freightTerm: SILastIndex.freight_term,
                        payer: SILastIndex.payer,
                        paymentLocation: SILastIndex.payment_location,
                        chargeType: SILastIndex.charge_type,
                        //Remarks
                        customerComments: SILastIndex.customer_comments,
                        //Notification Emails
                        partnerNotificationEmails: SILastIndex.partner_notif_email,
                    }
                    ])

                    const values = [...inputFieldsContainer];
                    dataVessel.containers.filter(order => order.order_integration_uuid === SI.sort((a, b) => b.version - a.version)[0].uuid).map((inputContainer, indexContainer) => {
                        if (dataVessel.details.product_type === 'general') {
                            values.push({
                                //container
                                containerNumber: inputContainer.attribute.container_number,
                                containerType: inputContainer.code,
                                containerTypeLabel: inputContainer.name,
                                containerSealNumber: inputContainer.attribute.seal_number,
                                containerSealNumberType: inputContainer.attribute.seal_number_type,
                                containerSealNumberTypeLabel: inputContainer.attribute.seal_number_type,
                                containerComments: inputContainer.attribute.container_comments,
                                //container cargo
                                cargo:
                                    inputContainer.cargos.map((inputCargo, indexCargo) => (
                                        {
                                            cargoDescription: inputCargo.description,
                                            cargoHsCode: inputCargo.hs_code,
                                            cargoHsCodeLabel: inputCargo.hs_code,
                                            cargoWeight: inputCargo.weight,
                                            cargoWeightType: inputCargo.weight_uom,
                                            cargoPackageCount: inputCargo.quantity,
                                            cargoPackageType: inputCargo.package_code,
                                            cargoPackageTypeLabel: inputCargo.package_code,
                                            cargoGrossVolume: inputCargo.gross_volume,
                                            cargoGrossVolumeType: inputCargo.gross_volume_uom,
                                            cargoPrimaryImoClass: '',
                                            cargoPrimaryImoClassLabel: 'Please select IMO Class',
                                            cargoUndgNumber: '',
                                            cargoPackingGroup: '',
                                            cargoPackingGroupLabel: 'Please select Packing Group',
                                            cargoProperShippingName: '',
                                            cargoEmergencyContactName: '',
                                            cargoEmergencyContactNumber: '',
                                        }
                                    ))
                            })
                        } else if (dataVessel.details.product_type === 'others') {
                            values.push({
                                //container
                                containerNumber: inputContainer.attribute.container_number,
                                containerType: inputContainer.code,
                                containerTypeLabel: inputContainer.name,
                                containerSealNumber: inputContainer.attribute.seal_number,
                                containerSealNumberType: inputContainer.attribute.seal_number_type,
                                containerSealNumberTypeLabel: inputContainer.attribute.seal_number_type,
                                containerComments: inputContainer.attribute.container_comments,
                                //container cargo
                                cargo:
                                    inputContainer.cargos.map((inputCargo, indexCargo) => (
                                        {
                                            cargoDescription: inputCargo.description,
                                            cargoHsCode: inputCargo.hs_code,
                                            cargoHsCodeLabel: inputCargo.hs_code,
                                            cargoWeight: inputCargo.weight,
                                            cargoWeightType: inputCargo.weight_uom,
                                            cargoPackageCount: inputCargo.quantity,
                                            cargoPackageType: inputCargo.package_code,
                                            cargoPackageTypeLabel: inputCargo.package_code,
                                            cargoGrossVolume: inputCargo.gross_volume,
                                            cargoGrossVolumeType: inputCargo.gross_volume_uom,
                                            cargoPrimaryImoClass: '',
                                            cargoPrimaryImoClassLabel: 'Please select IMO Class',
                                            cargoUndgNumber: '',
                                            cargoPackingGroup: '',
                                            cargoPackingGroupLabel: 'Please select Packing Group',
                                            cargoProperShippingName: '',
                                            cargoEmergencyContactName: '',
                                            cargoEmergencyContactNumber: '',
                                        }
                                    ))
                            })
                        } else if (dataVessel.details.product_type === 'special') {
                            values.push({
                                //container
                                containerNumber: inputContainer.attribute.container_number,
                                containerType: inputContainer.code,
                                containerTypeLabel: inputContainer.name,
                                containerSealNumber: inputContainer.attribute.seal_number,
                                containerSealNumberType: inputContainer.attribute.seal_number_type,
                                containerSealNumberTypeLabel: inputContainer.attribute.seal_number_type,
                                containerComments: inputContainer.attribute.container_comments,
                                //container cargo
                                cargo:
                                    inputContainer.cargos.map((inputCargo, indexCargo) => (
                                        {
                                            cargoDescription: inputCargo.description,
                                            cargoHsCode: inputCargo.hs_code,
                                            cargoHsCodeLabel: inputCargo.hs_code,
                                            cargoWeight: inputCargo.weight,
                                            cargoWeightType: inputCargo.weight_uom,
                                            cargoPackageCount: inputCargo.quantity,
                                            cargoPackageType: inputCargo.package_code,
                                            cargoPackageTypeLabel: inputCargo.package_code,
                                            cargoGrossVolume: inputCargo.gross_volume,
                                            cargoGrossVolumeType: inputCargo.gross_volume_uom,
                                            cargoPrimaryImoClass: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_imo,
                                            cargoPrimaryImoClassLabel: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_imo,
                                            cargoUndgNumber: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).undg_number,
                                            cargoPackingGroup: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).packing_group,
                                            cargoPackingGroupLabel: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).packing_group,
                                            cargoProperShippingName: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_proper_shipping,
                                            cargoEmergencyContactName: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_emergency_contact_name,
                                            cargoEmergencyContactNumber: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_emergency_contact_number,
                                        }
                                    ))
                            })
                        } else if (dataVessel.details.product_type === 'dangerous') {
                            values.push({
                                //container
                                containerNumber: inputContainer.attribute.container_number,
                                containerType: inputContainer.code,
                                containerTypeLabel: inputContainer.name,
                                containerSealNumber: inputContainer.attribute.seal_number,
                                containerSealNumberType: inputContainer.attribute.seal_number_type,
                                containerSealNumberTypeLabel: inputContainer.attribute.seal_number_type,
                                containerComments: inputContainer.attribute.container_comments,
                                //container cargo
                                cargo:
                                    inputContainer.cargos.map((inputCargo, indexCargo) => (
                                        {
                                            cargoDescription: inputCargo.description,
                                            cargoHsCode: inputCargo.hs_code,
                                            cargoHsCodeLabel: inputCargo.hs_code,
                                            cargoWeight: inputCargo.weight,
                                            cargoWeightType: inputCargo.weight_uom,
                                            cargoPackageCount: inputCargo.quantity,
                                            cargoPackageType: inputCargo.package_code,
                                            cargoPackageTypeLabel: inputCargo.package_code,
                                            cargoGrossVolume: inputCargo.gross_volume,
                                            cargoGrossVolumeType: inputCargo.gross_volume_uom,
                                            cargoPrimaryImoClass: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_imo,
                                            cargoPrimaryImoClassLabel: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_imo,
                                            cargoUndgNumber: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).undg_number,
                                            cargoPackingGroup: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).packing_group,
                                            cargoPackingGroupLabel: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).packing_group,
                                            cargoProperShippingName: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_proper_shipping,
                                            cargoEmergencyContactName: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_emergency_contact_name,
                                            cargoEmergencyContactNumber: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_emergency_contact_number,
                                        }
                                    ))
                            })
                        }
                        setInputFieldsContainer(values);
                    })

                    //spinner false after all data is updated
                    setIsRenderingComponent(false)
                })
                .catch(error => {
                    console.log(error)
                    //console.log(error.response.data.message)
                })
        }, 1000);
    }, [])

    return (
        isRenderingComponent ? (
            <Fragment>
                <h3 className={dashforgeCss.txBlack + ` ` + dashforgeCss.textCenter}>Please Wait...</h3>
            </Fragment>
        ) : (
                <Fragment>
                    <div className="row">
                        <div className="col-md-12">
                            {formSI.map((inputSI, indexSI) => (
                                <Fragment key={`${inputSI}~${indexSI}`}>
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>General Details</legend>

                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>SHIPPER</legend>

                                            <div className="row">

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Shipper Name</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.shipperName} />
                                                    </div>
                                                </div>

                                                <div className="col-6"></div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Shipper Address Line 1<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.shipperAddress1} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Shipper City<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.shipperCity} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Shipper Address Line 2<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.shipperAddress2} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Shipper Postal Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.shipperPostalCode} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Shipper Country<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.shipperCountry} />
                                                    </div>
                                                </div>

                                            </div>
                                        </fieldset>

                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Forwarder</legend>

                                            <div className="row">

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Forwarder<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.forwarderName} />
                                                    </div>
                                                </div>

                                                <div className="col-6"></div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Forwarder Address Line 1 (Printed on B/L)<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.forwarderAddress1} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Forwarder City<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.forwarderCity} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Forwarder Address Line 2 (Printed on B/L)<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.forwarderAddress2} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Forwarder Postal Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.forwarderPostalCode} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Forwarder Country<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.forwarderCountry} />
                                                    </div>
                                                </div>

                                            </div>

                                        </fieldset>

                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Carrier</legend>

                                            <div className="row">

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Carrier<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.carrierName} />
                                                    </div>
                                                </div>

                                            </div>
                                        </fieldset>

                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Consignee</legend>

                                            <div className="row">

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Consignee Name<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.consigneeName} />
                                                    </div>
                                                </div>

                                                <div className="col-6"></div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Consignee Address Line 1<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.consigneeAddress1} />

                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Consignee City<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.consigneeCity} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Consignee Address Line 2<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.consigneeAddress2} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Consignee Postal Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.consigneePostalCode} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Consignee Country<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.consigneeCountry} />
                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>

                                    </fieldset>

                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>Additional Party (optional)</legend>

                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Contract Party</legend>

                                            <div className="row">

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Contract Party</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.contractPartyName} />
                                                    </div>
                                                </div>

                                                <div className="col-6"></div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.contractPartyAddress1} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>City</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.contractPartyCity} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.contractPartyAddress2} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.contractPartyPostalCode} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Country</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.contractPartyCountryLabel} />
                                                    </div>
                                                </div>

                                            </div>
                                        </fieldset>

                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Notify Party</legend>

                                            <div className="row">

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Notify Party</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.notifyPartyName} />
                                                    </div>
                                                </div>

                                                <div className="col-6"></div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.notifyPartyAddress1} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>City</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.notifyPartyCity} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.notifyPartyAddress2} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.notifyPartyPostalCode} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Country</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.notifyPartyCountryLabel} />
                                                    </div>
                                                </div>

                                            </div>
                                        </fieldset>

                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>First Additional Notify Party</legend>

                                            <div className="row">

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>First Additional Notify Party</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.firstAdditionalPartyName} />
                                                    </div>
                                                </div>

                                                <div className="col-6"></div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.firstAdditionalPartyAddress1} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>City</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.firstAdditionalPartyCity} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.firstAdditionalPartyAddress2} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.firstAdditionalPartyPostalCode} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Country</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.firstAdditionalPartyCountryLabel} />
                                                    </div>
                                                </div>

                                            </div>
                                        </fieldset>

                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Second Additional Notify Party</legend>

                                            <div className="row">

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Second Additional Notify Party</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.secondAdditionalPartyName} />
                                                    </div>
                                                </div>

                                                <div className="col-6"></div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.secondAdditionalPartyAddress1} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>City</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.secondAdditionalPartyCity} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.secondAdditionalPartyAddress2} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.secondAdditionalPartyPostalCode} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Country</label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.secondAdditionalPartyCountryLabel} />
                                                    </div>
                                                </div>

                                            </div>
                                        </fieldset>

                                    </fieldset>

                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>Transport</legend>

                                        <div className="row">

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Vessel<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.vessel} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Voyage<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.voyage} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Origin of Goods<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.originOfGoods} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Print on B/L as<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.originOfGoodsPrint} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Place of Receipts<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.placeOfReceipt} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Print on B/L as<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.placeOfReceiptPrint} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Port of Load<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.portOfLoad} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Print on B/L as<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.portOfLoadPrint} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Port of Discharge<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.portOfDischarge} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Print on B/L as<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.portOfDischargePrint} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Destination (Place of carrier delivery)<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.destination} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Print on B/L as<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.destinationPrint} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Move type<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.moveType} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Shipment type<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.shipmentType} />
                                                </div>
                                            </div>

                                        </div>
                                    </fieldset>

                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>Customs Compliance (Government Tax IDs)</legend>

                                        <div className="row">

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Shipper</label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.shipperTaxId} />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Consignee</label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.consigneeTaxId} />
                                                </div>
                                            </div>

                                        </div>
                                    </fieldset>

                                </Fragment>
                            ))}

                            {inputFieldsContainer.map((inputContainer, indexContainer) => (
                                <Fragment key={`${inputContainer}~${indexContainer}`}>
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>Container</legend>

                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Container {indexContainer + 1}</legend>

                                            <div className="row">

                                                <div className="col-3">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Container Number<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputContainer.containerNumber} />
                                                        <span className={dashforgeCss.tx12}>4 letters + 7 digits (e.g. UNIS1234567)</span>
                                                    </div>
                                                </div>

                                                <div className="col-3">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Container Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputContainer.containerTypeLabel + ` (` + inputContainer.containerType + `)`} />
                                                    </div>
                                                </div>

                                                <div className="col-3">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Container Seal Number<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputContainer.containerSealNumber} />
                                                    </div>
                                                </div>

                                                <div className="col-3">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Container Seal Number Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputContainer.containerSealNumberTypeLabel} />
                                                    </div>
                                                </div>

                                                <div className="col-12">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Container Comments<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputContainer.containerComments} />
                                                    </div>
                                                </div>

                                            </div>

                                            {inputContainer.cargo.map((inputCargo, indexCargo) => (
                                                <div>
                                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '10px', marginTop: '10px' }}>

                                                        <div className="row">
                                                            <div className="col-md-8">
                                                                <h6 className={dashforgeCss.txBlack}>Cargo</h6>
                                                                <span className={dashforgeCss.tx12 + ' ' + dashforgeCss.txBlack}><div className={dashforgeCss.txDanger}>NOTE:</div> The sum of all Cargo Weights reflects the Gross Weight of the Cargo (excluding Tare) for the entire booking.</span>
                                                                <p className={dashforgeCss.tx12 + ' ' + dashforgeCss.txBlack}>Inaccurate declaration of cargo weight may endanger crew, port worker, and vessel safety. Please verify the reasonable accuracy of this information prior to submission</p>
                                                            </div>
                                                        </div>

                                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                            <div className="row">

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Cargo Description<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputCargo.cargoDescription} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Package Count<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputCargo.cargoPackageCount} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>HS Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputCargo.cargoHsCode} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Package Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputCargo.cargoPackageType} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Cargo Weight (Excludes Tares)<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <div className="input-group" style={{ flexDirection: 'row' }}>
                                                                            <div className="gross-volume-wrapper" style={{ display: 'flex' }}>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputCargo.cargoWeight} />
                                                                                <div className="input-group-append">
                                                                                    <select
                                                                                        disabled
                                                                                        className="dropdown-toggle"
                                                                                        type="button"
                                                                                        data-toggle="dropdown"
                                                                                        aria-haspopup="true"
                                                                                        aria-expanded="false"
                                                                                        value={inputCargo.cargoWeightType}>
                                                                                        <option value="KGM" className="dropdown-item">Kgs</option>
                                                                                        <option value="LBS" className="dropdown-item">Pounds</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Gross Volume<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <div className="input-group" style={{ flexDirection: 'row' }}>
                                                                            <div className="gross-volume-wrapper" style={{ display: 'flex' }}>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputCargo.cargoGrossVolume} />
                                                                                <div className="input-group-append">
                                                                                    <select
                                                                                        disabled
                                                                                        className="dropdown-toggle"
                                                                                        type="button"
                                                                                        data-toggle="dropdown"
                                                                                        aria-haspopup="true"
                                                                                        aria-expanded="false"
                                                                                        value={inputCargo.cargoGrossVolumeType}>
                                                                                        <option value="MTQ">Cubic meters</option>
                                                                                        <option value="FTQ">Cubic feet</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            {isHazmat && <Fragment>
                                                                <div className="row">
                                                                    <div className="col-12">
                                                                        <h6 className={dashforgeCss.txBlack}>Hazmat Details</h6>
                                                                    </div>
                                                                </div>

                                                                <div className="row">

                                                                    <div className="col-12">
                                                                        <div className="form-group">
                                                                            <label className={dashforgeCss.txBlack}>Primary IMO Class</label>
                                                                            <input
                                                                                type="text"
                                                                                disabled
                                                                                className="form-control"
                                                                                value={inputCargo.cargoPrimaryImoClass} />
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-6">
                                                                        <div className="form-group">
                                                                            <label className={dashforgeCss.txBlack}>UNDG Number</label>
                                                                            <input
                                                                                type="text"
                                                                                disabled
                                                                                className="form-control"
                                                                                value={inputCargo.cargoUndgNumber} />
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-6">
                                                                        <div className="form-group">
                                                                            <label className={dashforgeCss.txBlack}>Emergency Contact Name</label>
                                                                            <input
                                                                                type="text"
                                                                                disabled
                                                                                className="form-control"
                                                                                value={inputCargo.cargoEmergencyContactName} />
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-6">
                                                                        <div className="form-group">
                                                                            <label className={dashforgeCss.txBlack}>Packing Group</label>
                                                                            <input
                                                                                type="text"
                                                                                disabled
                                                                                className="form-control"
                                                                                value={inputCargo.cargoPackingGroup} />
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-6">
                                                                        <div className="form-group">
                                                                            <label className={dashforgeCss.txBlack}>Emergency Contact Number</label>
                                                                            <input
                                                                                type="text"
                                                                                disabled
                                                                                className="form-control"
                                                                                value={inputCargo.cargoEmergencyContactNumber} />
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-6">
                                                                        <div className="form-group">
                                                                            <label className={dashforgeCss.txBlack}>Proper Shipping Name</label>
                                                                            <input
                                                                                type="text"
                                                                                disabled
                                                                                className="form-control"
                                                                                value={inputCargo.cargoProperShippingName} />
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </Fragment>}

                                                        </fieldset>

                                                    </fieldset>
                                                </div>

                                            ))}

                                        </fieldset>

                                    </fieldset>

                                </Fragment>
                            ))}

                            {formSI.map((inputSI, indexSI) => (
                                <Fragment>
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>Documentation and Freight Charges</legend>

                                        <div className="row">

                                            <div className="col-3">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Freight Term<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.freightTerm} />
                                                </div>
                                            </div>

                                            <div className="col-3">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Payer<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.payer} />
                                                </div>
                                            </div>

                                            <div className="col-3">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Charge Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.chargeType} />
                                                </div>
                                            </div>

                                            <div className="col-3">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Payment Location<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.paymentLocation} />
                                                </div>
                                            </div>

                                        </div>

                                    </fieldset>

                                </Fragment>
                            ))}

                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                <legend>B/L Print Instructions (Provided to the Carrier, not printed on Bill of Landing)</legend>

                                <div className="row">

                                    <div className="col-4">

                                        <div className="form-group">
                                            <label for="">B/L Type</label>
                                            <div className="custom-control custom-radio">
                                                <input
                                                    disabled
                                                    type="radio"
                                                    id="bl_type_original"
                                                    name="bl_type"
                                                    value="original"
                                                    className="custom-control-input" />
                                                <label for="bl_type_original" className="custom-control-label">Original</label>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="custom-control custom-radio">
                                                <input
                                                    disabled
                                                    type="radio"
                                                    id="bl_type_seaway"
                                                    name="bl_type"
                                                    value="seaway"
                                                    className="custom-control-input" />
                                                <label for="bl_type_seaway" className="custom-control-label">Seaway/Express</label>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="custom-control custom-radio">
                                                <input
                                                    disabled
                                                    type="radio"
                                                    id="bl_type_house_bill"
                                                    name="bl_type"
                                                    value="house_bill"
                                                    className="custom-control-input" />
                                                <label for="bl_type_house_bill" className="custom-control-label">Stand Alone House Bill</label>
                                            </div>
                                        </div>

                                    </div>

                                    <div className="col-4">

                                        <div className="form-group">
                                            <label for="">Freighted</label>
                                            <input
                                                type="text"
                                                placeholder="Enter # Copies"
                                                className="form-control"
                                                style={{ marginBottom: '5px' }}
                                                value=""
                                                disabled />
                                            <input
                                                type="text"
                                                placeholder="Enter # Copies"
                                                className="form-control"
                                                style={{ marginBottom: '5px' }}
                                                value=""
                                                disabled />
                                            <input
                                                type="text"
                                                placeholder="Enter # Copies"
                                                className="form-control"
                                                style={{ marginBottom: '5px' }}
                                                value=""
                                                disabled />
                                        </div>

                                    </div>

                                    <div className="col-4">

                                        <div className="form-group">
                                            <label for="">Unfreighted</label>
                                            <input
                                                type="text"
                                                placeholder="Enter # Copies"
                                                className="form-control"
                                                style={{ marginBottom: '5px' }}
                                                value=""
                                                disabled />
                                            <input
                                                type="text"
                                                placeholder="Enter # Copies"
                                                className="form-control"
                                                style={{ marginBottom: '5px' }}
                                                value=""
                                                disabled />
                                            <input
                                                type="text"
                                                placeholder="Enter # Copies"
                                                className="form-control"
                                                style={{ marginBottom: '5px' }}
                                                value=""
                                                disabled />
                                        </div>

                                    </div>

                                </div>
                            </fieldset>

                            {formSI.map((inputSI, indexSI) => (
                                <Fragment>
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>Remarks</legend>

                                        <div className="row">

                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Customer Comments</label>
                                                    <textarea
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.customerComments}></textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </fieldset>

                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>Notification Emails</legend>

                                        <div className="row">

                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Partner Notification Emails<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <span className="float-right" style={{ fontSize: '12px', color: 'black' }}>Press enter or separate each email with ,</span>
                                                    <input
                                                        type="text"
                                                        disabled
                                                        className="form-control"
                                                        value={inputSI.partnerNotificationEmails} />
                                                </div>
                                            </div>

                                        </div>
                                    </fieldset>

                                </Fragment>
                            ))}

                        </div>
                    </div>
                    {/* <br />
                    <pre>
                        {JSON.stringify(inputFieldsContainer, null, 2)}
                    </pre> */}
                    {/* <br />
                    <pre>
                        {JSON.stringify(formSI, null, 2)}
                    </pre> */}
                </Fragment>
            )
    )
}

export default AfterCompleteSI
