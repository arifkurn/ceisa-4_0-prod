import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../../BookingRequest/dashforge.module.css'
import { useForm } from 'react-hook-form'
import { useToasts } from 'react-toast-notifications'
import axios from 'axios'
import Select from 'react-select'
import AsyncSelect from 'react-select/async'
import * as ApiClickargo from '../../../../Clickargo/ApiList.js'
import Alert from 'react-bootstrap/Alert'

const BeforeCompleteSI = params => {

    const { addToast } = useToasts()

    const { register, handleSubmit, errors, watch, setError, clearError, setValue, getValues } = useForm()

    const [isLoading, setIsLoading] = useState(false);

    const [isRenderingComponent, setIsRenderingComponent] = useState(true)

    const [isBookingNumber, setIsBookingNumber] = useState(false);

    const [isHazmat, setIsHazmat] = useState(false);

    const [blOriginalFreightedDisabled, setBlOriginalFreightedDisabled] = useState(true)
    const [blSeawayFreightedDisabled, setBlSeawayFreightedDisabled] = useState(true)
    const [blHouseBillFreightedDisabled, setBlHouseBillFreightedDisabled] = useState(true)

    const [blOriginalUnfreightedDisabled, setBlOriginalUnfreightedDisabled] = useState(true)
    const [blSeawayUnfreightedDisabled, setBlSeawayUnfreightedDisabled] = useState(true)
    const [blHouseBillUnfreightedDisabled, setBlHouseBillUnfreightedDisabled] = useState(true)

    const [arrCountryList, setArrCountryList] = useState([])

    const [formSI, setFormSI] = useState([])

    const [inputFieldsContainer, setInputFieldsContainer] = useState([])

    const [inputPaymentDetails, setInputPaymentDetails] = useState([
        {
            freightTerm: '',
            freightTermLabel: 'Select One',
            payer: '',
            payerLabel: 'Select One',
            chargeType: '',
            chargeTypeLabel: 'Select One',
            paymentLocation: '',
            paymentLocationLabel: 'Payment Port Location',
        }
    ])

    const [inputBlTypeOriginal, setinputBlTypeOriginal] = useState([
        {
            type: '',
            freighted: '',
            unfreighted: ''
        }
    ])

    const [inputBlTypeSeaway, setinputBlTypeSeaway] = useState([
        {
            type: '',
            freighted: '',
            unfreighted: ''
        }
    ])

    const [inputBlTypeHouseBill, setinputBlTypeHouseBill] = useState([
        {
            type: '',
            freighted: '',
            unfreighted: ''
        }
    ])

    useEffect(() => {
        setTimeout(() => {
            axios
                .post(ApiClickargo.CLICKARGO_FIND_ORDER_BY_JOB_NUMBER, {
                    job_number: localStorage.getItem('ckSmJobNumber')
                })
                .then(response => {
                    console.log('success get detail vessel')
                    const dataVessel = response.data.data

                    if (dataVessel.booking_number === null) {
                        setIsBookingNumber(false)
                    } else {
                        if (dataVessel.booking_number.length > 0) {
                            setIsBookingNumber(true)
                        } else {
                            setIsBookingNumber(false)
                        }
                    }

                    let br = dataVessel.booking_requests
                    let brLastIndex = br.sort((a, b) => b.version - a.version)[0].details

                    var splitData = function (data) {
                        return data.split(',')
                    }

                    let forwarder = brLastIndex.forwarder_address
                    let forwarderAddress = splitData(forwarder)[0]
                    let forwarderCity = splitData(forwarder)[1].substring(1)
                    let forwarderProvince = splitData(forwarder)[2].substring(1)
                    let forwarderPostalCode = splitData(forwarder)[3].substring(1)
                    let forwarderCountry = splitData(forwarder)[4].substring(1)

                    //set product type
                    if (dataVessel.details.product_type === 'special') {
                        setIsHazmat(true)
                    } else if (dataVessel.details.product_type === 'dangerous') {
                        setIsHazmat(true)
                    } else if (dataVessel.details.product_type === 'general') {
                        setIsHazmat(false)
                    } else if (dataVessel.details.product_type === 'others') {
                        setIsHazmat(false)
                    }

                    setFormSI([...formSI,
                    {
                        //uuids
                        orderUuid: dataVessel.uuid,
                        bookingRequestUuid: br.sort((a, b) => b.version - a.version)[0].uuid,
                        //general details
                        //shipper
                        shipperName: brLastIndex.shipper_name,
                        shipperTaxNumber: brLastIndex.shipper_npwp,
                        shipperAddress1: brLastIndex.shipper_address_1,
                        shipperAddress2: brLastIndex.shipper_address_2,
                        shipperCity: brLastIndex.shipper_city,
                        shipperPostalCode: brLastIndex.shipper_postal_code,
                        shipperCountry: brLastIndex.shipper_country_name,
                        //forwarder
                        forwarderName: brLastIndex.forwarder,
                        forwarderAddress1: forwarderAddress,
                        forwarderAddress2: forwarderProvince,
                        forwarderCity: forwarderCity,
                        forwarderPostalCode: forwarderPostalCode,
                        forwarderCountry: forwarderCountry,
                        //carrier
                        carrierName: brLastIndex.carrier_name,
                        //consignee
                        consigneeName: brLastIndex.consignee,
                        consigneeAddress1: brLastIndex.consignee_address_1,
                        consigneeAddress2: brLastIndex.consignee_address_2,
                        consigneeCity: brLastIndex.consignee_city,
                        consigneePostalCode: brLastIndex.consignee_postal_code,
                        consigneeCountry: brLastIndex.consignee_country_name,
                        //additional party
                        //contract party
                        contractPartyName: brLastIndex.contract_party_name,
                        contractPartyAddress1: brLastIndex.contract_party_address_1,
                        contractPartyAddress2: brLastIndex.contract_party_address_2,
                        contractPartyCity: brLastIndex.contract_party_city,
                        contractPartyPostalCode: brLastIndex.contract_party_postal_code,
                        contractPartyCountry: brLastIndex.contract_party_country_code,
                        contractPartyCountryLabel: brLastIndex.contract_party_country_name,
                        //notify party
                        notifyPartyName: brLastIndex.notify_party_name,
                        notifyPartyAddress1: brLastIndex.notify_party_address_1,
                        notifyPartyAddress2: brLastIndex.notify_party_address_2,
                        notifyPartyCity: brLastIndex.notify_party_city,
                        notifyPartyPostalCode: brLastIndex.notify_party_postal_code,
                        notifyPartyCountry: brLastIndex.notify_party_country_code,
                        notifyPartyCountryLabel: brLastIndex.notify_party_country_name,
                        //first additional notify party
                        firstAdditionalPartyName: brLastIndex.first_additional_party,
                        firstAdditionalPartyAddress1: brLastIndex.first_additional_party_address_1,
                        firstAdditionalPartyAddress2: brLastIndex.first_additional_party_address_2,
                        firstAdditionalPartyCity: brLastIndex.first_additional_party_city,
                        firstAdditionalPartyPostalCode: brLastIndex.first_additional_party_postal_code,
                        firstAdditionalPartyCountry: brLastIndex.first_additional_party_country_code,
                        firstAdditionalPartyCountryLabel: brLastIndex.first_additional_party_country_name,
                        //second additional notify party
                        secondAdditionalPartyName: brLastIndex.second_additional_party,
                        secondAdditionalPartyAddress1: brLastIndex.second_additional_party_address_1,
                        secondAdditionalPartyAddress2: brLastIndex.second_additional_party_address_2,
                        secondAdditionalPartyCity: brLastIndex.second_additional_party_city,
                        secondAdditionalPartyPostalCode: brLastIndex.second_additional_party_postal_code,
                        secondAdditionalPartyCountry: brLastIndex.second_additional_party_country_code,
                        secondAdditionalPartyCountryLabel: brLastIndex.second_additional_party_country_name,
                        //transport
                        vessel: brLastIndex.main_carriage.vessel,
                        voyage: brLastIndex.main_carriage.voyage,
                        originOfGoods: brLastIndex.main_carriage.port_of_load,
                        originOfGoodsPrint: brLastIndex.main_carriage.port_of_load,
                        placeOfReceipt: brLastIndex.main_carriage.port_of_load,
                        placeOfReceiptPrint: brLastIndex.main_carriage.port_of_load,
                        portOfLoad: brLastIndex.main_carriage.port_of_load,
                        portOfLoadPrint: brLastIndex.main_carriage.port_of_load,
                        portOfDischarge: brLastIndex.main_carriage.port_of_discharge,
                        portOfDischargePrint: brLastIndex.main_carriage.port_of_discharge,
                        destination: brLastIndex.main_carriage.port_of_discharge,
                        destinationPrint: brLastIndex.main_carriage.port_of_discharge,
                        moveType: dataVessel.move_type,
                        shipmentType: dataVessel.details.shipment_type,
                        //Customs Compliance (Government Tax IDs)
                        shipperTaxId: '',
                        consigneeTaxId: '',
                        //Remarks
                        customerComments: '',
                        //Notification Emails
                        partnerNotificationEmails: '',
                    }
                    ])

                    //set value to handle error
                    setTimeout(() => {
                        setValue([
                            { contractPartyAddress1: brLastIndex.contract_party_address_1 },
                            { contractPartyAddress2: brLastIndex.contract_party_address_2 },
                            { notifyPartyAddress1: brLastIndex.first_additional_party_address_1 },
                            { notifyPartyAddress2: brLastIndex.first_additional_party_address_2 },
                            { firstAdditionalPartyAddress1: brLastIndex.first_additional_party_address_1 },
                            { firstAdditionalPartyAddress2: brLastIndex.first_additional_party_address_2 },
                            { secondAdditionalPartyAddress1: brLastIndex.second_additional_party_address_1 },
                            { secondAdditionalPartyAddress2: brLastIndex.second_additional_party_address_2 },
                            { originOfGoodsPrint: brLastIndex.main_carriage.port_of_load },
                            { placeOfReceiptPrint: brLastIndex.main_carriage.port_of_load },
                            { portOfLoadPrint: brLastIndex.main_carriage.port_of_load },
                            { portOfDischargePrint: brLastIndex.main_carriage.port_of_discharge },
                            { destinationPrint: brLastIndex.main_carriage.port_of_discharge },
                            { partnerNotificationEmails: '' },
                        ])
                    }, 1000);

                    const values = [...inputFieldsContainer];
                    dataVessel.containers.filter(order => order.order_integration_uuid === br.sort((a, b) => b.version - a.version)[0].uuid).map((inputContainer, indexContainer) => {
                        if (dataVessel.details.product_type === 'general') {
                            values.push({
                                //container
                                containerNumber: inputContainer.attribute.container_number,
                                containerType: inputContainer.code,
                                containerTypeLabel: inputContainer.name,
                                containerSealNumber: inputContainer.attribute.seal_number,
                                containerSealNumberType: '',
                                containerSealNumberTypeLabel: 'Select one',
                                containerComments: inputContainer.attribute.container_comments,
                                //container cargo
                                cargo:
                                    inputContainer.cargos.map((inputCargo, indexCargo) => (
                                        {
                                            cargoDescription: inputCargo.description,
                                            cargoHsCode: inputCargo.hs_code,
                                            cargoHsCodeLabel: inputCargo.hs_code,
                                            cargoWeight: inputCargo.weight,
                                            cargoWeightType: inputCargo.weight_uom,
                                            cargoPackageCount: inputCargo.quantity,
                                            cargoPackageType: inputCargo.package_code,
                                            cargoPackageTypeLabel: inputCargo.package_code,
                                            cargoGrossVolume: inputCargo.gross_volume,
                                            cargoGrossVolumeType: inputCargo.gross_volume_uom,
                                            cargoPrimaryImoClass: '',
                                            cargoPrimaryImoClassLabel: 'Please select IMO Class',
                                            cargoUndgNumber: '',
                                            cargoPackingGroup: '',
                                            cargoPackingGroupLabel: 'Please select Packing Group',
                                            cargoProperShippingName: '',
                                            cargoEmergencyContactName: '',
                                            cargoEmergencyContactNumber: '',
                                        }
                                    ))
                            })
                        } else if (dataVessel.details.product_type === 'others') {
                            values.push({
                                //container
                                containerNumber: inputContainer.attribute.container_number,
                                containerType: inputContainer.code,
                                containerTypeLabel: inputContainer.name,
                                containerSealNumber: inputContainer.attribute.seal_number,
                                containerSealNumberType: '',
                                containerSealNumberTypeLabel: 'Select one',
                                containerComments: inputContainer.attribute.container_comments,
                                //container cargo
                                cargo:
                                    inputContainer.cargos.map((inputCargo, indexCargo) => (
                                        {
                                            cargoDescription: inputCargo.description,
                                            cargoHsCode: inputCargo.hs_code,
                                            cargoHsCodeLabel: inputCargo.hs_code,
                                            cargoWeight: inputCargo.weight,
                                            cargoWeightType: inputCargo.weight_uom,
                                            cargoPackageCount: inputCargo.quantity,
                                            cargoPackageType: inputCargo.package_code,
                                            cargoPackageTypeLabel: inputCargo.package_code,
                                            cargoGrossVolume: inputCargo.gross_volume,
                                            cargoGrossVolumeType: inputCargo.gross_volume_uom,
                                            cargoPrimaryImoClass: '',
                                            cargoPrimaryImoClassLabel: 'Please select IMO Class',
                                            cargoUndgNumber: '',
                                            cargoPackingGroup: '',
                                            cargoPackingGroupLabel: 'Please select Packing Group',
                                            cargoProperShippingName: '',
                                            cargoEmergencyContactName: '',
                                            cargoEmergencyContactNumber: '',
                                        }
                                    ))
                            })
                        } else if (dataVessel.details.product_type === 'special') {
                            values.push({
                                //container
                                containerNumber: inputContainer.attribute.container_number,
                                containerType: inputContainer.code,
                                containerTypeLabel: inputContainer.name,
                                containerSealNumber: inputContainer.attribute.seal_number,
                                containerSealNumberType: '',
                                containerSealNumberTypeLabel: 'Select one',
                                containerComments: inputContainer.attribute.container_comments,
                                //container cargo
                                cargo:
                                    inputContainer.cargos.map((inputCargo, indexCargo) => (
                                        {
                                            cargoDescription: inputCargo.description,
                                            cargoHsCode: inputCargo.hs_code,
                                            cargoHsCodeLabel: inputCargo.hs_code,
                                            cargoWeight: inputCargo.weight,
                                            cargoWeightType: inputCargo.weight_uom,
                                            cargoPackageCount: inputCargo.quantity,
                                            cargoPackageType: inputCargo.package_code,
                                            cargoPackageTypeLabel: inputCargo.package_code,
                                            cargoGrossVolume: inputCargo.gross_volume,
                                            cargoGrossVolumeType: inputCargo.gross_volume_uom,
                                            cargoPrimaryImoClass: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_imo,
                                            cargoPrimaryImoClassLabel: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_imo,
                                            cargoUndgNumber: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).undg_number,
                                            cargoPackingGroup: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).packing_group,
                                            cargoPackingGroupLabel: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).packing_group,
                                            cargoProperShippingName: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_proper_shipping,
                                            cargoEmergencyContactName: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_emergency_contact_name,
                                            cargoEmergencyContactNumber: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_emergency_contact_number,
                                        }
                                    ))
                            })
                        } else if (dataVessel.details.product_type === 'dangerous') {
                            values.push({
                                //container
                                containerNumber: inputContainer.attribute.container_number,
                                containerType: inputContainer.code,
                                containerTypeLabel: inputContainer.name,
                                containerSealNumber: inputContainer.attribute.seal_number,
                                containerSealNumberType: '',
                                containerSealNumberTypeLabel: 'Select one',
                                containerComments: inputContainer.attribute.container_comments,
                                //container cargo
                                cargo:
                                    inputContainer.cargos.map((inputCargo, indexCargo) => (
                                        {
                                            cargoDescription: inputCargo.description,
                                            cargoHsCode: inputCargo.hs_code,
                                            cargoHsCodeLabel: inputCargo.hs_code,
                                            cargoWeight: inputCargo.weight,
                                            cargoWeightType: inputCargo.weight_uom,
                                            cargoPackageCount: inputCargo.quantity,
                                            cargoPackageType: inputCargo.package_code,
                                            cargoPackageTypeLabel: inputCargo.package_code,
                                            cargoGrossVolume: inputCargo.gross_volume,
                                            cargoGrossVolumeType: inputCargo.gross_volume_uom,
                                            cargoPrimaryImoClass: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_imo,
                                            cargoPrimaryImoClassLabel: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_imo,
                                            cargoUndgNumber: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).undg_number,
                                            cargoPackingGroup: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).packing_group,
                                            cargoPackingGroupLabel: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).packing_group,
                                            cargoProperShippingName: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_proper_shipping,
                                            cargoEmergencyContactName: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_emergency_contact_name,
                                            cargoEmergencyContactNumber: JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_emergency_contact_number,
                                        }
                                    ))
                            })
                        }
                        setInputFieldsContainer(values);
                    })

                    setTimeout(() => {
                        dataVessel.containers.map((inputContainer, indexContainer) => {
                            setValue([
                                { [`containerNumber-${indexContainer}`]: inputContainer.attribute.container_number },
                                { [`containerSealNumber-${indexContainer}`]: inputContainer.attribute.seal_number },
                                { [`containerSealNumberType-${indexContainer}`]: '' },
                            ])
                        })
                    }, 1000);

                    //set country list value
                    setArrCountryList(params.dataCountryList)

                    setIsRenderingComponent(false)
                })
                .catch(error => {
                    console.log(error)
                    addToast(error, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                    //console.log(error.response.data.message)
                })
        }, 1000);
    }, [])

    useEffect(() => {
        if (errors.contractPartyAddress1) {
            if (errors.contractPartyAddress1.type === 'maxLength') {
                addToast('Contract Party Address Line 1 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.contractPartyAddress2) {
            if (errors.contractPartyAddress2.type === 'maxLength') {
                addToast('Contract Party Address Line 2 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.notifyPartyAddress1) {
            if (errors.notifyPartyAddress1.type === 'maxLength') {
                addToast('Notify Party Address Line 1 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.notifyPartyAddress2) {
            if (errors.notifyPartyAddress2.type === 'maxLength') {
                addToast('Notify Party Address Line 2 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.firstAdditionalPartyAddress1) {
            if (errors.firstAdditionalPartyAddress1.type === 'maxLength') {
                addToast('First Additional Party Address Line 1 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.firstAdditionalPartyAddress2) {
            if (errors.firstAdditionalPartyAddress2.type === 'maxLength') {
                addToast('First Additional Party Address Line 2 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.secondAdditionalPartyAddress1) {
            if (errors.secondAdditionalPartyAddress1.type === 'maxLength') {
                addToast('Second Additional Party Address Line 1 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.secondAdditionalPartyAddress2) {
            if (errors.secondAdditionalPartyAddress2.type === 'maxLength') {
                addToast('Second Additional Party Address Line 2 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.originOfGoodsPrint) {
            addToast('Origin of Goods - Print on B/L is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.placeOfReceiptPrint) {
            addToast('Place of Receipts - Print on B/L is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.portOfLoadPrint) {
            addToast('Port of Load - Print on B/L is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.portOfDischargePrint) {
            addToast('Port of Discharge - Print on B/L is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.destinationPrint) {
            addToast('Destination (Place of carrier delivery) - Print on B/L is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.partnerNotificationEmails) {
            addToast('Partner Notification Emails is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        //container
        inputFieldsContainer.map((inputContainer, indexContainer) => {
            if (errors[`containerNumber-${indexContainer}`]) {
                addToast(`Container Number is Required in Container ${indexContainer + 1}`, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }

            if (errors[`containerSealNumber-${indexContainer}`]) {
                addToast(`Container Seal Number is Required in Container ${indexContainer + 1}`, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }

            if (errors[`containerSealNumberType-${indexContainer}`]) {
                addToast(`Container Seal Number Type is Required in Container ${indexContainer + 1}`, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        })

        //payment details
        inputPaymentDetails.map((inputPayment, indexPayment) => {
            if (errors[`freightTerm-${indexPayment}`]) {
                addToast(`Freight Term is Required`, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }

            if (errors[`payer-${indexPayment}`]) {
                addToast(`Payer is Required`, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }

            if (errors[`chargeType-${indexPayment}`]) {
                addToast(`Charge Type is Required`, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }

            if (errors[`paymentLocation-${indexPayment}`]) {
                addToast(`Payment Location is Required`, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        })
    }, [errors])

    const [blTypeFinal, setBlTypeFinal] = useState('');

    const blType = (field, event) => {
        const valueOriginal = [...inputBlTypeOriginal]
        const valueSeaway = [...inputBlTypeSeaway]
        const valueHouseBill = [...inputBlTypeHouseBill]

        if (event.target.value === 'original') {
            setBlOriginalFreightedDisabled(false)
            setBlSeawayFreightedDisabled(true)
            setBlHouseBillFreightedDisabled(true)

            setBlOriginalUnfreightedDisabled(false)
            setBlSeawayUnfreightedDisabled(true)
            setBlHouseBillUnfreightedDisabled(true)

            //set value
            valueOriginal[0].type = 'original'
            setBlTypeFinal('original')

            //=====set others empty=====
            //seaway
            valueSeaway[0].type = ''
            valueSeaway[0].freighted = ''
            valueSeaway[0].unfreighted = ''

            //house_bill
            valueHouseBill[0].type = ''
            valueHouseBill[0].freighted = ''
            valueHouseBill[0].unfreighted = ''
        } else if (event.target.value === 'seaway') {
            setBlOriginalFreightedDisabled(true)
            setBlSeawayFreightedDisabled(false)
            setBlHouseBillFreightedDisabled(true)

            setBlOriginalUnfreightedDisabled(true)
            setBlSeawayUnfreightedDisabled(false)
            setBlHouseBillUnfreightedDisabled(true)

            //set value
            valueSeaway[0].type = 'seaway'
            setBlTypeFinal('seaway')

            //=====set others empty=====
            //original
            valueOriginal[0].type = ''
            valueOriginal[0].freighted = ''
            valueOriginal[0].unfreighted = ''

            //house_bill
            valueHouseBill[0].type = ''
            valueHouseBill[0].freighted = ''
            valueHouseBill[0].unfreighted = ''
        } else if (event.target.value === 'house_bill') {
            setBlOriginalFreightedDisabled(true)
            setBlSeawayFreightedDisabled(true)
            setBlHouseBillFreightedDisabled(false)

            setBlOriginalUnfreightedDisabled(true)
            setBlSeawayUnfreightedDisabled(true)
            setBlHouseBillUnfreightedDisabled(false)

            //set value
            valueHouseBill[0].type = 'house_bill'
            setBlTypeFinal('house_bill')

            //=====set others empty=====
            //original
            valueOriginal[0].type = ''
            valueOriginal[0].freighted = ''
            valueOriginal[0].unfreighted = ''

            //seaway
            valueSeaway[0].type = ''
            valueSeaway[0].freighted = ''
            valueSeaway[0].unfreighted = ''
        }

        setinputBlTypeOriginal(valueOriginal);
        setinputBlTypeSeaway(valueSeaway);
        setinputBlTypeHouseBill(valueHouseBill);
    }

    //update bl form
    const onChangeBlForm = (field, event) => {
        const valueOriginal = [...inputBlTypeOriginal]
        const valueSeaway = [...inputBlTypeSeaway]
        const valueHouseBill = [...inputBlTypeHouseBill]
        const valueFormSI = [...formSI]

        if (field === 'originalFreighted') {
            valueOriginal[0].freighted = event.target.value

            if (valueOriginal[0].freighted.length > 0) {
                setBlOriginalUnfreightedDisabled(true)
            } else {
                setBlOriginalUnfreightedDisabled(false)
            }
        }

        if (field === 'seawayFreighted') {
            valueSeaway[0].freighted = event.target.value

            if (valueSeaway[0].freighted.length > 0) {
                setBlSeawayUnfreightedDisabled(true)
            } else {
                setBlSeawayUnfreightedDisabled(false)
            }
        }

        if (field === 'houseBillFreighted') {
            valueHouseBill[0].freighted = event.target.value

            if (valueHouseBill[0].freighted.length > 0) {
                setBlHouseBillUnfreightedDisabled(true)
            } else {
                setBlHouseBillUnfreightedDisabled(false)
            }
        }

        if (field === 'originalUnfreighted') {
            valueOriginal[0].unfreighted = event.target.value

            if (valueOriginal[0].unfreighted.length > 0) {
                setBlOriginalFreightedDisabled(true)
            } else {
                setBlOriginalFreightedDisabled(false)
            }
        }

        if (field === 'seawayUnfreighted') {
            valueSeaway[0].unfreighted = event.target.value

            if (valueSeaway[0].unfreighted.length > 0) {
                setBlSeawayFreightedDisabled(true)
            } else {
                setBlSeawayFreightedDisabled(false)
            }
        }

        if (field === 'houseBillUnfreighted') {
            valueHouseBill[0].unfreighted = event.target.value

            if (valueHouseBill[0].unfreighted.length > 0) {
                setBlHouseBillFreightedDisabled(true)
            } else {
                setBlHouseBillFreightedDisabled(false)
            }
        }

        if (field === 'customerComments') {
            valueFormSI[0].customerComments = event.target.value
        }

        if (field === 'partnerNotificationEmails') {
            valueFormSI[0].partnerNotificationEmails = event.target.value

            setValue([
                { partnerNotificationEmails: formSI[0].partnerNotificationEmails },
            ])

            clearError('partnerNotificationEmails')
        }

        if (field === 'contractPartyName') {
            valueFormSI[0].contractPartyName = event.target.value
        }

        if (field === 'contractPartyAddress1') {
            valueFormSI[0].contractPartyAddress1 = event.target.value

            setValue([
                { contractPartyAddress1: formSI[0].contractPartyAddress1 },
            ])

            clearError('contractPartyAddress1')
        }

        if (field === 'contractPartyCity') {
            valueFormSI[0].contractPartyCity = event.target.value
        }

        if (field === 'contractPartyAddress2') {
            valueFormSI[0].contractPartyAddress2 = event.target.value

            setValue([
                { contractPartyAddress2: formSI[0].contractPartyAddress2 },
            ])

            clearError('contractPartyAddress2')
        }

        if (field === 'contractPartyPostalCode') {
            valueFormSI[0].contractPartyPostalCode = event.target.value
        }

        if (field === 'contractPartyCountry') {
            valueFormSI[0].contractPartyCountry = event.value
            valueFormSI[0].contractPartyCountryLabel = event.label
        }

        if (field === 'notifyPartyName') {
            valueFormSI[0].notifyPartyName = event.target.value
        }

        if (field === 'notifyPartyAddress1') {
            valueFormSI[0].notifyPartyAddress1 = event.target.value

            setValue([
                { notifyPartyAddress1: formSI[0].notifyPartyAddress1 },
            ])

            clearError('notifyPartyAddress1')
        }

        if (field === 'notifyPartyCity') {
            valueFormSI[0].notifyPartyCity = event.target.value
        }

        if (field === 'notifyPartyAddress2') {
            valueFormSI[0].notifyPartyAddress2 = event.target.value

            setValue([
                { notifyPartyAddress2: formSI[0].notifyPartyAddress2 },
            ])

            clearError('notifyPartyAddress2')
        }

        if (field === 'notifyPartyPostalCode') {
            valueFormSI[0].notifyPartyPostalCode = event.target.value
        }

        if (field === 'notifyPartyCountry') {
            valueFormSI[0].notifyPartyCountry = event.value
            valueFormSI[0].notifyPartyCountryLabel = event.label
        }

        if (field === 'firstAdditionalPartyName') {
            valueFormSI[0].firstAdditionalPartyName = event.target.value
        }

        if (field === 'firstAdditionalPartyAddress1') {
            valueFormSI[0].firstAdditionalPartyAddress1 = event.target.value

            setValue([
                { firstAdditionalPartyAddress1: formSI[0].firstAdditionalPartyAddress1 },
            ])

            clearError('firstAdditionalPartyAddress1')
        }

        if (field === 'firstAdditionalPartyCity') {
            valueFormSI[0].firstAdditionalPartyCity = event.target.value
        }

        if (field === 'firstAdditionalPartyAddress2') {
            valueFormSI[0].firstAdditionalPartyAddress2 = event.target.value

            setValue([
                { firstAdditionalPartyAddress2: formSI[0].firstAdditionalPartyAddress2 },
            ])

            clearError('firstAdditionalPartyAddress2')
        }

        if (field === 'firstAdditionalPartyPostalCode') {
            valueFormSI[0].firstAdditionalPartyPostalCode = event.target.value
        }

        if (field === 'firstAdditionalPartyCountry') {
            valueFormSI[0].firstAdditionalPartyCountry = event.value
            valueFormSI[0].firstAdditionalPartyCountryLabel = event.label
        }

        if (field === 'secondAdditionalPartyName') {
            valueFormSI[0].secondAdditionalPartyName = event.target.value
        }

        if (field === 'secondAdditionalPartyAddress1') {
            valueFormSI[0].secondAdditionalPartyAddress1 = event.target.value

            setValue([
                { secondAdditionalPartyAddress1: formSI[0].secondAdditionalPartyAddress1 },
            ])

            clearError('secondAdditionalPartyAddress1')
        }

        if (field === 'secondAdditionalPartyCity') {
            valueFormSI[0].secondAdditionalPartyCity = event.target.value
        }

        if (field === 'secondAdditionalPartyAddress2') {
            valueFormSI[0].secondAdditionalPartyAddress2 = event.target.value

            setValue([
                { secondAdditionalPartyAddress2: formSI[0].secondAdditionalPartyAddress2 },
            ])

            clearError('secondAdditionalPartyAddress2')
        }

        if (field === 'secondAdditionalPartyPostalCode') {
            valueFormSI[0].secondAdditionalPartyPostalCode = event.target.value
        }

        if (field === 'secondAdditionalPartyCountry') {
            valueFormSI[0].secondAdditionalPartyCountry = event.value
            valueFormSI[0].secondAdditionalPartyCountryLabel = event.label
        }

        if (field === 'originOfGoodsPrint') {
            valueFormSI[0].originOfGoodsPrint = event.target.value

            setValue([
                { originOfGoodsPrint: formSI[0].originOfGoodsPrint },
            ])

            clearError('originOfGoodsPrint')
        }

        if (field === 'placeOfReceiptPrint') {
            valueFormSI[0].placeOfReceiptPrint = event.target.value

            setValue([
                { placeOfReceiptPrint: formSI[0].placeOfReceiptPrint },
            ])

            clearError('placeOfReceiptPrint')
        }

        if (field === 'portOfLoadPrint') {
            valueFormSI[0].portOfLoadPrint = event.target.value

            setValue([
                { portOfLoadPrint: formSI[0].portOfLoadPrint },
            ])

            clearError('portOfLoadPrint')
        }

        if (field === 'portOfDischargePrint') {
            valueFormSI[0].portOfDischargePrint = event.target.value

            setValue([
                { portOfDischargePrint: formSI[0].portOfDischargePrint },
            ])

            clearError('portOfDischargePrint')
        }

        if (field === 'destinationPrint') {
            valueFormSI[0].destinationPrint = event.target.value

            setValue([
                { destinationPrint: formSI[0].destinationPrint },
            ])

            clearError('destinationPrint')
        }

        if (field === 'shipperTaxId') {
            valueFormSI[0].shipperTaxId = event.target.value
        }

        if (field === 'consigneeTaxId') {
            valueFormSI[0].consigneeTaxId = event.target.value
        }

        setinputBlTypeOriginal(valueOriginal);
        setinputBlTypeSeaway(valueSeaway);
        setinputBlTypeHouseBill(valueHouseBill);
        setFormSI(valueFormSI)
    }

    const onChangeContainer = (field, index, event) => {
        const values = [...inputFieldsContainer];

        if (field === "containerNumber") {
            values[index].containerNumber = event.target.value

            setValue([
                { [`containerNumber-${index}`]: inputFieldsContainer[index].containerNumber },
            ])

            clearError([`containerNumber-${index}`])
        }

        if (field === "containerSealNumber") {
            values[index].containerSealNumber = event.target.value

            setValue([
                { [`containerSealNumber-${index}`]: inputFieldsContainer[index].containerSealNumber },
            ])

            clearError([`containerSealNumber-${index}`])
        }

        if (field === "containerSealNumberType") {
            values[index].containerSealNumberType = event.value
            values[index].containerSealNumberTypeLabel = event.label

            setValue([
                { [`containerSealNumberType-${index}`]: inputFieldsContainer[index].containerSealNumberType },
            ])

            clearError([`containerSealNumberType-${index}`])
        }

        setInputFieldsContainer(values);
    }

    const onChangePaymentDetail = (field, index, event) => {
        const values = [...inputPaymentDetails];

        if (field === "freightTerm") {
            values[index].freightTerm = event.value
            values[index].freightTermLabel = event.label

            setValue([
                { [`freightTerm-${index}`]: inputPaymentDetails[index].freightTerm },
            ])

            clearError([`freightTerm-${index}`])
        }

        if (field === "payer") {
            values[index].payer = event.value
            values[index].payerLabel = event.label

            setValue([
                { [`payer-${index}`]: inputPaymentDetails[index].payer },
            ])

            clearError([`payer-${index}`])
        }

        if (field === "chargeType") {
            values[index].chargeType = event.value
            values[index].chargeTypeLabel = event.label

            setValue([
                { [`chargeType-${index}`]: inputPaymentDetails[index].chargeType },
            ])

            clearError([`chargeType-${index}`])
        }

        if (field === "paymentLocation") {
            values[index].paymentLocation = event.value
            values[index].paymentLocationLabel = event.label

            setValue([
                { [`paymentLocation-${index}`]: inputPaymentDetails[index].paymentLocation },
            ])

            clearError([`paymentLocation-${index}`])
        }

        setInputPaymentDetails(values)
    }

    //booking office
    const fetchDataBookingOffice = (inputValue, callback) => {
        if (!inputValue) {
            callback([]);
        } else {
            setTimeout(() => {
                axios
                    .post(ApiClickargo.CLICKARGO_UNLOCS, {
                        data: inputValue
                    })
                    .then((data) => {
                        const tempArray = [];
                        data.data.data.forEach((element) => {
                            tempArray.push({ label: `${element.name}` + ' (' + `${element.code}` + ')', value: element.code });
                        });
                        callback(tempArray);
                    })
                    .catch((error) => {
                        console.log(error, "catch the hoop")
                    });
            });
        }
    }

    let optionSealNumberType = [
        { value: '', label: 'Select One' },
        { value: 'carrier', label: 'Carrier' },
        { value: 'shipper', label: 'Shipper' }
    ]

    let optionPaymentDetailsFreightTerm = [
        { value: '', label: 'Select One' },
        { value: 'Prepaid', label: 'Pre-Paid' },
        { value: 'Collect', label: 'Collect' },
        { value: 'ThirdParty', label: 'Payable Elsewhere' },
    ]

    let optionPaymentDetailsPayer = [
        { value: '', label: 'Select One' },
        { value: 'Shipper', label: 'Shipper' },
        { value: 'Consignee', label: 'Consignee' },
        { value: 'Forwarder', label: 'Forwarder' },
        { value: 'NotifyParty', label: 'Notify Party' },
        { value: 'NotifyParty1', label: 'Additional Notify Party 1' },
        { value: 'NotifyParty2', label: 'Additional Notify Party 2' },
        { value: 'ContractParty', label: 'Contract Party' },
    ]

    let optionPaymentDetailsChargeType = [
        { value: '', label: 'Select One' },
        { value: 'BasicFreight', label: 'Basic Freight' },
    ]

    let optionCountryList = arrCountryList.map(obj => {
        return { value: obj.iso2, label: obj.name }
    })

    const onSubmit = () => {
        console.log('run submit handler for complete SI')

        setIsLoading(true)

        //get token by check api npwp
        const headersNpwp = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        axios
            .post(ApiClickargo.CLICKARGO_CHECK_NPWP, {
                tax_number: params.npwp,
                email: params.email,
                // tax_number: '541267799887999',
                // email: 'kusnle5@yopmail.com'
            }, {
                headers: headersNpwp
            })
            .then(responseNPWP => {
                if (responseNPWP.data.success === true) {
                    console.log('sukses check npwp')

                    const headersCompleteSI = {
                        'Content-Type': 'application/json',
                        'Accept-Language': 'application/json',
                        'Key': responseNPWP.data.data.key,
                        'Secret': responseNPWP.data.data.secret,
                    }

                    setTimeout(() => {

                        if (blTypeFinal === 'original') {
                            axios
                                .post(ApiClickargo.CLICKARGO_COMPLETE_SI, {
                                    order_uuid: formSI[0].orderUuid,
                                    booking_request_uuid: formSI[0].bookingRequestUuid,
                                    si: {
                                        bl_origin: formSI[0].originOfGoodsPrint,
                                        bl_receipts: formSI[0].placeOfReceiptPrint,
                                        bl_pol: formSI[0].portOfLoadPrint,
                                        bl_pod: formSI[0].portOfDischargePrint,
                                        bl_destination: formSI[0].destinationPrint,
                                        freight_term: inputPaymentDetails[0].freightTerm,
                                        payer: inputPaymentDetails[0].payer,
                                        payment_location: inputPaymentDetails[0].paymentLocation,
                                        charge_type: inputPaymentDetails[0].chargeType,
                                        partner_notif_email: formSI[0].partnerNotificationEmails,
                                        //contract party
                                        contract_party_name: formSI[0].contractPartyName,
                                        contract_party_country_code: formSI[0].contractPartyCountry,
                                        contract_party_country_name: formSI[0].contractPartyCountryLabel,
                                        contract_party_address_1: formSI[0].contractPartyAddress1,
                                        contract_party_address_2: formSI[0].contractPartyAddress2,
                                        contract_party_city: formSI[0].contractPartyCity,
                                        contract_party_postal_code: formSI[0].contractPartyPostalCode,
                                        //notify party
                                        notify_party_name: formSI[0].notifyPartyName,
                                        notify_party_country_code: formSI[0].notifyPartyCountry,
                                        notify_party_country_name: formSI[0].notifyPartyCountryLabel,
                                        notify_party_address_1: formSI[0].notifyPartyAddress1,
                                        notify_party_address_2: formSI[0].notifyPartyAddress2,
                                        notify_party_city: formSI[0].notifyPartyCity,
                                        notify_party_postal_code: formSI[0].notifyPartyPostalCode,
                                        //first additional party
                                        first_additional_party: formSI[0].firstAdditionalPartyName,
                                        first_additional_party_country_code: formSI[0].firstAdditionalPartyCountry,
                                        first_additional_party_country_name: formSI[0].firstAdditionalPartyCountryLabel,
                                        first_additional_party_address_1: formSI[0].firstAdditionalPartyAddress1,
                                        first_additional_party_address_2: formSI[0].firstAdditionalPartyAddress2,
                                        first_additional_party_city: formSI[0].firstAdditionalPartyCity,
                                        first_additional_party_postal_code: formSI[0].firstAdditionalPartyPostalCode,
                                        //second additional party
                                        second_additional_party: formSI[0].secondAdditionalPartyName,
                                        second_additional_party_country_code: formSI[0].secondAdditionalPartyCountry,
                                        second_additional_party_country_name: formSI[0].secondAdditionalPartyCountryLabel,
                                        second_additional_party_address_1: formSI[0].secondAdditionalPartyAddress1,
                                        second_additional_party_address_2: formSI[0].secondAdditionalPartyAddress2,
                                        second_additional_party_city: formSI[0].secondAdditionalPartyCity,
                                        second_additional_party_postal_code: formSI[0].secondAdditionalPartyPostalCode,
                                        //customs compliance
                                        shipper_govt_id: formSI[0].shipperTaxId,
                                        consignee_govt_id: formSI[0].consigneeTaxId,
                                        //remarks
                                        customer_comments: formSI[0].customerComments,
                                        bl_type: 'original',
                                        freighted_original: inputBlTypeOriginal[0].freighted,
                                        unfreighted_original: inputBlTypeOriginal[0].unfreighted,
                                    },
                                    container:
                                        inputFieldsContainer.map((inputContainer, indexContainer) => (
                                            {
                                                container_number: inputContainer.containerNumber,
                                                seal_number: inputContainer.containerSealNumber,
                                                seal_number_type: inputContainer.containerSealNumberType,
                                            }
                                        ))
                                }, {
                                    headers: headersCompleteSI
                                })
                                .then(responseCompleteSI => {
                                    console.log('Success Submit SI')
                                    addToast('Success Submit SI', {
                                        appearance: 'success',
                                        autoDismiss: true,
                                    })
                                    params.setCount(params.count + 1)
                                    setIsLoading(false)
                                    setIsBookingNumber(false)
                                })
                                .catch(error => {
                                    if (error.response) {
                                        console.log(error.response.data.message)
                                        addToast(error.response.data.message, {
                                            appearance: 'error',
                                            autoDismiss: true,
                                        })
                                        console.log('Failed Submit SI')
                                        setIsLoading(false)
                                    }
                                })
                        } else if (blTypeFinal === 'seaway') {
                            axios
                                .post(ApiClickargo.CLICKARGO_COMPLETE_SI, {
                                    order_uuid: formSI[0].orderUuid,
                                    booking_request_uuid: formSI[0].bookingRequestUuid,
                                    si: {
                                        bl_origin: formSI[0].originOfGoodsPrint,
                                        bl_receipts: formSI[0].placeOfReceiptPrint,
                                        bl_pol: formSI[0].portOfLoadPrint,
                                        bl_pod: formSI[0].portOfDischargePrint,
                                        bl_destination: formSI[0].destinationPrint,
                                        freight_term: inputPaymentDetails[0].freightTerm,
                                        payer: inputPaymentDetails[0].payer,
                                        payment_location: inputPaymentDetails[0].paymentLocation,
                                        charge_type: inputPaymentDetails[0].chargeType,
                                        partner_notif_email: formSI[0].partnerNotificationEmails,
                                        //contract party
                                        contract_party_name: formSI[0].contractPartyName,
                                        contract_party_country_code: formSI[0].contractPartyCountry,
                                        contract_party_country_name: formSI[0].contractPartyCountryLabel,
                                        contract_party_address_1: formSI[0].contractPartyAddress1,
                                        contract_party_address_2: formSI[0].contractPartyAddress2,
                                        contract_party_city: formSI[0].contractPartyCity,
                                        contract_party_postal_code: formSI[0].contractPartyPostalCode,
                                        //notify party
                                        notify_party_name: formSI[0].notifyPartyName,
                                        notify_party_country_code: formSI[0].notifyPartyCountry,
                                        notify_party_country_name: formSI[0].notifyPartyCountryLabel,
                                        notify_party_address_1: formSI[0].notifyPartyAddress1,
                                        notify_party_address_2: formSI[0].notifyPartyAddress2,
                                        notify_party_city: formSI[0].notifyPartyCity,
                                        notify_party_postal_code: formSI[0].notifyPartyPostalCode,
                                        //first additional party
                                        first_additional_party: formSI[0].firstAdditionalPartyName,
                                        first_additional_party_country_code: formSI[0].firstAdditionalPartyCountry,
                                        first_additional_party_country_name: formSI[0].firstAdditionalPartyCountryLabel,
                                        first_additional_party_address_1: formSI[0].firstAdditionalPartyAddress1,
                                        first_additional_party_address_2: formSI[0].firstAdditionalPartyAddress2,
                                        first_additional_party_city: formSI[0].firstAdditionalPartyCity,
                                        first_additional_party_postal_code: formSI[0].firstAdditionalPartyPostalCode,
                                        //second additional party
                                        second_additional_party: formSI[0].secondAdditionalPartyName,
                                        second_additional_party_country_code: formSI[0].secondAdditionalPartyCountry,
                                        second_additional_party_country_name: formSI[0].secondAdditionalPartyCountryLabel,
                                        second_additional_party_address_1: formSI[0].secondAdditionalPartyAddress1,
                                        second_additional_party_address_2: formSI[0].secondAdditionalPartyAddress2,
                                        second_additional_party_city: formSI[0].secondAdditionalPartyCity,
                                        second_additional_party_postal_code: formSI[0].secondAdditionalPartyPostalCode,
                                        //customs compliance
                                        shipper_govt_id: formSI[0].shipperTaxId,
                                        consignee_govt_id: formSI[0].consigneeTaxId,
                                        //remarks
                                        customer_comments: formSI[0].customerComments,
                                        bl_type: 'seaway',
                                        freighted_seaway: inputBlTypeSeaway[0].freighted,
                                        unfreighted_seaway: inputBlTypeSeaway[0].unfreighted,
                                    },
                                    container:
                                        inputFieldsContainer.map((inputContainer, indexContainer) => (
                                            {
                                                container_number: inputContainer.containerNumber,
                                                seal_number: inputContainer.containerSealNumber,
                                                seal_number_type: inputContainer.containerSealNumberType,
                                            }
                                        ))
                                }, {
                                    headers: headersCompleteSI
                                })
                                .then(responseCompleteSI => {
                                    console.log('Success Submit SI')
                                    addToast('Success Submit SI', {
                                        appearance: 'success',
                                        autoDismiss: true,
                                    })
                                    params.setCount(params.count + 1)
                                    setIsLoading(false)
                                    setIsBookingNumber(false)
                                })
                                .catch(error => {
                                    if (error.response) {
                                        console.log(error.response.data.message)
                                        addToast(error.response.data.message, {
                                            appearance: 'error',
                                            autoDismiss: true,
                                        })
                                        console.log('Failed Submit SI')
                                        setIsLoading(false)
                                    }
                                })
                        } else if (blTypeFinal === 'house_bill') {
                            axios
                                .post(ApiClickargo.CLICKARGO_COMPLETE_SI, {
                                    order_uuid: formSI[0].orderUuid,
                                    booking_request_uuid: formSI[0].bookingRequestUuid,
                                    si: {
                                        bl_origin: formSI[0].originOfGoodsPrint,
                                        bl_receipts: formSI[0].placeOfReceiptPrint,
                                        bl_pol: formSI[0].portOfLoadPrint,
                                        bl_pod: formSI[0].portOfDischargePrint,
                                        bl_destination: formSI[0].destinationPrint,
                                        freight_term: inputPaymentDetails[0].freightTerm,
                                        payer: inputPaymentDetails[0].payer,
                                        payment_location: inputPaymentDetails[0].paymentLocation,
                                        charge_type: inputPaymentDetails[0].chargeType,
                                        partner_notif_email: formSI[0].partnerNotificationEmails,
                                        //contract party
                                        contract_party_name: formSI[0].contractPartyName,
                                        contract_party_country_code: formSI[0].contractPartyCountry,
                                        contract_party_country_name: formSI[0].contractPartyCountryLabel,
                                        contract_party_address_1: formSI[0].contractPartyAddress1,
                                        contract_party_address_2: formSI[0].contractPartyAddress2,
                                        contract_party_city: formSI[0].contractPartyCity,
                                        contract_party_postal_code: formSI[0].contractPartyPostalCode,
                                        //notify party
                                        notify_party_name: formSI[0].notifyPartyName,
                                        notify_party_country_code: formSI[0].notifyPartyCountry,
                                        notify_party_country_name: formSI[0].notifyPartyCountryLabel,
                                        notify_party_address_1: formSI[0].notifyPartyAddress1,
                                        notify_party_address_2: formSI[0].notifyPartyAddress2,
                                        notify_party_city: formSI[0].notifyPartyCity,
                                        notify_party_postal_code: formSI[0].notifyPartyPostalCode,
                                        //first additional party
                                        first_additional_party: formSI[0].firstAdditionalPartyName,
                                        first_additional_party_country_code: formSI[0].firstAdditionalPartyCountry,
                                        first_additional_party_country_name: formSI[0].firstAdditionalPartyCountryLabel,
                                        first_additional_party_address_1: formSI[0].firstAdditionalPartyAddress1,
                                        first_additional_party_address_2: formSI[0].firstAdditionalPartyAddress2,
                                        first_additional_party_city: formSI[0].firstAdditionalPartyCity,
                                        first_additional_party_postal_code: formSI[0].firstAdditionalPartyPostalCode,
                                        //second additional party
                                        second_additional_party: formSI[0].secondAdditionalPartyName,
                                        second_additional_party_country_code: formSI[0].secondAdditionalPartyCountry,
                                        second_additional_party_country_name: formSI[0].secondAdditionalPartyCountryLabel,
                                        second_additional_party_address_1: formSI[0].secondAdditionalPartyAddress1,
                                        second_additional_party_address_2: formSI[0].secondAdditionalPartyAddress2,
                                        second_additional_party_city: formSI[0].secondAdditionalPartyCity,
                                        second_additional_party_postal_code: formSI[0].secondAdditionalPartyPostalCode,
                                        //customs compliance
                                        shipper_govt_id: formSI[0].shipperTaxId,
                                        consignee_govt_id: formSI[0].consigneeTaxId,
                                        //remarks
                                        customer_comments: formSI[0].customerComments,
                                        bl_type: 'house_bill',
                                        freighted_house_bill: inputBlTypeHouseBill[0].freighted,
                                        unfreighted_house_bill: inputBlTypeHouseBill[0].unfreighted,
                                    },
                                    container:
                                        inputFieldsContainer.map((inputContainer, indexContainer) => (
                                            {
                                                container_number: inputContainer.containerNumber,
                                                seal_number: inputContainer.containerSealNumber,
                                                seal_number_type: inputContainer.containerSealNumberType,
                                            }
                                        ))
                                }, {
                                    headers: headersCompleteSI
                                })
                                .then(responseCompleteSI => {
                                    console.log('Success Submit SI')
                                    addToast('Success Submit SI', {
                                        appearance: 'success',
                                        autoDismiss: true,
                                    })
                                    params.setCount(params.count + 1)
                                    setIsLoading(false)
                                    setIsBookingNumber(false)
                                })
                                .catch(error => {
                                    if (error.response) {
                                        console.log(error.response.data.message)
                                        addToast(error.response.data.message, {
                                            appearance: 'error',
                                            autoDismiss: true,
                                        })
                                        console.log('Failed Submit SI')
                                        setIsLoading(false)
                                    }
                                })
                        } else {
                            axios
                                .post(ApiClickargo.CLICKARGO_COMPLETE_SI, {
                                    order_uuid: formSI[0].orderUuid,
                                    booking_request_uuid: formSI[0].bookingRequestUuid,
                                    si: {
                                        bl_origin: formSI[0].originOfGoodsPrint,
                                        bl_receipts: formSI[0].placeOfReceiptPrint,
                                        bl_pol: formSI[0].portOfLoadPrint,
                                        bl_pod: formSI[0].portOfDischargePrint,
                                        bl_destination: formSI[0].destinationPrint,
                                        freight_term: inputPaymentDetails[0].freightTerm,
                                        payer: inputPaymentDetails[0].payer,
                                        payment_location: inputPaymentDetails[0].paymentLocation,
                                        charge_type: inputPaymentDetails[0].chargeType,
                                        partner_notif_email: formSI[0].partnerNotificationEmails,
                                        //contract party
                                        contract_party_name: formSI[0].contractPartyName,
                                        contract_party_country_code: formSI[0].contractPartyCountry,
                                        contract_party_country_name: formSI[0].contractPartyCountryLabel,
                                        contract_party_address_1: formSI[0].contractPartyAddress1,
                                        contract_party_address_2: formSI[0].contractPartyAddress2,
                                        contract_party_city: formSI[0].contractPartyCity,
                                        contract_party_postal_code: formSI[0].contractPartyPostalCode,
                                        //notify party
                                        notify_party_name: formSI[0].notifyPartyName,
                                        notify_party_country_code: formSI[0].notifyPartyCountry,
                                        notify_party_country_name: formSI[0].notifyPartyCountryLabel,
                                        notify_party_address_1: formSI[0].notifyPartyAddress1,
                                        notify_party_address_2: formSI[0].notifyPartyAddress2,
                                        notify_party_city: formSI[0].notifyPartyCity,
                                        notify_party_postal_code: formSI[0].notifyPartyPostalCode,
                                        //first additional party
                                        first_additional_party: formSI[0].firstAdditionalPartyName,
                                        first_additional_party_country_code: formSI[0].firstAdditionalPartyCountry,
                                        first_additional_party_country_name: formSI[0].firstAdditionalPartyCountryLabel,
                                        first_additional_party_address_1: formSI[0].firstAdditionalPartyAddress1,
                                        first_additional_party_address_2: formSI[0].firstAdditionalPartyAddress2,
                                        first_additional_party_city: formSI[0].firstAdditionalPartyCity,
                                        first_additional_party_postal_code: formSI[0].firstAdditionalPartyPostalCode,
                                        //second additional party
                                        second_additional_party: formSI[0].secondAdditionalPartyName,
                                        second_additional_party_country_code: formSI[0].secondAdditionalPartyCountry,
                                        second_additional_party_country_name: formSI[0].secondAdditionalPartyCountryLabel,
                                        second_additional_party_address_1: formSI[0].secondAdditionalPartyAddress1,
                                        second_additional_party_address_2: formSI[0].secondAdditionalPartyAddress2,
                                        second_additional_party_city: formSI[0].secondAdditionalPartyCity,
                                        second_additional_party_postal_code: formSI[0].secondAdditionalPartyPostalCode,
                                        //customs compliance
                                        shipper_govt_id: formSI[0].shipperTaxId,
                                        consignee_govt_id: formSI[0].consigneeTaxId,
                                        //remarks
                                        customer_comments: formSI[0].customerComments,
                                    },
                                    container:
                                        inputFieldsContainer.map((inputContainer, indexContainer) => (
                                            {
                                                container_number: inputContainer.containerNumber,
                                                seal_number: inputContainer.containerSealNumber,
                                                seal_number_type: inputContainer.containerSealNumberType,
                                            }
                                        ))
                                }, {
                                    headers: headersCompleteSI
                                })
                                .then(responseCompleteSI => {
                                    console.log('Success Submit SI')
                                    addToast('Success Submit SI', {
                                        appearance: 'success',
                                        autoDismiss: true,
                                    })
                                    params.setCount(params.count + 1)
                                    setIsLoading(false)
                                    setIsBookingNumber(false)
                                })
                                .catch(error => {
                                    if (error.response) {
                                        console.log(error.response.data.message)
                                        addToast(error.response.data.message, {
                                            appearance: 'error',
                                            autoDismiss: true,
                                        })
                                        console.log('Failed Submit SI')
                                        setIsLoading(false)
                                    }
                                })
                        }
                    }, 1000);
                }
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response.data.message)
                }
            })

        // //uji coba
        // setTimeout(() => {
        //     setIsLoading(false)
        // }, 5000);
    }

    return (
        isRenderingComponent ? (
            <Fragment>
                <h3 className={dashforgeCss.txBlack + ` ` + dashforgeCss.textCenter}>Please Wait...</h3>
            </Fragment>
        ) : (
                <Fragment>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="row">
                            <div className="col-md-12">

                                <Alert variant="info">
                                    <p>
                                        Please complete all the data below.
                                    </p>
                                </Alert>


                                {formSI.map((inputSI, indexSI) => (
                                    <Fragment key={`${inputSI}~${indexSI}`}>
                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>General Details</legend>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>SHIPPER</legend>

                                                <div className="row">

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Shipper Name</label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.shipperName} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Shipper's Tax Number</label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.shipperTaxNumber} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Shipper Address Line 1<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.shipperAddress1} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Shipper City<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.shipperCity} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Shipper Address Line 2<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.shipperAddress2} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Shipper Postal Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.shipperPostalCode} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Shipper Country<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.shipperCountry} />
                                                        </div>
                                                    </div>

                                                </div>
                                            </fieldset>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>Forwarder</legend>

                                                <div className="row">

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Forwarder<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.forwarderName} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6"></div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Forwarder Address Line 1 (Printed on B/L)<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.forwarderAddress1} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Forwarder City<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.forwarderCity} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Forwarder Address Line 2 (Printed on B/L)<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.forwarderAddress2} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Forwarder Postal Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.forwarderPostalCode} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Forwarder Country<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.forwarderCountry} />
                                                        </div>
                                                    </div>

                                                </div>

                                            </fieldset>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>Carrier</legend>

                                                <div className="row">

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Carrier<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.carrierName} />
                                                        </div>
                                                    </div>

                                                </div>
                                            </fieldset>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>Consignee</legend>

                                                <div className="row">

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Consignee Name<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.consigneeName} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6"></div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Consignee Address Line 1<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.consigneeAddress1} />

                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Consignee City<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.consigneeCity} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Consignee Address Line 2<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.consigneeAddress2} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Consignee Postal Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.consigneePostalCode} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Consignee Country<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputSI.consigneeCountry} />
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>

                                        </fieldset>

                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Additional Party (optional)</legend>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>Contract Party</legend>

                                                <div className="row">

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Contract Party</label>
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.contractPartyName}
                                                                onChange={event => onChangeBlForm('contractPartyName', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6"></div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                            <input
                                                                type="hidden"
                                                                name={`contractPartyAddress1`}
                                                                ref={register({
                                                                    maxLength: {
                                                                        value: 35,
                                                                        message: 'Maximum input length is 35'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.contractPartyAddress1}
                                                                onChange={event => onChangeBlForm('contractPartyAddress1', event)} />
                                                            {errors[`contractPartyAddress1`] && errors[`contractPartyAddress1`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>City</label>
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.contractPartyCity}
                                                                onChange={event => onChangeBlForm('contractPartyCity', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                            <input
                                                                type="hidden"
                                                                name={`contractPartyAddress2`}
                                                                ref={register({
                                                                    maxLength: {
                                                                        value: 35,
                                                                        message: 'Maximum input length is 35'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.contractPartyAddress2}
                                                                onChange={event => onChangeBlForm('contractPartyAddress2', event)} />
                                                            {errors[`contractPartyAddress2`] && errors[`contractPartyAddress2`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.contractPartyPostalCode}
                                                                onChange={event => onChangeBlForm('contractPartyPostalCode', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Country</label>
                                                            <Select
                                                                isDisabled={!isBookingNumber}
                                                                value={{ label: inputSI.contractPartyCountryLabel, value: inputSI.contractPartyCountry }}
                                                                options={optionCountryList}
                                                                onChange={event => onChangeBlForm('contractPartyCountry', event)} />
                                                        </div>
                                                    </div>

                                                </div>
                                            </fieldset>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>Notify Party</legend>

                                                <div className="row">

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Notify Party</label>
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.notifyPartyName}
                                                                onChange={event => onChangeBlForm('notifyPartyName', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6"></div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                            <input
                                                                type="hidden"
                                                                name={`notifyPartyAddress1`}
                                                                ref={register({
                                                                    maxLength: {
                                                                        value: 35,
                                                                        message: 'Maximum input length is 35'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.notifyPartyAddress1}
                                                                onChange={event => onChangeBlForm('notifyPartyAddress1', event)} />
                                                            {errors[`notifyPartyAddress1`] && errors[`notifyPartyAddress1`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>City</label>
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.notifyPartyCity}
                                                                onChange={event => onChangeBlForm('notifyPartyCity', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                            <input
                                                                type="hidden"
                                                                name={`notifyPartyAddress2`}
                                                                ref={register({
                                                                    maxLength: {
                                                                        value: 35,
                                                                        message: 'Maximum input length is 35'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.notifyPartyAddress2}
                                                                onChange={event => onChangeBlForm('notifyPartyAddress2', event)} />
                                                            {errors[`notifyPartyAddress2`] && errors[`notifyPartyAddress2`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.notifyPartyPostalCode}
                                                                onChange={event => onChangeBlForm('notifyPartyPostalCode', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Country</label>
                                                            <Select
                                                                isDisabled={!isBookingNumber}
                                                                value={{ label: inputSI.notifyPartyCountryLabel, value: inputSI.notifyPartyCountry }}
                                                                options={optionCountryList}
                                                                onChange={event => onChangeBlForm('notifyPartyCountry', event)} />
                                                        </div>
                                                    </div>

                                                </div>
                                            </fieldset>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>First Additional Notify Party</legend>

                                                <div className="row">

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>First Additional Notify Party</label>
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.firstAdditionalPartyName}
                                                                onChange={event => onChangeBlForm('firstAdditionalPartyName', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6"></div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                            <input
                                                                type="hidden"
                                                                name={`firstAdditionalPartyAddress1`}
                                                                ref={register({
                                                                    maxLength: {
                                                                        value: 35,
                                                                        message: 'Maximum input length is 35'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.firstAdditionalPartyAddress1}
                                                                onChange={event => onChangeBlForm('firstAdditionalPartyAddress1', event)} />
                                                            {errors[`firstAdditionalPartyAddress1`] && errors[`firstAdditionalPartyAddress1`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>City</label>
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.firstAdditionalPartyCity}
                                                                onChange={event => onChangeBlForm('firstAdditionalPartyCity', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                            <input
                                                                type="hidden"
                                                                name={`firstAdditionalPartyAddress2`}
                                                                ref={register({
                                                                    maxLength: {
                                                                        value: 35,
                                                                        message: 'Maximum input length is 35'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.firstAdditionalPartyAddress2}
                                                                onChange={event => onChangeBlForm('firstAdditionalPartyAddress2', event)} />
                                                            {errors[`firstAdditionalPartyAddress2`] && errors[`firstAdditionalPartyAddress2`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.firstAdditionalPartyPostalCode}
                                                                onChange={event => onChangeBlForm('firstAdditionalPartyPostalCode', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Country</label>
                                                            <Select
                                                                isDisabled={!isBookingNumber}
                                                                value={{ label: inputSI.firstAdditionalPartyCountryLabel, value: inputSI.firstAdditionalPartyCountry }}
                                                                options={optionCountryList}
                                                                onChange={event => onChangeBlForm('firstAdditionalPartyCountry', event)} />
                                                        </div>
                                                    </div>

                                                </div>
                                            </fieldset>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>Second Additional Notify Party</legend>

                                                <div className="row">

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Second Additional Notify Party</label>
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.secondAdditionalPartyName}
                                                                onChange={event => onChangeBlForm('secondAdditionalPartyName', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6"></div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                            <input
                                                                type="hidden"
                                                                name={`secondAdditionalPartyAddress1`}
                                                                ref={register({
                                                                    maxLength: {
                                                                        value: 35,
                                                                        message: 'Maximum input length is 35'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.secondAdditionalPartyAddress1}
                                                                onChange={event => onChangeBlForm('secondAdditionalPartyAddress1', event)} />
                                                            {errors[`secondAdditionalPartyAddress1`] && errors[`secondAdditionalPartyAddress1`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>City</label>
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.secondAdditionalPartyCity}
                                                                onChange={event => onChangeBlForm('secondAdditionalPartyCity', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                            <input
                                                                type="hidden"
                                                                name={`secondAdditionalPartyAddress2`}
                                                                ref={register({
                                                                    maxLength: {
                                                                        value: 35,
                                                                        message: 'Maximum input length is 35'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.secondAdditionalPartyAddress2}
                                                                onChange={event => onChangeBlForm('secondAdditionalPartyAddress2', event)} />
                                                            {errors[`secondAdditionalPartyAddress2`] && errors[`secondAdditionalPartyAddress2`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputSI.secondAdditionalPartyPostalCode}
                                                                onChange={event => onChangeBlForm('secondAdditionalPartyPostalCode', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Country</label>
                                                            <Select
                                                                isDisabled={!isBookingNumber}
                                                                value={{ label: inputSI.secondAdditionalPartyCountryLabel, value: inputSI.secondAdditionalPartyCountry }}
                                                                options={optionCountryList}
                                                                onChange={event => onChangeBlForm('secondAdditionalPartyCountry', event)} />
                                                        </div>
                                                    </div>

                                                </div>
                                            </fieldset>

                                        </fieldset>

                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Transport</legend>

                                            <div className="row">

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Vessel<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.vessel} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Voyage<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.voyage} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Origin of Goods<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.originOfGoods} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Print on B/L as<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`originOfGoodsPrint`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <input
                                                            type="text"
                                                            disabled={!isBookingNumber}
                                                            className="form-control"
                                                            value={inputSI.originOfGoodsPrint}
                                                            onChange={event => onChangeBlForm('originOfGoodsPrint', event)} />
                                                        {errors[`originOfGoodsPrint`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`originOfGoodsPrint`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Place of Receipts<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.placeOfReceipt} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Print on B/L as<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`placeOfReceiptPrint`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <input
                                                            type="text"
                                                            disabled={!isBookingNumber}
                                                            className="form-control"
                                                            value={inputSI.placeOfReceiptPrint}
                                                            onChange={event => onChangeBlForm('placeOfReceiptPrint', event)} />
                                                        {errors[`placeOfReceiptPrint`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`placeOfReceiptPrint`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Port of Load<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.portOfLoad} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Print on B/L as<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`portOfLoadPrint`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <input
                                                            type="text"
                                                            disabled={!isBookingNumber}
                                                            className="form-control"
                                                            value={inputSI.portOfLoadPrint}
                                                            onChange={event => onChangeBlForm('portOfLoadPrint', event)} />
                                                        {errors[`portOfLoadPrint`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`portOfLoadPrint`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Port of Discharge<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.portOfDischarge} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Print on B/L as<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`portOfDischargePrint`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <input
                                                            type="text"
                                                            disabled={!isBookingNumber}
                                                            className="form-control"
                                                            value={inputSI.portOfDischargePrint}
                                                            onChange={event => onChangeBlForm('portOfDischargePrint', event)} />
                                                        {errors[`portOfDischargePrint`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`portOfDischargePrint`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Destination (Place of carrier delivery)<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.destination} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Print on B/L as<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`destinationPrint`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <input
                                                            type="text"
                                                            disabled={!isBookingNumber}
                                                            className="form-control"
                                                            value={inputSI.destinationPrint}
                                                            onChange={event => onChangeBlForm('destinationPrint', event)} />
                                                        {errors[`destinationPrint`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`destinationPrint`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Move type<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.moveType} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Shipment type<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="text"
                                                            disabled
                                                            className="form-control"
                                                            value={inputSI.shipmentType} />
                                                    </div>
                                                </div>

                                            </div>
                                        </fieldset>

                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Customs Compliance (Government Tax IDs)</legend>

                                            <div className="row">

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Shipper</label>
                                                        <input
                                                            type="text"
                                                            disabled={!isBookingNumber}
                                                            className="form-control"
                                                            value={inputSI.shipperTaxId}
                                                            onChange={event => onChangeBlForm('shipperTaxId', event)} />
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Consignee</label>
                                                        <input
                                                            type="text"
                                                            disabled={!isBookingNumber}
                                                            className="form-control"
                                                            value={inputSI.consigneeTaxId}
                                                            onChange={event => onChangeBlForm('consigneeTaxId', event)} />
                                                    </div>
                                                </div>

                                            </div>
                                        </fieldset>

                                    </Fragment>
                                ))}

                                {inputFieldsContainer.map((inputContainer, indexContainer) => (
                                    <Fragment key={`${inputContainer}~${indexContainer}`}>
                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Container</legend>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>Container {indexContainer + 1}</legend>

                                                <div className="row">

                                                    <div className="col-3">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Container Number<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`containerNumber-${indexContainer}`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputContainer.containerNumber}
                                                                onChange={event => onChangeContainer('containerNumber', indexContainer, event)} />
                                                            <span className={dashforgeCss.tx12}>4 letters + 7 digits (e.g. UNIS1234567)</span>
                                                            {errors[`containerNumber-${indexContainer}`] && <br />}
                                                            {errors[`containerNumber-${indexContainer}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`containerNumber-${indexContainer}`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-3">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Container Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputContainer.containerTypeLabel + ` (` + inputContainer.containerType + `)`} />
                                                        </div>
                                                    </div>

                                                    <div className="col-3">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Container Seal Number<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`containerSealNumber-${indexContainer}`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                disabled={!isBookingNumber}
                                                                className="form-control"
                                                                value={inputContainer.containerSealNumber}
                                                                onChange={event => onChangeContainer('containerSealNumber', indexContainer, event)} />
                                                            {errors[`containerSealNumber-${indexContainer}`] && <br /> && <span style={{ fontSize: 13, color: "red" }}>{errors[`containerSealNumber-${indexContainer}`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-3">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Container Seal Number Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`containerSealNumberType-${indexContainer}`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <Select
                                                                isDisabled={!isBookingNumber}
                                                                value={{ label: inputContainer.containerSealNumberTypeLabel, value: inputContainer.containerSealNumberType }}
                                                                options={optionSealNumberType}
                                                                onChange={event => onChangeContainer('containerSealNumberType', indexContainer, event)} />
                                                            {errors[`containerSealNumberType-${indexContainer}`] && <br /> && <span style={{ fontSize: 13, color: "red" }}>{errors[`containerSealNumberType-${indexContainer}`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Container Comments<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled
                                                                className="form-control"
                                                                value={inputContainer.containerComments} />
                                                        </div>
                                                    </div>

                                                </div>

                                                {inputContainer.cargo.map((inputCargo, indexCargo) => (
                                                    <div>
                                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '10px', marginTop: '10px' }}>

                                                            <div className="row">
                                                                <div className="col-md-8">
                                                                    <h6 className={dashforgeCss.txBlack}>Cargo</h6>
                                                                    <span className={dashforgeCss.tx12 + ' ' + dashforgeCss.txBlack}><div className={dashforgeCss.txDanger}>NOTE:</div> The sum of all Cargo Weights reflects the Gross Weight of the Cargo (excluding Tare) for the entire booking.</span>
                                                                    <p className={dashforgeCss.tx12 + ' ' + dashforgeCss.txBlack}>Inaccurate declaration of cargo weight may endanger crew, port worker, and vessel safety. Please verify the reasonable accuracy of this information prior to submission</p>
                                                                </div>
                                                            </div>

                                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                                <div className="row">

                                                                    <div className="col-6">
                                                                        <div className="form-group">
                                                                            <label className={dashforgeCss.txBlack}>Cargo Description<span className={dashforgeCss.txDanger}>*</span></label>
                                                                            <input
                                                                                type="text"
                                                                                disabled
                                                                                className="form-control"
                                                                                value={inputCargo.cargoDescription} />
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-6">
                                                                        <div className="form-group">
                                                                            <label className={dashforgeCss.txBlack}>Package Count<span className={dashforgeCss.txDanger}>*</span></label>
                                                                            <input
                                                                                type="text"
                                                                                disabled
                                                                                className="form-control"
                                                                                value={inputCargo.cargoPackageCount} />
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-6">
                                                                        <div className="form-group">
                                                                            <label className={dashforgeCss.txBlack}>HS Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                                            <input
                                                                                type="text"
                                                                                disabled
                                                                                className="form-control"
                                                                                value={inputCargo.cargoHsCode} />
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-6">
                                                                        <div className="form-group">
                                                                            <label className={dashforgeCss.txBlack}>Package Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                                            <input
                                                                                type="text"
                                                                                disabled
                                                                                className="form-control"
                                                                                value={inputCargo.cargoPackageType} />
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-6">
                                                                        <div className="form-group">
                                                                            <label className={dashforgeCss.txBlack}>Cargo Weight (Excludes Tares)<span className={dashforgeCss.txDanger}>*</span></label>
                                                                            <div className="input-group" style={{ flexDirection: 'row' }}>
                                                                                <div className="gross-volume-wrapper" style={{ display: 'flex' }}>
                                                                                    <input
                                                                                        type="text"
                                                                                        disabled
                                                                                        className="form-control"
                                                                                        value={inputCargo.cargoWeight} />
                                                                                    <div className="input-group-append">
                                                                                        <select
                                                                                            disabled
                                                                                            className="dropdown-toggle"
                                                                                            type="button"
                                                                                            data-toggle="dropdown"
                                                                                            aria-haspopup="true"
                                                                                            aria-expanded="false"
                                                                                            value={inputCargo.cargoWeightType}>
                                                                                            <option value="KGM" className="dropdown-item">Kgs</option>
                                                                                            <option value="LBS" className="dropdown-item">Pounds</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-6">
                                                                        <div className="form-group">
                                                                            <label className={dashforgeCss.txBlack}>Gross Volume<span className={dashforgeCss.txDanger}>*</span></label>
                                                                            <div className="input-group" style={{ flexDirection: 'row' }}>
                                                                                <div className="gross-volume-wrapper" style={{ display: 'flex' }}>
                                                                                    <input
                                                                                        type="text"
                                                                                        disabled
                                                                                        className="form-control"
                                                                                        value={inputCargo.cargoGrossVolume} />
                                                                                    <div className="input-group-append">
                                                                                        <select
                                                                                            disabled
                                                                                            className="dropdown-toggle"
                                                                                            type="button"
                                                                                            data-toggle="dropdown"
                                                                                            aria-haspopup="true"
                                                                                            aria-expanded="false"
                                                                                            value={inputCargo.cargoGrossVolumeType}>
                                                                                            <option value="MTQ">Cubic meters</option>
                                                                                            <option value="FTQ">Cubic feet</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                {isHazmat && <Fragment>
                                                                    <div className="row">
                                                                        <div className="col-12">
                                                                            <h6 className={dashforgeCss.txBlack}>Hazmat Details</h6>
                                                                        </div>
                                                                    </div>

                                                                    <div className="row">

                                                                        <div className="col-12">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Primary IMO Class</label>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputCargo.cargoPrimaryImoClass} />
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>UNDG Number</label>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputCargo.cargoUndgNumber} />
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Emergency Contact Name</label>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputCargo.cargoEmergencyContactName} />
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Packing Group</label>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputCargo.cargoPackingGroup} />
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Emergency Contact Number</label>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputCargo.cargoEmergencyContactNumber} />
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Proper Shipping Name</label>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputCargo.cargoProperShippingName} />
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </Fragment>}

                                                            </fieldset>

                                                        </fieldset>
                                                    </div>

                                                ))}

                                            </fieldset>

                                        </fieldset>

                                    </Fragment>
                                ))}

                                {inputPaymentDetails.map((inputPayment, indexPayment) => (
                                    <Fragment>
                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Documentation and Freight Charges</legend>

                                            <div className="row">

                                                <div className="col-3">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Freight Term<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`freightTerm-${indexPayment}`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <Select
                                                            isDisabled={!isBookingNumber}
                                                            value={{ label: inputPayment.freightTermLabel, value: inputPayment.freightTerm }}
                                                            options={optionPaymentDetailsFreightTerm}
                                                            onChange={event => onChangePaymentDetail('freightTerm', indexPayment, event)} />
                                                        {errors[`freightTerm-${indexPayment}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`freightTerm-${indexPayment}`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-3">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Payer<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`payer-${indexPayment}`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <Select
                                                            isDisabled={!isBookingNumber}
                                                            value={{ label: inputPayment.payerLabel, value: inputPayment.payer }}
                                                            options={optionPaymentDetailsPayer}
                                                            onChange={event => onChangePaymentDetail('payer', indexPayment, event)} />
                                                        {errors[`payer-${indexPayment}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`payer-${indexPayment}`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-3">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Charge Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`chargeType-${indexPayment}`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <Select
                                                            isDisabled={!isBookingNumber}
                                                            value={{ label: inputPayment.chargeTypeLabel, value: inputPayment.chargeType }}
                                                            options={optionPaymentDetailsChargeType}
                                                            onChange={event => onChangePaymentDetail('chargeType', indexPayment, event)} />
                                                        {errors[`chargeType-${indexPayment}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`chargeType-${indexPayment}`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-3">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Payment Location<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`paymentLocation-${indexPayment}`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <AsyncSelect
                                                            isDisabled={!isBookingNumber}
                                                            value={{ label: inputPayment.paymentLocationLabel, value: inputPayment.paymentLocation }}
                                                            loadOptions={fetchDataBookingOffice}
                                                            placeholder="Payment port location"
                                                            onChange={event => onChangePaymentDetail('paymentLocation', indexPayment, event)}
                                                        />
                                                        {errors[`paymentLocation-${indexPayment}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`paymentLocation-${indexPayment}`].message}</span>}
                                                    </div>
                                                </div>

                                            </div>

                                        </fieldset>

                                    </Fragment>
                                ))}

                                <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                    <legend>B/L Print Instructions (Provided to the Carrier, not printed on Bill of Landing)</legend>

                                    <div className="row">

                                        <div className="col-4">

                                            <div className="form-group">
                                                <label for="">B/L Type</label>
                                                <div className="custom-control custom-radio">
                                                    <input
                                                        disabled={!isBookingNumber}
                                                        type="radio"
                                                        id="bl_type_original"
                                                        name="bl_type"
                                                        value="original"
                                                        className="custom-control-input"
                                                        onChange={event => blType('bl_type', event)} />
                                                    <label for="bl_type_original" className="custom-control-label">Original</label>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="custom-control custom-radio">
                                                    <input
                                                        disabled={!isBookingNumber}
                                                        type="radio"
                                                        id="bl_type_seaway"
                                                        name="bl_type"
                                                        value="seaway"
                                                        className="custom-control-input"
                                                        onChange={event => blType('bl_type', event)} />
                                                    <label for="bl_type_seaway" className="custom-control-label">Seaway/Express</label>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="custom-control custom-radio">
                                                    <input
                                                        disabled={!isBookingNumber}
                                                        type="radio"
                                                        id="bl_type_house_bill"
                                                        name="bl_type"
                                                        value="house_bill"
                                                        className="custom-control-input"
                                                        onChange={event => blType('bl_type', event)} />
                                                    <label for="bl_type_house_bill" className="custom-control-label">Stand Alone House Bill</label>
                                                </div>
                                            </div>

                                        </div>

                                        <div className="col-4">

                                            <div className="form-group">
                                                <label for="">Freighted</label>
                                                <input
                                                    type="text"
                                                    placeholder="Enter # Copies"
                                                    className="form-control"
                                                    style={{ marginBottom: '5px' }}
                                                    value={inputBlTypeOriginal[0].freighted}
                                                    disabled={blOriginalFreightedDisabled}
                                                    onChange={event => onChangeBlForm('originalFreighted', event)} />
                                                <input
                                                    type="text"
                                                    placeholder="Enter # Copies"
                                                    className="form-control"
                                                    style={{ marginBottom: '5px' }}
                                                    value={inputBlTypeSeaway[0].freighted}
                                                    disabled={blSeawayFreightedDisabled}
                                                    onChange={event => onChangeBlForm('seawayFreighted', event)} />
                                                <input
                                                    type="text"
                                                    placeholder="Enter # Copies"
                                                    className="form-control"
                                                    style={{ marginBottom: '5px' }}
                                                    value={inputBlTypeHouseBill[0].freighted}
                                                    disabled={blHouseBillFreightedDisabled}
                                                    onChange={event => onChangeBlForm('houseBillFreighted', event)} />
                                            </div>

                                        </div>

                                        <div className="col-4">

                                            <div className="form-group">
                                                <label for="">Unfreighted</label>
                                                <input
                                                    type="text"
                                                    placeholder="Enter # Copies"
                                                    className="form-control"
                                                    style={{ marginBottom: '5px' }}
                                                    value={inputBlTypeOriginal[0].unfreighted}
                                                    disabled={blOriginalUnfreightedDisabled}
                                                    onChange={event => onChangeBlForm('originalUnfreighted', event)} />
                                                <input
                                                    type="text"
                                                    placeholder="Enter # Copies"
                                                    className="form-control"
                                                    style={{ marginBottom: '5px' }}
                                                    value={inputBlTypeSeaway[0].unfreighted}
                                                    disabled={blSeawayUnfreightedDisabled}
                                                    onChange={event => onChangeBlForm('seawayUnfreighted', event)} />
                                                <input
                                                    type="text"
                                                    placeholder="Enter # Copies"
                                                    className="form-control"
                                                    style={{ marginBottom: '5px' }}
                                                    value={inputBlTypeHouseBill[0].unfreighted}
                                                    disabled={blHouseBillUnfreightedDisabled}
                                                    onChange={event => onChangeBlForm('houseBillUnfreighted', event)} />
                                            </div>

                                        </div>

                                    </div>
                                </fieldset>

                                {formSI.map((inputSI, indexSI) => (
                                    <Fragment>
                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Remarks</legend>

                                            <div className="row">

                                                <div className="col-12">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Customer Comments</label>
                                                        <textarea
                                                            disabled={!isBookingNumber}
                                                            className="form-control"
                                                            value={inputSI.customerComments}
                                                            onChange={event => onChangeBlForm('customerComments', event)}></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                        </fieldset>

                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>Notification Emails</legend>

                                            <div className="row">

                                                <div className="col-12">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Partner Notification Emails<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <span className="float-right" style={{ fontSize: '12px', color: 'black' }}>Press enter or separate each email with ,</span>
                                                        <input
                                                            type="hidden"
                                                            name={`partnerNotificationEmails`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <input
                                                            type="text"
                                                            disabled={!isBookingNumber}
                                                            className="form-control"
                                                            value={inputSI.partnerNotificationEmails}
                                                            onChange={event => onChangeBlForm('partnerNotificationEmails', event)} />
                                                        {errors[`partnerNotificationEmails`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`partnerNotificationEmails`].message}</span>}
                                                    </div>
                                                </div>

                                            </div>
                                        </fieldset>

                                    </Fragment>
                                ))}

                            </div>
                        </div>

                        <div className="row">
                            <div className={`col-md-12 ` + dashforgeCss.textRight}>
                                <button
                                    type="submit"
                                    className="btn btn-primary"
                                    disabled={!isBookingNumber}>
                                    {isLoading && <i className="fa fa-circle-notch fa-spin"></i>}
                                    {isLoading && <span>Please wait...</span>}
                                    {!isLoading && <span>Complete Shipping Instruction</span>}
                                </button>
                            </div>
                        </div>
                    </form>
                    {/* <br />
                    <pre>
                        {JSON.stringify(formSI, null, 2)}
                    </pre> */}
                    {/* <br />
                    <pre>
                        {JSON.stringify(inputFieldsContainer, null, 2)}
                    </pre> */}
                    {/* <br />
                    <pre>
                        {JSON.stringify(inputPaymentDetails, null, 2)}
                    </pre> */}
                    {/* <br />
                    <pre>
                        {JSON.stringify(inputBlTypeOriginal, null, 2)}
                    </pre> */}
                    {/* <br />
                    <pre>
                        {JSON.stringify(inputBlTypeSeaway, null, 2)}
                    </pre> */}
                    {/* <br />
                    <pre>
                        {JSON.stringify(inputBlTypeHouseBill, null, 2)}
                    </pre> */}
                    {/* <br />
                    <pre>
                        {JSON.stringify(getValues(), null, 2)}
                    </pre> */}
                </Fragment>
            )
    )
}

export default BeforeCompleteSI
