import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../BookingRequest/dashforge.module.css'
import Select from 'react-select'
import AsyncSelect from 'react-select/async'
import axios from 'axios'
import { useForm } from 'react-hook-form'
import { useToasts } from 'react-toast-notifications'
import BeforeCompleteSI from '../../ShipmentManagement/Tabs/SI/BeforeCompleteSI'
import AfterCompleteSI from '../../ShipmentManagement/Tabs/SI/AfterCompleteSI'
import Spinner from 'react-bootstrap/Spinner'
import { ToastProvider } from 'react-toast-notifications'
import * as ApiClickargo from '../../../Clickargo/ApiList.js'

function ShippingInstruction(props) {

    const [count, setCount] = useState(0)

    const [didMount, setDidMount] = useState(false);
    // Setting didMount to true upon mounting
    useEffect(() => setDidMount(true), []);

    const [isSIComplete, setIsSIComplete] = useState(true)

    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            axios
                .post(ApiClickargo.CLICKARGO_FIND_ORDER_BY_JOB_NUMBER, {
                    job_number: localStorage.getItem('ckSmJobNumber')
                })
                .then(response => {
                    console.log('success get detail vessel')
                    const dataVessel = response.data.data
                    
                    if(dataVessel.shipping_instructions.length > 0) {
                        setIsSIComplete(true)
                    } else {
                        setIsSIComplete(false)
                    }

                    //spinner false after all data is updated
                    setIsLoading(false)
                })
                .catch(error => {
                    console.log(error)
                    //console.log(error.response.data.message)
                })
        }, 1000);
    }, [])

    useEffect(() => {
        if (didMount) {
            console.log('check Shipping Instruction Information')
            setIsLoading(true)

            setTimeout(() => {
                axios
                    .post(ApiClickargo.CLICKARGO_FIND_ORDER_BY_JOB_NUMBER, {
                        job_number: localStorage.getItem('ckSmJobNumber')
                    })
                    .then(response => {
                        console.log('success get detail vessel')
                        const dataVessel = response.data.data
                        
                        if(dataVessel.shipping_instructions.length > 0) {
                            setIsSIComplete(true)
                        } else {
                            setIsSIComplete(false)
                        }
    
                        //spinner false after all data is updated
                        setIsLoading(false)
                    })
                    .catch(error => {
                        console.log(error)
                        //console.log(error.response.data.message)
                    })
            }, 1000);
        }
    }, [count])

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
            isSIComplete ? (
                <ToastProvider>
                    <Fragment>
                        <AfterCompleteSI />
                    </Fragment>
                </ToastProvider>
            ) : (
                <ToastProvider>
                    <Fragment>
                        {/* <h1>Now: {count}</h1>
                        <br /> */}
                        <BeforeCompleteSI setCount={setCount} count={count} dataCountryList={props.dataCountryList} npwp={props.dataUserPortal.npwp} email={props.dataUserPortal.email} />
                    </Fragment>
                </ToastProvider>
            )
        )
    )
}

export default ShippingInstruction
