import React, { useState, Fragment, useEffect, useRef } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import Select from 'react-select'
import AsyncSelect from 'react-select/async'
//import './dashforge.css'
import dashforgeCss from './dashforge.module.css'
import { Link } from "react-router-dom"
import axios from 'axios'
import { useForm } from 'react-hook-form'
import { useToasts } from 'react-toast-notifications'
import Swal from 'sweetalert2'
import moment from 'moment'
import * as ApiClickargo from '../../Clickargo/ApiList.js'

function BookingRequest(props) {

    const [showPreCarriage, setShowPreCarriage] = useState(false);
    const [showOnCarriage, setShowOnCarriage] = useState(false);

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const [modalOffline, setModalOffline] = useState(true);

    const toggleOffline = () => setModal(!modal);

    const { register, handleSubmit, errors, watch, setError, clearError, setValue, getValues } = useForm();

    const {
        register: register2,
        handleSubmit: handleSubmit2,
        errors: errors2,
        watch: watch2,
        clearError: clearError2,
        setValue: setValue2,
        getValues: getValues2 } = useForm();

    const { addToast } = useToasts()

    const [isFullContainerLoaded, setIsFullContainerLoaded] = useState(false);

    const [isProductTypeChanged, setIsProductTypeChanged] = useState(false);

    const [isHazmat, setIsHazmat] = useState(false);

    const [arrContainerTypeList, setArrContainerTypeList] = useState([]);

    const [arrPackageTypeList, setArrPackageTypeList] = useState([]);

    const routeProps = {
        location: props.location
    };

    const [inputFields, setInputFields] = useState([]);

    const [inputShipperTaxNumber, setInputShipperTaxNumber] = useState('9999999999999999999');

    useEffect(() => {
        setInputShipperTaxNumber(routeProps.location.state.shipperTaxNumber)

        routeProps.location.state.generalArr.map((inputGeneral, indexGeneral) => {
            setInputFields([...inputFields,
            {
                shipmentType: inputGeneral.shipmentType,
                shipmentTypeLabel: inputGeneral.shipmentTypeLabel,
                productType: inputGeneral.productType,
                productTypeLabel: inputGeneral.productTypeLabel,
                //transport
                transportMoveType: inputGeneral.transportMoveType,
                transportMoveTypeLabel: inputGeneral.transportMoveTypeLabel,
                transportPOCReceipt: inputGeneral.transportPOCReceipt,
                transportPOCDelivery: inputGeneral.transportPOCDelivery,
                transportEarliestDepartureDate: inputGeneral.transportEarliestDepartureDate,
                transportLatestDeliveryDate: inputGeneral.transportLatestDeliveryDate,
                //pre carriage
                preCarriageStart: inputGeneral.preCarriageStart,
                preCarriageStartLabel: inputGeneral.preCarriageStartLabel,
                preCarriageMode: inputGeneral.preCarriageMode,
                preCarriageModeLabel: inputGeneral.preCarriageModeLabel,
                preCarriageETD: inputGeneral.preCarriageETD,
                preCarriageETA: inputGeneral.preCarriageETA,
                //main carriage
                mainCarriagePortOfLoading: inputGeneral.mainCarriagePortOfLoading,
                mainCarriagePortOfLoadingName: inputGeneral.mainCarriagePortOfLoadingName,
                mainCarriagePortOfLoadingCountry: inputGeneral.mainCarriagePortOfLoadingCountry,
                mainCarriagePortOfDischarge: inputGeneral.mainCarriagePortOfDischarge,
                mainCarriagePortOfDischargeName: inputGeneral.mainCarriagePortOfDischargeName,
                mainCarriagePortOfDischargeCountry: inputGeneral.mainCarriagePortOfDischargeCountry,
                mainCarriageETD: inputGeneral.mainCarriageETD,
                mainCarriageETA: inputGeneral.mainCarriageETA,
                mainCarriageVessel: inputGeneral.mainCarriageVessel,
                mainCarriageVoyage: inputGeneral.mainCarriageVoyage,
                //on carriage
                onCarriageStart: inputGeneral.onCarriageStart,
                onCarriageStartLabel: inputGeneral.onCarriageStartLabel,
                onCarriageMode: inputGeneral.onCarriageMode,
                onCarriageModeLabel: inputGeneral.onCarriageModeLabel,
                onCarriageETD: inputGeneral.onCarriageETD,
                onCarriageETA: inputGeneral.onCarriageETA,
                //general details
                generalDetailsCarrier: inputGeneral.generalDetailsCarrier,
                generalDetailsCarrierLabel: inputGeneral.generalDetailsCarrierLabel,
                generalDetailsCarrierReceipt: inputGeneral.generalDetailsCarrierReceipt,
                generalDetailsCarrierDelivery: inputGeneral.generalDetailsCarrierDelivery,
                generalDetailsEarliestDeparture: inputGeneral.generalDetailsEarliestDeparture,
                generalDetailsLatestDelivery: inputGeneral.generalDetailsLatestDelivery,
                generalDetailsBookingOffice: inputGeneral.generalDetailsBookingOffice,
                generalDetailsBookingOfficeLabel: inputGeneral.generalDetailsBookingOfficeLabel,
                generalDetailsContractNumber: inputGeneral.generalDetailsContractNumber,
                //parties
                partiesShipperName: inputGeneral.partiesShipperName,
                partiesShipperEmail: inputGeneral.partiesShipperEmail,
                partiesShipperTaxNumber: inputGeneral.partiesShipperTaxNumber,
                partiesShipperAddress1: inputGeneral.partiesShipperAddress1, //update parties //22-05-2010
                partiesShipperAddress2: inputGeneral.partiesShipperAddress2, //update parties //22-05-2010
                partiesShipperCountryCode: inputGeneral.partiesShipperCountryCode,
                partiesShipperCountryName: inputGeneral.partiesShipperCountryName,
                partiesShipperCity: inputGeneral.partiesShipperCity,
                partiesShipperPostalCode: inputGeneral.partiesShipperPostalCode,
                partiesForwarder: inputGeneral.partiesForwarder,
                partiesConsignee: inputGeneral.partiesConsignee,
                partiesConsigneeAddress1: inputGeneral.partiesConsigneeAddress1, //update parties //22-05-2010
                partiesConsigneeAddress2: inputGeneral.partiesConsigneeAddress2, //update parties //22-05-2010
                partiesConsigneeCountryCode: inputGeneral.partiesConsigneeCountryCode,
                partiesConsigneeCountryName: inputGeneral.partiesConsigneeCountryName,
                partiesConsigneeCity: inputGeneral.partiesConsigneeCity,
                partiesConsigneePostalCode: inputGeneral.partiesConsigneePostalCode,
                partiesShipperInttraId: inputGeneral.partiesShipperInttraId,
                partiesForwarderInttraId: inputGeneral.partiesForwarderInttraId,
                partiesForwarderAddress: inputGeneral.partiesForwarderAddress,
                //additional party
                //contact party
                contractPartyName: inputGeneral.contractPartyName, //update additional party //22-05-2010
                contractPartyAddress1: inputGeneral.contractPartyAddress1, //update additional party //22-05-2010
                contractPartyAddress2: inputGeneral.contractPartyAddress2, //update additional party //22-05-2010
                contractPartyCountryCode: inputGeneral.contractPartyCountryCode, //update additional party //22-05-2010
                contractPartyCountryName: inputGeneral.contractPartyCountryName, //update additional party //22-05-2010
                contractPartyCity: inputGeneral.contractPartyCity, //update additional party //22-05-2010
                contractPartyPostalCode: inputGeneral.contractPartyPostalCode, //update additional party //22-05-2010
                //notify party
                notifyPartyName: inputGeneral.notifyPartyName, //update additional party //22-05-2010
                notifyPartyAddress1: inputGeneral.notifyPartyAddress1, //update additional party //22-05-2010
                notifyPartyAddress2: inputGeneral.notifyPartyAddress2, //update additional party //22-05-2010
                notifyPartyCountryCode: inputGeneral.notifyPartyCountryCode, //update additional party //22-05-2010
                notifyPartyCountryName: inputGeneral.notifyPartyCountryName, //update additional party //22-05-2010
                notifyPartyCity: inputGeneral.notifyPartyCity, //update additional party //22-05-2010
                notifyPartyPostalCode: inputGeneral.notifyPartyPostalCode, //update additional party //22-05-2010
                //first additional notify party
                firstNotifyPartyName: inputGeneral.firstNotifyPartyName, //update additional party //22-05-2010
                firstNotifyPartyAddress1: inputGeneral.firstNotifyPartyAddress1, //update additional party //22-05-2010
                firstNotifyPartyAddress2: inputGeneral.firstNotifyPartyAddress2, //update additional party //22-05-2010
                firstNotifyPartyCountryCode: inputGeneral.notifyPartyPostalCode, //update additional party //22-05-2010
                firstNotifyPartyCountryName: inputGeneral.firstNotifyPartyCountryName, //update additional party //22-05-2010
                firstNotifyPartyCity: inputGeneral.firstNotifyPartyCity, //update additional party //22-05-2010
                firstNotifyPartyPostalCode: inputGeneral.firstNotifyPartyPostalCode, //update additional party //22-05-2010
                //second additional notify party
                secondNotifyPartyName: inputGeneral.secondNotifyPartyName, //update additional party //22-05-2010
                secondNotifyPartyAddress1: inputGeneral.secondNotifyPartyAddress1, //update additional party //22-05-2010
                secondNotifyPartyAddress2: inputGeneral.secondNotifyPartyAddress2, //update additional party //22-05-2010
                secondNotifyPartyCountryCode: inputGeneral.secondNotifyPartyCountryCode, //update additional party //22-05-2010
                secondNotifyPartyCountryName: inputGeneral.secondNotifyPartyCountryName, //update additional party //22-05-2010
                secondNotifyPartyCity: inputGeneral.secondNotifyPartyCity, //update additional party //22-05-2010
                secondNotifyPartyPostalCode: inputGeneral.secondNotifyPartyPostalCode, //update additional party //22-05-2010
                //references
                referencesShipperRefNumber: inputGeneral.referencesShipperRefNumber,
                referencesForwarderRefNumber: inputGeneral.referencesForwarderRefNumber,
                referencesPurchaseOrderNumber: inputGeneral.referencesPurchaseOrderNumber,
                referencesConsigneeNumber: inputGeneral.referencesConsigneeNumber,
                referencesBLNumber: inputGeneral.referencesBLNumber,
                referencesContractParty: inputGeneral.referencesContractParty,
                //comments and notifications
                customerComments: inputGeneral.customerComments,
                partnerEmailNotifications: inputGeneral.partnerEmailNotifications,
                //update new field schedule
                scheduleOriginUnloc: inputGeneral.scheduleOriginUnloc,
                scheduleOriginPortName: inputGeneral.scheduleOriginPortName,
                scheduleOriginCountry: inputGeneral.scheduleOriginCountry,
                scheduleDestinationUnloc: inputGeneral.scheduleDestinationUnloc,
                scheduleDestinationPortName: inputGeneral.scheduleDestinationPortName,
                scheduleDestinationCountry: inputGeneral.scheduleDestinationCountry,
            }
            ])
            //set values to handle error message
            setTimeout(() => {
                setTimeout(() => {
                    if (inputGeneral.shipmentType === 'full container loaded') {
                        setIsFullContainerLoaded(true)
                        setTimeout(() => {
                            const valUploadContainer = [...uploadContainerData];
                            valUploadContainer[0].shipment_type = inputGeneral.shipmentType
                            setUploadContainerData(valUploadContainer)
                        }, 1000);
                    } else {
                        setIsFullContainerLoaded(false)
                    }

                    if (inputGeneral.productType === 'special') {
                        setIsHazmat(true)
                        setIsProductTypeChanged(true)
                        setTimeout(() => {
                            const valUploadContainer = [...uploadContainerData];
                            valUploadContainer[0].product_type = inputGeneral.productType
                            setUploadContainerData(valUploadContainer)
                        }, 1000);
                    } else if (inputGeneral.productType === 'dangerous') {
                        setIsHazmat(true)
                        setIsProductTypeChanged(true)
                        setTimeout(() => {
                            const valUploadContainer = [...uploadContainerData];
                            valUploadContainer[0].product_type = inputGeneral.productType
                            setUploadContainerData(valUploadContainer)
                        }, 1000);
                    } else if (inputGeneral.productType === 'general') {
                        setIsHazmat(false)
                        setIsProductTypeChanged(true)
                        setTimeout(() => {
                            const valUploadContainer = [...uploadContainerData];
                            valUploadContainer[0].product_type = inputGeneral.productType
                            setUploadContainerData(valUploadContainer)
                        }, 1000);
                        //clear hazmat detail state
                        const values = [...inputFieldsContainer]

                        values.map((container, indexContainer) => {
                            container.cargo.map((cargo, indexCargo) => {
                                values[indexContainer].cargo[indexCargo].cargoPrimaryImoClassLabel = 'Please select IMO Class'
                                values[indexContainer].cargo[indexCargo].cargoPrimaryImoClass = ''
                                values[indexContainer].cargo[indexCargo].cargoUndgNumber = ''
                                values[indexContainer].cargo[indexCargo].cargoPackingGroup = ''
                                values[indexContainer].cargo[indexCargo].cargoPackingGroupLabel = 'Please select Packing Group'
                                values[indexContainer].cargo[indexCargo].cargoProperShippingName = ''
                                values[indexContainer].cargo[indexCargo].cargoEmergencyContactName = ''
                                values[indexContainer].cargo[indexCargo].cargoEmergencyContactNumber = ''
                                setInputFieldsContainer(values);
                            })
                        })
                    } else if (inputGeneral.productType === 'others') {
                        setIsHazmat(false)
                        setIsProductTypeChanged(true)
                        setTimeout(() => {
                            const valUploadContainer = [...uploadContainerData];
                            valUploadContainer[0].product_type = inputGeneral.productType
                            setUploadContainerData(valUploadContainer)
                        }, 1000);
                        //clear hazmat detail state
                        const values = [...inputFieldsContainer]

                        values.map((container, indexContainer) => {
                            container.cargo.map((cargo, indexCargo) => {
                                values[indexContainer].cargo[indexCargo].cargoPrimaryImoClassLabel = 'Please select IMO Class'
                                values[indexContainer].cargo[indexCargo].cargoPrimaryImoClass = ''
                                values[indexContainer].cargo[indexCargo].cargoUndgNumber = ''
                                values[indexContainer].cargo[indexCargo].cargoPackingGroup = ''
                                values[indexContainer].cargo[indexCargo].cargoPackingGroupLabel = 'Please select Packing Group'
                                values[indexContainer].cargo[indexCargo].cargoProperShippingName = ''
                                values[indexContainer].cargo[indexCargo].cargoEmergencyContactName = ''
                                values[indexContainer].cargo[indexCargo].cargoEmergencyContactNumber = ''
                                setInputFieldsContainer(values);
                            })
                        })
                    }

                    if (inputGeneral.transportMoveType === 'DoorToDoor') {
                        setShowPreCarriage(true)
                        setShowOnCarriage(true)

                        setValue([
                            { transportMoveType: inputGeneral.transportMoveType },
                            //pre carriage
                            { preCarriageStart: inputGeneral.preCarriageStart },
                            { preCarriageStartLabel: inputGeneral.preCarriageStartLabel },
                            { preCarriageMode: inputGeneral.preCarriageMode },
                            { preCarriageModeLabel: inputGeneral.preCarriageModeLabel },
                            { preCarriageETD: inputGeneral.preCarriageETD },
                            { preCarriageETA: inputGeneral.preCarriageETA },
                            //oncarriage
                            { onCarriageStart: inputGeneral.onCarriageStart },
                            { onCarriageStartLabel: inputGeneral.onCarriageStartLabel },
                            { onCarriageMode: inputGeneral.onCarriageMode },
                            { onCarriageModeLabel: inputGeneral.onCarriageModeLabel },
                            { onCarriageETD: inputGeneral.onCarriageETD },
                            { onCarriageETA: inputGeneral.onCarriageETA },
                        ])

                        clearError('transportMoveType')
                    } else if (inputGeneral.transportMoveType === 'DoorToPort') {
                        setShowPreCarriage(true)
                        setShowOnCarriage(false)

                        setValue([
                            { transportMoveType: inputGeneral.transportMoveType },
                            //pre carriage
                            { preCarriageStart: inputGeneral.preCarriageStart },
                            { preCarriageStartLabel: inputGeneral.preCarriageStartLabel },
                            { preCarriageMode: inputGeneral.preCarriageMode },
                            { preCarriageModeLabel: inputGeneral.preCarriageModeLabel },
                            { preCarriageETD: inputGeneral.preCarriageETD },
                            { preCarriageETA: inputGeneral.preCarriageETA },
                            //oncarriage
                            { onCarriageStart: inputGeneral.onCarriageStart },
                            { onCarriageStartLabel: inputGeneral.onCarriageStartLabel },
                            { onCarriageMode: inputGeneral.onCarriageMode },
                            { onCarriageModeLabel: inputGeneral.onCarriageModeLabel },
                            { onCarriageETD: inputGeneral.onCarriageETD },
                            { onCarriageETA: inputGeneral.onCarriageETA },
                        ])

                        clearError('transportMoveType')
                    } else if (inputGeneral.transportMoveType === 'PortToDoor') {
                        setShowPreCarriage(false)
                        setShowOnCarriage(true)

                        setValue([
                            { transportMoveType: inputGeneral.transportMoveType },
                            //pre carriage
                            { preCarriageStart: inputGeneral.preCarriageStart },
                            { preCarriageStartLabel: inputGeneral.preCarriageStartLabel },
                            { preCarriageMode: inputGeneral.preCarriageMode },
                            { preCarriageModeLabel: inputGeneral.preCarriageModeLabel },
                            { preCarriageETD: inputGeneral.preCarriageETD },
                            { preCarriageETA: inputGeneral.preCarriageETA },
                            //oncarriage
                            { onCarriageStart: inputGeneral.onCarriageStart },
                            { onCarriageStartLabel: inputGeneral.onCarriageStartLabel },
                            { onCarriageMode: inputGeneral.onCarriageMode },
                            { onCarriageModeLabel: inputGeneral.onCarriageModeLabel },
                            { onCarriageETD: inputGeneral.onCarriageETD },
                            { onCarriageETA: inputGeneral.onCarriageETA },
                        ])

                        clearError('transportMoveType')
                    } else if (inputGeneral.transportMoveType === 'PortToPort') {
                        setShowPreCarriage(false)
                        setShowOnCarriage(false)

                        setValue([
                            { transportMoveType: inputGeneral.transportMoveType },
                            //pre carriage
                            { preCarriageStart: inputGeneral.preCarriageStart },
                            { preCarriageStartLabel: inputGeneral.preCarriageStartLabel },
                            { preCarriageMode: inputGeneral.preCarriageMode },
                            { preCarriageModeLabel: inputGeneral.preCarriageModeLabel },
                            { preCarriageETD: inputGeneral.preCarriageETD },
                            { preCarriageETA: inputGeneral.preCarriageETA },
                            //oncarriage
                            { onCarriageStart: inputGeneral.onCarriageStart },
                            { onCarriageStartLabel: inputGeneral.onCarriageStartLabel },
                            { onCarriageMode: inputGeneral.onCarriageMode },
                            { onCarriageModeLabel: inputGeneral.onCarriageModeLabel },
                            { onCarriageETD: inputGeneral.onCarriageETD },
                            { onCarriageETA: inputGeneral.onCarriageETA },
                        ])

                        clearError('transportMoveType')
                    }
                }, 1000);

                setValue([
                    { shipmentTypeHidden: inputGeneral.shipmentType },
                    { productTypeHidden: inputGeneral.productType },
                    { preCarriageStart: inputGeneral.preCarriageStart },
                    { preCarriageMode: inputGeneral.preCarriageMode },
                    { preCarriageETD: inputGeneral.preCarriageETD },
                    { preCarriageETA: inputGeneral.preCarriageETA },
                    { onCarriageStart: inputGeneral.onCarriageStart },
                    { onCarriageMode: inputGeneral.onCarriageMode },
                    { onCarriageETD: inputGeneral.onCarriageETD },
                    { onCarriageETA: inputGeneral.onCarriageETA },
                    { generalDetailsCarrier: inputGeneral.generalDetailsCarrier },
                    { generalDetailsContractNumber: inputGeneral.generalDetailsContractNumber },
                    { generalDetailsBookingOffice: inputGeneral.generalDetailsBookingOffice },
                    { partiesShipperTaxNumber: inputGeneral.partiesShipperTaxNumber },
                    { partiesShipperAddress1: inputGeneral.partiesShipperAddress1 }, //update parties //22-05-2010
                    { partiesShipperAddress2: inputGeneral.partiesShipperAddress2 }, //update parties //22-05-2010
                    { partiesShipperCountryCode: inputGeneral.partiesShipperCountryCode },
                    { partiesShipperCity: inputGeneral.partiesShipperCity },
                    { partiesShipperPostalCode: inputGeneral.partiesShipperPostalCode },
                    { partiesConsignee: inputGeneral.partiesConsignee },
                    { partiesConsigneeCountryCode: inputGeneral.partiesConsigneeCountryCode },
                    { partiesConsigneeCity: inputGeneral.partiesConsigneeCity },
                    { partiesConsigneePostalCode: inputGeneral.partiesConsigneePostalCode },
                    { partiesConsigneeAddress1: inputGeneral.partiesConsigneeAddress1 }, //update parties //22-05-2010
                    { partiesConsigneeAddress2: inputGeneral.partiesConsigneeAddress2 }, //update parties //22-05-2010
                    { contractPartyAddress1: inputGeneral.contractPartyAddress1 },  //update additional party //22-05-2010
                    { contractPartyAddress2: inputGeneral.contractPartyAddress2 },  //update additional party //22-05-2010
                    { contractPartyCity: inputGeneral.contractPartyCity },  //update additional party //15-07-2020
                    { notifyPartyAddress1: inputGeneral.notifyPartyAddress1 },  //update additional party //22-05-2010
                    { notifyPartyAddress2: inputGeneral.notifyPartyAddress2 },  //update additional party //22-05-2010
                    { notifyPartyCity: inputGeneral.notifyPartyCity },  //update additional party //15-07-2020
                    { firstNotifyPartyAddress1: inputGeneral.firstNotifyPartyAddress1 },  //update additional party //22-05-2010
                    { firstNotifyPartyAddress2: inputGeneral.firstNotifyPartyAddress2 },  //update additional party //22-05-2010
                    { firstNotifyPartyCity: inputGeneral.firstNotifyPartyCity },  //update additional party //15-07-2020
                    { secondNotifyPartyAddress1: inputGeneral.secondNotifyPartyAddress1 },  //update additional party //22-05-2010
                    { secondNotifyPartyAddress2: inputGeneral.secondNotifyPartyAddress2 },  //update additional party //22-05-2010
                    { secondNotifyPartyCity: inputGeneral.secondNotifyPartyCity },  //update additional party //15-07-2020
                    { partnerEmailNotifications: inputGeneral.partnerEmailNotifications },
                ])
            }, 1000);
        })

    }, [routeProps.location.state.generalArr])

    const [inputFieldsContainer, setInputFieldsContainer] = useState([]);

    useEffect(() => {
        setTimeout(() => {
            const values = [...inputFieldsContainer];
            routeProps.location.state.containerArr.map((inputContainerArr, indexContainerArr) => {
                values.push({
                    //container
                    containerType: inputContainerArr.containerType,
                    containerTypeLabel: inputContainerArr.containerTypeLabel,
                    containerComments: inputContainerArr.containerComments,
                    //container cargo
                    cargo:
                        inputContainerArr.cargo.map((inputCargo, indexCargo) => (
                            {
                                cargoDescription: inputCargo.cargoDescription,
                                cargoHsCode: inputCargo.cargoHsCode,
                                cargoHsCodeLabel: inputCargo.cargoHsCodeLabel,
                                cargoWeight: inputCargo.cargoWeight,
                                cargoWeightType: inputCargo.cargoWeightType,
                                cargoPackageCount: inputCargo.cargoPackageCount,
                                cargoPackageType: inputCargo.cargoPackageType,
                                cargoPackageTypeLabel: inputCargo.cargoPackageTypeLabel,
                                cargoGrossVolume: inputCargo.cargoGrossVolume,
                                cargoGrossVolumeType: inputCargo.cargoGrossVolumeType,
                                cargoPrimaryImoClass: inputCargo.cargoPrimaryImoClass,
                                cargoPrimaryImoClassLabel: inputCargo.cargoPrimaryImoClassLabel,
                                cargoUndgNumber: inputCargo.cargoUndgNumber,
                                cargoPackingGroup: inputCargo.cargoPackingGroup,
                                cargoPackingGroupLabel: inputCargo.cargoPackingGroupLabel,
                                cargoProperShippingName: inputCargo.cargoProperShippingName,
                                cargoEmergencyContactName: inputCargo.cargoEmergencyContactName,
                                cargoEmergencyContactNumber: inputCargo.cargoEmergencyContactNumber,
                            }
                        ))
                })
                setInputFieldsContainer(values);
            })

            setTimeout(() => {
                routeProps.location.state.containerArr.map((inputContainerArr, indexContainerArr) => {
                    setValue([
                        { [`containerTypeHidden-${indexContainerArr}`]: inputContainerArr.containerType },
                        { [`containerComments-${indexContainerArr}`]: inputContainerArr.containerComments },
                    ])
                })


                routeProps.location.state.containerArr.map((inputContainerArr, indexContainerArr) => {
                    inputContainerArr.cargo.map((inputCargoArr, indexCargoArr) => {
                        setValue([
                            { [`cargoDescription-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargoDescription },
                            { [`cargoHsCode-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargoHsCode },
                            { [`cargoWeight-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargoWeight },
                            { [`cargoPackageCount-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargoPackageCount },
                            { [`cargoPackageType-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargoPackageType },
                            { [`cargoGrossVolume-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargoGrossVolume },
                            { [`cargoPrimaryImoClass-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargoPrimaryImoClass },
                            { [`cargoUndgNumber-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargoUndgNumber },
                            { [`cargoPackingGroup-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargoPackingGroup },
                            { [`cargoProperShippingName-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargoProperShippingName },
                            { [`cargoEmergencyContactName-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargoEmergencyContactName },
                            { [`cargoEmergencyContactNumber-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargoEmergencyContactNumber },
                        ])
                    })
                })

            }, 1000);

        }, 1000);
    }, [routeProps.location.state.containerArr])

    const [inputFieldsPaymentDetails, setInputFieldsPaymentDetails] = useState([]);

    useEffect(() => {
        setTimeout(() => {
            const values = [...inputFieldsPaymentDetails];
            routeProps.location.state.paymentDetailsArr.map((inputPaymentDetailsArr, indexPaymentDetailsArr) => {
                values.push({
                    paymentDetailsChangeType: inputPaymentDetailsArr.paymentDetailsChangeType,
                    paymentDetailsChangeTypeLabel: inputPaymentDetailsArr.paymentDetailsChangeTypeLabel,
                    paymentDetailsFreightTerm: inputPaymentDetailsArr.paymentDetailsFreightTerm,
                    paymentDetailsFreightTermLabel: inputPaymentDetailsArr.paymentDetailsFreightTermLabel,
                    paymentDetailsPayer: inputPaymentDetailsArr.paymentDetailsPayer,
                    paymentDetailsPayerLabel: inputPaymentDetailsArr.paymentDetailsPayerLabel,
                    paymentDetailsPaymentLocation: inputPaymentDetailsArr.paymentDetailsPaymentLocation,
                    paymentDetailsPaymentLocationLabel: inputPaymentDetailsArr.paymentDetailsPaymentLocationLabel,
                })
                setInputFieldsPaymentDetails(values);

                setTimeout(() => {
                    setValue([
                        { [`paymentDetailsChangeType-${indexPaymentDetailsArr}`]: inputPaymentDetailsArr.paymentDetailsChangeType },
                        { [`paymentDetailsFreightTerm-${indexPaymentDetailsArr}`]: inputPaymentDetailsArr.paymentDetailsFreightTerm },
                        { [`paymentDetailsPayer-${indexPaymentDetailsArr}`]: inputPaymentDetailsArr.paymentDetailsPayer },
                        { [`paymentDetailsPaymentLocation-${indexPaymentDetailsArr}`]: inputPaymentDetailsArr.paymentDetailsPaymentLocation },
                    ])
                }, 1000);
            })
        }, 1000)
    }, [routeProps.location.state.paymentDetailsArr])

    let optionShipmentType = [
        { value: '', label: 'Please Select Shipment Type' },
        { value: 'full container loaded', label: 'Full Container Loaded' }
    ]

    let optionProductType = [
        { value: '', label: 'Please Select Product Type' },
        { value: 'general', label: 'General Cargo' },
        { value: 'special', label: 'Special Cargo' },
        { value: 'dangerous', label: 'Dangerous Goods' },
        { value: 'others', label: 'Others' }
    ]

    const handleInputChange = (field, event) => {
        const values = [...inputFields];

        if (field === "shipmentType") {
            values[0].shipmentType = event.value;
            values[0].shipmentTypeLabel = event.label;

            setValue([
                { shipmentTypeHidden: inputFields[0].shipmentType },
            ])

            clearError('shipmentTypeHidden')

            if (event.value === 'full container loaded') {
                setIsFullContainerLoaded(true)
            } else {
                setIsFullContainerLoaded(false)
            }

            const valUploadContainer = [...uploadContainerData];
            valUploadContainer[0].shipment_type = event.value
            setUploadContainerData(valUploadContainer)
        }
        if (field === "productType") {
            values[0].productType = event.value;
            values[0].productTypeLabel = event.label;

            setValue([
                { productTypeHidden: inputFields[0].productType },
            ])

            clearError('productTypeHidden')

            if (event.value === '') {
                setIsProductTypeChanged(false)
            } else {
                setIsProductTypeChanged(true)
            }

            const valUploadContainer = [...uploadContainerData];
            valUploadContainer[0].product_type = event.value
            setUploadContainerData(valUploadContainer)

            if (event.value === 'special') {
                setIsHazmat(true)
            } else if (event.value === 'dangerous') {
                setIsHazmat(true)
            } else if (event.value === 'general') {
                setIsHazmat(false)
                //clear hazmat detail state
                const values = [...inputFieldsContainer]

                values.map((container, indexContainer) => {
                    container.cargo.map((cargo, indexCargo) => {
                        values[indexContainer].cargo[indexCargo].cargoPrimaryImoClassLabel = 'Please select IMO Class'
                        values[indexContainer].cargo[indexCargo].cargoPrimaryImoClass = ''
                        values[indexContainer].cargo[indexCargo].cargoUndgNumber = ''
                        values[indexContainer].cargo[indexCargo].cargoPackingGroup = ''
                        values[indexContainer].cargo[indexCargo].cargoPackingGroupLabel = 'Please select Packing Group'
                        values[indexContainer].cargo[indexCargo].cargoProperShippingName = ''
                        values[indexContainer].cargo[indexCargo].cargoEmergencyContactName = ''
                        values[indexContainer].cargo[indexCargo].cargoEmergencyContactNumber = ''
                        setInputFieldsContainer(values);
                    })
                })
            } else if (event.value === 'others') {
                setIsHazmat(false)
                //clear hazmat detail state
                const values = [...inputFieldsContainer]

                values.map((container, indexContainer) => {
                    container.cargo.map((cargo, indexCargo) => {
                        values[indexContainer].cargo[indexCargo].cargoPrimaryImoClassLabel = 'Please select IMO Class'
                        values[indexContainer].cargo[indexCargo].cargoPrimaryImoClass = ''
                        values[indexContainer].cargo[indexCargo].cargoUndgNumber = ''
                        values[indexContainer].cargo[indexCargo].cargoPackingGroup = ''
                        values[indexContainer].cargo[indexCargo].cargoPackingGroupLabel = 'Please select Packing Group'
                        values[indexContainer].cargo[indexCargo].cargoProperShippingName = ''
                        values[indexContainer].cargo[indexCargo].cargoEmergencyContactName = ''
                        values[indexContainer].cargo[indexCargo].cargoEmergencyContactNumber = ''
                        setInputFieldsContainer(values);
                    })
                })
            }
        }

        if (field === "transportMoveType") {
            values[0].transportMoveType = event.value
            values[0].transportMoveTypeLabel = event.label

            if (values[0].transportMoveType === 'DoorToDoor') {
                setShowPreCarriage(true)
                setShowOnCarriage(true)
            } else if (values[0].transportMoveType === 'DoorToPort') {
                setShowPreCarriage(true)

                setShowOnCarriage(false)
                const valOnCarriage = [...inputFields]
                valOnCarriage[0].onCarriageStart = ''
                valOnCarriage[0].onCarriageStartLabel = 'Enter Location'
                valOnCarriage[0].onCarriageMode = ''
                valOnCarriage[0].onCarriageModeLabel = 'Please choose transport mode'
                valOnCarriage[0].onCarriageETD = ''
                valOnCarriage[0].onCarriageETA = ''
                setInputFields(valOnCarriage)
            } else if (values[0].transportMoveType === 'PortToDoor') {
                setShowPreCarriage(false)
                const valPreCarriage = [...inputFields]
                valPreCarriage[0].preCarriageStart = ''
                valPreCarriage[0].preCarriageStartLabel = 'Enter Location'
                valPreCarriage[0].preCarriageMode = ''
                valPreCarriage[0].preCarriageModeLabel = 'Please choose transport mode'
                valPreCarriage[0].preCarriageETD = ''
                valPreCarriage[0].preCarriageETA = ''
                setInputFields(valPreCarriage)

                setShowOnCarriage(true)
            } else if (values[0].transportMoveType === 'PortToPort') {
                setShowPreCarriage(false)
                const valPreCarriage = [...inputFields]
                valPreCarriage[0].preCarriageStart = ''
                valPreCarriage[0].preCarriageStartLabel = 'Enter Location'
                valPreCarriage[0].preCarriageMode = ''
                valPreCarriage[0].preCarriageModeLabel = 'Please choose transport mode'
                valPreCarriage[0].preCarriageETD = ''
                valPreCarriage[0].preCarriageETA = ''
                setInputFields(valPreCarriage)

                setShowOnCarriage(false)
                const valOnCarriage = [...inputFields]
                valOnCarriage[0].onCarriageStart = ''
                valOnCarriage[0].onCarriageStartLabel = 'Enter Location'
                valOnCarriage[0].onCarriageMode = ''
                valOnCarriage[0].onCarriageModeLabel = 'Please choose transport mode'
                valOnCarriage[0].onCarriageETD = ''
                valOnCarriage[0].onCarriageETA = ''
                setInputFields(valOnCarriage)
            }

            setValue([
                { transportMoveType: inputFields[0].transportMoveType },
            ])

            clearError('transportMoveType')
        }

        if (field === "preCarriageStart") {
            values[0].preCarriageStart = event.value
            values[0].preCarriageStartLabel = event.label

            setValue([
                { preCarriageStart: inputFields[0].preCarriageStart },
            ])

            clearError('preCarriageStart')
        }

        if (field === "preCarriageMode") {
            values[0].preCarriageMode = event.value
            values[0].preCarriageModeLabel = event.label

            setValue([
                { preCarriageMode: inputFields[0].preCarriageMode },
            ])

            clearError('preCarriageMode')
        }

        if (field === "preCarriageETD") {
            values[0].preCarriageETD = event.target.value

            setValue([
                { preCarriageETD: inputFields[0].preCarriageETD },
            ])

            clearError('preCarriageETD')
        }

        if (field === "preCarriageETA") {
            values[0].preCarriageETA = event.target.value

            setValue([
                { preCarriageETA: inputFields[0].preCarriageETA },
            ])

            clearError('preCarriageETA')
        }

        if (field === "onCarriageStart") {
            values[0].onCarriageStart = event.value
            values[0].onCarriageStartLabel = event.label

            setValue([
                { onCarriageStart: inputFields[0].onCarriageStart },
            ])

            clearError('onCarriageStart')
        }

        if (field === "onCarriageMode") {
            values[0].onCarriageMode = event.value
            values[0].onCarriageModeLabel = event.label

            setValue([
                { onCarriageMode: inputFields[0].onCarriageMode },
            ])

            clearError('onCarriageMode')
        }

        if (field === "onCarriageETD") {
            values[0].onCarriageETD = event.target.value

            setValue([
                { onCarriageETD: inputFields[0].onCarriageETD },
            ])

            clearError('onCarriageETD')
        }

        if (field === "onCarriageETA") {
            values[0].onCarriageETA = event.target.value

            setValue([
                { onCarriageETA: inputFields[0].onCarriageETA },
            ])

            clearError('onCarriageETA')
        }

        if (field === "generalDetailsContractNumber") {
            values[0].generalDetailsContractNumber = event.target.value

            setValue([
                { generalDetailsContractNumber: inputFields[0].generalDetailsContractNumber },
            ])

            clearError('generalDetailsContractNumber')
        }

        if (field === "generalDetailsBookingOffice") {
            values[0].generalDetailsBookingOffice = event.value
            values[0].generalDetailsBookingOfficeLabel = event.label

            setValue([
                { generalDetailsBookingOffice: inputFields[0].generalDetailsBookingOffice },
            ])

            clearError('generalDetailsBookingOffice')
        }

        if (field === "partiesShipperName") {
            values[0].partiesShipperName = event.target.value
        }

        if (field === "partiesShipperInttraId") {
            values[0].partiesShipperInttraId = event.target.value
        }

        if (field === "partiesShipperTaxNumber") {
            values[0].partiesShipperTaxNumber = event.target.value

            setValue([
                { partiesShipperTaxNumber: inputFields[0].partiesShipperTaxNumber },
            ])

            //validate shipper tax number to nle api profile
            //check if data in user profile oss in nle api exist then set state

            setTimeout(() => {
                axios
                    .get(ApiClickargo.NLE_USER_PROFILE_OSS + '/' + inputFields[0].partiesShipperTaxNumber,
                        {
                            headers: {
                                'beacukai-api-key': ApiClickargo.NLE_USER_PROFILE_OSS_API_KEY
                            }
                        })
                    .then(response => {
                        if (response.data['data Customer Oss'].length > 0) {
                            setInputShipperTaxNumber(inputFields[0].partiesShipperTaxNumber)
                            clearError('partiesShipperTaxNumber')
                        }
                        else {
                            //check api profile

                            axios
                                .get(ApiClickargo.NLE_USER_PROFILE + '/' + inputFields[0].partiesShipperTaxNumber,
                                    {
                                        headers: {
                                            'beacukai-api-key': ApiClickargo.NLE_USER_PROFILE_API_KEY
                                        }
                                    })
                                .then(response => {
                                    if (response.data['data Customer'].length > 0) {
                                        setInputShipperTaxNumber(inputFields[0].partiesShipperTaxNumber)
                                        clearError('partiesShipperTaxNumber')
                                    } else {
                                        setInputShipperTaxNumber('9999999999999999999')
                                    }
                                })
                                .catch(error => {
                                    console.log(error)
                                    addToast('Something wrong. Please Check the server', {
                                        appearance: 'error',
                                        autoDismiss: true,
                                    })
                                })
                        }
                    })
                    .catch(error => {
                        console.log(error)
                        addToast('Something wrong. Please Check the server', {
                            appearance: 'error',
                            autoDismiss: true,
                        })
                    })
            }, 1000);
        }

        if (field === "partiesShipperAddress1") {
            values[0].partiesShipperAddress1 = event.target.value

            setValue([
                { partiesShipperAddress1: inputFields[0].partiesShipperAddress1 },
            ])

            clearError('partiesShipperAddress1')
        }

        if (field === "partiesShipperAddress2") {
            values[0].partiesShipperAddress2 = event.target.value

            setValue([
                { partiesShipperAddress2: inputFields[0].partiesShipperAddress2 },
            ])

            clearError('partiesShipperAddress2')
        }

        if (field === "partiesShipperCountryCode") {
            values[0].partiesShipperCountryCode = event.value
            values[0].partiesShipperCountryName = event.label

            setValue([
                { partiesShipperCountryCode: inputFields[0].partiesShipperCountryCode },
            ])

            clearError('partiesShipperCountryCode')
        }

        if (field === "partiesShipperCity") {
            values[0].partiesShipperCity = event.target.value

            setValue([
                { partiesShipperCity: inputFields[0].partiesShipperCity },
            ])

            clearError('partiesShipperCity')
        }

        if (field === "partiesShipperPostalCode") {
            values[0].partiesShipperPostalCode = event.target.value

            setValue([
                { partiesShipperPostalCode: inputFields[0].partiesShipperPostalCode },
            ])

            clearError('partiesShipperPostalCode')
        }

        if (field === "partiesConsignee") {
            values[0].partiesConsignee = event.target.value

            setValue([
                { partiesConsignee: inputFields[0].partiesConsignee },
            ])

            clearError('partiesConsignee')
        }

        if (field === "partiesConsigneeCountryCode") {
            values[0].partiesConsigneeCountryCode = event.value
            values[0].partiesConsigneeCountryName = event.label

            setValue([
                { partiesConsigneeCountryCode: inputFields[0].partiesConsigneeCountryCode },
            ])

            clearError('partiesConsigneeCountryCode')
        }

        if (field === "partiesConsigneeCity") {
            values[0].partiesConsigneeCity = event.target.value

            setValue([
                { partiesConsigneeCity: inputFields[0].partiesConsigneeCity },
            ])

            clearError('partiesConsigneeCity')
        }

        if (field === "partiesConsigneePostalCode") {
            values[0].partiesConsigneePostalCode = event.target.value

            setValue([
                { partiesConsigneePostalCode: inputFields[0].partiesConsigneePostalCode },
            ])

            clearError('partiesConsigneePostalCode')
        }

        if (field === "partiesConsigneeAddress1") {
            values[0].partiesConsigneeAddress1 = event.target.value

            setValue([
                { partiesConsigneeAddress1: inputFields[0].partiesConsigneeAddress1 },
            ])

            clearError('partiesConsigneeAddress1')
        }

        if (field === "partiesConsigneeAddress2") {
            values[0].partiesConsigneeAddress2 = event.target.value

            setValue([
                { partiesConsigneeAddress2: inputFields[0].partiesConsigneeAddress2 },
            ])

            clearError('partiesConsigneeAddress2')
        }

        if (field === "contractPartyName") {
            values[0].contractPartyName = event.target.value
        }

        if (field === "contractPartyCountryCode") {
            values[0].contractPartyCountryCode = event.value
            values[0].contractPartyCountryName = event.label
        }

        if (field === "contractPartyAddress1") {
            values[0].contractPartyAddress1 = event.target.value

            setValue([
                { contractPartyAddress1: inputFields[0].contractPartyAddress1 },
            ])

            clearError('contractPartyAddress1')
        }

        if (field === "contractPartyAddress2") {
            values[0].contractPartyAddress2 = event.target.value

            setValue([
                { contractPartyAddress2: inputFields[0].contractPartyAddress2 },
            ])

            clearError('contractPartyAddress2')
        }

        if (field === "contractPartyCity") {
            values[0].contractPartyCity = event.target.value

            setValue([
                { contractPartyCity: inputFields[0].contractPartyCity },
            ])

            values[0].contractPartyCity = event.target.value
        }

        if (field === "contractPartyPostalCode") {
            values[0].contractPartyPostalCode = event.target.value
        }

        if (field === "notifyPartyName") {
            values[0].notifyPartyName = event.target.value
        }

        if (field === "notifyPartyCountryCode") {
            values[0].notifyPartyCountryCode = event.value
            values[0].notifyPartyCountryName = event.label
        }

        if (field === "notifyPartyAddress1") {
            values[0].notifyPartyAddress1 = event.target.value

            setValue([
                { notifyPartyAddress1: inputFields[0].notifyPartyAddress1 },
            ])

            clearError('notifyPartyAddress1')
        }

        if (field === "notifyPartyAddress2") {
            values[0].notifyPartyAddress2 = event.target.value

            setValue([
                { notifyPartyAddress2: inputFields[0].notifyPartyAddress2 },
            ])

            clearError('notifyPartyAddress2')
        }

        if (field === "notifyPartyCity") {
            values[0].notifyPartyCity = event.target.value

            setValue([
                { notifyPartyCity: inputFields[0].notifyPartyCity },
            ])

            clearError('notifyPartyCity')
        }

        if (field === "notifyPartyPostalCode") {
            values[0].notifyPartyPostalCode = event.target.value
        }

        if (field === "firstNotifyPartyName") {
            values[0].firstNotifyPartyName = event.target.value
        }

        if (field === "firstNotifyPartyCountryCode") {
            values[0].firstNotifyPartyCountryCode = event.value
            values[0].firstNotifyPartyCountryName = event.label
        }

        if (field === "firstNotifyPartyAddress1") {
            values[0].firstNotifyPartyAddress1 = event.target.value

            setValue([
                { firstNotifyPartyAddress1: inputFields[0].firstNotifyPartyAddress1 },
            ])

            clearError('firstNotifyPartyAddress1')
        }

        if (field === "firstNotifyPartyAddress2") {
            values[0].firstNotifyPartyAddress2 = event.target.value

            setValue([
                { firstNotifyPartyAddress2: inputFields[0].firstNotifyPartyAddress2 },
            ])

            clearError('firstNotifyPartyAddress2')
        }

        if (field === "firstNotifyPartyCity") {
            values[0].firstNotifyPartyCity = event.target.value

            setValue([
                { firstNotifyPartyCity: inputFields[0].firstNotifyPartyCity },
            ])

            clearError('firstNotifyPartyCity')
        }

        if (field === "firstNotifyPartyPostalCode") {
            values[0].firstNotifyPartyPostalCode = event.target.value
        }

        if (field === "secondNotifyPartyName") {
            values[0].secondNotifyPartyName = event.target.value
        }

        if (field === "secondNotifyPartyCountryCode") {
            values[0].secondNotifyPartyCountryCode = event.value
            values[0].secondNotifyPartyCountryName = event.label
        }

        if (field === "secondNotifyPartyAddress1") {
            values[0].secondNotifyPartyAddress1 = event.target.value

            setValue([
                { secondNotifyPartyAddress1: inputFields[0].secondNotifyPartyAddress1 },
            ])

            clearError('secondNotifyPartyAddress1')
        }

        if (field === "secondNotifyPartyAddress2") {
            values[0].secondNotifyPartyAddress2 = event.target.value

            setValue([
                { secondNotifyPartyAddress2: inputFields[0].secondNotifyPartyAddress2 },
            ])

            clearError('secondNotifyPartyAddress2')
        }

        if (field === "secondNotifyPartyCity") {
            values[0].secondNotifyPartyCity = event.target.value

            setValue([
                { secondNotifyPartyCity: inputFields[0].secondNotifyPartyCity },
            ])

            clearError('secondNotifyPartyCity')
        }

        if (field === "secondNotifyPartyPostalCode") {
            values[0].secondNotifyPartyPostalCode = event.target.value
        }

        if (field === "additionalPartyNotifyParty1") {
            values[0].additionalPartyNotifyParty1 = event.target.value
        }

        if (field === "additionalPartyNotifyParty") {
            values[0].additionalPartyNotifyParty = event.target.value
        }

        if (field === "additionalPartyNotifyParty2") {
            values[0].additionalPartyNotifyParty2 = event.target.value
        }

        if (field === "referencesShipperRefNumber") {
            values[0].referencesShipperRefNumber = event.target.value
        }

        if (field === "referencesForwarderRefNumber") {
            values[0].referencesForwarderRefNumber = event.target.value
        }

        if (field === "referencesPurchaseOrderNumber") {
            values[0].referencesPurchaseOrderNumber = event.target.value
        }

        if (field === "referencesConsigneeNumber") {
            values[0].referencesConsigneeNumber = event.target.value
        }

        if (field === "referencesBLNumber") {
            values[0].referencesBLNumber = event.target.value
        }

        if (field === "referencesContractParty") {
            values[0].referencesContractParty = event.target.value
        }

        if (field === "customerComments") {
            values[0].customerComments = event.target.value
        }

        if (field === "partnerEmailNotifications") {
            values[0].partnerEmailNotifications = event.target.value

            setValue([
                { partnerEmailNotifications: inputFields[0].partnerEmailNotifications },
            ])

            clearError('partnerEmailNotifications')
        }

        setInputFields(values);
    }

    const handleInputContainer = (field, index, event) => {
        // event.persist()
        const values = [...inputFieldsContainer];

        if (field === "containerType") {
            values[index].containerType = event.value;
            values[index].containerTypeLabel = event.label;

            setValue([
                { [`containerTypeHidden-${index}`]: inputFieldsContainer[index].containerType },
            ])

            clearError([`containerTypeHidden-${index}`])
        }
        if (field === "containerComments") {
            values[index].containerComments = event.target.value;
        }

        setInputFieldsContainer(values);
    }

    const handleAddContainers = () => {
        const values = [...inputFieldsContainer];
        values.push({
            //container
            containerType: '',
            containerTypeLabel: 'Please Select Container Type',
            containerComments: '',
            //container cargo
            cargo: [
                {
                    cargoDescription: '',
                    cargoHsCode: '',
                    cargoHsCodeLabel: 'Enter Number',
                    cargoWeight: '',
                    cargoWeightType: 'KGM',
                    cargoPackageCount: '',
                    cargoPackageType: '',
                    cargoPackageTypeLabel: 'Please select package type',
                    cargoGrossVolume: '',
                    cargoGrossVolumeType: 'MTQ',
                    cargoPrimaryImoClass: '',
                    cargoPrimaryImoClassLabel: 'Please select IMO Class',
                    cargoUndgNumber: '',
                    cargoPackingGroup: '',
                    cargoPackingGroupLabel: 'Please select Packing Group',
                    cargoProperShippingName: '',
                    cargoEmergencyContactName: '',
                    cargoEmergencyContactNumber: '',
                }
            ]
        })
        setInputFieldsContainer(values);
    }

    const handleRemoveContainers = index => {
        const values = [...inputFieldsContainer];
        let test = values.splice(index, 1);
        setInputFieldsContainer(values);
    }

    const handleAddCargos = (indexContainer) => {

        const updatedInputFields = inputFieldsContainer.map((item, i) => {
            if (indexContainer === i) {
                return {
                    ...item, cargo: item.cargo.concat({
                        cargoDescription: '',
                        cargoHsCode: '',
                        cargoHsCodeLabel: 'Enter Number',
                        cargoWeight: '',
                        cargoWeightType: 'KGM',
                        cargoPackageCount: '',
                        cargoPackageType: '',
                        cargoPackageTypeLabel: 'Please select package type',
                        cargoGrossVolume: '',
                        cargoGrossVolumeType: 'MTQ',
                        cargoPrimaryImoClass: '',
                        cargoPrimaryImoClassLabel: 'Please select IMO Class',
                        cargoUndgNumber: '',
                        cargoPackingGroup: '',
                        cargoPackingGroupLabel: 'Please select Packing Group',
                        cargoProperShippingName: '',
                        cargoEmergencyContactName: '',
                        cargoEmergencyContactNumber: '',
                    })
                }
            } else {
                return item
            }
        })

        setInputFieldsContainer(updatedInputFields)
    }

    const handleRemoveCargos = (indexContainer, indexCargo) => {

        const updatedInputFields = inputFieldsContainer.map((item, i) => {
            if (indexContainer === i) {
                return { ...item, cargo: item.cargo.filter((cargo, c) => c !== indexCargo) }
            } else {
                return item
            }
        })

        setInputFieldsContainer(updatedInputFields)
    }

    const handleInputCargo = (field, indexContainer, indexCargo, event) => {
        const values = [...inputFieldsContainer]

        if (field === 'cargoDescription-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoDescription = event.target.value

            setValue([
                { [`cargoDescription-${indexContainer}-${indexCargo}`]: inputFieldsContainer[indexContainer].cargo[indexCargo].cargoDescription }
            ])

            clearError([`cargoDescription-${indexContainer}-${indexCargo}`])
        }
        if (field === 'cargoHsCode-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoHsCode = event.value
            values[indexContainer].cargo[indexCargo].cargoHsCodeLabel = event.label

            setValue([
                { [`cargoHsCode-${indexContainer}-${indexCargo}`]: inputFieldsContainer[indexContainer].cargo[indexCargo].cargoHsCode }
            ])

            clearError([`cargoHsCode-${indexContainer}-${indexCargo}`])
        }
        if (field === 'cargoWeight-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoWeight = event.target.value

            setValue([
                { [`cargoWeight-${indexContainer}-${indexCargo}`]: inputFieldsContainer[indexContainer].cargo[indexCargo].cargoWeight }
            ])

            clearError([`cargoWeight-${indexContainer}-${indexCargo}`])
        }
        if (field === 'cargoWeightType-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoWeightType = event.target.value
        }
        if (field === 'cargoPackageCount-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoPackageCount = event.target.value

            setValue([
                { [`cargoPackageCount-${indexContainer}-${indexCargo}`]: inputFieldsContainer[indexContainer].cargo[indexCargo].cargoPackageCount }
            ])

            clearError([`cargoPackageCount-${indexContainer}-${indexCargo}`])
        }
        if (field === 'cargoPackageType-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoPackageType = event.value
            values[indexContainer].cargo[indexCargo].cargoPackageTypeLabel = event.label

            setValue([
                { [`cargoPackageType-${indexContainer}-${indexCargo}`]: inputFieldsContainer[indexContainer].cargo[indexCargo].cargoPackageType }
            ])

            clearError([`cargoPackageType-${indexContainer}-${indexCargo}`])
        }
        if (field === 'cargoGrossVolume-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoGrossVolume = event.target.value

            setValue([
                { [`cargoGrossVolume-${indexContainer}-${indexCargo}`]: inputFieldsContainer[indexContainer].cargo[indexCargo].cargoGrossVolume }
            ])

            clearError([`cargoGrossVolume-${indexContainer}-${indexCargo}`])
        }
        if (field === 'cargoGrossVolumeType-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoGrossVolumeType = event.target.value
        }
        if (field === 'cargoPrimaryImoClass-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoPrimaryImoClass = event.value
            values[indexContainer].cargo[indexCargo].cargoPrimaryImoClassLabel = event.label

            setValue([
                { [`cargoPrimaryImoClass-${indexContainer}-${indexCargo}`]: inputFieldsContainer[indexContainer].cargo[indexCargo].cargoPrimaryImoClass }
            ])

            clearError([`cargoPrimaryImoClass-${indexContainer}-${indexCargo}`])
        }
        if (field === 'cargoUndgNumber-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoUndgNumber = event.target.value

            setValue([
                { [`cargoUndgNumber-${indexContainer}-${indexCargo}`]: inputFieldsContainer[indexContainer].cargo[indexCargo].cargoUndgNumber }
            ])

            clearError([`cargoUndgNumber-${indexContainer}-${indexCargo}`])
        }
        if (field === 'cargoPackingGroup-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoPackingGroup = event.value
            values[indexContainer].cargo[indexCargo].cargoPackingGroupLabel = event.label

            setValue([
                { [`cargoPackingGroup-${indexContainer}-${indexCargo}`]: inputFieldsContainer[indexContainer].cargo[indexCargo].cargoPackingGroup }
            ])

            clearError([`cargoPackingGroup-${indexContainer}-${indexCargo}`])
        }
        if (field === 'cargoProperShippingName-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoProperShippingName = event.target.value

            setValue([
                { [`cargoProperShippingName-${indexContainer}-${indexCargo}`]: inputFieldsContainer[indexContainer].cargo[indexCargo].cargoProperShippingName }
            ])

            clearError([`cargoProperShippingName-${indexContainer}-${indexCargo}`])
        }
        if (field === 'cargoEmergencyContactName-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoEmergencyContactName = event.target.value

            setValue([
                { [`cargoEmergencyContactName-${indexContainer}-${indexCargo}`]: inputFieldsContainer[indexContainer].cargo[indexCargo].cargoEmergencyContactName }
            ])

            clearError([`cargoEmergencyContactName-${indexContainer}-${indexCargo}`])
        }
        if (field === 'cargoEmergencyContactNumber-' + indexContainer + '-' + indexCargo) {
            values[indexContainer].cargo[indexCargo].cargoEmergencyContactNumber = event.target.value

            setValue([
                { [`cargoEmergencyContactNumber-${indexContainer}-${indexCargo}`]: inputFieldsContainer[indexContainer].cargo[indexCargo].cargoEmergencyContactNumber }
            ])

            clearError([`cargoEmergencyContactNumber-${indexContainer}-${indexCargo}`])
        }

        setInputFieldsContainer(values);
    }

    const handleAddPaymentDetails = () => {
        const values = [...inputFieldsPaymentDetails];
        values.push({
            paymentDetailsChangeType: '',
            paymentDetailsChangeTypeLabel: 'Select one',
            paymentDetailsFreightTerm: '',
            paymentDetailsFreightTermLabel: 'Select one',
            paymentDetailsPayer: '',
            paymentDetailsPayerLabel: 'Select one',
            paymentDetailsPaymentLocation: '',
            paymentDetailsPaymentLocationLabel: 'Enter Location',
        })
        setInputFieldsPaymentDetails(values);
    }

    const handleRemovePaymentDetails = index => {
        const values = [...inputFieldsPaymentDetails];
        values.splice(index, 1);
        setInputFieldsPaymentDetails(values);
    }

    const handleInputPaymentDetails = (field, indexPaymentDetails, event) => {
        const values = [...inputFieldsPaymentDetails]

        if (field === "paymentDetailsChangeType") {
            values[indexPaymentDetails].paymentDetailsChangeType = event.value
            values[indexPaymentDetails].paymentDetailsChangeTypeLabel = event.label

            setValue([
                { [`paymentDetailsChangeType-${indexPaymentDetails}`]: inputFieldsPaymentDetails[indexPaymentDetails].paymentDetailsChangeType },
            ])

            clearError([`paymentDetailsChangeType-${indexPaymentDetails}`])
        }

        if (field === "paymentDetailsFreightTerm") {
            values[indexPaymentDetails].paymentDetailsFreightTerm = event.value
            values[indexPaymentDetails].paymentDetailsFreightTermLabel = event.label

            setValue([
                { [`paymentDetailsFreightTerm-${indexPaymentDetails}`]: inputFieldsPaymentDetails[indexPaymentDetails].paymentDetailsFreightTerm },
            ])

            clearError([`paymentDetailsFreightTerm-${indexPaymentDetails}`])
        }

        if (field === "paymentDetailsPayer") {
            values[indexPaymentDetails].paymentDetailsPayer = event.value
            values[indexPaymentDetails].paymentDetailsPayerLabel = event.label

            setValue([
                { [`paymentDetailsPayer-${indexPaymentDetails}`]: inputFieldsPaymentDetails[indexPaymentDetails].paymentDetailsPayer },
            ])

            clearError([`paymentDetailsPayer-${indexPaymentDetails}`])
        }

        if (field === "paymentDetailsPaymentLocation") {
            values[indexPaymentDetails].paymentDetailsPaymentLocation = event.value
            values[indexPaymentDetails].paymentDetailsPaymentLocationLabel = event.label

            setValue([
                { [`paymentDetailsPaymentLocation-${indexPaymentDetails}`]: inputFieldsPaymentDetails[indexPaymentDetails].paymentDetailsPaymentLocation },
            ])

            clearError([`paymentDetailsPaymentLocation-${indexPaymentDetails}`])
        }

        setInputFieldsPaymentDetails(values);
    }

    useEffect(() => {
        if (errors.shipmentTypeHidden) {
            addToast('Shipment Type is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }
        if (errors.productTypeHidden) {
            addToast('Product Type is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        inputFieldsContainer.map((inputContainer, indexContainer) => {
            if (errors[`containerTypeHidden-${indexContainer}`]) {
                addToast(`Container Type is Required in Container ${indexContainer + 1}`, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
            if (errors[`containerComments-${indexContainer}`]) {
                addToast(`Container Comments is Required in Container ${indexContainer + 1}`, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }

            inputContainer.cargo.map((inputCargo, indexCargo) => {
                if (errors[`cargoDescription-${indexContainer}-${indexCargo}`]) {
                    addToast(`Cargo Description Code is Required in Container ${indexContainer + 1} and Cargo ${indexCargo + 1}`, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }

                if (errors[`cargoHsCode-${indexContainer}-${indexCargo}`]) {
                    addToast(`Cargo Hs Code is Required in Container ${indexContainer + 1} and Cargo ${indexCargo + 1}`, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }

                if (errors[`cargoWeight-${indexContainer}-${indexCargo}`]) {
                    addToast(`Cargo Weight is Required in Container ${indexContainer + 1} and Cargo ${indexCargo + 1}`, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }

                if (errors[`cargoPackageCount-${indexContainer}-${indexCargo}`]) {
                    addToast(`Cargo Package Count is Required in Container ${indexContainer + 1} and Cargo ${indexCargo + 1}`, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }

                if (errors[`cargoPackageType-${indexContainer}-${indexCargo}`]) {
                    addToast(`Cargo Package Type is Required in Container ${indexContainer + 1} and Cargo ${indexCargo + 1}`, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }

                if (errors[`cargoGrossVolume-${indexContainer}-${indexCargo}`]) {
                    addToast(`Cargo Gross Volume is Required in Container ${indexContainer + 1} and Cargo ${indexCargo + 1}`, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }

                if (errors[`cargoPrimaryImoClass-${indexContainer}-${indexCargo}`]) {
                    addToast(`Cargo Primary IMO Class is Required in Container ${indexContainer + 1} and Cargo ${indexCargo + 1}`, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }

                if (errors[`cargoUndgNumber-${indexContainer}-${indexCargo}`]) {
                    if (errors[`cargoUndgNumber-${indexContainer}-${indexCargo}`].type === 'maxLength') {
                        addToast(`Cargo UNDG Number Maximum input Length is 4 in Container ${indexContainer + 1} and Cargo ${indexCargo + 1}`, {
                            appearance: 'error',
                            autoDismiss: true,
                        })
                    } else {
                        addToast(`Cargo UNDG Number is Required in Container ${indexContainer + 1} and Cargo ${indexCargo + 1}`, {
                            appearance: 'error',
                            autoDismiss: true,
                        })
                    }
                }

                if (errors[`cargoPackingGroup-${indexContainer}-${indexCargo}`]) {
                    addToast(`Cargo Packing Group is Required in Container ${indexContainer + 1} and Cargo ${indexCargo + 1}`, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }

                if (errors[`cargoProperShippingName-${indexContainer}-${indexCargo}`]) {
                    addToast(`Cargo Proper Shipping Name is Required in Container ${indexContainer + 1} and Cargo ${indexCargo + 1}`, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }

                if (errors[`cargoEmergencyContactName-${indexContainer}-${indexCargo}`]) {
                    addToast(`Cargo Emergency Contact Name is Required in Container ${indexContainer + 1} and Cargo ${indexCargo + 1}`, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }

                if (errors[`cargoEmergencyContactNumber-${indexContainer}-${indexCargo}`]) {
                    addToast(`Cargo Emergency Contact Number is Required in Container ${indexContainer + 1} and Cargo ${indexCargo + 1}`, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }
            })
        })

        if (errors.transportMoveType) {
            addToast('Transport Move Type is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.preCarriageStart) {
            addToast('Pre Carriage Start is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.preCarriageMode) {
            addToast('Pre Carriage Mode is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.preCarriageETD) {
            addToast('Pre Carriage ETD is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.preCarriageETA) {
            addToast('Pre Carriage ETA is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.onCarriageStart) {
            addToast('On Carriage Start is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.onCarriageMode) {
            addToast('On Carriage Mode is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.onCarriageETD) {
            addToast('On Carriage ETD is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.onCarriageETA) {
            addToast('On Carriage ETA is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.generalDetailsBookingOffice) {
            addToast('General Details Booking Office is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.generalDetailsContractNumber) {
            addToast('General Details Contract Number is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.partiesShipperTaxNumber) {
            if (errors.partiesShipperTaxNumber.type === 'validate') {
                addToast('Shipper Tax Number is not Valid!', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            } else {
                addToast('Shipper Tax Number is Required!', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.partiesShipperAddress1) {
            if (errors.partiesShipperAddress1.type === 'maxLength') {
                addToast('Shipper Address Line 1 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            } else {
                addToast('Shipper Address Line 1 is Required!', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.partiesShipperAddress2) {
            if (errors.partiesShipperAddress2.type === 'maxLength') {
                addToast('Shipper Address Line 2 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.partiesShipperCountryCode) {
            addToast('Shipper Country is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.partiesShipperCity) {
            addToast('Shipper City is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.partiesShipperPostalCode) {
            addToast('Shipper Postal Code is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.partiesConsignee) {
            addToast('Consignee is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.partiesConsigneeCountryCode) {
            addToast('Consignee Country is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.partiesConsigneeCity) {
            addToast('Consignee City is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.partiesConsigneePostalCode) {
            addToast('Consignee Postal Code is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.partiesConsigneeAddress1) {
            if (errors.partiesConsigneeAddress1.type === 'maxLength') {
                addToast('Consignee Address Line 1 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            } else {
                addToast('Consignee Address Line 1 is Required!', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.partiesConsigneeAddress2) {
            if (errors.partiesConsigneeAddress2.type === 'maxLength') {
                addToast('Consignee Address Line 2 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.contractPartyAddress1) {
            if (errors.contractPartyAddress1.type === 'maxLength') {
                addToast('Contract Party Address Line 1 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.contractPartyAddress2) {
            if (errors.contractPartyAddress2.type === 'maxLength') {
                addToast('Contract Party Address Line 2 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.contractPartyCity) {
            if (errors.contractPartyCity.type === 'maxLength') {
                addToast('Contract Party City maximum length is 30', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.notifyPartyAddress1) {
            if (errors.notifyPartyAddress1.type === 'maxLength') {
                addToast('Notify Party Address Line 1 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.notifyPartyAddress2) {
            if (errors.notifyPartyAddress2.type === 'maxLength') {
                addToast('Notify Party Address Line 2 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.notifyPartyCity) {
            if (errors.notifyPartyCity.type === 'maxLength') {
                addToast('Notify Party City maximum length is 30', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.firstNotifyPartyAddress1) {
            if (errors.firstNotifyPartyAddress1.type === 'maxLength') {
                addToast('First Additional Notify Party Address Line 1 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.firstNotifyPartyAddress2) {
            if (errors.firstNotifyPartyAddress2.type === 'maxLength') {
                addToast('First Additional Notify Party Address Line 2 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.firstNotifyPartyCity) {
            if (errors.firstNotifyPartyCity.type === 'maxLength') {
                addToast('First Additional Notify Party City maximum length is 30', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.secondNotifyPartyAddress1) {
            if (errors.secondNotifyPartyAddress1.type === 'maxLength') {
                addToast('Second Additional Notify Party Address Line 1 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.secondNotifyPartyAddress2) {
            if (errors.secondNotifyPartyAddress2.type === 'maxLength') {
                addToast('Second Additional Notify Party Address Line 2 maximum length is 35', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        if (errors.secondNotifyPartyCity) {
            if (errors.secondNotifyPartyCity.type === 'maxLength') {
                addToast('Second Additional Notify Party City maximum length is 30', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        }

        inputFieldsPaymentDetails.map((inputPaymentDetails, indexPaymentDetails) => {
            if (errors[`paymentDetailsChangeType-${indexPaymentDetails}`]) {
                addToast(`Payment Details Charge Type is Required in Payment Details Section ${indexPaymentDetails + 1}`, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }

            if (errors[`paymentDetailsFreightTerm-${indexPaymentDetails}`]) {
                addToast(`Payment Details Freight Term is Required in Payment Details Section ${indexPaymentDetails + 1}`, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }

            if (errors[`paymentDetailsPayer-${indexPaymentDetails}`]) {
                addToast(`Payment Details Payer is Required in Payment Details Section ${indexPaymentDetails + 1}`, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }

            if (errors[`paymentDetailsPaymentLocation-${indexPaymentDetails}`]) {
                addToast(`Payment Details Payment Location is Required in Payment Details Section ${indexPaymentDetails + 1}`, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        })

        if (errors.partnerEmailNotifications) {
            addToast('Partner Email Notifications is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }


    }, [errors])

    const [arrCountryList, setArrCountryList] = useState([])

    useEffect(() => {
        setTimeout(() => {
            //container type list
            axios
                .get(ApiClickargo.CLICKARGO_CONTAINER_TYPE_LIST)
                .then(response => {
                    setArrContainerTypeList(response.data.data)
                })
                .catch(error => {
                    if (error.response) {
                        console.log(error.response.data.message)
                    }
                })
            //package type list
            axios
                .get(ApiClickargo.CLICKARGO_PACKAGE_TYPE_LIST)
                .then(response => {
                    setArrPackageTypeList(response.data.data)
                })
                .catch(error => {
                    if (error.response) {
                        console.log(error.response.data.message)
                    }
                })
            //country list
            axios
                .get(ApiClickargo.CLICKARGO_COUNTRY_LIST)
                .then(response => {
                    setArrCountryList(response.data.data)
                })
                .catch(error => {
                    if (error.response) {
                        console.log(error.response.data.message)
                    }
                })
        }, 1000);
    }, []);

    //hscode
    const fetchData = (inputValue, callback) => {
        if (!inputValue) {
            callback([]);
        } else {
            setTimeout(() => {
                axios
                    .post(ApiClickargo.CLICKARGO_HS_CODE, {
                        hscode: inputValue
                    })
                    .then((data) => {
                        const tempArray = [];
                        data.data.data.forEach((element) => {
                            tempArray.push({ label: `${element.label}`, value: element.id });
                        });
                        callback(tempArray);
                    })
                    .catch((error) => {
                        console.log(error, "catch the hoop")
                    });
            });
        }
    }

    //booking office
    const fetchDataBookingOffice = (inputValue, callback) => {
        if (!inputValue) {
            callback([]);
        } else {
            setTimeout(() => {
                axios
                    .post(ApiClickargo.CLICKARGO_UNLOCS, {
                        data: inputValue
                    })
                    .then((data) => {
                        const tempArray = [];
                        data.data.data.forEach((element) => {
                            tempArray.push({ label: `${element.name}` + ' (' + `${element.code}` + ')', value: element.code });
                        });
                        callback(tempArray);
                    })
                    .catch((error) => {
                        console.log(error, "catch the hoop")
                    });
            });
        }
    }

    let optionCountryList = arrCountryList.map(obj => {
        return { value: obj.iso2, label: obj.name }
    })

    const filterCountryList = (query) => {
        return optionCountryList.filter((el) =>
            // console.log(el)
            el.label.toString().toLowerCase().indexOf(query.toLowerCase()) > -1
        );
    }

    //countries
    const fetchCountries = (inputValue, callback) => {
        if (!inputValue) {
            callback([]);
        } else {
            setTimeout(() => {
                const tempArray = []
                let filterResult = filterCountryList(inputValue)

                filterResult.forEach((element) => {
                    tempArray.push({ label: element.label, value: element.value });
                })
                callback(tempArray)
            }, 1000);
        }
    }

    let optionContainerTypeList = arrContainerTypeList.map(obj => {
        return { value: obj.code, label: obj.size + ` ` + obj.name + ` (` + obj.code + `)` }
    })

    let optionPackageTypeList = arrPackageTypeList.map(obj => {
        return { value: obj.code, label: obj.name }
    })

    let optionHazmatImoClass = [
        { value: '', label: 'Please Select IMO Class' },
        { value: '1', label: '1. Explosives' },
        { value: '1.1', label: '1.1 - Explosives with a mass explosion hazard' },
        { value: '1.2', label: '1.2 - Explosives with a projection hazard' }
    ]

    let optionHazmatPackingGroup = [
        { value: '', label: 'Please Select Packing Group' },
        { value: 'GreatDanger', label: 'Great Danger' },
        { value: 'MediumDanger', label: 'Medium Danger' },
        { value: 'MinorDanger', label: 'Minor Danger' }
    ]

    let optionMoveType = [
        { value: '', label: 'Please Select Move Type' },
        { value: 'DoorToDoor', label: 'Door To Door' },
        { value: 'DoorToPort', label: 'Door To Port' },
        { value: 'PortToDoor', label: 'Port To Door' },
        { value: 'PortToPort', label: 'Port To Port' }
    ]

    let optionPreCarriageMode = [
        { value: '', label: 'Please choose transport mode' },
        { value: 'InlandWaterTransport', label: 'Inland Water Transport' },
        { value: 'MaritimeTransport', label: 'Maritime Transport' },
        { value: 'Rail/RoadTransport', label: 'Rail / Road' },
        { value: 'Rail/WaterTransport', label: 'Rail / Water' },
        { value: 'RailTransport', label: 'Rail Transport' },
        { value: 'Road/WaterTransport', label: 'Road / Water' },
        { value: 'RoadTranport', label: 'Road Tranport' }
    ]

    let optionPaymentDetailsChangeType = [
        { value: '', label: 'Select One' },
        { value: 'AdditionalCharges', label: 'Additional Charges' },
        { value: 'OceanFreight', label: 'Basic Freight' },
        { value: 'DestinationHaulageCharges', label: 'Destination Haulage Charges' },
        { value: 'DestinationTerminalHandling', label: 'Destination Terminal Handling' },
        { value: 'OriginHaulageCharges', label: 'Origin Haulage CHarges' },
        { value: 'OriginTerminalHandling', label: 'Origin Terminal Handling' },
    ]

    let optionPaymentDetailsFreightTerm = [
        { value: '', label: 'Select One' },
        { value: 'PrePaid', label: 'Pre-Paid' },
        { value: 'Collect', label: 'Collect' },
        { value: 'ThirdParty', label: 'Payable Elsewhere' },
    ]

    let optionPaymentDetailsPayer = [
        { value: '', label: 'Select One' },
        { value: 'Booker', label: 'Booker' },
        { value: 'Consignee', label: 'Consignee' },
        { value: 'Forwarder', label: 'Forwarder' },
        { value: 'Shipper', label: 'Shipper' },
        { value: 'ContractParty', label: 'Contract Party' },
        { value: 'FirstAdditionalNotifyParty', label: 'First Additional Notify Party' },
        { value: 'SecondAdditionalNotifyParty', label: 'Second Additional Notify Party' },
        { value: 'MainNotifyParty', label: 'Main Notify Party' },
    ]

    const onSubmit = () => {
        // event.preventDefault()

        console.log('run submit handler')

        props.history.push({
            pathname: '/booking_request/review',
            state: {
                generalArr: inputFields,
                containerArr: inputFieldsContainer,
                paymentDetailsArr: inputFieldsPaymentDetails,
                shipperTaxNumber: inputShipperTaxNumber
            }
        })


    }

    const [uploadContainerData, setUploadContainerData] = useState([
        {
            file: '',
            product_type: '',
            shipment_type: '',
        }
    ]);

    const handleChangeUploadContainer = (field, event) => {
        const values = [...uploadContainerData];

        if (field === "documentContainer") {
            values[0].file = event.target.files[0]
        }

        setUploadContainerData(values)
    }

    const onSubmitUploadContainerData = e => {

        setTimeout(() => {
            const headers = {
                'Content-Type': 'multipart/form-data'
            }

            let formData = new FormData()

            formData.append('file', uploadContainerData[0].file)
            formData.append('product_type', uploadContainerData[0].product_type)
            formData.append('shipment_type', uploadContainerData[0].shipment_type)

            axios
                .post(ApiClickargo.CLICKARGO_CONVERT_CONTAINER_EXCEL, formData, {
                    headers: headers
                })
                .then(response => {
                    console.log(response.data.data)
                    setTimeout(() => {
                        const values = [...inputFieldsContainer];
                        values.splice(0, values.length)
                        if (uploadContainerData[0].product_type == 'special' || 'dangerous') {
                            response.data.data.container.map((inputContainerArr, indexContainerArr) => {
                                values.push({
                                    //container
                                    containerType: inputContainerArr.type.value,
                                    containerTypeLabel: inputContainerArr.type.label,
                                    containerComments: inputContainerArr.container_comments,
                                    //container cargo
                                    cargo:
                                        inputContainerArr.cargo.map((inputCargo, indexCargo) => (
                                            {
                                                cargoDescription: inputCargo.cargo_description,
                                                cargoHsCode: inputCargo.cargo_hs_code.value,
                                                cargoHsCodeLabel: inputCargo.cargo_hs_code.value + ' (' + inputCargo.cargo_hs_code.label + ')',
                                                cargoWeight: inputCargo.cargo_weight,
                                                cargoWeightType: inputCargo.cargo_weight_type,
                                                cargoPackageCount: inputCargo.cargo_package_count,
                                                cargoPackageType: inputCargo.cargo_package_type.value,
                                                cargoPackageTypeLabel: inputCargo.cargo_package_type.label,
                                                cargoGrossVolume: inputCargo.cargo_gross_vol,
                                                cargoGrossVolumeType: inputCargo.cargo_vol_type,
                                                cargoPrimaryImoClass: inputCargo.cargo_imo.value,
                                                cargoPrimaryImoClassLabel: inputCargo.cargo_imo.label,
                                                cargoUndgNumber: inputCargo.cargo_undg,
                                                cargoPackingGroup: inputCargo.cargo_packing_group.value,
                                                cargoPackingGroupLabel: inputCargo.cargo_packing_group.label,
                                                cargoProperShippingName: inputCargo.cargo_proper_shipping,
                                                cargoEmergencyContactName: inputCargo.cargo_emergency_contact_name,
                                                cargoEmergencyContactNumber: inputCargo.cargo_emergency_contact_number,
                                            }
                                        ))
                                })
                                setInputFieldsContainer(values)
                            })

                            setTimeout(() => {
                                response.data.data.container.map((inputContainerArr, indexContainerArr) => {
                                    console.log(inputContainerArr)
                                    setValue([
                                        { [`containerTypeHidden-${indexContainerArr}`]: inputContainerArr.type.value },
                                        { [`containerComments-${indexContainerArr}`]: inputContainerArr.container_comments },
                                    ])
                                    clearError(`containerTypeHidden-${indexContainerArr}`)
                                    clearError(`containerComments-${indexContainerArr}`)
                                })


                                response.data.data.container.map((inputContainerArr, indexContainerArr) => {
                                    inputContainerArr.cargo.map((inputCargoArr, indexCargoArr) => {
                                        setValue([
                                            { [`cargoDescription-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_description },
                                            { [`cargoHsCode-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_hs_code.value },
                                            { [`cargoWeight-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_weight },
                                            { [`cargoPackageCount-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_package_count },
                                            { [`cargoPackageType-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_package_type.value },
                                            { [`cargoGrossVolume-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_gross_vol },
                                            { [`cargoPrimaryImoClass-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_imo.value },
                                            { [`cargoUndgNumber-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_undg },
                                            { [`cargoPackingGroup-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_packing_group.value },
                                            { [`cargoProperShippingName-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_proper_shipping },
                                            { [`cargoEmergencyContactName-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_emergency_contact_name },
                                            { [`cargoEmergencyContactNumber-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_emergency_contact_number },
                                        ])
                                        clearError(`cargoDescription-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoHsCode-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoWeight-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoPackageCount-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoPackageType-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoGrossVolume-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoPrimaryImoClass-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoUndgNumber-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoPackingGroup-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoProperShippingName-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoEmergencyContactName-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoEmergencyContactNumber-${indexContainerArr}-${indexCargoArr}`)
                                    })
                                })

                            }, 1000);
                        } else {
                            response.data.data.container.map((inputContainerArr, indexContainerArr) => {
                                values.push({
                                    //container
                                    containerType: inputContainerArr.type.value,
                                    containerTypeLabel: inputContainerArr.type.label,
                                    containerComments: inputContainerArr.container_comments,
                                    //container cargo
                                    cargo:
                                        inputContainerArr.cargo.map((inputCargo, indexCargo) => (
                                            {
                                                cargoDescription: inputCargo.cargo_description,
                                                cargoHsCode: inputCargo.cargo_hs_code.value,
                                                cargoHsCodeLabel: inputCargo.cargo_hs_code.value + ' (' + inputCargo.cargo_hs_code.label + ')',
                                                cargoWeight: inputCargo.cargo_weight,
                                                cargoWeightType: inputCargo.cargo_weight_type,
                                                cargoPackageCount: inputCargo.cargo_package_count,
                                                cargoPackageType: inputCargo.cargo_package_type.value,
                                                cargoPackageTypeLabel: inputCargo.cargo_package_type.label,
                                                cargoGrossVolume: inputCargo.cargo_gross_vol,
                                                cargoGrossVolumeType: inputCargo.cargo_vol_type,
                                            }
                                        ))
                                })
                                setInputFieldsContainer(values)
                            })

                            setTimeout(() => {
                                response.data.data.container.map((inputContainerArr, indexContainerArr) => {
                                    console.log(inputContainerArr)
                                    setValue([
                                        { [`containerTypeHidden-${indexContainerArr}`]: inputContainerArr.type.value },
                                        { [`containerComments-${indexContainerArr}`]: inputContainerArr.container_comments },
                                    ])
                                    clearError(`containerTypeHidden-${indexContainerArr}`)
                                    clearError(`containerComments-${indexContainerArr}`)
                                })


                                response.data.data.container.map((inputContainerArr, indexContainerArr) => {
                                    inputContainerArr.cargo.map((inputCargoArr, indexCargoArr) => {
                                        setValue([
                                            { [`cargoDescription-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_description },
                                            { [`cargoHsCode-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_hs_code.value },
                                            { [`cargoWeight-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_weight },
                                            { [`cargoPackageCount-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_package_count },
                                            { [`cargoPackageType-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_package_type.value },
                                            { [`cargoGrossVolume-${indexContainerArr}-${indexCargoArr}`]: inputCargoArr.cargo_gross_vol },
                                        ])
                                        clearError(`cargoDescription-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoHsCode-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoWeight-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoPackageCount-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoPackageType-${indexContainerArr}-${indexCargoArr}`)
                                        clearError(`cargoGrossVolume-${indexContainerArr}-${indexCargoArr}`)
                                    })
                                })

                            }, 1000);
                        }
                        setModal(!modal)
                        Swal.fire({
                            icon: 'success',
                            title: 'Success Upload Container Data!',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }, 1000);
                })
                .catch(error => {
                    Swal.fire({
                        icon: 'error',
                        title: error.response.data.message[0],
                    })
                })
        }, 1000);
    }

    //onclick download button
    const downloadAction = () => {
        console.log('action download run')
        let a = document.createElement('a')
        let urlGeneral = ApiClickargo.CLIKARGO_DOMAIN + 'assets/dashboard/file/Form%20Container%20Export%20General.xlsx';
        let urlSpecial = ApiClickargo.CLIKARGO_DOMAIN + 'assets/dashboard/file/Form%20Container%20Export%20Special.xlsx';
        let urlDangerous = ApiClickargo.CLIKARGO_DOMAIN + 'assets/dashboard/file/Form%20Container%20Export%20Dangerous.xlsx';
        let urlOthers = ApiClickargo.CLIKARGO_DOMAIN + 'assets/dashboard/file/Form%20Container%20Export%20Others.xlsx';
        if (inputFields[0].productType === 'general') {
            a.href = urlGeneral
        } else if (inputFields[0].productType === 'special') {
            a.href = urlSpecial
        } else if (inputFields[0].productType === 'dangerous') {
            a.href = urlDangerous
        } else if (inputFields[0].productType === 'others') {
            a.href = urlOthers
        }
        a.click()
    }

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div style={{ marginBottom: "10px" }}>
                    <div style={{ float: "left" }}>
                        <h3 style={{ display: "inline" }}>Details and Booking Request</h3>
                    </div>
                    <div style={{ float: "right" }}>
                        <img style={{ width: '200px' }} src={window.location.origin + '/assets/images/Clickargo5.png'} />
                    </div>
                </div>

                <br />
                <br />

                <div style={{ marginBottom: "10px" }}>
                    <Link to="/vessel" style={{ display: "inline", color: "#2d9ff7" }} >
                        Back
                    </Link>
                </div>

                <div className="kt-portlet kt-portlet--height-fluid">
                    <div className="kt-portlet__body">
                        <div className="kt-widget15">
                            <div className="row">

                                <div className="col-md-4">
                                    <label >Action Type</label>
                                    <h6 className={dashforgeCss.txBlack}>Export</h6>
                                </div>

                                <div className="col-md-4">
                                    <label >Freight Mode</label>
                                    <h6 className={dashforgeCss.txBlack}>Ocean</h6>
                                </div>

                                <div className="col-md-4">
                                    <label >Transaction</label>
                                    <h6 className={dashforgeCss.txBlack}>Freight</h6>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div className="kt-portlet kt-portlet--height-fluid">
                    <div className="kt-portlet__head">

                        <div className="kt-portlet__head-label">
                            <h3 className="kt-portlet__head-title">
                            </h3>
                        </div>

                        {isProductTypeChanged && <div className="kt-portlet__head-toolbar">
                            <Button type="button" className="btn btn-outline-brand" onClick={downloadAction}>
                                <i className="la la-cloud-download"></i>
                                <span className="kt-menu__link-text">Download</span>
                            </Button>
                                &nbsp;
                                <Button type="button" className="btn btn-outline-brand" onClick={toggle}>
                                <i className="la la-cloud-upload"></i>
                                <span className="kt-menu__link-text">Upload</span>
                            </Button>
                        </div>}
                    </div>

                    <div className="kt-portlet__body">
                        <div className="kt-widget15">
                            <div className="row">

                                {inputFields.map((input, index) => (
                                    <Fragment key={`${input}~${index}`}>
                                        <div className="col-md-6" >
                                            <label className={dashforgeCss.txBlack}>Select Shipment Type</label><label style={{ color: "red" }}>*</label>
                                            <input
                                                type="hidden"
                                                name="shipmentTypeHidden"
                                                ref={register({
                                                    required: {
                                                        value: true,
                                                        message: 'This input field is required!'
                                                    }
                                                })} />
                                            <Select
                                                defaultValue={{ label: input.shipmentTypeLabel, value: input.shipmentType }}
                                                options={optionShipmentType}
                                                onChange={event => handleInputChange('shipmentType', event)} />
                                            {errors.shipmentTypeHidden && <span style={{ fontSize: 13, color: "red" }}>{errors.shipmentTypeHidden.message}</span>}
                                        </div>

                                        <div className="col-md-6">
                                            <label className={dashforgeCss.txBlack}>Select Product Type</label><label style={{ color: "red" }}>*</label>
                                            <input
                                                type="hidden"
                                                name="productTypeHidden"
                                                ref={register({
                                                    required: {
                                                        value: true,
                                                        message: 'This input field is required!'
                                                    }
                                                })} />
                                            <Select
                                                defaultValue={{ label: input.productTypeLabel, value: input.productType }}
                                                options={optionProductType}
                                                onChange={event => handleInputChange('productType', event)} />
                                            {errors.productTypeHidden && <span style={{ fontSize: 13, color: "red" }}>{errors.productTypeHidden.message}</span>}
                                        </div>
                                    </Fragment>
                                ))}

                            </div>

                            {inputFieldsContainer.map((inputContainer, indexContainer) => (
                                <Fragment key={`${inputContainer}~${indexContainer}`}>
                                    <div className="row" style={{ paddingTop: "15px" }}>
                                        <div className="col-md-12">

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>CONTAINER</legend>

                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <h4 className={dashforgeCss.txBlack}>Container {indexContainer + 1}</h4>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="form-group float-right">
                                                            {isFullContainerLoaded && <button
                                                                type="button"
                                                                className="btn btn-xl btn-outline-primary"
                                                                onClick={() => handleAddContainers()}>+</button>}
                                                            &nbsp;
                                                            {indexContainer !== 0 && <button
                                                                type="button"
                                                                className="btn btn-xl btn-outline-primary"
                                                                onClick={() => handleRemoveContainers(indexContainer)}>-</button>}
                                                        </div>
                                                    </div>

                                                    <div className="col-3">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Container Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`containerTypeHidden-${indexContainer}`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <Select
                                                                value={{ label: inputContainer.containerTypeLabel, value: inputContainer.containerType }}
                                                                options={optionContainerTypeList}
                                                                onChange={event => handleInputContainer('containerType', indexContainer, event)} />
                                                            {errors[`containerTypeHidden-${indexContainer}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`containerTypeHidden-${indexContainer}`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-md-9">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Container Comments<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                placeholder="Enter Comments"
                                                                className="form-control"
                                                                value={inputContainer.containerComments}
                                                                onChange={event => handleInputContainer('containerComments', indexContainer, event)}
                                                                name={`containerComments-${indexContainer}`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            {errors[`containerComments-${indexContainer}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`containerComments-${indexContainer}`].message}</span>}
                                                        </div>
                                                    </div>
                                                </div>


                                                {inputContainer.cargo.map((inputCargo, indexCargo) => (
                                                    <Fragment key={`${inputCargo}~${indexCargo}`}>
                                                        <div className="cargo_form">
                                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '10px', marginTop: '10px' }}>

                                                                <div className="row">
                                                                    <div className="col-md-8">
                                                                        <h6 className={dashforgeCss.txBlack}>Cargo</h6>
                                                                        <span className={dashforgeCss.tx12 + ' ' + dashforgeCss.txBlack}><div className={dashforgeCss.txDanger}>NOTE:</div> The sum of all Cargo Weights reflects the Gross Weight of the Cargo (excluding Tare) for the entire booking.</span>
                                                                        <p className={dashforgeCss.tx12 + ' ' + dashforgeCss.txBlack}>Inaccurate declaration of cargo weight may endanger crew, port worker, and vessel safety. Please verify the reasonable accuracy of this information prior to submission</p>
                                                                    </div>
                                                                    <div className="col-md-4">
                                                                        <div className="form-group float-right" style={{ marginTop: '25px' }}>
                                                                            <button
                                                                                type="button"
                                                                                className="btn btn-md btn-outline-primary"
                                                                                onClick={() => handleAddCargos(indexContainer)}>+</button>
                                                                        &nbsp;
                                                                        {indexCargo !== 0 && <button
                                                                                type="button"
                                                                                className="btn btn-md btn-outline-primary"
                                                                                onClick={() => handleRemoveCargos(indexContainer, indexCargo)}>-</button>}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                                    <div className="row">

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                {/* max length 35 - requested 11 june 2020 */}
                                                                                <label className={dashforgeCss.txBlack}>Cargo Description<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                <input
                                                                                    type="hidden"
                                                                                    name={`cargoDescription-${indexContainer}-${indexCargo}`}
                                                                                    ref={register({
                                                                                        required: {
                                                                                            value: true,
                                                                                            message: 'This input field is required!'
                                                                                        }
                                                                                    })} />
                                                                                <input
                                                                                    type="text"
                                                                                    placeholder="Enter Description"
                                                                                    className="form-control"
                                                                                    value={inputCargo.cargoDescription}
                                                                                    onChange={event => handleInputCargo('cargoDescription-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)} />
                                                                                {errors[`cargoDescription-${indexContainer}-${indexCargo}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`cargoDescription-${indexContainer}-${indexCargo}`].message}</span>}
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <div>
                                                                                    <label className={dashforgeCss.txBlack}>Package Count<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                    <input
                                                                                        type="hidden"
                                                                                        name={`cargoPackageCount-${indexContainer}-${indexCargo}`}
                                                                                        ref={register({
                                                                                            required: {
                                                                                                value: true,
                                                                                                message: 'This input field is required!'
                                                                                            }
                                                                                        })} />
                                                                                    <input
                                                                                        type="number"
                                                                                        placeholder="Enter Count"
                                                                                        className="form-control"
                                                                                        value={inputCargo.cargoPackageCount}
                                                                                        onChange={event => handleInputCargo('cargoPackageCount-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)} />
                                                                                    {errors[`cargoPackageCount-${indexContainer}-${indexCargo}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`cargoPackageCount-${indexContainer}-${indexCargo}`].message}</span>}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>HS Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                <div className="custom-form-group">
                                                                                    <input
                                                                                        type="hidden"
                                                                                        name={`cargoHsCode-${indexContainer}-${indexCargo}`}
                                                                                        ref={register({
                                                                                            required: {
                                                                                                value: true,
                                                                                                message: 'This input field is required!'
                                                                                            }
                                                                                        })} />
                                                                                    <AsyncSelect
                                                                                        value={{ label: inputCargo.cargoHsCodeLabel, value: inputCargo.cargoHsCode }}
                                                                                        loadOptions={fetchData}
                                                                                        onChange={event => handleInputCargo('cargoHsCode-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)}
                                                                                        defaultOptions={false} />
                                                                                    {errors[`cargoHsCode-${indexContainer}-${indexCargo}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`cargoHsCode-${indexContainer}-${indexCargo}`].message}</span>}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Package Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                <div className="custom-form-group">
                                                                                    <input
                                                                                        type="hidden"
                                                                                        name={`cargoPackageType-${indexContainer}-${indexCargo}`}
                                                                                        ref={register({
                                                                                            required: {
                                                                                                value: true,
                                                                                                message: 'This input field is required!'
                                                                                            }
                                                                                        })} />
                                                                                    <Select
                                                                                        value={{ label: inputCargo.cargoPackageTypeLabel, value: inputCargo.cargoPackageType }}
                                                                                        options={optionPackageTypeList}
                                                                                        onChange={event => handleInputCargo('cargoPackageType-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)} />
                                                                                    {errors[`cargoPackageType-${indexContainer}-${indexCargo}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`cargoPackageType-${indexContainer}-${indexCargo}`].message}</span>}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Cargo Weight (Excludes Tares)<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                <div className="input-group" id="cargo_wrapper" style={{ flexDirection: 'row' }}>
                                                                                    <div className="cargo-weight-wrapper" style={{ display: 'flex' }}>
                                                                                        <input
                                                                                            type="hidden"
                                                                                            name={`cargoWeight-${indexContainer}-${indexCargo}`}
                                                                                            ref={register({
                                                                                                required: {
                                                                                                    value: true,
                                                                                                    message: 'This input field is required!'
                                                                                                }
                                                                                            })} />
                                                                                        <input
                                                                                            type="number"
                                                                                            className="form-control"
                                                                                            placeholder="Enter Weight"
                                                                                            value={inputCargo.cargoWeight}
                                                                                            onChange={event => handleInputCargo('cargoWeight-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)} />
                                                                                        <div className="input-group-append">
                                                                                            <select
                                                                                                className="dropdown-toggle"
                                                                                                type="button"
                                                                                                data-toggle="dropdown"
                                                                                                aria-haspopup="true"
                                                                                                aria-expanded="false"
                                                                                                onChange={event => handleInputCargo('cargoWeightType-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)}>
                                                                                                <option value="KGM" className="dropdown-item" selected={'KGM' === inputCargo.cargoWeightType}>Kgs</option>
                                                                                                <option value="LBS" className="dropdown-item" selected={'LBS' === inputCargo.cargoWeightType}>Pounds</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                {errors[`cargoWeight-${indexContainer}-${indexCargo}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`cargoWeight-${indexContainer}-${indexCargo}`].message}</span>}
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Gross Volume<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                <div className="input-group" style={{ flexDirection: 'row' }}>
                                                                                    <div className="gross-volume-wrapper" style={{ display: 'flex' }}>
                                                                                        <input
                                                                                            type="hidden"
                                                                                            name={`cargoGrossVolume-${indexContainer}-${indexCargo}`}
                                                                                            ref={register({
                                                                                                required: {
                                                                                                    value: true,
                                                                                                    message: 'This input field is required!'
                                                                                                }
                                                                                            })} />
                                                                                        <input
                                                                                            type="number"
                                                                                            className="form-control"
                                                                                            placeholder="Enter Volume"
                                                                                            value={inputCargo.cargoGrossVolume}
                                                                                            onChange={event => handleInputCargo('cargoGrossVolume-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)} />
                                                                                        <div className="input-group-append">
                                                                                            <select
                                                                                                className="dropdown-toggle vol_type"
                                                                                                type="button"
                                                                                                data-toggle="dropdown"
                                                                                                aria-haspopup="true"
                                                                                                aria-expanded="false"
                                                                                                onChange={event => handleInputCargo('cargoGrossVolumeType-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)}>
                                                                                                <option value="MTQ" selected={'MTQ' === inputCargo.cargoGrossVolumeType}>Cubic meters</option>
                                                                                                <option value="FTQ" selected={'FTQ' === inputCargo.cargoGrossVolumeType}>Cubic feet</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                {errors[`cargoGrossVolume-${indexContainer}-${indexCargo}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`cargoGrossVolume-${indexContainer}-${indexCargo}`].message}</span>}
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    {isHazmat && <Fragment>
                                                                        <div className="row">
                                                                            <div className="col-12">
                                                                                <h6 className={dashforgeCss.txBlack}>Hazmat Details</h6>
                                                                            </div>
                                                                        </div>

                                                                        <div className="row">
                                                                            <div className="col-12">
                                                                                <div className="form-group">
                                                                                    <label className={dashforgeCss.txBlack}>Primary IMO Class<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                    <input
                                                                                        type="hidden"
                                                                                        name={`cargoPrimaryImoClass-${indexContainer}-${indexCargo}`}
                                                                                        ref={register({
                                                                                            required: {
                                                                                                value: true,
                                                                                                message: 'This input field is required!'
                                                                                            }
                                                                                        })} />
                                                                                    <Select
                                                                                        value={{ label: inputCargo.cargoPrimaryImoClassLabel, value: inputCargo.cargoPrimaryImoClass }}
                                                                                        options={optionHazmatImoClass}
                                                                                        onChange={event => handleInputCargo('cargoPrimaryImoClass-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)}
                                                                                    />
                                                                                    {errors[`cargoPrimaryImoClass-${indexContainer}-${indexCargo}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`cargoPrimaryImoClass-${indexContainer}-${indexCargo}`].message}</span>}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div className="row" style={{ marginBottom: '15px' }}>

                                                                            <div className="col-6">
                                                                                <div className="form-group">
                                                                                    <label className={dashforgeCss.txBlack}>UNDG Number<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                    <input
                                                                                        type="hidden"
                                                                                        name={`cargoUndgNumber-${indexContainer}-${indexCargo}`}
                                                                                        ref={register({
                                                                                            required: {
                                                                                                value: true,
                                                                                                message: 'This input field is required!'
                                                                                            },
                                                                                            maxLength: {
                                                                                                value: 4,
                                                                                                message: 'Maximum input Length is 4'
                                                                                            }
                                                                                        })} />
                                                                                    <input
                                                                                        type="number"
                                                                                        className="form-control"
                                                                                        value={inputCargo.cargoUndgNumber}
                                                                                        onChange={event => handleInputCargo('cargoUndgNumber-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)} />
                                                                                    {errors[`cargoUndgNumber-${indexContainer}-${indexCargo}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`cargoUndgNumber-${indexContainer}-${indexCargo}`].message}</span>}
                                                                                </div>
                                                                            </div>

                                                                            <div className="col-6">
                                                                                <div className="form-group">
                                                                                    <label className={dashforgeCss.txBlack}>Emergency Contact Name<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                    <input
                                                                                        type="hidden"
                                                                                        name={`cargoEmergencyContactName-${indexContainer}-${indexCargo}`}
                                                                                        ref={register({
                                                                                            required: {
                                                                                                value: true,
                                                                                                message: 'This input field is required!'
                                                                                            }
                                                                                        })} />
                                                                                    <input
                                                                                        type="text"
                                                                                        className="form-control"
                                                                                        value={inputCargo.cargoEmergencyContactName}
                                                                                        onChange={event => handleInputCargo('cargoEmergencyContactName-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)} />
                                                                                    {errors[`cargoEmergencyContactName-${indexContainer}-${indexCargo}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`cargoEmergencyContactName-${indexContainer}-${indexCargo}`].message}</span>}
                                                                                </div>
                                                                            </div>

                                                                            <div className="col-6">
                                                                                <div className="form-group">
                                                                                    <label className={dashforgeCss.txBlack}>Packing Group<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                    <input
                                                                                        type="hidden"
                                                                                        name={`cargoPackingGroup-${indexContainer}-${indexCargo}`}
                                                                                        ref={register({
                                                                                            required: {
                                                                                                value: true,
                                                                                                message: 'This input field is required!'
                                                                                            }
                                                                                        })} />
                                                                                    <Select
                                                                                        value={{ label: inputCargo.cargoPackingGroupLabel, value: inputCargo.cargoPackingGroup }}
                                                                                        options={optionHazmatPackingGroup}
                                                                                        onChange={event => handleInputCargo('cargoPackingGroup-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)} />
                                                                                    {errors[`cargoPackingGroup-${indexContainer}-${indexCargo}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`cargoPackingGroup-${indexContainer}-${indexCargo}`].message}</span>}
                                                                                </div>
                                                                            </div>

                                                                            <div className="col-6">
                                                                                <div className="form-group">
                                                                                    <label className={dashforgeCss.txBlack}>Emergency Contact Number<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                    <input
                                                                                        type="hidden"
                                                                                        name={`cargoEmergencyContactNumber-${indexContainer}-${indexCargo}`}
                                                                                        ref={register({
                                                                                            required: {
                                                                                                value: true,
                                                                                                message: 'This input field is required!'
                                                                                            }
                                                                                        })} />
                                                                                    <input
                                                                                        type="text"
                                                                                        className="form-control"
                                                                                        value={inputCargo.cargoEmergencyContactNumber}
                                                                                        onChange={event => handleInputCargo('cargoEmergencyContactNumber-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)} />
                                                                                    {errors[`cargoEmergencyContactNumber-${indexContainer}-${indexCargo}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`cargoEmergencyContactNumber-${indexContainer}-${indexCargo}`].message}</span>}
                                                                                </div>
                                                                            </div>

                                                                            <div className="col-6">
                                                                                <div className="form-group">
                                                                                    <label className={dashforgeCss.txBlack}>Proper Shipping Name<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                    <input
                                                                                        type="hidden"
                                                                                        name={`cargoProperShippingName-${indexContainer}-${indexCargo}`}
                                                                                        ref={register({
                                                                                            required: {
                                                                                                value: true,
                                                                                                message: 'This input field is required!'
                                                                                            }
                                                                                        })} />
                                                                                    <input
                                                                                        type="text"
                                                                                        className="form-control"
                                                                                        value={inputCargo.cargoProperShippingName}
                                                                                        onChange={event => handleInputCargo('cargoProperShippingName-' + indexContainer + '-' + indexCargo, indexContainer, indexCargo, event)} />
                                                                                    {errors[`cargoProperShippingName-${indexContainer}-${indexCargo}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`cargoProperShippingName-${indexContainer}-${indexCargo}`].message}</span>}
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </Fragment>
                                                                    }

                                                                </fieldset>



                                                            </fieldset>
                                                        </div>
                                                    </Fragment>
                                                ))}

                                            </fieldset>


                                        </div>
                                    </div>

                                </Fragment>
                            ))}

                            <div className="row">
                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>GENERAL DETAILS</legend>

                                        <div className="row">
                                            {inputFields.map((input, index) => (
                                                <Fragment key={`${input}~${index}`}>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Carrier/NVOCC/Booking Agent<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                disabled="disabled"
                                                                value={input.generalDetailsCarrierLabel} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Booking Office<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`generalDetailsBookingOffice`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <AsyncSelect
                                                                value={{ label: input.generalDetailsBookingOfficeLabel, value: input.generalDetailsBookingOffice }}
                                                                loadOptions={fetchDataBookingOffice}
                                                                placeholder="Enter Location"
                                                                onChange={event => handleInputChange('generalDetailsBookingOffice', event)}
                                                            />
                                                            {errors[`generalDetailsBookingOffice`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`generalDetailsBookingOffice`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Contract Number<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`generalDetailsContractNumber`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                placeholder="Enter Number"
                                                                className="form-control"
                                                                value={input.generalDetailsContractNumber}
                                                                onChange={event => handleInputChange('generalDetailsContractNumber', event)} />
                                                            {errors[`generalDetailsContractNumber`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`generalDetailsContractNumber`].message}</span>}
                                                        </div>
                                                    </div>

                                                </Fragment>
                                            ))}

                                        </div>

                                    </fieldset>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>PARTIES</legend>

                                        {inputFields.map((input, index) => (
                                            <Fragment key={`${input}~${index}`}>


                                                <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                    <legend>SHIPPER</legend>

                                                    <div className="row">

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Shipper Name</label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    value={input.partiesShipperName}
                                                                    onChange={event => handleInputChange('partiesShipperName', event)} />
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Shipper INTTRA ID</label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    value={input.partiesShipperInttraId}
                                                                    onChange={event => handleInputChange('partiesShipperInttraId', event)} />
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Shipper Tax Number<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`partiesShipperTaxNumber`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        },
                                                                        validate: value => value === inputShipperTaxNumber
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    placeholder="Enter Shipper's Tax Number"
                                                                    value={input.partiesShipperTaxNumber}
                                                                    onChange={event => handleInputChange('partiesShipperTaxNumber', event)} />
                                                                {errors[`partiesShipperTaxNumber`] && errors[`partiesShipperTaxNumber`].type === 'required' && <span style={{ fontSize: 13, color: "red" }}>This input field is required!</span>}
                                                                {errors[`partiesShipperTaxNumber`] && errors[`partiesShipperTaxNumber`].type === 'validate' && <span style={{ fontSize: 13, color: "red" }}>Shipper Tax Number is not Valid!</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6"></div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Shipper Address Line 1<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`partiesShipperAddress1`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        },
                                                                        maxLength: {
                                                                            value: 35,
                                                                            message: 'Maximum input length is 35'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Shipper Address Line 1"
                                                                    className="form-control"
                                                                    value={input.partiesShipperAddress1}
                                                                    onChange={event => handleInputChange('partiesShipperAddress1', event)} />
                                                                {errors[`partiesShipperAddress1`] && errors[`partiesShipperAddress1`].type === 'required' && <span style={{ fontSize: 13, color: "red" }}>This input field is required!</span>}
                                                                {errors[`partiesShipperAddress1`] && errors[`partiesShipperAddress1`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Shipper City<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`partiesShipperCity`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Enter Shipper City"
                                                                    className="form-control"
                                                                    value={input.partiesShipperCity}
                                                                    onChange={event => handleInputChange('partiesShipperCity', event)} />
                                                                {errors[`partiesShipperCity`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`partiesShipperCity`].message}</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Shipper Address Line 2</label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`partiesShipperAddress2`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 35,
                                                                            message: 'Maximum input length is 35'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Shipper Address Line 2"
                                                                    className="form-control"
                                                                    value={input.partiesShipperAddress2}
                                                                    onChange={event => handleInputChange('partiesShipperAddress2', event)} />
                                                                {errors[`partiesShipperAddress2`] && errors[`partiesShipperAddress2`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Shipper Postal Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`partiesShipperPostalCode`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Enter Shipper Postal Code"
                                                                    className="form-control"
                                                                    value={input.partiesShipperPostalCode}
                                                                    onChange={event => handleInputChange('partiesShipperPostalCode', event)} />
                                                                {errors[`partiesShipperPostalCode`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`partiesShipperPostalCode`].message}</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Shipper Country<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`partiesShipperCountryCode`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        }
                                                                    })} />
                                                                <AsyncSelect
                                                                    value={{ label: input.partiesShipperCountryName, value: input.partiesShipperCountryCode }}
                                                                    loadOptions={fetchCountries}
                                                                    onChange={event => handleInputChange('partiesShipperCountryCode', event)} />
                                                                {errors[`partiesShipperCountryCode`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`partiesShipperCountryCode`].message}</span>}
                                                            </div>
                                                        </div>

                                                    </div>
                                                </fieldset>

                                                <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                    <legend>FORWARDER</legend>

                                                    <div className="row">
                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Forwarder</label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    disabled="disabled"
                                                                    value={input.partiesForwarder} />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>

                                                <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                    <legend>CONSIGNEE</legend>

                                                    <div className="row">
                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Consignee Name<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`partiesConsignee`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Enter Consignee Name"
                                                                    className="form-control"
                                                                    value={input.partiesConsignee}
                                                                    onChange={event => handleInputChange('partiesConsignee', event)} />
                                                                {errors[`partiesConsignee`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`partiesConsignee`].message}</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6"></div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Consignee Address Line 1<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`partiesConsigneeAddress1`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        },
                                                                        maxLength: {
                                                                            value: 35,
                                                                            message: 'Maximum input length is 35'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Consignee Address Line 1"
                                                                    className="form-control"
                                                                    value={input.partiesConsigneeAddress1}
                                                                    onChange={event => handleInputChange('partiesConsigneeAddress1', event)} />
                                                                {errors[`partiesConsigneeAddress1`] && errors[`partiesConsigneeAddress1`].type === 'required' && <span style={{ fontSize: 13, color: "red" }}>This input field is required!</span>}
                                                                {errors[`partiesConsigneeAddress1`] && errors[`partiesConsigneeAddress1`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Consignee City<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`partiesConsigneeCity`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Enter Consignee City"
                                                                    className="form-control"
                                                                    value={input.partiesConsigneeCity}
                                                                    onChange={event => handleInputChange('partiesConsigneeCity', event)} />
                                                                {errors[`partiesConsigneeCity`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`partiesConsigneeCity`].message}</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Consignee Address Line 2</label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`partiesConsigneeAddress2`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 35,
                                                                            message: 'Maximum input length is 35'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Consignee Address Line 2"
                                                                    className="form-control"
                                                                    value={input.partiesConsigneeAddress2}
                                                                    onChange={event => handleInputChange('partiesConsigneeAddress2', event)} />
                                                                {errors[`partiesConsigneeAddress2`] && errors[`partiesConsigneeAddress2`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Consignee Postal Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`partiesConsigneePostalCode`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Enter Consignee Postal Code"
                                                                    className="form-control"
                                                                    value={input.partiesConsigneePostalCode}
                                                                    onChange={event => handleInputChange('partiesConsigneePostalCode', event)} />
                                                                {errors[`partiesConsigneePostalCode`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`partiesConsigneePostalCode`].message}</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Consignee Country<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`partiesConsigneeCountryCode`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        }
                                                                    })} />
                                                                <AsyncSelect
                                                                    value={{ label: input.partiesConsigneeCountryName, value: input.partiesConsigneeCountryCode }}
                                                                    loadOptions={fetchCountries}
                                                                    onChange={event => handleInputChange('partiesConsigneeCountryCode', event)} />
                                                                {errors[`partiesConsigneeCountryCode`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`partiesConsigneeCountryCode`].message}</span>}
                                                            </div>
                                                        </div>

                                                    </div>
                                                </fieldset>

                                            </Fragment>
                                        ))}

                                    </fieldset>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>ADDITIONAL PARTY (OPTIONAL)</legend>


                                        {inputFields.map((input, index) => (
                                            <Fragment key={`${input}~${index}`}>

                                                <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                    <legend>Contract Party</legend>

                                                    <div className="row">

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Contract Party</label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    placeholder="Enter Contract Party"
                                                                    value={input.contractPartyName}
                                                                    onChange={event => handleInputChange('contractPartyName', event)} />
                                                            </div>
                                                        </div>

                                                        <div className="col-6"></div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`contractPartyAddress1`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 35,
                                                                            message: 'Maximum input length is 35'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Contract Party Line 1"
                                                                    className="form-control"
                                                                    value={input.contractPartyAddress1}
                                                                    onChange={event => handleInputChange('contractPartyAddress1', event)} />
                                                                {errors[`contractPartyAddress1`] && errors[`contractPartyAddress1`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>City</label>
                                                                <input
                                                                    type="text"
                                                                    placeholder="Enter Contract Party City"
                                                                    className="form-control"
                                                                    value={input.contractPartyCity}
                                                                    name={`contractPartyCity`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 30,
                                                                            message: 'Maximum input length is 30'
                                                                        }
                                                                    })}
                                                                    onChange={event => handleInputChange('contractPartyCity', event)} />
                                                                {errors[`contractPartyCity`] && errors[`contractPartyCity`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 30</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`contractPartyAddress2`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 35,
                                                                            message: 'Maximum input length is 35'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Contract Party Line 2"
                                                                    className="form-control"
                                                                    value={input.contractPartyAddress2}
                                                                    onChange={event => handleInputChange('contractPartyAddress2', event)} />
                                                                {errors[`contractPartyAddress2`] && errors[`contractPartyAddress2`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                                <input
                                                                    type="text"
                                                                    placeholder="Enter Contract Party Postal Code"
                                                                    className="form-control"
                                                                    value={input.contractPartyPostalCode}
                                                                    onChange={event => handleInputChange('contractPartyPostalCode', event)} />
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Country</label>
                                                                <AsyncSelect
                                                                    value={{ label: input.contractPartyCountryName, value: input.contractPartyCountryCode }}
                                                                    loadOptions={fetchCountries}
                                                                    onChange={event => handleInputChange('contractPartyCountryCode', event)} />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </fieldset>

                                                <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                    <legend>Notify Party</legend>

                                                    <div className="row">

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Notify Party</label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    placeholder="Enter Notify Party"
                                                                    value={input.notifyPartyName}
                                                                    onChange={event => handleInputChange('notifyPartyName', event)} />
                                                            </div>
                                                        </div>

                                                        <div className="col-6"></div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`notifyPartyAddress1`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 35,
                                                                            message: 'Maximum input length is 35'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Notify Party Line 1"
                                                                    className="form-control"
                                                                    value={input.notifyPartyAddress1}
                                                                    onChange={event => handleInputChange('notifyPartyAddress1', event)} />
                                                                {errors[`notifyPartyAddress1`] && errors[`notifyPartyAddress1`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>City</label>
                                                                <input
                                                                    type="text"
                                                                    placeholder="Enter Notify Party City"
                                                                    className="form-control"
                                                                    value={input.notifyPartyCity}
                                                                    name={`notifyPartyCity`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 30,
                                                                            message: 'Maximum input length is 30'
                                                                        }
                                                                    })}
                                                                    onChange={event => handleInputChange('notifyPartyCity', event)} />
                                                                {errors[`notifyPartyCity`] && errors[`notifyPartyCity`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 30</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`notifyPartyAddress2`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 35,
                                                                            message: 'Maximum input length is 35'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Notify Party Line 2"
                                                                    className="form-control"
                                                                    value={input.notifyPartyAddress2}
                                                                    onChange={event => handleInputChange('notifyPartyAddress2', event)} />
                                                                {errors[`notifyPartyAddress2`] && errors[`notifyPartyAddress2`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                                <input
                                                                    type="text"
                                                                    placeholder="Enter Notify Party Postal Code"
                                                                    className="form-control"
                                                                    value={input.notifyPartyPostalCode}
                                                                    onChange={event => handleInputChange('notifyPartyPostalCode', event)} />
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Country</label>
                                                                <AsyncSelect
                                                                    value={{ label: input.notifyPartyCountryName, value: input.notifyPartyCountryCode }}
                                                                    loadOptions={fetchCountries}
                                                                    onChange={event => handleInputChange('notifyPartyCountryCode', event)} />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </fieldset>

                                                <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                    <legend>First Additional Notify Party</legend>

                                                    <div className="row">

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>First Additional Notify Party</label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    placeholder="Enter First Additional Notify Party"
                                                                    value={input.firstNotifyPartyName}
                                                                    onChange={event => handleInputChange('firstNotifyPartyName', event)} />
                                                            </div>
                                                        </div>

                                                        <div className="col-6"></div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`firstNotifyPartyAddress1`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 35,
                                                                            message: 'Maximum input length is 35'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="First Additional Notify Party Line 1"
                                                                    className="form-control"
                                                                    value={input.firstNotifyPartyAddress1}
                                                                    onChange={event => handleInputChange('firstNotifyPartyAddress1', event)} />
                                                                {errors[`firstNotifyPartyAddress1`] && errors[`firstNotifyPartyAddress1`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>City</label>
                                                                <input
                                                                    type="text"
                                                                    placeholder="Enter First Additional Notify Party City"
                                                                    className="form-control"
                                                                    value={input.firstNotifyPartyCity}
                                                                    name={`firstNotifyPartyCity`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 30,
                                                                            message: 'Maximum input length is 30'
                                                                        }
                                                                    })}
                                                                    onChange={event => handleInputChange('firstNotifyPartyCity', event)} />
                                                                {errors[`firstNotifyPartyCity`] && errors[`firstNotifyPartyCity`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 30</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`firstNotifyPartyAddress2`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 35,
                                                                            message: 'Maximum input length is 35'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="First Additional Notify Party Line 2"
                                                                    className="form-control"
                                                                    value={input.firstNotifyPartyAddress2}
                                                                    onChange={event => handleInputChange('firstNotifyPartyAddress2', event)} />
                                                                {errors[`firstNotifyPartyAddress2`] && errors[`firstNotifyPartyAddress2`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                                <input
                                                                    type="text"
                                                                    placeholder="Enter First Additional Notify Party Postal Code"
                                                                    className="form-control"
                                                                    value={input.firstNotifyPartyPostalCode}
                                                                    onChange={event => handleInputChange('firstNotifyPartyPostalCode', event)} />
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Country</label>
                                                                <AsyncSelect
                                                                    value={{ label: input.firstNotifyPartyCountryName, value: input.firstNotifyPartyCountryCode }}
                                                                    loadOptions={fetchCountries}
                                                                    onChange={event => handleInputChange('firstNotifyPartyCountryCode', event)} />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </fieldset>

                                                <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                    <legend>Second Additional Notify Party</legend>

                                                    <div className="row">

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Second Additional Notify Party</label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    placeholder="Enter Second Additional Notify Party"
                                                                    value={input.secondNotifyPartyName}
                                                                    onChange={event => handleInputChange('secondNotifyPartyName', event)} />
                                                            </div>
                                                        </div>

                                                        <div className="col-6"></div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`secondNotifyPartyAddress1`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 35,
                                                                            message: 'Maximum input length is 35'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Second Additional Notify Party Line 1"
                                                                    className="form-control"
                                                                    value={input.secondNotifyPartyAddress1}
                                                                    onChange={event => handleInputChange('secondNotifyPartyAddress1', event)} />
                                                                {errors[`secondNotifyPartyAddress1`] && errors[`secondNotifyPartyAddress1`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>City</label>
                                                                <input
                                                                    type="text"
                                                                    placeholder="Enter Second Additional Notify Party City"
                                                                    className="form-control"
                                                                    value={input.secondNotifyPartyCity}
                                                                    name={`secondNotifyPartyCity`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 30,
                                                                            message: 'Maximum input length is 30'
                                                                        }
                                                                    })}
                                                                    onChange={event => handleInputChange('secondNotifyPartyCity', event)} />
                                                                {errors[`secondNotifyPartyCity`] && errors[`secondNotifyPartyCity`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 30</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`secondNotifyPartyAddress2`}
                                                                    ref={register({
                                                                        maxLength: {
                                                                            value: 35,
                                                                            message: 'Maximum input length is 35'
                                                                        }
                                                                    })} />
                                                                <input
                                                                    type="text"
                                                                    placeholder="Second Additional Notify Party Line 2"
                                                                    className="form-control"
                                                                    value={input.secondNotifyPartyAddress2}
                                                                    onChange={event => handleInputChange('secondNotifyPartyAddress2', event)} />
                                                                {errors[`secondNotifyPartyAddress2`] && errors[`secondNotifyPartyAddress2`].type === 'maxLength' && <span style={{ fontSize: 13, color: "red" }}>Maximum input length is 35</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                                <input
                                                                    type="text"
                                                                    placeholder="Enter Second Additional Notify Party Postal Code"
                                                                    className="form-control"
                                                                    value={input.secondNotifyPartyPostalCode}
                                                                    onChange={event => handleInputChange('secondNotifyPartyPostalCode', event)} />
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Country</label>
                                                                <AsyncSelect
                                                                    value={{ label: input.secondNotifyPartyCountryName, value: input.secondNotifyPartyCountryCode }}
                                                                    loadOptions={fetchCountries}
                                                                    onChange={event => handleInputChange('secondNotifyPartyCountryCode', event)} />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </fieldset>

                                            </Fragment>
                                        ))}

                                    </fieldset>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>REFERENCES (OPTIONAL)</legend>

                                        <div className="row">
                                            {inputFields.map((input, index) => (
                                                <Fragment key={`${input}~${index}`}>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Shipper's References Number</label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                placeholder="Enter Number"
                                                                value={input.referencesShipperRefNumber}
                                                                onChange={event => handleInputChange('referencesShipperRefNumber', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Purchase Order Number</label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                placeholder="Enter Number"
                                                                value={input.referencesPurchaseOrderNumber}
                                                                onChange={event => handleInputChange('referencesPurchaseOrderNumber', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Forwarder's References Number</label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                placeholder="Enter Number"
                                                                value={input.referencesForwarderRefNumber}
                                                                onChange={event => handleInputChange('referencesForwarderRefNumber', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Consignee's Reference Number</label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                placeholder="Enter Number"
                                                                value={input.referencesConsigneeNumber}
                                                                onChange={event => handleInputChange('referencesConsigneeNumber', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>B/L Reference Number</label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                placeholder="Enter Number"
                                                                value={input.referencesBLNumber}
                                                                onChange={event => handleInputChange('referencesBLNumber', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Contract Party Reference Number</label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                placeholder="Enter Number"
                                                                value={input.referencesContractParty}
                                                                onChange={event => handleInputChange('referencesContractParty', event)} />
                                                        </div>
                                                    </div>

                                                </Fragment>
                                            ))}

                                        </div>

                                    </fieldset>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>TRANSPORT</legend>

                                        <div className="row">

                                            {inputFields.map((input, index) => (
                                                <Fragment key={`${input}~${index}`}>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Move Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`transportMoveType`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <Select
                                                                value={{ label: input.transportMoveTypeLabel, value: input.transportMoveType }}
                                                                options={optionMoveType}
                                                                onChange={event => handleInputChange('transportMoveType', event)} />
                                                            {errors[`transportMoveType`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`transportMoveType`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Earliest Departure Date<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled="disabled"
                                                                className="form-control"
                                                                value={input.transportEarliestDepartureDate} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Place of Carrier Receipt<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled="disabled"
                                                                className="form-control"
                                                                value={input.transportPOCReceipt} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Latest Delivery Date<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled="disabled"
                                                                className="form-control"
                                                                value={input.transportLatestDeliveryDate} />
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Place of Carrier Delivery<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled="disabled"
                                                                className="form-control"
                                                                value={input.transportPOCDelivery} />
                                                        </div>
                                                    </div>

                                                </Fragment>
                                            ))}

                                        </div>
                                    </fieldset>
                                </div>
                            </div>

                            {showPreCarriage && <div className="row">
                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>PRE CARRIAGE</legend>

                                        <div className="row">
                                            {inputFields.map((input, index) => (
                                                <Fragment key={`${input}~${index}`}>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Start<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`preCarriageStart`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <AsyncSelect
                                                                value={{ label: input.preCarriageStartLabel, value: input.preCarriageStart }}
                                                                loadOptions={fetchDataBookingOffice}
                                                                placeholder="Enter Location"
                                                                onChange={event => handleInputChange('preCarriageStart', event)} />
                                                            {errors[`preCarriageStart`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`preCarriageStart`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>ETD<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`preCarriageETD`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="date"
                                                                className="form-control"
                                                                value={input.preCarriageETD}
                                                                onChange={event => handleInputChange('preCarriageETD', event)} />
                                                            {errors[`preCarriageETD`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`preCarriageETD`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Mode<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`preCarriageMode`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <Select
                                                                value={{ label: input.preCarriageModeLabel, value: input.preCarriageMode }}
                                                                options={optionPreCarriageMode}
                                                                onChange={event => handleInputChange('preCarriageMode', event)} />
                                                            {errors[`preCarriageMode`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`preCarriageMode`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>ETA<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`preCarriageETA`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="date"
                                                                className="form-control"
                                                                value={input.preCarriageETA}
                                                                onChange={event => handleInputChange('preCarriageETA', event)} />
                                                            {errors[`preCarriageETA`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`preCarriageETA`].message}</span>}
                                                        </div>
                                                    </div>

                                                </Fragment>
                                            ))}

                                        </div>

                                    </fieldset>
                                </div>
                            </div>
                            }

                            <div className="row">
                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>MAIN CARRIAGE</legend>

                                        <div className="row">

                                            {inputFields.map((input, index) => (
                                                <Fragment key={`${input}~${index}`}>

                                                    <div className="col-4">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Port of Loading<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled="disabled"
                                                                className="form-control"
                                                                value={input.mainCarriagePortOfLoadingName + ` (` + input.mainCarriagePortOfLoading + `)`} />
                                                        </div>
                                                    </div>

                                                    <div className="col-4">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>ETD<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled="disabled"
                                                                className="form-control"
                                                                value={moment(input.mainCarriageETD).format('DD/MM/YYYY hh:mm:ss')} />
                                                        </div>
                                                    </div>

                                                    <div className="col-4">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Vessel<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled="disabled"
                                                                className="form-control"
                                                                value={input.mainCarriageVessel} />
                                                        </div>
                                                    </div>

                                                    <div className="col-4">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Port of Discharge<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled="disabled"
                                                                className="form-control"
                                                                value={input.mainCarriagePortOfDischargeName + ` (` + input.mainCarriagePortOfDischarge + `)`} />
                                                        </div>
                                                    </div>

                                                    <div className="col-4">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>ETA<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled="disabled"
                                                                className="form-control"
                                                                value={moment(input.mainCarriageETA).format('DD/MM/YYYY hh:mm:ss')} />
                                                        </div>
                                                    </div>

                                                    <div className="col-4">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Voyage<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                disabled="disabled"
                                                                className="form-control"
                                                                value={input.mainCarriageVoyage} />
                                                        </div>
                                                    </div>

                                                </Fragment>
                                            ))}

                                        </div>
                                    </fieldset>
                                </div>
                            </div>

                            {showOnCarriage && <div className="row">
                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>ON CARRIAGE</legend>

                                        <div className="row">
                                            {inputFields.map((input, index) => (
                                                <Fragment key={`${input}~${index}`}>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Start<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`onCarriageStart`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <AsyncSelect
                                                                value={{ label: input.onCarriageStartLabel, value: input.onCarriageStart }}
                                                                loadOptions={fetchDataBookingOffice}
                                                                placeholder="Enter Location"
                                                                onChange={event => handleInputChange('onCarriageStart', event)} />
                                                            {errors[`onCarriageStart`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`onCarriageStart`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>ETD<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`onCarriageETD`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="date"
                                                                className="form-control"
                                                                value={input.onCarriageETD}
                                                                onChange={event => handleInputChange('onCarriageETD', event)} />
                                                            {errors[`onCarriageETD`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`onCarriageETD`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Mode<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`onCarriageMode`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <Select
                                                                value={{ label: input.onCarriageModeLabel, value: input.onCarriageMode }}
                                                                options={optionPreCarriageMode}
                                                                onChange={event => handleInputChange('onCarriageMode', event)} />
                                                            {errors[`onCarriageMode`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`onCarriageMode`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>ETA<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`onCarriageETA`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="date"
                                                                className="form-control"
                                                                value={input.onCarriageETA}
                                                                onChange={event => handleInputChange('onCarriageETA', event)} />
                                                            {errors[`onCarriageETA`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`onCarriageETA`].message}</span>}
                                                        </div>
                                                    </div>

                                                </Fragment>
                                            ))}

                                        </div>

                                    </fieldset>
                                </div>
                            </div>
                            }

                            <div className="row">
                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>PAYMENT DETAILS</legend>

                                        {inputFieldsPaymentDetails.map((inputPaymentDetails, indexPaymentDetails) =>
                                            <Fragment key={`${inputPaymentDetails}~${indexPaymentDetails}`}>
                                                <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                    <div className="row">

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Charge Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`paymentDetailsChangeType-${indexPaymentDetails}`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        }
                                                                    })} />
                                                                <Select
                                                                    value={{ label: inputPaymentDetails.paymentDetailsChangeTypeLabel, value: inputPaymentDetails.paymentDetailsChangeType }}
                                                                    options={optionPaymentDetailsChangeType}
                                                                    onChange={event => handleInputPaymentDetails('paymentDetailsChangeType', indexPaymentDetails, event)} />
                                                                {errors[`paymentDetailsChangeType-${indexPaymentDetails}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`paymentDetailsChangeType-${indexPaymentDetails}`].message}</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Payer<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`paymentDetailsPayer-${indexPaymentDetails}`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        }
                                                                    })} />
                                                                <Select
                                                                    value={{ label: inputPaymentDetails.paymentDetailsPayerLabel, value: inputPaymentDetails.paymentDetailsPayer }}
                                                                    options={optionPaymentDetailsPayer}
                                                                    onChange={event => handleInputPaymentDetails('paymentDetailsPayer', indexPaymentDetails, event)} />
                                                                {errors[`paymentDetailsPayer-${indexPaymentDetails}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`paymentDetailsPayer-${indexPaymentDetails}`].message}</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Freight Term<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`paymentDetailsFreightTerm-${indexPaymentDetails}`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        }
                                                                    })} />
                                                                <Select
                                                                    value={{ label: inputPaymentDetails.paymentDetailsFreightTermLabel, value: inputPaymentDetails.paymentDetailsFreightTerm }}
                                                                    options={optionPaymentDetailsFreightTerm}
                                                                    onChange={event => handleInputPaymentDetails('paymentDetailsFreightTerm', indexPaymentDetails, event)} />
                                                                {errors[`paymentDetailsFreightTerm-${indexPaymentDetails}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`paymentDetailsFreightTerm-${indexPaymentDetails}`].message}</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className={dashforgeCss.txBlack}>Payment Location<span className={dashforgeCss.txDanger}>*</span></label>
                                                                <input
                                                                    type="hidden"
                                                                    name={`paymentDetailsPaymentLocation-${indexPaymentDetails}`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        }
                                                                    })} />
                                                                <AsyncSelect
                                                                    value={{ label: inputPaymentDetails.paymentDetailsPaymentLocationLabel, value: inputPaymentDetails.paymentDetailsPaymentLocation }}
                                                                    loadOptions={fetchDataBookingOffice}
                                                                    placeholder="Enter Location"
                                                                    onChange={event => handleInputPaymentDetails('paymentDetailsPaymentLocation', indexPaymentDetails, event)} />
                                                                {errors[`paymentDetailsPaymentLocation-${indexPaymentDetails}`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`paymentDetailsPaymentLocation-${indexPaymentDetails}`].message}</span>}
                                                            </div>
                                                        </div>

                                                        <div className="col-md-12">
                                                            <div className="form-group float-right">
                                                                <button
                                                                    type="button"
                                                                    className="btn btn-xl btn-outline-primary"
                                                                    onClick={() => handleAddPaymentDetails()}>+</button>
                                                                    &nbsp;
                                                                {indexPaymentDetails !== 0 && <button
                                                                    type="button"
                                                                    className="btn btn-xl btn-outline-primary"
                                                                    onClick={() => handleRemovePaymentDetails(indexPaymentDetails)}>-</button>}
                                                            </div>
                                                        </div>

                                                    </div>
                                                </fieldset>
                                            </Fragment>
                                        )}

                                    </fieldset>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>COMMENTS AND NOTIFICATION</legend>

                                        <div className="row">

                                            {inputFields.map((input, index) => (
                                                <Fragment key={`${input}~${index}`}>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Customer Comments</label>
                                                            <textarea
                                                                laceholder="Enter Comments"
                                                                className="form-control"
                                                                onChange={event => handleInputChange('customerComments', event)}>{input.customerComments}</textarea>
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Partner Email Notifications<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <span className="float-right" style={{ fontSize: '12px', color: 'black' }}>Press enter or separate each email with , (You can add up to 9 emails)</span>
                                                            <input
                                                                type="hidden"
                                                                name={`partnerEmailNotifications`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                value={input.partnerEmailNotifications}
                                                                onChange={event => handleInputChange('partnerEmailNotifications', event)} />
                                                            {errors[`partnerEmailNotifications`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`partnerEmailNotifications`].message}</span>}
                                                        </div>
                                                    </div>

                                                </Fragment>
                                            ))}

                                        </div>
                                    </fieldset>
                                </div>
                            </div>

                            {/* <br />
                            <pre>
                                {JSON.stringify(inputFields, null, 2)}
                            </pre> */}
                            {/* <br />
                            <pre>
                                {JSON.stringify(inputShipperTaxNumber, null, 2)}
                            </pre> */}
                            {/* <br />
                            <pre>
                                {JSON.stringify(inputFieldsContainer, null, 2)}
                            </pre> */}
                            {/* <br />
                            <pre>
                                {JSON.stringify(inputFieldsPaymentDetails, null, 2)}
                            </pre> */}

                            {/* <br />
                            <pre>
                                {JSON.stringify(getValues(), null, 2)}
                            </pre> */}

                            {/* <br />
                            <pre>
                                {JSON.stringify(uploadContainerData, null, 2)}
                            </pre> */}

                            <div style={{ textAlign: 'center' }}>
                                <button
                                    type="submit"
                                    className="btn btn-primary btn-md">Next</button>
                            </div>

                        </div>
                    </div>

                </div>

            </form>


            <Modal isOpen={modal} toggle={toggle} backdrop="static">
                <form key={2} onSubmit={handleSubmit2(onSubmitUploadContainerData)}>
                    <ModalHeader toggle={toggle}>Upload Container Data</ModalHeader>
                    <ModalBody>
                        <div className="row">
                            <div className="col-12">
                                <div className="form-group">
                                    <label>File<span className={dashforgeCss.txDanger}>*</span></label>
                                    <input
                                        type="file"
                                        className="form-control"
                                        onChange={event => handleChangeUploadContainer('documentContainer', event)}
                                        name="documentContainer"
                                        ref={register2({
                                            required: {
                                                value: true,
                                                message: 'This input field is required!'
                                            }
                                        })}
                                    />
                                    {errors2.documentContainer && <span style={{ fontSize: 13, color: "red" }}>{errors2.documentContainer.message}</span>}
                                </div>
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={toggle}>Close</Button>{' '}
                        <Button color="primary" type="submit">Upload</Button>
                    </ModalFooter>
                </form>
            </Modal>
        </>
    )
}

export default BookingRequest
