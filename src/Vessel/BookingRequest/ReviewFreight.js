import React, { useState, Fragment, useEffect } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
//import './dashforge.css'
import dashforgeCss from './dashforge.module.css'
import { Link } from "react-router-dom"
import axios from 'axios'
import moment from 'moment'
import { useToasts } from 'react-toast-notifications'
import Swal from 'sweetalert2'
import Select from 'react-select'
import { useForm } from 'react-hook-form'
import BootstrapTable from 'react-bootstrap-table-next'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css'
import uuid from 'react-uuid'
import Carousel from 'nuka-carousel'
import * as Constants from '../../Constants.js'
import * as ApiClickargo from '../../Clickargo/ApiList.js'

function ReviewFreight(props) {

    console.log(props)

    const [isHazmat, setIsHazmat] = useState(false);

    const { register, handleSubmit, errors, watch, clearError, setValue } = useForm();

    const [modal, setModal] = useState(false);

    const [isTableEmpty, setIsTableEmpty] = useState(false);

    const toggle = () => setModal(!modal);

    const [isLoading, setIsLoading] = useState(false);

    const { addToast } = useToasts()

    const routeProps = {
        location: props.location
    };

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-primary',
            cancelButton: 'btn btn-info'
        },
        buttonsStyling: false
    })

    const [inputFieldsDocuments, setInputFieldsDocuments] = useState([
        {
            documentType: '',
            documentTypeLabel: 'Please select document type',
            documentNumber: '',
            documentDate: '',
            documentFile: '',
            documentFileName: ''
        }
    ])

    let optionDocumentType = [
        { value: '', label: 'Please select document type' },
        { value: 'invoices', label: 'Invoices' },
        { value: 'packing lists', label: 'Packing Lists' },
    ]

    const handleInputChangeDocuments = (field, event) => {
        const values = [...inputFieldsDocuments];

        if (field === "documentType") {
            values[0].documentType = event.value
            values[0].documentTypeLabel = event.label

            setValue([
                { documentType: inputFieldsDocuments[0].documentType },
            ])

            clearError('documentType')
        }

        if (field === "documentNumber") {
            values[0].documentNumber = event.target.value
        }

        if (field === "documentDate") {
            values[0].documentDate = event.target.value
        }

        if (field === "documentFile") {
            values[0].documentFile = event.target.files[0]
            values[0].documentFileName = event.target.files[0].name
        }

        setInputFieldsDocuments(values);
    }

    const [inputArrDocuments, setInputArrDocuments] = useState([])

    let actionRemoveDocs = (cell, event) => {
        event.preventDefault()
        // console.log(inputArrDocuments.filter(docs => docs.documentUuid !== cell))
        setTimeout(() => {
            setInputArrDocuments(prev =>
                prev.filter(docs => docs.documentUuid !== cell)
            );
        }, 500);
    }

    let actionDownloadDocs = (cell, event) => {
        event.preventDefault()
        setTimeout(() => {
            let fileUrl = URL.createObjectURL(cell)
            let fileName = cell.name
            let a = document.createElement('a')
            a.href = fileUrl
            a.download = fileName
            a.click()
        }, 500);
    }

    let actionDownload = (cell) => {
        return (
            <div>
                <Button
                    className="btn btn-sm btn-primary"
                    onClick={event => actionDownloadDocs(cell, event)}><i class="fa fa-download del"></i> Download</Button>
            </div>
        )
    }

    let actionFormatter = (cell) => {
        return (
            <div>
                <Button
                    className="btn btn-sm btn-primary"
                    onClick={event => actionRemoveDocs(cell, event)}><i class="fa fa-times del"></i> Remove</Button>
            </div>
        )
    }

    const columns = [
        {
            dataField: 'documentNumber',
            text: 'No',
            headerStyle: { backgroundColor: "#52c8f3", color: "white" },
            align: 'center',
            style: { backgroundColor: "#ebedf2", color: "black" }
        },
        {
            dataField: 'documentDate',
            text: 'Date',
            headerStyle: { backgroundColor: "#52c8f3", color: "white" },
            align: 'center',
            style: { backgroundColor: "#ebedf2", color: "black" }
        },
        {
            dataField: 'documentFileName',
            text: 'Name',
            headerStyle: { backgroundColor: "#52c8f3", color: "white" },
            align: 'center',
            style: { backgroundColor: "#ebedf2", color: "black" }
        },
        {
            dataField: 'documentType',
            text: 'Type',
            headerStyle: { backgroundColor: "#52c8f3", color: "white" },
            align: 'center',
            style: { backgroundColor: "#ebedf2", color: "black" }
        },
        {
            dataField: 'documentFile',
            text: '',
            headerStyle: { backgroundColor: "#52c8f3", color: "white" },
            align: 'center',
            style: { backgroundColor: "#ebedf2", color: "black" },
            formatter: actionDownload
        },
        {
            dataField: 'documentUuid',
            text: '',
            headerStyle: { backgroundColor: "#52c8f3", color: "white" },
            align: 'center',
            style: { backgroundColor: "#ebedf2", color: "black" },
            formatter: actionFormatter
        },
    ]

    const onSubmit = e => {
        console.log('submit docs run')
        setInputArrDocuments(oldArray => [...inputArrDocuments, {
            documentArray: oldArray.length,
            documentUuid: uuid(),
            documentType: inputFieldsDocuments[0].documentType,
            documentTypeLabel: inputFieldsDocuments[0].documentTypeLabel,
            documentNumber: inputFieldsDocuments[0].documentNumber,
            documentDate: inputFieldsDocuments[0].documentDate,
            documentFile: inputFieldsDocuments[0].documentFile,
            documentFileName: inputFieldsDocuments[0].documentFileName
        }]);

        setTimeout(() => {
            const values = [...inputFieldsDocuments];
            values[0].documentType = ''
            values[0].documentTypeLabel = 'Please select document type'
            values[0].documentNumber = ''
            values[0].documentDate = ''
            values[0].documentFile = ''
            values[0].documentFileName = ''
            setInputFieldsDocuments(values);

            setModal(!modal)
        }, 500);
    }

    const createOrderHandler = (event) => {
        event.preventDefault()

        setIsLoading(true)

        setTimeout(() => {
            let formData = new FormData()

            //filter document type invoices
            let fileDataInvoices = inputArrDocuments.filter(docs => docs.documentType === 'invoices')

            fileDataInvoices.map((inputInvoices, indexInvoices) => {
                formData.append('invoices[' + indexInvoices + '][number]', inputInvoices.documentNumber)
                formData.append('invoices[' + indexInvoices + '][date]', inputInvoices.documentDate)
                formData.append('invoices[' + indexInvoices + '][file]', inputInvoices.documentFile)
            })

            //filter document type packing_lists
            let fileDataPackingLists = inputArrDocuments.filter(docs => docs.documentType === 'packing lists')

            fileDataPackingLists.map((inputPackingList, indexPackingList) => {
                formData.append('packing_lists[' + indexPackingList + '][number]', inputPackingList.documentNumber)
                formData.append('packing_lists[' + indexPackingList + '][date]', inputPackingList.documentDate)
                formData.append('packing_lists[' + indexPackingList + '][file]', inputPackingList.documentFile)
            })

            formData.append('order[freight_mode]', "ocean")
            formData.append('order[move_type]', routeProps.location.state.generalArr[0].transportMoveType)
            formData.append('detail[shipment_type]', routeProps.location.state.generalArr[0].shipmentType)
            formData.append('detail[product_type]', routeProps.location.state.generalArr[0].productType)
            formData.append('detail[schedule]', localStorage.getItem('scheduleUuid'))
            //start container loop
            routeProps.location.state.containerArr.map((inputContainer, indexContainer) => {
                formData.append('container[' + indexContainer + '][type]', inputContainer.containerType)
                formData.append('container[' + indexContainer + '][container_comments]', inputContainer.containerComments)

                //start cargo loop
                inputContainer.cargo.map((inputCargo, indexCargo) => {
                    formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_description]', inputCargo.cargoDescription)
                    formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_hs_code]', inputCargo.cargoHsCode)
                    formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_weight]', inputCargo.cargoWeight)
                    formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_weight_type]', inputCargo.cargoWeightType)
                    formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_package_count]', inputCargo.cargoPackageCount)
                    formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_package_type]', inputCargo.cargoPackageType)
                    formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_gross_vol]', inputCargo.cargoGrossVolume)
                    formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_vol_type]', inputCargo.cargoGrossVolumeType)

                    if (routeProps.location.state.generalArr[0].productType === 'special') {
                        formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_imo]', inputCargo.cargoPrimaryImoClass)
                        formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_undg]', inputCargo.cargoUndgNumber)
                        formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_packing_group]', inputCargo.cargoPackingGroup)
                        formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_proper_shipping]', inputCargo.cargoProperShippingName)
                        formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_emergency_contact_name]', inputCargo.cargoEmergencyContactName)
                        formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_emergency_contact_number]', inputCargo.cargoEmergencyContactNumber)
                    }

                    if (routeProps.location.state.generalArr[0].productType === 'dangerous') {
                        formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_imo]', inputCargo.cargoPrimaryImoClass)
                        formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_undg]', inputCargo.cargoUndgNumber)
                        formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_packing_group]', inputCargo.cargoPackingGroup)
                        formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_proper_shipping]', inputCargo.cargoProperShippingName)
                        formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_emergency_contact_name]', inputCargo.cargoEmergencyContactName)
                        formData.append('container[' + indexContainer + '][cargo][' + indexCargo + '][cargo_emergency_contact_number]', inputCargo.cargoEmergencyContactNumber)
                    }
                })
                //end cargo loop
            })
            //end container loop

            let current_time = moment().format("HH:mm:ss")

            //start main carriage
            formData.append('booking_request[main_carriage][port_of_load]', routeProps.location.state.generalArr[0].mainCarriagePortOfLoading)
            formData.append('booking_request[main_carriage][port_of_load_name]', routeProps.location.state.generalArr[0].mainCarriagePortOfLoadingName)
            formData.append('booking_request[main_carriage][port_of_load_country]', routeProps.location.state.generalArr[0].mainCarriagePortOfLoadingCountry)
            formData.append('booking_request[main_carriage][port_of_discharge]', routeProps.location.state.generalArr[0].mainCarriagePortOfDischarge)
            formData.append('booking_request[main_carriage][port_of_discharge_name]', routeProps.location.state.generalArr[0].mainCarriagePortOfDischargeName)
            formData.append('booking_request[main_carriage][port_of_discharge_country]', routeProps.location.state.generalArr[0].mainCarriagePortOfDischargeCountry)
            formData.append('booking_request[main_carriage][vessel]', routeProps.location.state.generalArr[0].mainCarriageVessel)
            formData.append('booking_request[main_carriage][voyage]', routeProps.location.state.generalArr[0].mainCarriageVoyage)
            formData.append('booking_request[main_carriage][etd]', routeProps.location.state.generalArr[0].mainCarriageETD)
            formData.append('booking_request[main_carriage][eta]', routeProps.location.state.generalArr[0].mainCarriageETA)
            //end main carriage

            //start pre carriage
            formData.append('booking_request[pre_carriage][start]', routeProps.location.state.generalArr[0].preCarriageStart)
            formData.append('booking_request[pre_carriage][mode]', routeProps.location.state.generalArr[0].preCarriageMode)
            let preCarriageETDModify = routeProps.location.state.generalArr[0].preCarriageETD + ' ' + current_time
            formData.append('booking_request[pre_carriage][etd]', preCarriageETDModify)
            let preCarriageETAModify = routeProps.location.state.generalArr[0].preCarriageETA + ' ' + current_time
            formData.append('booking_request[pre_carriage][eta]', preCarriageETAModify)
            //end pre carriage

            //start on carriage
            formData.append('booking_request[on_carriage][end]', routeProps.location.state.generalArr[0].onCarriageStart)
            formData.append('booking_request[on_carriage][mode]', routeProps.location.state.generalArr[0].onCarriageMode)
            let onCarriageETDModify = routeProps.location.state.generalArr[0].onCarriageETD + ' ' + current_time
            formData.append('booking_request[on_carriage][etd]', onCarriageETDModify)
            let onCarriageETAModify = routeProps.location.state.generalArr[0].onCarriageETA + ' ' + current_time
            formData.append('booking_request[on_carriage][eta]', onCarriageETAModify)
            //end on carriage

            //general details
            formData.append('booking_request[carrier]', routeProps.location.state.generalArr[0].generalDetailsCarrier)
            formData.append('booking_request[carrier_name]', routeProps.location.state.generalArr[0].generalDetailsCarrierLabel)
            formData.append('booking_request[carrier_receipt]', routeProps.location.state.generalArr[0].generalDetailsCarrierReceipt)
            formData.append('booking_request[carrier_delivery]', routeProps.location.state.generalArr[0].generalDetailsCarrierDelivery)
            formData.append('booking_request[earliest_departure]', routeProps.location.state.generalArr[0].generalDetailsEarliestDeparture)
            formData.append('booking_request[latest_delivery]', routeProps.location.state.generalArr[0].generalDetailsLatestDelivery)
            formData.append('booking_request[booking_office]', routeProps.location.state.generalArr[0].generalDetailsBookingOffice)
            formData.append('booking_request[contract_number]', routeProps.location.state.generalArr[0].generalDetailsContractNumber)

            //parties
            formData.append('booking_request[shipper_inttra_id]', routeProps.location.state.generalArr[0].partiesShipperInttraId) //update 14-05-2020 //temporary
            formData.append('booking_request[shipper_npwp]', routeProps.location.state.generalArr[0].partiesShipperTaxNumber)
            formData.append('booking_request[shipper_address_1]', routeProps.location.state.generalArr[0].partiesShipperAddress1) //update 22-05-2020
            formData.append('booking_request[shipper_address_2]', routeProps.location.state.generalArr[0].partiesShipperAddress2) //update 22-05-2020
            formData.append('booking_request[shipper_country_code]', routeProps.location.state.generalArr[0].partiesShipperCountryCode)
            formData.append('booking_request[shipper_country_name]', routeProps.location.state.generalArr[0].partiesShipperCountryName)
            formData.append('booking_request[shipper_city]', routeProps.location.state.generalArr[0].partiesShipperCity)
            formData.append('booking_request[shipper_postal_code]', routeProps.location.state.generalArr[0].partiesShipperPostalCode)
            formData.append('booking_request[forwarder]', routeProps.location.state.generalArr[0].partiesForwarder)
            formData.append('booking_request[forwarder_inttra_id]', '808208') //update 14-05-2020 //temporary
            formData.append('booking_request[forwarder_address]', routeProps.location.state.generalArr[0].partiesForwarderAddress)
            formData.append('booking_request[consignee]', routeProps.location.state.generalArr[0].partiesConsignee)
            formData.append('booking_request[consignee_address_1]', routeProps.location.state.generalArr[0].partiesConsigneeAddress1) //update 22-05-2020
            formData.append('booking_request[consignee_address_2]', routeProps.location.state.generalArr[0].partiesConsigneeAddress2) //update 22-05-2020
            formData.append('booking_request[consignee_country_code]', routeProps.location.state.generalArr[0].partiesConsigneeCountryCode)
            formData.append('booking_request[consignee_country_name]', routeProps.location.state.generalArr[0].partiesConsigneeCountryName)
            formData.append('booking_request[consignee_city]', routeProps.location.state.generalArr[0].partiesConsigneeCity)
            formData.append('booking_request[consignee_postal_code]', routeProps.location.state.generalArr[0].partiesConsigneePostalCode)

            //additional party
            //contact party
            formData.append('booking_request[contract_party_name]', routeProps.location.state.generalArr[0].contractPartyName) //update 22-05-2020
            formData.append('booking_request[contract_party_country_code]', routeProps.location.state.generalArr[0].contractPartyCountryCode) //update 22-05-2020
            formData.append('booking_request[contract_party_country_name]', routeProps.location.state.generalArr[0].contractPartyCountryName) //update 22-05-2020
            formData.append('booking_request[contract_party_address_1]', routeProps.location.state.generalArr[0].contractPartyAddress1) //update 22-05-2020
            formData.append('booking_request[contract_party_address_2]', routeProps.location.state.generalArr[0].contractPartyAddress2) //update 22-05-2020
            formData.append('booking_request[contract_party_city]', routeProps.location.state.generalArr[0].contractPartyCity) //update 22-05-2020
            formData.append('booking_request[contract_party_postal_code]', routeProps.location.state.generalArr[0].contractPartyPostalCode) //update 22-05-2020
            //notify party
            formData.append('booking_request[notify_party_name]', routeProps.location.state.generalArr[0].notifyPartyName) //update 22-05-2020
            formData.append('booking_request[notify_party_country_code]', routeProps.location.state.generalArr[0].notifyPartyCountryCode) //update 22-05-2020
            formData.append('booking_request[notify_party_country_name]', routeProps.location.state.generalArr[0].notifyPartyCountryName) //update 22-05-2020
            formData.append('booking_request[notify_party_address_1]', routeProps.location.state.generalArr[0].notifyPartyAddress1) //update 22-05-2020
            formData.append('booking_request[notify_party_address_2]', routeProps.location.state.generalArr[0].notifyPartyAddress2) //update 22-05-2020
            formData.append('booking_request[notify_party_city]', routeProps.location.state.generalArr[0].notifyPartyCity) //update 22-05-2020
            formData.append('booking_request[notify_party_postal_code]', routeProps.location.state.generalArr[0].notifyPartyPostalCode) //update 22-05-2020
            //first additional notify party
            formData.append('booking_request[first_additional_party]', routeProps.location.state.generalArr[0].firstNotifyPartyName) //update 22-05-2020
            formData.append('booking_request[first_additional_party_country_code]', routeProps.location.state.generalArr[0].firstNotifyPartyCountryCode) //update 22-05-2020
            formData.append('booking_request[first_additional_party_country_name]', routeProps.location.state.generalArr[0].firstNotifyPartyCountryName) //update 22-05-2020
            formData.append('booking_request[first_additional_party_address_1]', routeProps.location.state.generalArr[0].firstNotifyPartyAddress1) //update 22-05-2020
            formData.append('booking_request[first_additional_party_address_2]', routeProps.location.state.generalArr[0].firstNotifyPartyAddress2) //update 22-05-2020
            formData.append('booking_request[first_additional_party_city]', routeProps.location.state.generalArr[0].firstNotifyPartyCity) //update 22-05-2020
            formData.append('booking_request[first_additional_party_postal_code]', routeProps.location.state.generalArr[0].firstNotifyPartyPostalCode) //update 22-05-2020
            //second additional notify party
            formData.append('booking_request[second_additional_party]', routeProps.location.state.generalArr[0].secondNotifyPartyName) //update 22-05-2020
            formData.append('booking_request[second_additional_party_country_code]', routeProps.location.state.generalArr[0].secondNotifyPartyCountryCode) //update 22-05-2020
            formData.append('booking_request[second_additional_party_country_name]', routeProps.location.state.generalArr[0].secondNotifyPartyCountryName) //update 22-05-2020
            formData.append('booking_request[second_additional_party_address_1]', routeProps.location.state.generalArr[0].secondNotifyPartyAddress1) //update 22-05-2020
            formData.append('booking_request[second_additional_party_address_2]', routeProps.location.state.generalArr[0].secondNotifyPartyAddress2) //update 22-05-2020
            formData.append('booking_request[second_additional_party_city]', routeProps.location.state.generalArr[0].secondNotifyPartyCity) //update 22-05-2020
            formData.append('booking_request[second_additional_party_postal_code]', routeProps.location.state.generalArr[0].secondNotifyPartyPostalCode) //update 22-05-2020

            //references
            formData.append('booking_request[shipper_ref_number]', routeProps.location.state.generalArr[0].referencesShipperRefNumber)
            formData.append('booking_request[po_number]', routeProps.location.state.generalArr[0].referencesPurchaseOrderNumber)
            formData.append('booking_request[forwarder_ref_number]', routeProps.location.state.generalArr[0].referencesForwarderRefNumber)
            formData.append('booking_request[consignee_ref_number]', routeProps.location.state.generalArr[0].referencesConsigneeNumber)
            formData.append('booking_request[contract_party_ref_number]', routeProps.location.state.generalArr[0].referencesContractParty)
            formData.append('booking_request[bl_ref_number]', routeProps.location.state.generalArr[0].referencesBLNumber)

            formData.append('booking_request[customer_comments]', routeProps.location.state.generalArr[0].customerComments)
            formData.append('booking_request[partner_email]', routeProps.location.state.generalArr[0].partnerEmailNotifications)

            //update new field schedule
            formData.append('booking_request[origin_unloc]', routeProps.location.state.generalArr[0].scheduleOriginUnloc)
            formData.append('booking_request[origin_port_name]', routeProps.location.state.generalArr[0].scheduleOriginPortName)
            formData.append('booking_request[origin_country]', routeProps.location.state.generalArr[0].scheduleOriginCountry)
            formData.append('booking_request[destination_unloc]', routeProps.location.state.generalArr[0].scheduleDestinationUnloc)
            formData.append('booking_request[destination_port_name]', routeProps.location.state.generalArr[0].scheduleDestinationPortName)
            formData.append('booking_request[destination_country]', routeProps.location.state.generalArr[0].scheduleDestinationCountry)

            //start payment details loop
            routeProps.location.state.paymentDetailsArr.map((inputPaymentDetails, indexPaymentDetails) => {
                formData.append('booking_request[payment_detail][' + indexPaymentDetails + '][charge_type]', inputPaymentDetails.paymentDetailsChangeType)
                formData.append('booking_request[payment_detail][' + indexPaymentDetails + '][freight_term]', inputPaymentDetails.paymentDetailsFreightTerm)
                formData.append('booking_request[payment_detail][' + indexPaymentDetails + '][payer]', inputPaymentDetails.paymentDetailsPayer)
                formData.append('booking_request[payment_detail][' + indexPaymentDetails + '][payment_location]', inputPaymentDetails.paymentDetailsPaymentLocation)
            })
            //end payment details loop

            //update link integration
            formData.append('order[is_outsource]', 1)
            formData.append('order[link_outsource]', window.location.origin+'/?enc='+encodeURIComponent(Constants.ENC_ID)+'#/clickargo/redirect_orders/')

            const headers = {
                'Content-Type': 'multipart/form-data',
                'Accept-Language': 'application/json',
                'Accept': 'application/json',
                'Key': localStorage.getItem('ckHeaderKey'),
                'Secret': localStorage.getItem('ckHeaderSecret'),
                'Authorization': 'Bearer ' + localStorage.getItem('ckHeaderToken')
            }

            axios
                .post(ApiClickargo.CLICKARGO_STORE_ORDER, formData, {
                    headers: headers
                })
                .then(response => {
                    setIsLoading(false)

                    //send to nle api
                    axios
                        .post(ApiClickargo.NLE_VESSEL_POST, {
                            id_platform: 'PL002',
                            job_number: response.data.data.job_number,
                            status: 'Processing',
                            vessel_number: routeProps.location.state.generalArr[0].mainCarriageVessel,
                            npwp: props.dataUserPortal.npwp
                        }, {
                            headers: {
                                'Accept': 'application/json'
                            }
                        })
                        .then(response => {
                            console.log(response)
                            console.log('submit to nle api success')
                        })
                        .catch(error => {
                            console.log(error)
                        })

                    //update link outsource value
                    const headerUpdateOrderLink = {
                        'Content-Type': 'application/json',
                        'Key': localStorage.getItem('ckHeaderKey'),
                        'Secret': localStorage.getItem('ckHeaderSecret'),
                    }

                    axios
                        .put(ApiClickargo.CLICKARGO_UPDATE_ORDER_LINK, {
                            uuid: response.data.data.uuid,
                            link_outsource: window.location.origin + '/?enc=' + encodeURIComponent(Constants.ENC_ID) + '#/clickargo/redirect_orders/' + response.data.data.uuid
                        }, {
                            headers: headerUpdateOrderLink
                        })
                        .then(responseUpdateOrderLink => {
                            console.log('Update order link_outsource success')
                            swalWithBootstrapButtons.fire({
                                title: 'Order Succesfully Placed',
                                icon: 'success',
                                html: '<h3 style="color: black;">JOB NUMBER</h3>' + '<h3 style="color: black;">' + response.data.data.job_number + '</h3>',
                                showCancelButton: true,
                                confirmButtonText: 'Vessel',
                                cancelButtonText: 'Dashboard',
                                reverseButtons: true,
                                allowOutsideClick: false
                            }).then((result) => {
                                if (result.value) {
                                    props.history.push({
                                        pathname: '/vessel'
                                    })
                                } else if (result.dismiss === Swal.DismissReason.cancel) {
                                    props.history.push({
                                        pathname: '/dashboard'
                                    })
                                }
                            })
                        })
                        .catch(error => {
                            if (!error.response) {
                                setIsLoading(false)
                                addToast('You seems to be offline, please check your internet connection!', {
                                    appearance: 'error',
                                    autoDismiss: false,
                                })
                            } else {
                                setIsLoading(false)
                                addToast(error.response.data.message, {
                                    appearance: 'error',
                                    autoDismiss: false,
                                })
                            }
                        })
                })
                .catch(error => {
                    if (!error.response) {
                        setIsLoading(false)
                        addToast('You seems to be offline, please check your internet connection!', {
                            appearance: 'error',
                            autoDismiss: false,
                        })
                    } else {
                        setIsLoading(false)
                        addToast(error.response.data.message, {
                            appearance: 'error',
                            autoDismiss: false,
                        })
                    }
                })
        }, 2000);

    }

    useEffect(() => {
        if (routeProps.location.state.generalArr[0].productType === 'special') {
            setIsHazmat(true)
        } else if (routeProps.location.state.generalArr[0].productType === 'dangerous') {
            setIsHazmat(true)
        } else if (routeProps.location.state.generalArr[0].productType === 'general') {
            setIsHazmat(false)
        } else if (routeProps.location.state.generalArr[0].productType === 'others') {
            setIsHazmat(false)
        }
    }, [])

    return (
        <>

            <form>
                <div style={{ marginBottom: "10px" }}>
                    <div style={{ float: "left" }}>
                        <h3 style={{ display: "inline" }}>Review and Update Document</h3>
                    </div>
                    <div style={{ float: "right" }}>
                        <img style={{ width: '200px' }} src={window.location.origin + '/assets/images/Clickargo5.png'} />
                    </div>
                </div>

                <br />
                <br />

                <div style={{ marginBottom: "10px" }}>
                    <Link to={{
                        pathname: "/booking_request_back",
                        state: routeProps.location.state
                    }} style={{ display: "inline", color: "#2d9ff7" }} >
                        Back
                    </Link>
                </div>

                <div className="kt-portlet kt-portlet--height-fluid">
                    <div className="kt-portlet__body">
                        <div className="kt-widget15">
                            <div className="row">

                                <div className="col-md-3">
                                    <label >Action Type</label>
                                    <h6 className={dashforgeCss.txBlack}>Export</h6>
                                </div>

                                <div className="col-md-3">
                                    <label >Freight Mode</label>
                                    <h6 className={dashforgeCss.txBlack}>Ocean</h6>
                                </div>

                                <div className="col-md-3">
                                    <label >Move Type</label>
                                    <h6 className={dashforgeCss.txBlack}>{routeProps.location.state.generalArr[0].transportMoveTypeLabel}</h6>
                                </div>

                                <div className="col-md-3">
                                    <label >Transaction</label>
                                    <h6 className={dashforgeCss.txBlack}>Freight</h6>
                                </div>

                            </div>

                        </div>

                        <div>
                            <hr style={{ border: "1px solid #f3f3f7" }} />
                        </div>

                        <div className="row">
                            <div className="col-md-6">
                                <label>Shipment Type</label>
                                <h6 className={dashforgeCss.txBlack}>{routeProps.location.state.generalArr[0].shipmentTypeLabel}</h6>
                            </div>

                            <div className="col-md-6">
                                <label>Product Type</label>
                                <h6 className={dashforgeCss.txBlack}>{routeProps.location.state.generalArr[0].productTypeLabel}</h6>
                            </div>
                        </div>

                        <div>
                            <hr style={{ border: "1px solid #f3f3f7" }} />
                        </div>

                        <Carousel heightMode={`current`}>
                            {routeProps.location.state.containerArr.map((inputContainer, indexContainer) => (
                                <Fragment key={`${inputContainer}~${indexContainer}`}>
                                    <div className="row" style={{ paddingTop: "15px" }}>
                                        <div className="col-md-12">

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend style={{ color: '#5bd874' }}>CONTAINER</legend>

                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <h4 className={dashforgeCss.txBlack}>Container {indexContainer + 1}</h4>
                                                    </div>

                                                    <div className="col-md-2">
                                                        <div className="form-group">
                                                            <label>Number</label>
                                                            <h6 className={dashforgeCss.txBlack}>-</h6>
                                                        </div>
                                                    </div>

                                                    <div className="col-md-2">
                                                        <div className="form-group">
                                                            <label>Type</label>
                                                            <h6 className={dashforgeCss.txBlack}>{inputContainer.containerTypeLabel}</h6>
                                                        </div>
                                                    </div>

                                                    <div className="col-md-2">
                                                        <div className="form-group">
                                                            <label>Seal Number</label>
                                                            <h6 className={dashforgeCss.txBlack}>-</h6>
                                                        </div>
                                                    </div>

                                                    <div className="col-md-2">
                                                        <div className="form-group">
                                                            <label>Seal Type</label>
                                                            <h6 className={dashforgeCss.txBlack}>-</h6>
                                                        </div>
                                                    </div>

                                                    <div className="col-md-4">
                                                        <div className="form-group">
                                                            <label>Comments</label>
                                                            <h6 className={dashforgeCss.txBlack}>{inputContainer.containerComments}</h6>
                                                        </div>
                                                    </div>

                                                </div>



                                                <div className="cargo_form">
                                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '10px', marginTop: '10px' }}>

                                                        <div className="row" style={{ marginBottom: '15px' }}>
                                                            <div className="col-md-12">
                                                                <h6 className={dashforgeCss.txBlack}>Cargo</h6>
                                                            </div>

                                                            {inputContainer.cargo.map((inputCargo, indexCargo) => (
                                                                <Fragment key={`${inputCargo}~${indexCargo}`}>
                                                                    <div className="col-md-4">
                                                                        <fieldset className={dashforgeCss.formFieldset} style={{ backgroundColor: '#f2f7fd', marginBottom: '15px' }}>
                                                                            <div className="row">

                                                                                <div className="col-2">
                                                                                    <div className="form-group">
                                                                                        <h6 className={dashforgeCss.txBlack}>{indexCargo + 1}</h6>
                                                                                    </div>
                                                                                </div>

                                                                                <div className="col-5">
                                                                                    <div className="form-group">
                                                                                        <label >Cargo Description<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                        <h6 className={dashforgeCss.txBlack}>{inputCargo.cargoDescription}</h6>
                                                                                    </div>
                                                                                </div>

                                                                                <div className="col-5">
                                                                                    <div className="form-group">
                                                                                        <label >Package Count<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                        <div className="custom-form-group">
                                                                                            <h6 className={dashforgeCss.txBlack}>{inputCargo.cargoPackageCount}</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div className="col-2">
                                                                                    <div className="form-group">
                                                                                        {/* <h6 className="grey">2</h6> */}
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-5">
                                                                                    <div className="form-group">
                                                                                        <label>HS Code</label>
                                                                                        <h6 className={dashforgeCss.txBlack}>{inputCargo.cargoHsCode}</h6>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-5">
                                                                                    <div className="form-group">
                                                                                        <label>Package Type</label>
                                                                                        <h6 className={dashforgeCss.txBlack}>{inputCargo.cargoPackageTypeLabel}</h6>
                                                                                    </div>
                                                                                </div>

                                                                                <div className="col-2">
                                                                                    <div className="form-group">
                                                                                        {/* <h6 className="grey">2</h6> */}
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-5">
                                                                                    <div className="form-group">
                                                                                        <label>Cargo Weight</label>
                                                                                        <h6 className={dashforgeCss.txBlack}>{inputCargo.cargoWeight} {inputCargo.cargoWeightType == 'KGM' ? 'Kgs' : 'Pounds'}</h6>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-5">
                                                                                    <div className="form-group">
                                                                                        <label>Cargo Volume</label>
                                                                                        <h6 className={dashforgeCss.txBlack}>{inputCargo.cargoGrossVolume} {inputCargo.cargoGrossVolumeType == 'MTQ' ? 'Cubic meters' : 'Cubic feet'}</h6>
                                                                                    </div>
                                                                                </div>

                                                                                {isHazmat && <Fragment>
                                                                                    <div className="col-12" style={{ marginTop: '15px' }}>
                                                                                        <div className="form-group">
                                                                                            <h6 style={{ color: 'red' }}>Hazmat Details</h6>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div className="col-2">
                                                                                        <div className="form-group cargo_description_div">
                                                                                            {/* <label style={{color: 'red'}}>Hazmat</label> */}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-5">
                                                                                        <div className="form-group cargo_description_div">
                                                                                            <label>Primary IMO Class</label>
                                                                                            <h6 className={dashforgeCss.txBlack}>{inputCargo.cargoPrimaryImoClassLabel}</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-5">
                                                                                        <div className="form-group cargo_description_div">
                                                                                            <label>UNDG Number</label>
                                                                                            <h6 className={dashforgeCss.txBlack}>{inputCargo.cargoUndgNumber}</h6>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div className="col-2">
                                                                                        <div className="form-group cargo_description_div">
                                                                                            {/* <label style={{color: 'red'}}>Hazmat</label> */}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-5">
                                                                                        <div className="form-group cargo_description_div">
                                                                                            <label>Packing Group Name</label>
                                                                                            <h6 className={dashforgeCss.txBlack}>{inputCargo.cargoPackingGroupLabel}</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-5">
                                                                                        <div className="form-group cargo_description_div">
                                                                                            <label>Emergency Contact</label>
                                                                                            <h6 className={dashforgeCss.txBlack}>{inputCargo.cargoEmergencyContactName}</h6>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div className="col-2">
                                                                                        <div className="form-group cargo_description_div">
                                                                                            {/* <label style={{color: 'red'}}>Hazmat</label> */}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-5">
                                                                                        <div className="form-group cargo_description_div">
                                                                                            <label>Emergency Number</label>
                                                                                            <h6 className={dashforgeCss.txBlack}>{inputCargo.cargoEmergencyContactNumber}</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-5">
                                                                                        <div className="form-group cargo_description_div">
                                                                                            <label>Proper Shipping Name</label>
                                                                                            <h6 className={dashforgeCss.txBlack}>{inputCargo.cargoProperShippingName}</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                </Fragment>
                                                                                }

                                                                            </div>

                                                                        </fieldset>

                                                                    </div>
                                                                </Fragment>
                                                            ))}
                                                        </div>


                                                    </fieldset>
                                                </div>

                                            </fieldset>
                                        </div>
                                    </div>
                                </Fragment>
                            ))}
                        </Carousel>

                        <div>
                            <hr style={{ border: "1px solid #f3f3f7" }} />
                        </div>

                        <div className="row">
                            <div className="col-md-6">
                                <label>Place of Loading</label>
                                <h6 className={dashforgeCss.txBlack}>{routeProps.location.state.generalArr[0].transportPOCReceipt}</h6>
                            </div>

                            <div className="col-md-6">
                                <label>Place of Destination</label>
                                <h6 className={dashforgeCss.txBlack}>{routeProps.location.state.generalArr[0].transportPOCDelivery}</h6>
                            </div>
                        </div>

                        <div>
                            <hr style={{ border: "1px solid #f3f3f7" }} />
                        </div>

                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                            <legend>DOCUMENTS</legend>
                            <button className="btn btn-primary pull-right add-document" style={{ marginTop: '15px' }} type="button" onClick={toggle}>Add Documents</button>

                            <div className="d-flex justify-content-between">
                                <div className="align-self-start bd-highlight bg-white warning-box" style={{ marginTop: '20px' }}>
                                    <p style={{ color: "red" }}>Important Note</p>
                                    <ul>
                                        <li className={dashforgeCss.txBlack}>All product type need to attached invoice, and packing list</li>
                                        <li className={dashforgeCss.txBlack}><span className={dashforgeCss.txBold + ' ' + dashforgeCss.txBlack}>Special Cargo</span> and <span className={dashforgeCss.txBold + ' ' + dashforgeCss.txBlack}>Dangerous Goods</span> also need to attached <span className={dashforgeCss.txBold + ' ' + dashforgeCss.txBlack}>MSDS ( Material Safety Data Sheet )<span /></span></li>
                                    </ul>
                                </div>
                            </div>

                            <div className="kt-portlet__body">
                                <div className="kt-widget15">
                                    <BootstrapTable
                                        bootstrap4
                                        keyField='documentUuid'
                                        data={inputArrDocuments}
                                        columns={columns} />
                                </div>
                            </div>
                        </fieldset>

                        {/* <br />
                        <pre>
                            {JSON.stringify(inputFieldsDocuments, null, 2)}
                        </pre>
                        <br />
                        <pre>
                            {JSON.stringify(inputArrDocuments, null, 2)}
                        </pre> */}

                        < div className="text-center" >
                            <button
                                type="button"
                                className="btn btn-primary submit-btn"
                                onClick={event => createOrderHandler(event)}
                                disabled={isLoading}>
                                {isLoading && <i className="fa fa-circle-notch fa-spin"></i>}
                                {isLoading && <span>Please wait...</span>}
                                {!isLoading && <span>Create Order</span>}
                            </button>
                        </div>

                    </div>
                </div>

            </form>


            <Modal isOpen={modal} toggle={toggle} backdrop="static">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <ModalHeader toggle={toggle}>Add Document</ModalHeader>
                    <ModalBody>
                        <div className="row">
                            <div className="col-12">
                                <div className="form-group">
                                    <label className={dashforgeCss.txBlack}>Select document type<span className={dashforgeCss.txDanger}>*</span></label>
                                    <input
                                        type="hidden"
                                        name="documentType"
                                        ref={register({
                                            required: {
                                                value: true,
                                                message: 'This input field is required!'
                                            }
                                        })} />
                                    <Select
                                        options={optionDocumentType}
                                        value={{ label: inputFieldsDocuments[0].documentTypeLabel, value: inputFieldsDocuments[0].documentType }}
                                        onChange={event => handleInputChangeDocuments('documentType', event)} />
                                    {errors.documentType && <span style={{ fontSize: 13, color: "red" }}>{errors.documentType.message}</span>}
                                </div>
                                <div className="form-group">
                                    <label className={dashforgeCss.txBlack}>Document Number<span className={dashforgeCss.txDanger}>*</span></label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        onChange={event => handleInputChangeDocuments('documentNumber', event)}
                                        name="documentNumber"
                                        ref={register({
                                            required: {
                                                value: true,
                                                message: 'This input field is required!'
                                            }
                                        })} />
                                    {errors.documentNumber && <span style={{ fontSize: 13, color: "red" }}>{errors.documentNumber.message}</span>}
                                </div>
                                <div className="form-group">
                                    <label className={dashforgeCss.txBlack}>Document Date<span className={dashforgeCss.txDanger}>*</span></label>
                                    <input
                                        type="date"
                                        className="form-control"
                                        onChange={event => handleInputChangeDocuments('documentDate', event)}
                                        name="documentDate"
                                        ref={register({
                                            required: {
                                                value: true,
                                                message: 'This input field is required!'
                                            }
                                        })} />
                                    {errors.documentDate && <span style={{ fontSize: 13, color: "red" }}>{errors.documentDate.message}</span>}
                                </div>
                                <div className="form-group">
                                    <label className={dashforgeCss.txBlack}>File<span className={dashforgeCss.txDanger}>*</span></label>
                                    <input
                                        type="file"
                                        className="form-control"
                                        onChange={event => handleInputChangeDocuments('documentFile', event)}
                                        name="documentFile"
                                        ref={register({
                                            required: {
                                                value: true,
                                                message: 'This input field is required!'
                                            }
                                        })} />
                                    {errors.documentFile && <span style={{ fontSize: 13, color: "red" }}>{errors.documentFile.message}</span>}
                                </div>
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={toggle}>Close</Button>{' '}
                        <Button color="primary" type="submit">Add File</Button>
                    </ModalFooter>
                </form>
            </Modal>

        </>
    )
}

export default ReviewFreight
