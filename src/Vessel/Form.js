import React, { Component } from 'react';
import { Card, Button } from 'reactstrap';
import Select from 'react-select'

class Form extends Component {
    render() {
        return (
            <div>
                <Card body>
                    <div class="row">
                        <div class="col-md-4">
                            <h6>Port of loading</h6>
                            <Select options={[]} />
                        </div>
                        <div class="col-md-4">
                            <h6>Port of destination</h6>
                            <Select options={[]} />
                        </div>
                        <div class="col-md-4">
                            <Button color="primary" size="md">Search</Button>
                        </div>
                    </div>
                </Card>
            </div>
        )
    }
}

export default Form