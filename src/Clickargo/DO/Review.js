import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../Vessel/BookingRequest/dashforge.module.css'
import { Link } from 'react-router-dom'
import Spinner from 'react-bootstrap/Spinner'
import BootstrapTable from 'react-bootstrap-table-next'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import Select from 'react-select'
import { useForm } from 'react-hook-form'
import uuid from 'react-uuid'
import Swal from 'sweetalert2'
import { useToasts } from 'react-toast-notifications'
import axios from 'axios'
import * as Constants from '../../Constants.js'
import * as ApiClickargo from '../ApiList.js'

function Review(props) {

    const { addToast } = useToasts()

    const [isLoading, setIsLoading] = useState(true)

    const [isLocalStorageExist, setIsLocalStorageExist] = useState(true)

    const [modal, setModal] = useState(false)

    const toggle = () => setModal(!modal)

    const { register, handleSubmit, errors, watch, clearError, setValue } = useForm()

    const [isSubmitDO, setIsSubmitDO] = useState(false)

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-primary',
            cancelButton: 'btn btn-info'
        },
        buttonsStyling: false
    })

    const [inputFieldsDocuments, setInputFieldsDocuments] = useState([
        {
            documentType: '',
            documentTypeLabel: 'Please select document type',
            documentNumber: '',
            documentDate: '',
            documentFile: '',
            documentFileName: ''
        }
    ])

    const [inputArrDocuments, setInputArrDocuments] = useState([])

    useEffect(() => {
        setTimeout(() => {
            if ('ckDOInputForm' in localStorage && 'ckDOCountryList' in localStorage && 'ckHeaderKey' in localStorage && 'ckHeaderSecret' in localStorage && 'ckHeaderToken' in localStorage) {
                setIsLocalStorageExist(true)
                setIsLoading(false)

                //set surat kuasa if exists
                if ('ckDOSuratKuasa' in localStorage) {
                    if (JSON.parse(localStorage.getItem('ckDOSuratKuasa')).length > 0) {
                        setInputArrDocuments(oldArray => [...inputArrDocuments, {
                            documentArray: oldArray.length,
                            documentUuid: 'isPPJK',
                            documentType: 'surat_kuasa',
                            documentTypeLabel: 'Surat Kuasa',
                            documentNumber: JSON.parse(localStorage.getItem('ckDOSuratKuasa'))[0].bl_no,
                            documentDate: JSON.parse(localStorage.getItem('ckDOSuratKuasa'))[0].bl_date,
                            documentFile: JSON.parse(localStorage.getItem('ckDOSuratKuasa'))[0].surat_kuasa.file,
                            documentFileName: 'Surat-Kuasa-File'
                        }]);
                    }
                }
            } else {
                setIsLocalStorageExist(false)
            }
        }, 2500);
    }, [])

    let actionRemoveDocs = (cell, event) => {
        event.preventDefault()
        setTimeout(() => {
            setInputArrDocuments(prev =>
                prev.filter(docs => docs.documentUuid !== cell)
            );
        }, 500);
    }

    //function if value is url
    const isURL = (str) => {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return pattern.test(str);
    }

    //function get filename
    const fileNameFromUrl = (url) => {
        var matches = url.match(/\/([^\/?#]+)[^\/]*$/);
        if (matches.length > 1) {
            return matches[1];
        }
        return null;
    }

    let actionDownloadDocs = (cell, event) => {
        event.preventDefault()
        setTimeout(() => {
            if (isURL(cell) === true) {
                let a = document.createElement("a");
                a.href = cell;
                a.target = '_blank'
                a.download = fileNameFromUrl(cell);
                a.click();
            } else {
                let fileUrl = URL.createObjectURL(cell)
                let fileName = cell.name
                let a = document.createElement('a')
                a.href = fileUrl
                a.download = fileName
                a.click()
            }
        }, 500);
    }

    let actionDownload = (cell) => {
        return (
            <div>
                <Button
                    className="btn btn-sm btn-primary"
                    onClick={event => actionDownloadDocs(cell, event)}><i class="fa fa-download del"></i> Download</Button>
            </div>
        )
    }

    let actionFormatter = (cell) => {
        if (cell === 'isPPJK') {
            return (
                <div></div>
            )
        } else {
            return (
                <div>
                    <Button
                        className="btn btn-sm btn-primary"
                        onClick={event => actionRemoveDocs(cell, event)}><i class="fa fa-times del"></i> Remove</Button>
                </div>
            )
        }

    }

    const columns = [
        {
            dataField: 'documentNumber',
            text: 'No',
            headerStyle: { backgroundColor: "#52c8f3", color: "white" },
            align: 'center',
            style: { backgroundColor: "#ebedf2", color: "black" }
        },
        {
            dataField: 'documentDate',
            text: 'Date',
            headerStyle: { backgroundColor: "#52c8f3", color: "white" },
            align: 'center',
            style: { backgroundColor: "#ebedf2", color: "black" }
        },
        {
            dataField: 'documentFileName',
            text: 'Name',
            headerStyle: { backgroundColor: "#52c8f3", color: "white" },
            align: 'center',
            style: { backgroundColor: "#ebedf2", color: "black" }
        },
        {
            dataField: 'documentTypeLabel',
            text: 'Type',
            headerStyle: { backgroundColor: "#52c8f3", color: "white" },
            align: 'center',
            style: { backgroundColor: "#ebedf2", color: "black" }
        },
        {
            dataField: 'documentFile',
            text: '',
            headerStyle: { backgroundColor: "#52c8f3", color: "white" },
            align: 'center',
            style: { backgroundColor: "#ebedf2", color: "black" },
            formatter: actionDownload
        },
        {
            dataField: 'documentUuid',
            text: '',
            headerStyle: { backgroundColor: "#52c8f3", color: "white" },
            align: 'center',
            style: { backgroundColor: "#ebedf2", color: "black" },
            formatter: actionFormatter
        },
    ]

    let optionDocumentType = [
        { value: '', label: 'Please select document type' },
        { value: 'letter_of_indemnity', label: 'Letter of Indemnity' },
        { value: 'spk', label: 'Surat Peminjaman Kontainer' },
        { value: 'mbl', label: JSON.parse(localStorage.getItem('ckDOInputForm'))[0].blTypeLabel },
    ]

    const handleInputChangeDocuments = (field, event) => {
        const values = [...inputFieldsDocuments]

        if (field === "documentType") {
            values[0].documentType = event.value
            values[0].documentTypeLabel = event.label

            if (event.value === 'mbl') {
                values[0].documentNumber = JSON.parse(localStorage.getItem('ckDOInputForm'))[0].blNumber
            } else {
                values[0].documentNumber = ''
            }

            setValue([
                { documentType: inputFieldsDocuments[0].documentType },
            ])

            clearError('documentType')
        }

        if (field === "documentNumber") {
            values[0].documentNumber = event.target.value
        }

        if (field === "documentDate") {
            values[0].documentDate = event.target.value
        }

        if (field === "documentFile") {
            values[0].documentFile = event.target.files[0]
            values[0].documentFileName = event.target.files[0].name
        }

        setInputFieldsDocuments(values)
    }

    const onSubmit = e => {
        console.log('submit docs run')
        setInputArrDocuments(oldArray => [...inputArrDocuments, {
            documentArray: oldArray.length,
            documentUuid: uuid(),
            documentType: inputFieldsDocuments[0].documentType,
            documentTypeLabel: inputFieldsDocuments[0].documentTypeLabel,
            documentNumber: inputFieldsDocuments[0].documentNumber,
            documentDate: inputFieldsDocuments[0].documentDate,
            documentFile: inputFieldsDocuments[0].documentFile,
            documentFileName: inputFieldsDocuments[0].documentFileName
        }]);

        setTimeout(() => {
            const values = [...inputFieldsDocuments];
            values[0].documentType = ''
            values[0].documentTypeLabel = 'Please select document type'
            values[0].documentNumber = ''
            values[0].documentDate = ''
            values[0].documentFile = ''
            values[0].documentFileName = ''
            setInputFieldsDocuments(values);

            setModal(!modal)
        }, 500);
    }

    const onClickBack = () => {
        props.history.push({
            pathname: '/clickargo_do/form'
        })
    }

    const createOrderHandler = (event) => {
        // alert('create order handler run')
        setIsSubmitDO(true)

        setTimeout(() => {

            let formData = new FormData()

            //filter document type letter of indemnity
            let fileDataLetterOfIndemnity = inputArrDocuments.filter(docs => docs.documentType === 'letter_of_indemnity')
            fileDataLetterOfIndemnity.map((inputLOI, indexSOI) => {
                formData.append('letter_of_indemnity[' + indexSOI + '][number]', inputLOI.documentNumber)
                formData.append('letter_of_indemnity[' + indexSOI + '][date]', inputLOI.documentDate)
                formData.append('letter_of_indemnity[' + indexSOI + '][file]', inputLOI.documentFile)
            })

            //filter document type surat peminjaman container
            let fileDataSuratPeminjamanKontainer = inputArrDocuments.filter(docs => docs.documentType === 'spk')

            fileDataSuratPeminjamanKontainer.map((inputSpk, indexSpk) => {
                formData.append('surat_peminjaman_kontainer[' + indexSpk + '][number]', inputSpk.documentNumber)
                formData.append('surat_peminjaman_kontainer[' + indexSpk + '][date]', inputSpk.documentDate)
                formData.append('surat_peminjaman_kontainer[' + indexSpk + '][file]', inputSpk.documentFile)
            })

            //filter document type bl
            let fileDataBL = inputArrDocuments.filter(docs => docs.documentType === 'mbl')

            fileDataBL.map((inputBL, indexBL) => {
                formData.append('mbl[' + indexBL + '][number]', inputBL.documentNumber)
                formData.append('mbl[' + indexBL + '][date]', inputBL.documentDate)
                formData.append('mbl[' + indexBL + '][file]', inputBL.documentFile)
                formData.append('mbl[' + indexBL + '][bl_type]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].blTypeLabel)
                formData.append('mbl[' + indexBL + '][port_of_discharge]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].placeOfDischargeLocation)
                formData.append('mbl[' + indexBL + '][npwp_number]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].requestorNPWP)
            })

            //filter document surat kuasa
            let fileDataSuratKuasa = inputArrDocuments.filter(docs => docs.documentType === 'surat_kuasa')
            fileDataSuratKuasa.map((inputSK, indexSK) => {
                formData.append('surat_kuasa[' + indexSK + '][number]', inputSK.documentNumber)
                formData.append('surat_kuasa[' + indexSK + '][date]', inputSK.documentDate)
                formData.append('surat_kuasa[' + indexSK + '][path]', inputSK.documentFile)
                formData.append('surat_kuasa[' + indexSK + '][extension]', /[^.]+$/.exec(inputSK.documentFile))
            })

            formData.append('order[freight_mode]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].freightMode)
            formData.append('order[move_type]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].moveType)
            formData.append('order[is_outsource]', 1)
            formData.append('order[link_outsource]', window.location.origin + '/?enc=' + encodeURIComponent(Constants.ENC_ID) + '#/clickargo/redirect_orders/')

            formData.append('detail[place_of_receipt_location]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].placeOfReceiptLocation)
            formData.append('detail[place_of_destination_location]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].portOfLoadingLocation)
            formData.append('detail[place_of_loading_location]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].portOfDestinationLocation)
            formData.append('detail[place_of_discharge_location]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].placeOfDischargeLocation)
            formData.append('detail[place_of_discharge_state]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].placeOfDischargeState)
            formData.append('detail[place_of_discharge_address]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].placeOfDischargeAddress)

            formData.append('integration[requestor_npwp]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].requestorNPWP)
            formData.append('integration[requestor_nib]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].requestorNIB)
            formData.append('integration[requestor_name]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].requestorName)
            formData.append('integration[requestor_address]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].requestorAddress)

            formData.append('integration[consignee_name]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].consigneeName)
            formData.append('integration[consignee_npwp]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].consigneeNPWP)

            formData.append('integration[notify_party_name]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].notifyPartyName)
            formData.append('integration[notify_party_npwp]', JSON.parse(localStorage.getItem('ckDOInputForm'))[0].notifyPartyNPWP)

            const headers = {
                'Content-Type': 'multipart/form-data',
                'Accept-Language': 'application/json',
                'Accept': 'application/json',
                'Key': localStorage.getItem('ckHeaderKey'),
                'Secret': localStorage.getItem('ckHeaderSecret'),
                'Authorization': 'Bearer ' + localStorage.getItem('ckHeaderToken')
            }

            axios
                .post(ApiClickargo.CLICKARGO_STORE_DO, formData, {
                    headers: headers
                })
                .then(response => {
                    console.log('store DO success')
                    const headerUpdateOrderLink = {
                        'Content-Type': 'application/json',
                        'Key': localStorage.getItem('ckHeaderKey'),
                        'Secret': localStorage.getItem('ckHeaderSecret'),
                    }
                    axios
                        .put(ApiClickargo.CLICKARGO_UPDATE_ORDER_LINK, {
                            uuid: response.data.data.store[0].uuid,
                            link_outsource: window.location.origin + '/?enc=' + encodeURIComponent(Constants.ENC_ID) + '#/clickargo/redirect_orders/' + response.data.data.store[0].uuid
                        }, {
                            headers: headerUpdateOrderLink
                        })
                        .then(responseUpdateOrderLink => {
                            console.log('Update order link_outsource success')
                            setIsSubmitDO(false)

                            swalWithBootstrapButtons.fire({
                                title: 'Order Succesfully Placed',
                                icon: 'success',
                                html: '<h3 style="color: black;">JOB NUMBER</h3>' + '<h3 style="color: black;">' + response.data.data.store[0].job_number + '</h3>',
                                showCancelButton: false,
                                confirmButtonText: 'My DO Transaction',
                                reverseButtons: true,
                                allowOutsideClick: false
                            }).then((result) => {
                                if (result.value) {
                                    props.history.push({
                                        pathname: '/clickargo_do/my_transaction'
                                    })
                                }
                            })
                        })
                        .catch(error => {
                            if (!error.response) {
                                setIsSubmitDO(false)
                                addToast('You seems to be offline, please check your internet connection!', {
                                    appearance: 'error',
                                    autoDismiss: false,
                                })
                            } else {
                                setIsSubmitDO(false)
                                addToast(error.response.data.message, {
                                    appearance: 'error',
                                    autoDismiss: false,
                                })
                            }
                        })
                })
                .catch(error => {
                    if (!error.response) {
                        setIsSubmitDO(false)
                        addToast('You seems to be offline, please check your internet connection!', {
                            appearance: 'error',
                            autoDismiss: false,
                        })
                    } else {
                        setIsSubmitDO(false)
                        addToast(error.response.data.message, {
                            appearance: 'error',
                            autoDismiss: false,
                        })
                    }
                })

        }, 2500);
    }

    if (isLocalStorageExist === true) {
        return (
            isLoading ? (
                <Fragment>
                    <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
                </Fragment>
            ) : (
                    <Fragment>
                        <div style={{ marginBottom: "10px" }}>
                            <div style={{ float: "left" }}>
                                <h3 style={{ display: "inline" }} className={dashforgeCss.txBlack}>Review Your Request And Upload Document</h3>
                            </div>
                            <div style={{ float: "right" }}>
                                <img style={{ width: '200px' }} src={window.location.origin + '/assets/images/Clickargo5.png'} />
                            </div>
                        </div>

                        <br />
                        <br />

                        <div style={{ marginBottom: "10px" }}>
                            <label style={{ display: "inline", color: "#2d9ff7", cursor: 'pointer' }} onClick={onClickBack}>Back</label>
                        </div>

                        <div className="kt-portlet">
                            <div className="kt-portlet__body">

                                <div className="row">
                                    <div className="col-md-3">
                                        <label >Action Type</label>
                                        <h6 className={dashforgeCss.txBlack}>{JSON.parse(localStorage.getItem('ckDOInputForm'))[0].actionTypeLabel}</h6>
                                    </div>

                                    <div className="col-md-3">
                                        <label >Freight Mode</label>
                                        <h6 className={dashforgeCss.txBlack}>{JSON.parse(localStorage.getItem('ckDOInputForm'))[0].freightModeLabel}</h6>
                                    </div>

                                    <div className="col-md-3">
                                        <label >Move Type</label>
                                        <h6 className={dashforgeCss.txBlack}>{JSON.parse(localStorage.getItem('ckDOInputForm'))[0].moveTypeLabel}</h6>
                                    </div>

                                    <div className="col-md-3">
                                        <label >Transaction</label>
                                        <h6 className={dashforgeCss.txBlack}>E-DO</h6>
                                    </div>
                                </div>

                                <div>
                                    <hr style={{ border: "1px solid #f3f3f7" }} />
                                    <hr style={{ border: "1px solid #f3f3f7" }} />
                                </div>

                                <div className="row">
                                    <div className="col-md-6">
                                        <label>Place of Receipt</label>
                                        <h6 className={dashforgeCss.txBlack}>{JSON.parse(localStorage.getItem('ckDOInputForm'))[0].placeOfReceiptLocationLabel}, {JSON.parse(localStorage.getItem('ckDOInputForm'))[0].placeOfReceiptCountryLabel}</h6>
                                    </div>

                                    <div className="col-md-6">
                                        <label>Port of Loading</label>
                                        <h6 className={dashforgeCss.txBlack}>{JSON.parse(localStorage.getItem('ckDOInputForm'))[0].portOfLoadingLocationLabel}, {JSON.parse(localStorage.getItem('ckDOInputForm'))[0].portOfLoadingCountryLabel}</h6>
                                    </div>

                                    <div className="col-md-6">
                                        <label>Place of Destination</label>
                                        <h6 className={dashforgeCss.txBlack}>{JSON.parse(localStorage.getItem('ckDOInputForm'))[0].portOfDestinationLocationLabel}, {JSON.parse(localStorage.getItem('ckDOInputForm'))[0].portOfDestinationCountryLabel}</h6>
                                    </div>

                                    <div className="col-md-6">
                                        <label>Place of Discharge</label>
                                        <h6 className={dashforgeCss.txBlack}>{JSON.parse(localStorage.getItem('ckDOInputForm'))[0].placeOfDischargeLocationLabel}, {JSON.parse(localStorage.getItem('ckDOInputForm'))[0].placeOfDischargeCountryLabel}, {JSON.parse(localStorage.getItem('ckDOInputForm'))[0].placeOfDischargeStateLabel}, {JSON.parse(localStorage.getItem('ckDOInputForm'))[0].placeOfDischargeAddress}</h6>
                                    </div>
                                </div>

                                <div>
                                    <hr style={{ border: "1px solid #f3f3f7" }} />
                                </div>

                                <div className="row">
                                    <div className="col-md-12">
                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>DOCUMENTS</legend>
                                            <button className="btn btn-primary pull-right add-document" style={{ marginTop: '15px' }} type="button" onClick={toggle}>Add Documents</button>

                                            <br />
                                            <br />
                                            <br />

                                            <div className="kt-portlet__body">
                                                <div className="kt-widget15">
                                                    <BootstrapTable
                                                        bootstrap4
                                                        keyField='documentUuid'
                                                        data={inputArrDocuments}
                                                        columns={columns} />
                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                {/* <br />
                                <pre>
                                    {JSON.stringify(inputFieldsDocuments, null, 2)}
                                </pre>
                                <br />
                                <pre>
                                    {JSON.stringify(inputArrDocuments, null, 2)}
                                </pre> */}

                                <div className="text-center" >
                                    <button
                                        type="button"
                                        onClick={event => createOrderHandler(event)}
                                        className="btn btn-primary submit-btn"
                                        disabled={isSubmitDO}>
                                        {isSubmitDO && <i className="fa fa-circle-notch fa-spin"></i>}
                                        {isSubmitDO && <span>Please wait...</span>}
                                        {!isSubmitDO && <span>Create Order</span>}
                                    </button>
                                </div>

                            </div>
                        </div>

                        <Modal isOpen={modal} toggle={toggle} backdrop="static">
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <ModalHeader toggle={toggle}>Add Document</ModalHeader>
                                <ModalBody>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="form-group">
                                                <label className={dashforgeCss.txBlack}>Select document type<span className={dashforgeCss.txDanger}>*</span></label>
                                                <input
                                                    type="hidden"
                                                    name="documentType"
                                                    ref={register({
                                                        required: {
                                                            value: true,
                                                            message: 'This input field is required!'
                                                        }
                                                    })} />
                                                <Select
                                                    options={optionDocumentType}
                                                    value={{ label: inputFieldsDocuments[0].documentTypeLabel, value: inputFieldsDocuments[0].documentType }}
                                                    onChange={event => handleInputChangeDocuments('documentType', event)} />
                                                {errors.documentType && <span style={{ fontSize: 13, color: "red" }}>{errors.documentType.message}</span>}
                                            </div>
                                            <div className="form-group">
                                                <label className={dashforgeCss.txBlack}>Document Number<span className={dashforgeCss.txDanger}>*</span></label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="documentNumber"
                                                    placeholder="Enter Document Number"
                                                    onChange={event => handleInputChangeDocuments('documentNumber', event)}
                                                    value={inputFieldsDocuments[0].documentNumber}
                                                    ref={register({
                                                        required: {
                                                            value: true,
                                                            message: 'This input field is required!'
                                                        }
                                                    })} />
                                                {errors.documentNumber && <span style={{ fontSize: 13, color: "red" }}>{errors.documentNumber.message}</span>}
                                            </div>
                                            <div className="form-group">
                                                <label className={dashforgeCss.txBlack}>Document Date<span className={dashforgeCss.txDanger}>*</span></label>
                                                <input
                                                    type="date"
                                                    className="form-control"
                                                    name="documentDate"
                                                    onChange={event => handleInputChangeDocuments('documentDate', event)}
                                                    ref={register({
                                                        required: {
                                                            value: true,
                                                            message: 'This input field is required!'
                                                        }
                                                    })} />
                                                {errors.documentDate && <span style={{ fontSize: 13, color: "red" }}>{errors.documentDate.message}</span>}
                                            </div>
                                            <div className="form-group">
                                                <label className={dashforgeCss.txBlack}>File<span className={dashforgeCss.txDanger}>*</span></label>
                                                <input
                                                    type="file"
                                                    className="form-control"
                                                    name="documentFile"
                                                    onChange={event => handleInputChangeDocuments('documentFile', event)}
                                                    ref={register({
                                                        required: {
                                                            value: true,
                                                            message: 'This input field is required!'
                                                        }
                                                    })} />
                                                {errors.documentFile && <span style={{ fontSize: 13, color: "red" }}>{errors.documentFile.message}</span>}
                                            </div>
                                        </div>
                                    </div>
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="secondary" onClick={toggle}>Close</Button>{' '}
                                    <Button color="primary" type="submit">Add File</Button>
                                </ModalFooter>
                            </form>
                        </Modal>

                    </Fragment>
                )
        )
    } else {
        return (
            <Fragment>
                <div className="kt-portlet">
                    <div className="kt-portlet__body">

                        <div className="row">
                            <div className="col-md-12">

                                <label>Oops! You can't directly access this page. Please start from E-DO Menu</label>

                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }

}

export default Review
