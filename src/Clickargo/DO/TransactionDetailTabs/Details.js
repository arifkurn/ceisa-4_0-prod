import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../../Vessel/BookingRequest/dashforge.module.css'
import Spinner from 'react-bootstrap/Spinner'
import Table from 'react-bootstrap/Table'

function Details(props) {

    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(false)
        }, 1000);
    }, [])

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
                <Fragment>
                    <div className="row">
                        <div className="col-12">
                            <div className="row">

                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Loading Location</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_loading_location}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Loading Country</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_loading_country}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Loading Location Name</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_loading_location_name}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Destination Location</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_destination_location}</p>
                                </div>

                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Destination Country</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_destination_country}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Destination Location Name</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_destination_location_name}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Discharge Location</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_discharge_location}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Discharge Location Name</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_discharge_location_name}</p>
                                </div>

                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Discharge Country</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_discharge_country}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Discharge State</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_discharge_state}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Discharge Address</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_discharge_address}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Name Of User</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.name_of_user}</p>
                                </div>

                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Receipt Location</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_receipt_location}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Receipt Country</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_receipt_country}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Place Of Receipt Location Name</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.place_of_receipt_location_name}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Bl Date</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.bl_date}</p>
                                </div>

                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Bl Number</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.bl_number}</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </Fragment>
            )
    )
}

export default Details
