import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../../../Vessel/BookingRequest/dashforge.module.css'
import Table from 'react-bootstrap/Table'
import { withRouter } from 'react-router-dom'
import moment from "moment"
import axios from 'axios'
import * as ApiClickargo from '../../../ApiList.js'

function Invoice(props) {

    const onCancel = () => {
        props.history.push('/clickargo_do/my_transaction_detail')
    }

    const onFinish = () => {
        axios
            .post(ApiClickargo.CLICKARGO_FIND_ORDER_BY_JOB_NUMBER, {
                job_number: localStorage.getItem('ckDOJobNumber')
            })
            .then(response => {
                axios
                    .put('https://elogistic.ms.api.demo-qs.xyz/api/v1/order/status/update', {
                        uuid: response.data.data.uuid,
                        admin_status: 'complete',
                        user_status: 'complete',
                        payment_status: 3
                    })
                    .then(responseUpdateStatus => {
                        props.history.push('/clickargo_do/my_transaction')
                    })
            })
            .catch(error => {
                console.log(error)
            })
    }

    return (
        <Fragment>
            <div className="kt-portlet">
                <div className="kt-portlet__body">
                    <div className="kt-widget15">
                        <div className="row">
                            <div className="col-12">
                                <div className="text-center">
                                    <i className="fas fa-shopping-cart fa-4x" style={{ color: '#1053b3' }}></i>
                                    <br /><br /><br />
                                    <h4 className={dashforgeCss.txBlack}>Invoice</h4>
                                    <hr />
                                    <h5 className={dashforgeCss.txBlack}>Please confirm to proceed the payment</h5>
                                    <br />
                                    <Table responsive>
                                        <thead style={{ backgroundColor: 'black', color: 'white' }}>
                                            <tr>
                                                <th style={{ textAlign: 'center' }}>Component Billing</th>
                                                <th style={{ textAlign: 'center' }}>Qty</th>
                                                <th style={{ textAlign: 'center' }}>Amount (IDR)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Ocean Freight</td>
                                                <td>1</td>
                                                <td>4.249.500</td>
                                            </tr>
                                            <tr>
                                                <td>Ocean Freight</td>
                                                <td>1</td>
                                                <td>3.966.200</td>
                                            </tr>
                                            <tr>
                                                <td>Subtotal</td>
                                                <td></td>
                                                <td>8.215.722</td>
                                            </tr>
                                            <tr>
                                                <td>PPN (10%)</td>
                                                <td></td>
                                                <td>821.572</td>
                                            </tr>
                                            <tr>
                                                <td>Total</td>
                                                <td></td>
                                                <td>9.037.294</td>
                                            </tr>
                                            <tr>
                                                <td>Payment Method</td>
                                                <td></td>
                                                <td>VA Bank BNI</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                    <br/>
                                    <label className={dashforgeCss.txBlack}><b>BNI Virtual Account : 110239834384348</b></label>
                                    <br/>
                                    <label className={dashforgeCss.txBlack}><b>Please Pay Before : 25-06-2020 {moment().format("hh:mm:ss")}</b></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="text-right">
                        <button
                            type="button"
                            className="btn btn-warning submit-btn"
                            style={{color: 'white'}}
                            onClick={onCancel}>
                            Cancel
                        </button>
                        &nbsp;
                        <button
                            type="button"
                            className="btn btn-success submit-btn"
                            style={{color: 'white'}}
                            onClick={onFinish}>
                            Finish
                        </button>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default withRouter(Invoice)
