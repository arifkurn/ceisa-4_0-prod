import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../Vessel/BookingRequest/dashforge.module.css'
import { withRouter, Link } from 'react-router-dom'
import { Tabs, Tab } from 'react-bootstrap'
import Spinner from 'react-bootstrap/Spinner'
import { ToastProvider } from 'react-toast-notifications'
import axios from 'axios'
import Documents from './TransactionDetailTabs/Documents'
import Payment from './TransactionDetailTabs/Payment'
import Details from './TransactionDetailTabs/Details'
import { useToasts } from 'react-toast-notifications'
import * as ApiClickargo from '../ApiList.js'

function MyTransactionDetail() {

    const { addToast } = useToasts()

    const [activeTabs, setActiveTabs] = useState('shipmentDetail')

    const [orderDetails, setOrderDetails] = useState([])

    const [orderCreatedAt, setOrderCreatedAt] = useState('')

    const [isLoading, setIsLoading] = useState(true)

    const [statusOrder, setStatusOrder] = useState(false)

    useEffect(() => {
        //update state
        setTimeout(() => {
            setOrderCreatedAt(localStorage.getItem('ckDODateConvert'))

            const headerOrderGetDetailBilling = {
                'Key': localStorage.getItem('ckHeaderKey'),
                'Secret': localStorage.getItem('ckHeaderSecret'),
            }

            axios
                .post(ApiClickargo.CLICKARGO_FIND_ORDER_BY_JOB_NUMBER, {
                    job_number: localStorage.getItem('ckDOJobNumber')
                })
                .then(response => {
                    setOrderDetails([...orderDetails, response.data.data])

                    axios
                        .post(ApiClickargo.CLICKARGO_ORDER_GET_DETAIL_BILLING, {
                            order_no: localStorage.getItem('ckDOJobNumber')
                        }, {
                            headers: headerOrderGetDetailBilling
                        })
                        .then(responseGetDetailBilling => {

                            if (responseGetDetailBilling.data.success === true) {
                                setStatusOrder(true)

                                setIsLoading(false)
                            }

                        })
                        .catch(error => {
                            console.log(error.response)
                            if (error.response.data.success === false) {
                                setStatusOrder(false)
                                setIsLoading(false)
                            } else {
                                addToast('Failed get Billing Information, please try again!', {
                                    appearance: 'error',
                                    autoDismiss: true,
                                })
                            }
                        })
                })
                .catch(error => {
                    console.log(error.response.data.message)
                    addToast(error.response.data.message, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                })


        }, 1000);

    }, [])

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
                <Fragment>
                    <div style={{ marginBottom: "10px" }}>
                        <Link to="/clickargo_do/my_transaction" style={{ display: "inline", color: "#2d9ff7" }} >
                            Back
                        </Link>
                    </div>

                    <div className="kt-portlet">
                        <div className="kt-portlet__body" style={{ paddingLeft: '25px', paddingTop: '0px', paddingRight: '0px', paddingBottom: '0px' }}>
                            <div className="kt-widget15">
                                <div className={`col-12`}>
                                    <div className="row">
                                        <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20}>
                                            <h3 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails[0].type}</h3>
                                            <span className={dashforgeCss.upperTitle + ` ` + dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderDetails[0].move_type}</span>
                                            <span className={dashforgeCss.upperTitle + ` ` + dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderCreatedAt}</span>
                                            <h5 className={dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderDetails[0].job_number}</h5>
                                        </div>
                                        <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                            <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>BL NO</h4>
                                            <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails[0].details.bl_number}</h5>
                                        </div>
                                        <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                            <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>BL Date</h4>
                                            <h4 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails[0].details.bl_date}</h4>
                                        </div>
                                        <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                            <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Transaction</h4>
                                            <h4 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails[0].transaction}</h4>
                                        </div>
                                        <div className={dashforgeCss.statusBox + ` ` + dashforgeCss.alignSelfCenter + ` col-md-4 ` + dashforgeCss.pd60And20}>
                                            <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Status</h4>
                                            <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txWhite}>{orderDetails[0].user_status}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="kt-portlet">
                        <div className="kt-portlet__body">
                            <div className="kt-widget15">
                                <ToastProvider>
                                    <Tabs activeKey={activeTabs} onSelect={k => setActiveTabs(k)} unmountOnExit>
                                        <Tab eventKey="shipmentDetail" title="Details">
                                            <Details detailsOrder={orderDetails[0]} />
                                        </Tab>
                                        <Tab eventKey="shipmentDocuments" title="Documents">
                                            <Documents detailsOrder={orderDetails[0]} />
                                        </Tab>
                                        {statusOrder ? (
                                            <Tab eventKey="shipmentPayment" title="Payment">
                                                <Payment dataPaymentDetail={orderDetails[0]} />
                                            </Tab>
                                        ) : ''}
                                    </Tabs>
                                </ToastProvider>
                            </div>
                        </div>
                    </div>
                </Fragment>
            )
    )
}

export default withRouter(MyTransactionDetail)
