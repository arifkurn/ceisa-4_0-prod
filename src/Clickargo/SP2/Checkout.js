import React, { Fragment, useState, useEffect } from 'react'
import dashforgeCss from '../../Vessel/BookingRequest/dashforge.module.css'
import Spinner from 'react-bootstrap/Spinner'
import Table from 'react-bootstrap/Table'
import { useToasts } from 'react-toast-notifications'
import { useForm } from 'react-hook-form'
import { Button, Modal, ModalBody, ModalFooter } from 'reactstrap'
import axios from 'axios'
import * as Constants from '../../Constants.js'
import Swal from 'sweetalert2'
import { withRouter } from 'react-router-dom'
import moment from 'moment'
import * as ApiClickargo from '../ApiList.js'

function Checkout({ backToContainer, props }) {

    const { register, handleSubmit, errors, watch, setError, clearError, setValue, getValues } = useForm()

    const { addToast } = useToasts()

    const [isLoading, setIsLoading] = useState(true)

    const [isSubmitSP2, setIsSubmitSP2] = useState(false)

    const [paymentMethod, setPaymentMethod] = useState([
        {
            payment: '',
        }
    ])

    const [modal, setModal] = useState(false)

    const toggle = () => setModal(!modal)

    const [billingInformation, setBillingInformation] = useState([])

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-primary',
            cancelButton: 'btn btn-info'
        },
        buttonsStyling: false
    })

    useEffect(() => {
        setTimeout(() => {

            const headerGenerateBilling = {
                'Secret': localStorage.getItem('ckHeaderSecret'),
                'Key': localStorage.getItem('ckHeaderKey'),
                'Accept-Language': 'application/json'
            }

            axios
                .post(ApiClickargo.CLICKARGO_SP2_GENERATE_BILLING, {
                    transaction_id: localStorage.getItem('ckSP2ConfirmTrxId')
                }, {
                    headers: headerGenerateBilling
                })
                .then(response => {
                    setBillingInformation([...billingInformation, response.data.data])
                    setIsLoading(false)
                })
                .catch(error => {
                    addToast('Failed get Billing Information, please try again!', {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                    console.log(error)
                })
        }, 1000);
    }, [])

    const handleInputChange = (field, event) => {
        const values = [...paymentMethod]

        if (field === "payment") {
            values[0].payment = event.target.value
        }

        setPaymentMethod(values)
    }

    const onCreatedOrder = () => {
        setModal(!modal)
    }

    const onConfirmed = () => {
        setModal(!modal)
        setIsSubmitSP2(true)

        setTimeout(() => {
            const headerStoreSP2 = {
                'Secret': localStorage.getItem('ckHeaderSecret'),
                'Key': localStorage.getItem('ckHeaderKey'),
                'Accept-Language': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('ckHeaderToken')
            }

            let formData = new FormData()

            const checkSP2Form = JSON.parse(localStorage.getItem('ckSP2Form'))

            formData.append('order[freight_mode]', 'ocean')
            formData.append('order[move_type]', checkSP2Form.moveType)

            formData.append('details[transaction_type]', checkSP2Form.transactionType)
            formData.append('details[transaction_type_name]', checkSP2Form.transactionTypeLabel)
            formData.append('details[document_id]', checkSP2Form.documentType)
            formData.append('details[document_type]', checkSP2Form.documentTypeLabel)
            formData.append('details[document_date]', checkSP2Form.date)
            formData.append('details[document_shipping_date]', checkSP2Form.doDate)
            formData.append('details[terminal_operator]', checkSP2Form.terminalOperatorLabel)
            formData.append('details[document_no]', JSON.parse(localStorage.getItem('ckSP2AdditionalForm')).noDO)
            formData.append('details[bl_nbr]', JSON.parse(localStorage.getItem('ckSP2AdditionalForm')).blNo)
            formData.append('details[pin]', JSON.parse(localStorage.getItem('ckSP2AdditionalForm')).doPin)
            formData.append('details[paid_thru]', JSON.parse(localStorage.getItem('ckSP2AdditionalForm')).paidThru)

            formData.append('details[payment_method]', paymentMethod[0].payment)
            formData.append('details[payment_detail]', JSON.stringify(billingInformation[0], null, 2))

            JSON.parse(localStorage.getItem('ckSP2ContainerSelected')).map((inputContainer, indexContainer) => {
                formData.append('containers[' + indexContainer + '][container_id]', inputContainer.id)
                formData.append('containers[' + indexContainer + '][customer_id]', inputContainer.cust_id)
                formData.append('containers[' + indexContainer + '][vessel_id]', inputContainer.vessel_id)
                formData.append('containers[' + indexContainer + '][voyage_no]', inputContainer.voyage_no)
                formData.append('containers[' + indexContainer + '][vessel_name]', inputContainer.vessel_name)
                formData.append('containers[' + indexContainer + '][eta]', inputContainer.eta)
                formData.append('containers[' + indexContainer + '][ata]', inputContainer.ata)
                formData.append('containers[' + indexContainer + '][etd]', inputContainer.etd)
                formData.append('containers[' + indexContainer + '][line_id]', inputContainer.line_id)
                formData.append('containers[' + indexContainer + '][no_container]', inputContainer.no_container)
                formData.append('containers[' + indexContainer + '][code]', inputContainer.container_size + inputContainer.container_type)
                formData.append('containers[' + indexContainer + '][owner]', inputContainer.owner)
            })

            formData.append('integrations[transaction_id]', localStorage.getItem('ckSP2ConfirmTrxId'))
            formData.append('order[is_outsource]', 1)
            //filled link to route user into complete payment page
            formData.append('order[link_outsource]', window.location.origin + '/?enc=' + encodeURIComponent(Constants.ENC_ID) + '#/clickargo/redirect_orders/')

            axios
                .post(ApiClickargo.CLICKARGO_SP2_STORE_ORDER, formData, {
                    headers: headerStoreSP2
                })
                .then(response => {
                    console.log('sp2 submitted success')
                    //update link outsource value
                    const headerUpdateOrderLink = {
                        'Content-Type': 'application/json',
                        'Key': localStorage.getItem('ckHeaderKey'),
                        'Secret': localStorage.getItem('ckHeaderSecret'),
                    }

                    axios
                        .put(ApiClickargo.CLICKARGO_UPDATE_ORDER_LINK, {
                            uuid: response.data.data.store[0].uuid,
                            link_outsource: window.location.origin + '/?enc=' + encodeURIComponent(Constants.ENC_ID) + '#/clickargo/redirect_orders/' + response.data.data.store[0].uuid
                        }, {
                            headers: headerUpdateOrderLink
                        })
                        .then(responseUpdateOrderLink => {
                            console.log('Update order link_outsource success')
                            //create order billing VA
                            const headerStoreGenerateBilling = {
                                'Secret': localStorage.getItem('ckHeaderSecret'),
                                'Key': localStorage.getItem('ckHeaderKey')
                            }

                            let platform_fee = JSON.parse(localStorage.getItem('ckSP2ContainerSelected')).length * 10000
                            let ppn_platform_fee = (platform_fee * 10) / 100

                            let billingTotal = billingInformation[0].total
                            let actualAmountFee = platform_fee + ppn_platform_fee
                            let actualAmountTotal = parseInt(billingTotal) + platform_fee + ppn_platform_fee

                            axios
                                .post(ApiClickargo.CLICKARGO_SP2_ORDER_GENERATE_BILLING, {
                                    customer: props.dataUserPortal.nama_perusahaan,
                                    email: props.dataUserPortal.email,
                                    phone: '',
                                    stakeholder: 'KOJA',
                                    proforma_invoice_no: 'KOJA-' + billingInformation[0].proforma_invoice_no,
                                    order_no: response.data.data.store[0].job_number,
                                    action: 'IMPORT',
                                    product: 'SP2',
                                    bank: paymentMethod[0].payment.substring(3).toUpperCase(),
                                    payment_method: paymentMethod[0].payment.substring(0, 2),
                                    payment_expired: moment(billingInformation[0].expired).format('YYYY-MM-DD hh:mm'),
                                    shipment_mode: 'OCEAN',
                                    amount: billingTotal,
                                    amount_fee: actualAmountFee.toString(),
                                    amount_total: actualAmountTotal.toString(),
                                    components: [
                                        {
                                            name: 'SUB TOTAL',
                                            amount: billingInformation[0].subtotal,
                                        },
                                        {
                                            name: 'PPN',
                                            amount: billingInformation[0].ppn,
                                        },
                                        {
                                            name: 'TOTAL',
                                            amount: billingInformation[0].total,
                                        },
                                        {
                                            name: 'PLATFORM FEE',
                                            amount: platform_fee.toString(),
                                            is_fee: 1
                                        },
                                        {
                                            name: 'PPN PLATFORM FEE',
                                            amount: ppn_platform_fee.toString(),
                                            is_fee: 1
                                        },
                                    ],
                                    billing_attributes: [
                                        {
                                            name: 'CUSTOMER NPWP',
                                            value: props.dataUserPortal.npwp,
                                        },
                                        {
                                            name: 'CUSTOMER NAME',
                                            value: props.dataUserPortal.nama_kontak_person,
                                        },
                                    ]
                                }, {
                                    headers: headerStoreGenerateBilling
                                })
                                .then(responseOrderCreateBilling => {
                                    console.log('submit order create billing success')
                                    //sp2 notification email send
                                    axios
                                        .post(ApiClickargo.CLICKARGO_SP2_NOTIFICATION_EMAIL_SEND, {
                                            uuid: responseOrderCreateBilling.data.data.uuid,
                                            job_number: response.data.data.store[0].job_number,
                                            email: props.dataUserPortal.email, //comment for testing purposed
                                            // email: 'kusnle5@yopmail.com', //testing purposed
                                            bank_name: paymentMethod[0].payment,
                                            va_number: responseOrderCreateBilling.data.data.bank_account_no,
                                            amount: responseOrderCreateBilling.data.data.amount,
                                            expired: responseOrderCreateBilling.data.data.payment_expired
                                        }, {
                                            headers: headerStoreGenerateBilling
                                        })
                                        .then(responseNotifSendEmail => {
                                            console.log('success send email sp2 notification')
                                            swalWithBootstrapButtons.fire({
                                                title: 'Order Succesfully Placed',
                                                icon: 'success',
                                                html: '<h3 style="color: black;">JOB NUMBER</h3>' + '<h3 style="color: black;">' + response.data.data.store[0].job_number + '</h3>',
                                                showCancelButton: false,
                                                confirmButtonText: 'My SP 2 Transaction',
                                                reverseButtons: true,
                                                allowOutsideClick: false
                                            }).then((result) => {
                                                if (result.value) {
                                                    props.history.push({
                                                        pathname: '/clickargo_sp2/my_transaction'
                                                    })
                                                }
                                            })
                                            setIsSubmitSP2(false)
                                        })
                                        .catch(error => {
                                            setIsSubmitSP2(false)
                                            addToast('Failed Send SP 2 Email Notification', {
                                                appearance: 'error',
                                                autoDismiss: true,
                                            })
                                            console.log(error)
                                        })
                                })
                                .catch(error => {
                                    setIsSubmitSP2(false)
                                    addToast('Failed Store Order Generate Billing, please try again!', {
                                        appearance: 'error',
                                        autoDismiss: true,
                                    })
                                    console.log(error)
                                })
                        })
                        .catch(error => {
                            if (!error.response) {
                                setIsSubmitSP2(false)
                                addToast('You seems to be offline, please check your internet connection!', {
                                    appearance: 'error',
                                    autoDismiss: false,
                                })
                            } else {
                                setIsSubmitSP2(false)
                                addToast('Failed Update Order Link', {
                                    appearance: 'error',
                                    autoDismiss: false,
                                })
                            }
                        })

                })
                .catch(error => {
                    setIsSubmitSP2(false)
                    addToast('Failed store SP2, please try again!', {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                    console.log(error)
                })
        }, 1000);
    }

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
                <Fragment>
                    <div style={{ marginBottom: "10px" }}>
                        <div style={{ float: "left" }}>
                            <h3 style={{ display: "inline" }} className={dashforgeCss.txBlack}>Clickargo SP2 - Checkout</h3>
                        </div>
                        <div style={{ float: "right" }}>
                            <img style={{ width: '200px' }} src={window.location.origin + '/assets/images/Clickargo5.png'} />
                        </div>
                    </div>

                    <br />
                    <br />

                    <div style={{ marginBottom: "10px" }}>
                        <label style={{ display: "inline", color: "#2d9ff7", cursor: 'pointer' }} onClick={backToContainer}>Back</label>
                    </div>

                    <form onSubmit={handleSubmit(onCreatedOrder)}>
                        <div className="kt-portlet">
                            <div className="kt-portlet__body">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="row">

                                            <div className="col-3">
                                                <label className={dashforgeCss.txBlack}>Payment Method</label>
                                                <hr />
                                                <div className="form-group">
                                                    <input
                                                        type="radio"
                                                        className="ml-3"
                                                        onChange={event => handleInputChange('payment', event)}
                                                        value="VA Bank BNI"
                                                        name={`payment`}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })}
                                                    />&nbsp;
                                                 <img style={{ width: '50px', paddingBottom: '5px' }} src={window.location.origin + '/assets/images/clickargo-bni.png'} />
                                                 &nbsp;BNI Virtual Account
                                                 {errors[`payment`] && <br />}
                                                    {errors[`payment`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`payment`].message}</span>}
                                                </div>
                                            </div>
                                            <div className="col-9">
                                                <label className={dashforgeCss.txBlack}>Order Detail</label>
                                                <hr />
                                                <Table responsive>
                                                    <thead style={{ backgroundColor: 'black', color: 'white' }}>
                                                        <tr>
                                                            <th style={{ textAlign: 'left' }}>Container No</th>
                                                            <th style={{ textAlign: 'left' }}>Container Type</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {JSON.parse(localStorage.getItem('ckSP2ContainerSelected')).map((inputContainer, indexContainer) => (
                                                            <Fragment>
                                                                <tr>
                                                                    <td>{inputContainer.no_container}</td>
                                                                    <td>{inputContainer.container_size + inputContainer.container_type}</td>
                                                                </tr>
                                                            </Fragment>
                                                        ))}
                                                        <tr>
                                                            <td>Subtotal</td>
                                                            <td>{billingInformation[0].subtotal}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>PPN</td>
                                                            <td>{billingInformation[0].ppn}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total</td>
                                                            <td>{billingInformation[0].total}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Payment Method</td>
                                                            <td>{paymentMethod[0].payment}</td>
                                                        </tr>
                                                    </tbody>
                                                </Table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="text-right">
                                    <button
                                        type="submit"
                                        className="btn btn-primary submit-btn"
                                        disabled={isSubmitSP2}>
                                        {isSubmitSP2 && <i className="fa fa-circle-notch fa-spin"></i>}
                                        {isSubmitSP2 && <span>Please wait...</span>}
                                        {!isSubmitSP2 && <span>Create Order</span>}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    {/* <br />
                    <pre>
                        {JSON.stringify(billingInformation, null, 2)}
                    </pre> */}
                    {/* <br />
                    <pre>
                        {JSON.stringify(paymentMethod, null, 2)}
                    </pre> */}

                    <Modal isOpen={modal} toggle={toggle} backdrop="static">
                        <ModalBody>
                            <div className="row">
                                <div className="col-12">
                                    <div className="text-center">
                                        <i className="fas fa-shopping-cart fa-4x" style={{ color: '#1053b3' }}></i>
                                        <br /><br /><br />
                                        <h4 className={dashforgeCss.txBlack}>ORDER SUMMARY</h4>
                                        <hr />
                                        <h5 className={dashforgeCss.txBlack}>Please confirm to proceed the payment</h5>
                                        <br />
                                        <Table responsive>
                                            <thead style={{ backgroundColor: 'black', color: 'white' }}>
                                                <tr>
                                                    <th style={{ textAlign: 'center' }}>Container No</th>
                                                    <th style={{ textAlign: 'center' }}>Container Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {JSON.parse(localStorage.getItem('ckSP2ContainerSelected')).map((inputContainer, indexContainer) => (
                                                    <Fragment>
                                                        <tr>
                                                            <td>{inputContainer.no_container}</td>
                                                            <td>{inputContainer.container_size + inputContainer.container_type}</td>
                                                        </tr>
                                                    </Fragment>
                                                ))}
                                                <tr>
                                                    <td>Subtotal</td>
                                                    <td>{billingInformation[0].subtotal}</td>
                                                </tr>
                                                <tr>
                                                    <td>PPN</td>
                                                    <td>{billingInformation[0].ppn}</td>
                                                </tr>
                                                <tr>
                                                    <td>Total</td>
                                                    <td>{billingInformation[0].total}</td>
                                                </tr>
                                                <tr>
                                                    <td>Payment Method</td>
                                                    <td>{paymentMethod[0].payment}</td>
                                                </tr>
                                            </tbody>
                                        </Table>
                                    </div>
                                </div>
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="secondary" onClick={toggle}>Close</Button>{' '}
                            <Button color="primary" type="button" onClick={onConfirmed}>Process</Button>
                        </ModalFooter>
                    </Modal>
                </Fragment>
            )

    )
}

export default withRouter(Checkout)
