import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../../Vessel/BookingRequest/dashforge.module.css'
import Spinner from 'react-bootstrap/Spinner'
import Table from 'react-bootstrap/Table'

function Details(props) {

    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(false)
        }, 1000);
    }, [])

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
                <Fragment>
                    <div className="row">
                        <div className="col-12">
                            <div className="row">

                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Transaction Type</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.transaction_type}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Transaction Type Name</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.transaction_type_name}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Document Id</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.document_id}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Document Type</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.document_type}</p>
                                </div>

                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Document Date</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.document_date}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Document Shipping Date</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.document_shipping_date}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Terminal Operator</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.terminal_operator}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Document No</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.document_no}</p>
                                </div>

                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>BL Number</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.bl_nbr}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Pin</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.pin.length > 0 ? props.detailsOrder.details.pin : '-'}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Paid Thru</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.paid_thru}</p>
                                </div>
                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Payment Method</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.payment_method}</p>
                                </div>

                                <div className="col-3">
                                    <p className={dashforgeCss.textTitle + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.noUpper}>Name Of User</p>
                                    <p className={dashforgeCss.txBlack}>{props.detailsOrder.details.name_of_user}</p>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div className={dashforgeCss.marginTop25}>
                        <h6 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.marginBottom25 + ` ` + dashforgeCss.txBlack}>Containers</h6>
                        <Table responsive>
                            <thead style={{ backgroundColor: '#1f53b3', color: 'white' }}>
                                <tr>
                                    <th style={{ textAlign: 'left' }}>No</th>
                                    <th style={{ textAlign: 'left' }}>Container Number</th>
                                    <th style={{ textAlign: 'left' }}>Container Code</th>
                                    <th style={{ textAlign: 'left' }}>ETA</th>
                                    <th style={{ textAlign: 'left' }}>ETD</th>
                                </tr>
                            </thead>
                            <tbody>
                                {props.detailsOrder.containers.map((inputContainer, indexContainer) => (
                                    <Fragment key={`${inputContainer}~${indexContainer}`}>
                                        <tr>
                                            <td>{indexContainer + 1}</td>
                                            <td>{inputContainer.attribute.no_container}</td>
                                            <td>{inputContainer.code}</td>
                                            <td>{inputContainer.attribute.eta}</td>
                                            <td>{inputContainer.attribute.etd}</td>
                                        </tr>
                                    </Fragment>
                                ))}

                            </tbody>
                        </Table>
                    </div>
                </Fragment>
            )
    )
}

export default Details
