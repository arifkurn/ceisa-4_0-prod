import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../Vessel/BookingRequest/dashforge.module.css'
import { UncontrolledCollapse } from 'reactstrap'
import Spinner from 'react-bootstrap/Spinner'
import axios from 'axios'
import Swal from 'sweetalert2'
import * as ApiClickargo from '../ApiList.js'

function MyTransaction(props) {

    const [isLoading, setIsLoading] = useState(true)

    const [arrVesselList, setArrVesselList] = useState([]);

    const detailShipment = (job_number, date_convert) => {
        localStorage.setItem('ckSP2JobNumber', job_number)
        localStorage.setItem('ckSP2DateConvert', date_convert)
        props.history.push('/clickargo_sp2/my_transaction_detail')
    }

    useEffect(() => {
        var month_name = function (dt) {
            var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            return mlist[dt.getMonth()];
        }

        axios
            .post(ApiClickargo.CLICKARGO_ORDER_LIST_BY_NPWP, {
                // tax_number: '541267799887999'
                tax_number: props.dataUserPortal.npwp
            })
            .then(response => {
                console.log('success get job vessel list')
                const values = [...arrVesselList]
                response.data.data.filter(opt => opt.orders[0].transaction === 'sp2' && opt.orders[0].type === 'import').map((inputOrder, indexOrder) => {
                    const dateOrder = new Date(inputOrder.created_at)
                    values.push({
                        reference_number: inputOrder.value,
                        created_at: dateOrder.getDate() + ' ' + month_name(dateOrder) + ' ' + dateOrder.getFullYear(),
                        created_by: JSON.parse(inputOrder.orders[0].attribute).user_name,
                        orders:
                            inputOrder.orders.map((orderDetails, indexOrderDetails) => (
                                {
                                    type: orderDetails.type,
                                    move_type: orderDetails.move_type,
                                    created_at: dateOrder.getDate() + ' ' + month_name(dateOrder) + ' ' + dateOrder.getFullYear(),
                                    job_number: orderDetails.job_number,
                                    product_type: orderDetails.product_type,
                                    freight_mode: orderDetails.freight_mode,
                                    transaction: orderDetails.transaction,
                                    user_status: orderDetails.user_status,
                                    document_type: JSON.parse(orderDetails.attribute).document_type,
                                    document_date: JSON.parse(orderDetails.attribute).document_date
                                }
                            ))
                    })
                    setArrVesselList(values)
                })
                setIsLoading(false)
            })
            .catch(error => {
                if (!error.response) {
                    Swal.fire(
                        'Oops!',
                        'Please check your internet connection!',
                        'error'
                    )
                } else {
                    Swal.fire(
                        'Oops!',
                        'Failed get SP 2 Transaction, please try again!',
                        'error'
                    )
                    console.log(error.response.data.message)
                }
                setIsLoading(false)
            })
    }, [])

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
                <Fragment>
                    {arrVesselList.map((inputOrder, indexOrder) => (
                        <Fragment>
                            <div className="kt-portlet" id={`toggler` + indexOrder}>
                                <div className="kt-portlet__body" style={{ cursor: 'pointer', paddingLeft: '25px', paddingTop: '0px', paddingRight: '0px', paddingBottom: '0px' }}>
                                    <div className="kt-widget15">
                                        <div className="row">
                                            <div className={dashforgeCss.alignSelfCenter + ` col-md-3 ` + dashforgeCss.pd15And20}>
                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Reference No</h4>
                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{inputOrder.reference_number}</h5>
                                            </div>
                                            <div className={dashforgeCss.alignSelfCenter + ` col-md-3 ` + dashforgeCss.pd15And20}>
                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Created At</h4>
                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{inputOrder.created_at}</h5>
                                            </div>
                                            <div className={dashforgeCss.alignSelfCenter + ` col-md-3 ` + dashforgeCss.pd15And20}>
                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Created By</h4>
                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{inputOrder.created_by}</h5>
                                            </div>
                                            <div className={dashforgeCss.statusBox + ` ` + dashforgeCss.alignSelfCenter + ` col-md-3 ` + dashforgeCss.pd60And20}>
                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txWhite}>Click to See Transaction</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {inputOrder.orders.map((orderDetails, indexOrderDetails) => (
                                <Fragment>
                                    <UncontrolledCollapse toggler={`#toggler` + indexOrder} style={{ paddingLeft: '10px', paddingRight: '5px' }}>
                                        <div className="kt-portlet">
                                            <div className="kt-portlet__body" onClick={() => detailShipment(orderDetails.job_number, orderDetails.created_at)} style={{ cursor: 'pointer', paddingLeft: '25px', paddingTop: '0px', paddingRight: '0px', paddingBottom: '0px' }}>
                                                <div className="kt-widget15">
                                                    <div className={`col-12`}>
                                                        <div className="row">
                                                            <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20}>
                                                                <h3 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails.type}</h3>
                                                                <span className={dashforgeCss.upperTitle + ` ` + dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderDetails.move_type}</span>
                                                                <span className={dashforgeCss.upperTitle + ` ` + dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderDetails.created_at}</span>
                                                                <h5 className={dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderDetails.job_number}</h5>
                                                            </div>
                                                            <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Document Type</h4>
                                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails.document_type}</h5>
                                                            </div>
                                                            <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Document Date</h4>
                                                                <h4 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails.document_date}</h4>
                                                            </div>
                                                            <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Transaction</h4>
                                                                <h4 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails.transaction}</h4>
                                                            </div>
                                                            <div className={dashforgeCss.statusBox + ` ` + dashforgeCss.alignSelfCenter + ` col-md-4 ` + dashforgeCss.pd60And20}>
                                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Status</h4>
                                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txWhite}>{orderDetails.user_status}</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </UncontrolledCollapse>
                                </Fragment>
                            ))}
                        </Fragment>
                    ))}

                </Fragment>
            )
    )
}

export default MyTransaction
