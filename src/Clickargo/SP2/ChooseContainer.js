import React, { useMemo, useState, Fragment, useEffect, useCallback } from 'react'
import dashforgeCss from '../../Vessel/BookingRequest/dashforge.module.css'
import Spinner from 'react-bootstrap/Spinner'
import DataTable from 'react-data-table-component'
import { Collapse } from 'reactstrap'
import { useToasts } from 'react-toast-notifications'
import { useForm } from 'react-hook-form'
import Checkout from './Checkout'
import axios from 'axios'
import * as ApiClickargo from '../ApiList.js'

function ChooseContainer(props) {

    const { register, handleSubmit, errors, watch, setError, clearError, setValue, getValues } = useForm()

    const { addToast } = useToasts()

    const [isLoading, setIsLoading] = useState(true)

    const [selectedRows, setSelectedRows] = useState([])

    const [isSubmitSP2, setIsSubmitSP2] = useState(false)

    const [isOpen, setIsOpen] = useState(false)

    const toggle = () => setIsOpen(!isOpen)

    const [dataContainer, setDataContainer] = useState([])

    const [additionalData, setAdditionalData] = useState([
        {
            noDO: JSON.parse(localStorage.getItem('ckSP2Form')).no,
            blNo: JSON.parse(localStorage.getItem('ckSP2Form')).no,
            doPin: '',
            paidThru: ''
        }
    ])

    const [isCheckout, setIsCheckout] = useState(false)


    useEffect(() => {
        setTimeout(() => {
            setDataContainer(JSON.parse(localStorage.getItem('ckSP2ContainerList')).container)
            setIsLoading(false)
        }, 1200);
    }, [])

    const handleSelectedRows = useCallback(state => {
        setSelectedRows(state.selectedRows);
    }, []);

    const onContinueToPayment = () => {
        setIsSubmitSP2(true)

        setTimeout(() => {
            if (selectedRows.length > 0) {
                let formData = new FormData()

                const checkSP2Form = JSON.parse(localStorage.getItem('ckSP2Form'))
                
                formData.append('details[transaction_type]', checkSP2Form.transactionType)
                formData.append('details[transaction_type_name]', checkSP2Form.transactionTypeLabel)
                formData.append('details[document_id]', checkSP2Form.documentType)
                formData.append('details[document_type]', checkSP2Form.documentTypeLabel)
                formData.append('details[document_date]', checkSP2Form.date)
                formData.append('details[document_shipping_date]', checkSP2Form.doDate)
                formData.append('details[terminal_operator]', checkSP2Form.terminalOperatorLabel)
                formData.append('details[document_no]', additionalData[0].noDO)
                formData.append('details[bl_nbr]', additionalData[0].blNo)
                formData.append('details[pin]', additionalData[0].doPin)
                formData.append('details[paid_thru]', additionalData[0].paidThru)

                selectedRows.map((inputContainer, indexContainer) => {
                    formData.append('containers[' + indexContainer + '][container_id]', inputContainer.id)
                    formData.append('containers[' + indexContainer + '][customer_id]', inputContainer.cust_id)
                    formData.append('containers[' + indexContainer + '][vessel_id]', inputContainer.vessel_id)
                    formData.append('containers[' + indexContainer + '][voyage_no]', inputContainer.voyage_no)
                    formData.append('containers[' + indexContainer + '][vessel_name]', inputContainer.vessel_name)
                    formData.append('containers[' + indexContainer + '][eta]', inputContainer.eta)
                    formData.append('containers[' + indexContainer + '][ata]', inputContainer.ata)
                    formData.append('containers[' + indexContainer + '][etd]', inputContainer.etd)
                    formData.append('containers[' + indexContainer + '][line_id]', inputContainer.line_id)
                    formData.append('containers[' + indexContainer + '][no_container]', inputContainer.no_container)
                    formData.append('containers[' + indexContainer + '][code]', inputContainer.container_size + inputContainer.container_type)
                    formData.append('containers[' + indexContainer + '][owner]', inputContainer.owner)
                })

                const headerConfirmTransaction = {
                    'Secret': localStorage.getItem('ckHeaderSecret'),
                    'Key': localStorage.getItem('ckHeaderKey'),
                    'Accept-Language': 'application/json'
                }

                //confirm transaction
                axios
                    .post(ApiClickargo.CLICKARGO_SP2_CONFIRM_TRANSACTION, formData, {
                        headers: headerConfirmTransaction
                    })
                    .then(response => {
                        localStorage.setItem('ckSP2AdditionalForm', JSON.stringify(additionalData[0], null, 2))
                        localStorage.setItem('ckSP2ContainerSelected', JSON.stringify(selectedRows, null, 2))
                        localStorage.setItem('ckSP2ConfirmTrxId', response.data.data.transaction_id)

                        setIsSubmitSP2(false)
                        setIsCheckout(true)
                    })
                    .catch(error => {
                        setIsSubmitSP2(false)
                        addToast('Failed to confirm transaction, please try again!', {
                            appearance: 'error',
                            autoDismiss: true,
                        })
                        console.log(error)
                    })
            } else {
                setIsCheckout(false)
                setIsSubmitSP2(false)
                addToast('Please choose container!', {
                    appearance: 'error',
                    autoDismiss: true,
                })

            }
        }, 1500);

    }

    const columns = [
        {
            name: 'No Master BL AWB',
            selector: 'no_master_bl_awb',
            sortable: true,
        },
        {
            name: 'Status Container SPBB',
            selector: 'status_container_spbb',
            sortable: true,
        },
        {
            name: 'Sender',
            selector: 'sender',
            sortable: true,
        },
        {
            name: 'Vessel ID',
            selector: 'vessel_id',
            sortable: true,
        },
        {
            name: 'Voyage NO',
            selector: 'voyage_no',
            sortable: true,
        },
        {
            name: 'Vessel Name',
            selector: 'vessel_name',
            sortable: true,
        },
        {
            name: 'ETA',
            selector: 'eta',
            sortable: true,
        },
        {
            name: 'ATA',
            selector: 'ata',
            sortable: true,
        },
        {
            name: 'ETD',
            selector: 'etd',
            sortable: true,
        },
        {
            name: 'Line ID',
            selector: 'line_id',
            sortable: true,
        },
        {
            name: 'NO Container',
            selector: 'no_container',
            sortable: true,
        },
        {
            name: 'Container SIZE',
            selector: 'container_size',
            sortable: true,
        },
        {
            name: 'Container Type',
            selector: 'container_type',
            sortable: true,
        },
        {
            name: 'Location at terminal',
            selector: 'ls_location_type',
            sortable: true,
        },
        {
            name: 'Stacking Date',
            selector: 'stacking_date',
            sortable: true,
        },
        {
            name: 'Owner',
            selector: 'owner',
            sortable: true,
        },
        {
            name: 'Status container refer',
            selector: 'reefer',
            sortable: true,
        },
        {
            name: 'Temperature container',
            selector: 'temperature',
            sortable: true,
        },
        {
            name: 'Container Voltage',
            selector: 'voltage',
            sortable: true,
        },
        {
            name: 'Status Karantina',
            selector: 'status_karantina',
            sortable: true,
        },
        {
            name: 'Status',
            selector: 'status_paid',
            sortable: true,
        },
    ]

    const dataContainerList = dataContainer.map(datum => ({ ...datum }))

    const rowDisabledCriteria = row => row.status_paid !== 'VALID'

    const onClickBack = () => {
        props.history.push({
            pathname: '/clickargo_sp2/form'
        })
    }

    const handleInputChange = (field, event) => {
        const values = [...additionalData]

        if (field === "noDO") {
            values[0].noDO = event.target.value
        }

        if (field === "blNo") {
            values[0].blNo = event.target.value
        }

        if (field === "doPin") {
            values[0].doPin = event.target.value
        }

        if (field === "paidThru") {
            values[0].paidThru = event.target.value
        }

        setAdditionalData(values)
    }

    const backToThisComponent = () => {
        setIsLoading(true)
        setIsCheckout(false)
        setSelectedRows([])

        setTimeout(() => {
            setIsLoading(false)
        }, 1200);
    }

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
            isCheckout ? (
                <Fragment>
                    <Checkout backToContainer={backToThisComponent} props={props} />
                </Fragment>
            ) : (
                <Fragment>
                    <div style={{ marginBottom: "10px" }}>
                        <div style={{ float: "left" }}>
                            <h3 style={{ display: "inline" }} className={dashforgeCss.txBlack}>Clickargo SP2</h3>
                        </div>
                        <div style={{ float: "right" }}>
                            <img style={{ width: '200px' }} src={window.location.origin + '/assets/images/Clickargo5.png'} />
                        </div>
                    </div>

                    <br />
                    <br />

                    <div style={{ marginBottom: "10px" }}>
                        <label style={{ display: "inline", color: "#2d9ff7", cursor: 'pointer' }} onClick={onClickBack}>Back</label>
                    </div>

                    <div className="kt-portlet" onClick={toggle}>
                        <div style={{ cursor: 'pointer' }} className="kt-portlet__head">

                            <div className="kt-portlet__head-label">
                                <h3 className="kt-portlet__head-title">
                                    Claim SP2
                                </h3>
                            </div>

                            <div className="kt-portlet__head-toolbar">
                                {isOpen ? <i className="la la-angle-down"></i> : <i className="la la-angle-up"></i>}
                            </div>
                        </div>
                    </div>

                    <Collapse isOpen={isOpen}>
                        <div className="kt-portlet">
                            <div className="kt-portlet__body">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="row">

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Transaction type<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        className="form-control"
                                                        type="text"
                                                        value={JSON.parse(localStorage.getItem('ckSP2Form')).transactionTypeLabel}
                                                        disabled />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Document type<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        className="form-control"
                                                        type="text"
                                                        value={JSON.parse(localStorage.getItem('ckSP2Form')).documentTypeLabel}
                                                        disabled />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>No {JSON.parse(localStorage.getItem('ckSP2Form')).documentTypeLabel}<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        value={JSON.parse(localStorage.getItem('ckSP2Form')).no}
                                                        disabled />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>{JSON.parse(localStorage.getItem('ckSP2Form')).documentTypeLabel} Date<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        value={JSON.parse(localStorage.getItem('ckSP2Form')).date}
                                                        disabled />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>{JSON.parse(localStorage.getItem('ckSP2Form')).documentTypeLabel} DO Date<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        value={JSON.parse(localStorage.getItem('ckSP2Form')).doDate}
                                                        disabled />
                                                </div>
                                            </div>

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Terminal Operator<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        value={JSON.parse(localStorage.getItem('ckSP2Form')).terminalOperatorLabel}
                                                        disabled />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </Collapse>

                    <br />

                    <form onSubmit={handleSubmit(onContinueToPayment)}>
                        <div className="kt-portlet">
                            <div className="kt-portlet__body">

                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="row">

                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>No DO<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        placeholder="No DO"
                                                        value={additionalData[0].noDO}
                                                        onChange={event => handleInputChange('noDO', event)}
                                                        name={`noDO`}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    {errors[`noDO`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`noDO`].message}</span>}
                                                </div>
                                            </div>

                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>BL No<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        placeholder="BL No"
                                                        value={additionalData[0].blNo}
                                                        onChange={event => handleInputChange('blNo', event)}
                                                        name={`blNo`}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    {errors[`blNo`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`blNo`].message}</span>}
                                                </div>
                                            </div>

                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>DO PIN</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        placeholder="DO PIN"
                                                        value={additionalData[0].doPin}
                                                        onChange={event => handleInputChange('doPin', event)} />
                                                </div>
                                            </div>

                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Paid Thru<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="date"
                                                        className="form-control"
                                                        value={additionalData[0].paidThru}
                                                        onChange={event => handleInputChange('paidThru', event)}
                                                        name={`paidThru`}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    <small className="form-text text-muted">Tanggal keluar kontainer, *Optional untuk export</small>
                                                    {errors[`paidThru`] && <br />}
                                                    {errors[`paidThru`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`paidThru`].message}</span>}
                                                </div>
                                            </div>

                                            <div className="col-12">
                                                <DataTable
                                                    title="Container List"
                                                    columns={columns}
                                                    data={dataContainerList}
                                                    pagination
                                                    paginationServer
                                                    subHeader={true}
                                                    subHeaderAlign={"left"}
                                                    onSelectedRowsChange={handleSelectedRows}
                                                    selectableRows
                                                    selectableRowDisabled={rowDisabledCriteria}
                                                />
                                            </div>
                                        </div>
                                        <div><br /></div>
                                        <div className="text-center">
                                            <button
                                                type="submit"
                                                className="btn btn-primary submit-btn"
                                                disabled={isSubmitSP2}>
                                                {isSubmitSP2 && <i className="fa fa-circle-notch fa-spin"></i>}
                                                {isSubmitSP2 && <span>Please wait...</span>}
                                                {!isSubmitSP2 && <span>Continue to Payment</span>}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    {/* <br />
                    <pre>
                        {JSON.stringify(selectedRows, null, 2)}
                    </pre> */}
                    {/* <br />
                    <pre>
                        {JSON.stringify(additionalData, null, 2)}
                    </pre> */}
                    {/* <br />
                    <pre>
                        {JSON.stringify(getValues(), null, 2)}
                    </pre> */}
                </Fragment>
            )
                
            )
    )
}

export default ChooseContainer
