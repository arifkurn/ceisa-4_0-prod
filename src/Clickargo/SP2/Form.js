import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../Vessel/BookingRequest/dashforge.module.css'
import Spinner from 'react-bootstrap/Spinner'
import { Link } from 'react-router-dom'
import Select from 'react-select'
import axios from 'axios'
import { useToasts } from 'react-toast-notifications'
import { useForm } from 'react-hook-form'
import * as ApiClickargo from '../ApiList.js'
import moment from 'moment'

function Form(props) {

    const { register, handleSubmit, errors, watch, setError, clearError, setValue, getValues } = useForm()

    const { addToast } = useToasts()

    const [isLoading, setIsLoading] = useState(true)

    const [inputForm, setInputForm] = useState([])

    const [isLocalStorageExist, setIsLocalStorageExist] = useState(false)

    const [transactionType, setTransactionType] = useState([])

    const [documentType, setDocumentType] = useState([])

    const [containerList, setContainerList] = useState(false)

    const [isPPJK, setIsPPJK] = useState(false)

    const [arrBLNumber, setArrBLNumber] = useState([])

    useEffect(() => {
        if ('ckSP2Form' in localStorage && 'ckSP2Docs' in localStorage && 'ckHeaderKey' in localStorage && 'ckHeaderSecret' in localStorage && 'ckHeaderToken' in localStorage) {
            setIsLocalStorageExist(true)

            setTimeout(() => {
                setTransactionType(JSON.parse(localStorage.getItem('ckSP2Docs')).transaction_type)
                setDocumentType(JSON.parse(localStorage.getItem('ckSP2Docs')).document_type)

                //check if users ppjk
                axios
                    .get(ApiClickargo.NLE_DATAPELIMPAHAN + '/?ppjk=' + props.dataUserPortal.npwp)
                    // .get(ApiClickargo.NLE_DATAPELIMPAHAN + '/?ppjk=3048503498509348')
                    .then(response => {
                        let dataPPJK = response.data.content
                        //filter DO only
                        var newArrayPPJK = dataPPJK.filter(function (el) {
                            return el.sp2 === 1
                        })
                        if (newArrayPPJK.length > 0) {
                            setIsPPJK(true)
                            setArrBLNumber(newArrayPPJK)
                        } else {
                            setIsPPJK(false)
                        }

                        //set input form value
                        const claimSP2Form = JSON.parse(localStorage.getItem('ckSP2Form'))
                        setInputForm([...inputForm, {
                            actionType: claimSP2Form.actionType,
                            actionTypeLabel: claimSP2Form.actionTypeLabel,
                            moveType: claimSP2Form.moveType,
                            moveTypeLabel: claimSP2Form.moveTypeLabel,
                            transactionType: claimSP2Form.transactionType,
                            transactionTypeLabel: claimSP2Form.transactionTypeLabel,
                            documentType: claimSP2Form.documentType,
                            documentTypeLabel: claimSP2Form.documentTypeLabel,
                            blNumber: claimSP2Form.blNumber,
                            blNumberLabel: claimSP2Form.blNumberLabel,
                            blDate: claimSP2Form.blDate,
                            no: claimSP2Form.no,
                            date: claimSP2Form.date,
                            doDate: claimSP2Form.doDate,
                            terminalOperator: claimSP2Form.terminalOperator,
                            terminalOperatorLabel: claimSP2Form.terminalOperatorLabel,
                        }])

                        setTimeout(() => {
                            setValue([
                                { moveType: claimSP2Form.moveType },
                                { transactionType: claimSP2Form.transactionType },
                                { documentType: claimSP2Form.documentType },
                                { blNumberHidden: claimSP2Form.blNumber },
                                { blDate: claimSP2Form.blDate },
                                { terminalOperator: claimSP2Form.terminalOperator },
                            ])
                        }, 1000);
                        setIsLoading(false)
                    })
                    .catch(error => {
                        console.log(error)
                        addToast('Failed get data pelimpahan', {
                            appearance: 'error',
                            autoDismiss: true,
                        })
                    })
            }, 1000);
        } else {
            setIsLocalStorageExist(false)
        }
    }, [])

    const handleInputChange = (field, event) => {
        const values = [...inputForm]

        if (field === "moveType") {
            values[0].moveType = event.value
            values[0].moveTypeLabel = event.label

            setValue([
                { moveType: inputForm[0].moveType },
            ])

            clearError('moveType')
        }

        if (field === "transactionType") {
            values[0].transactionType = event.value
            values[0].transactionTypeLabel = event.label

            setValue([
                { transactionType: inputForm[0].transactionType },
            ])

            clearError('transactionType')
        }

        if (field === "documentType") {
            values[0].documentType = event.value
            values[0].documentTypeLabel = event.label

            setValue([
                { documentType: inputForm[0].documentType },
            ])

            clearError('documentType')
        }

        if (field === "blNumberSelect") {
            addToast('Please wait, we trying to get SPPB PIB Data', {
                appearance: 'info',
                autoDismiss: true,
            })

            values[0].blNumber = event.value
            values[0].blNumberLabel = event.label

            //find and bl detail from bl number 
            var findBlDate = arrBLNumber.find(function (el) {
                return el.bl_no === event.value
            })
            //set bl date value from bl details
            values[0].blDate = findBlDate.bl_date
            //set value bl date and clear error if any
            setValue([
                { blDate: inputForm[0].blDate },
            ])
            clearError('blDate')

            setValue([
                { blNumberHidden: inputForm[0].blNumber },
            ])

            clearError('blNumberHidden')

            //find sppb no and sppb date
            axios
                .post(ApiClickargo.CLICKARGO_SP2_FIND_SPPB, {
                    bl_number: inputForm[0].blNumber,
                    bl_date: moment(inputForm[0].blDate).format("YYYYMMDD")
                })
                .then(response => {
                    if (response.data.success === true) {
                        if (response.data.data['Status PIB'].length > 0) {
                            let statusPIB = response.data.data['Status PIB'][0]

                            values[0].no = statusPIB.NOSPPB
                            setValue([
                                { no: inputForm[0].no },
                            ])
                            clearError('no')

                            values[0].date = moment(statusPIB.TGLSPPB).format("YYYY-MM-DD")
                            setValue([
                                { date: inputForm[0].date },
                            ])
                            clearError('date')

                            addToast('SPPB PIB Successfully retrieved!', {
                                appearance: 'success',
                                autoDismiss: true,
                            })
                        } else {
                            addToast('Data SPPB PIB is empty, please try to refresh the page or contact administrator', {
                                appearance: 'error',
                                autoDismiss: false,
                            })
                        }
                    }
                })
                .catch(error => {
                    console.log(error)
                    addToast('Failed get Status PIB, we will refresh this page now', {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                    setTimeout(() => {
                        window.location.reload(false)
                    }, 5000);
                })
        }

        if (field === "blNumberText") {
            values[0].blNumber = event.target.value

            setValue([
                { blNumberHidden: inputForm[0].blNumber },
            ])

            clearError('blNumberHidden')
        }

        if (field === "blDate") {
            values[0].blDate = event.target.value

            addToast('Please wait, we trying to get SPPB PIB Data', {
                appearance: 'info',
                autoDismiss: true,
            })

            //find sppb no and sppb date
            axios
                .post(ApiClickargo.CLICKARGO_SP2_FIND_SPPB, {
                    bl_number: inputForm[0].blNumber,
                    bl_date: moment(inputForm[0].blDate).format("YYYYMMDD")
                })
                .then(response => {
                    if (response.data.success === true) {
                        if (response.data.data['Status PIB'].length > 0) {
                            let statusPIB = response.data.data['Status PIB'][0]

                            values[0].no = statusPIB.NOSPPB
                            setValue([
                                { no: inputForm[0].no },
                            ])
                            clearError('no')

                            values[0].date = moment(statusPIB.TGLSPPB).format("YYYY-MM-DD")
                            setValue([
                                { date: inputForm[0].date },
                            ])
                            clearError('date')

                            addToast('SPPB PIB Successfully retrieved!', {
                                appearance: 'success',
                                autoDismiss: true,
                            })
                        } else {
                            addToast('Data SPPB PIB is empty, please try to refresh the page or contact administrator', {
                                appearance: 'error',
                                autoDismiss: false,
                            })
                        }
                    }
                })
                .catch(error => {
                    console.log(error)
                    addToast('Failed get Status PIB, we will refresh this page now', {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                    setTimeout(() => {
                        window.location.reload(false)
                    }, 5000);
                })
        }

        if (field === "no") {
            values[0].no = event.target.value
        }

        if (field === "date") {
            values[0].date = event.target.value
        }

        if (field === "doDate") {
            values[0].doDate = event.target.value
        }

        if (field === "terminalOperator") {
            values[0].terminalOperator = event.value
            values[0].terminalOperatorLabel = event.label

            setValue([
                { terminalOperator: inputForm[0].terminalOperator },
            ])

            clearError('terminalOperator')
        }

        setInputForm(values)
    }

    const onClickProcess = () => {
        setContainerList(true)

        setTimeout(() => {
            const headerSP2 = {
                'Secret': localStorage.getItem('ckHeaderSecret'),
                'Key': localStorage.getItem('ckHeaderKey'),
            }
            //container list
            axios
                .post(ApiClickargo.CLICKARGO_SP2_CONTAINER_LIST, {
                    npwp_depo: '',
                    document_no: inputForm[0].no,
                    customs_document_id: inputForm[0].documentType,
                    transactions_type_id: inputForm[0].transactionType,
                    document_shipping_date: '',
                    document_date: inputForm[0].date,
                    document_shipping_no: '',
                    cust_id_ppjk: '',
                    terminal_id: inputForm[0].terminalOperatorLabel
                }, {
                    headers: headerSP2
                })
                .then(responseSP2 => {
                    setContainerList(false)
                    localStorage.setItem('ckSP2Form', JSON.stringify(inputForm[0], null, 2))
                    localStorage.setItem('ckSP2ContainerList', JSON.stringify(responseSP2.data.data, null, 2))
                    props.history.push({
                        pathname: '/clickargo_sp2/choose_container'
                    })
                })
                .catch(error => {
                    setContainerList(false)
                    addToast('Failed get container list, please try again!', {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                })
        }, 2500);
    }

    let optionTransasctionType = transactionType.map(obj => {
        return { value: obj.id, label: obj.value }
    })

    let optionDocumentType = documentType.map(obj => {
        return { value: obj.id, label: obj.value }
    })

    let optionTerminalOperator = [
        { value: '', label: 'Select terminal operator' },
        { value: 'koja', label: 'KOJA' },
    ]

    let optionMoveType = [
        { value: '', label: 'Please Select Move Type' },
        { value: 'POL To Door', label: 'Port of Loading to Door' },
        { value: 'Port To Port', label: 'Port of Loading to Port of Destination' },
        { value: 'POD To Door', label: 'Port of Destination to Door' }
    ]

    let optionBLNumber = arrBLNumber.map(obj => {
        return { value: obj.bl_no, label: obj.bl_no }
    })

    if (isLocalStorageExist === true) {
        return (
            isLoading ? (
                <Fragment>
                    <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
                </Fragment>
            ) : (
                    <Fragment>
                        <form onSubmit={handleSubmit(onClickProcess)}>
                            <div style={{ marginBottom: "10px" }}>
                                <div style={{ float: "left" }}>
                                    <h3 style={{ display: "inline" }} className={dashforgeCss.txBlack}>Clickargo SP2</h3>
                                </div>
                                <div style={{ float: "right" }}>
                                    <img style={{ width: '200px' }} src={window.location.origin + '/assets/images/Clickargo5.png'} />
                                </div>
                            </div>

                            <br />
                            <br />

                            <div style={{ marginBottom: "10px" }}>
                                <Link to="/sp2" style={{ display: "inline", color: "#2d9ff7" }} >
                                    Back
                            </Link>
                            </div>

                            <div className="kt-portlet">
                                <div className="kt-portlet__head">
                                    <div className="kt-portlet__head-label">
                                        <h3 className="kt-portlet__head-title">
                                            Claim SP2
                                    </h3>
                                    </div>
                                </div>

                                <div className="kt-portlet__body">

                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="row">

                                                {/* <div className="col-12">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Move Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`moveType`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <Select
                                                            value={{ label: inputForm[0].moveTypeLabel, value: inputForm[0].moveType }}
                                                            options={optionMoveType}
                                                            onChange={event => handleInputChange('moveType', event)} />
                                                        {errors[`moveType`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`moveType`].message}</span>}
                                                    </div>
                                                </div> */}

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Transaction type<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`transactionType`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <Select
                                                            value={{ label: inputForm[0].transactionTypeLabel, value: inputForm[0].transactionType }}
                                                            options={optionTransasctionType}
                                                            onChange={event => handleInputChange('transactionType', event)} />
                                                        {errors[`transactionType`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`transactionType`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Document type<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`documentType`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <Select
                                                            value={{ label: inputForm[0].documentTypeLabel, value: inputForm[0].documentType }}
                                                            options={optionDocumentType}
                                                            onChange={event => handleInputChange('documentType', event)} />
                                                        {errors[`documentType`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`documentType`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>B/L Number<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`blNumberHidden`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        {isPPJK ? (<Fragment>
                                                            <Select
                                                                value={{ label: inputForm[0].blNumberLabel, value: inputForm[0].blNumber }}
                                                                options={optionBLNumber}
                                                                placeholder="Please Select BL Number"
                                                                onChange={event => handleInputChange('blNumberSelect', event)}
                                                            />
                                                        </Fragment>) : (<Fragment>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                value={inputForm[0].blNumber}
                                                                placeholder="Please input BL Number"
                                                                onChange={event => handleInputChange('blNumberText', event)} />
                                                        </Fragment>)}
                                                        {errors[`blNumberHidden`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`blNumberHidden`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>B/L Date<span className={dashforgeCss.txDanger}>*</span></label>
                                                        {isPPJK ? (<Fragment><span className="float-right" style={{ fontSize: '12px', color: 'black' }}>(Will be filled automatically when selecting <b>B/L Number</b>)</span></Fragment>) : ''}
                                                        {isPPJK ? (<Fragment>
                                                            <input
                                                                type="date"
                                                                className="form-control"
                                                                disabled
                                                                value={inputForm[0].blDate}
                                                                onChange={event => handleInputChange('blDate', event)}
                                                                name={`blDate`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                        </Fragment>) : (<Fragment>
                                                            {inputForm[0].blNumber.length > 0 ? (<Fragment>
                                                                <input
                                                                    type="date"
                                                                    className="form-control"
                                                                    value={inputForm[0].blDate}
                                                                    onChange={event => handleInputChange('blDate', event)}
                                                                    name={`blDate`}
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        }
                                                                    })} />
                                                            </Fragment>) : (
                                                                <Fragment>
                                                                    <input
                                                                    type="date"
                                                                    className="form-control"
                                                                    value={inputForm[0].blDate}
                                                                    onChange={event => handleInputChange('blDate', event)}
                                                                    name={`blDate`}
                                                                    disabled
                                                                    ref={register({
                                                                        required: {
                                                                            value: true,
                                                                            message: 'This input field is required!'
                                                                        }
                                                                    })} />
                                                                </Fragment>
                                                            )}
                                                        </Fragment>)}
                                                        {errors[`blDate`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`blDate`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>No {inputForm[0].documentType.length > 0 ? inputForm[0].documentTypeLabel : ''}<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <span className="float-right" style={{ fontSize: '12px', color: 'black' }}>(Will be filled automatically when selecting {isPPJK ? (<Fragment><b>B/L Number</b></Fragment>) : (<Fragment><b>B/L Date</b></Fragment>)})</span>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            placeholder="No Document"
                                                            value={inputForm[0].no}
                                                            onChange={event => handleInputChange('no', event)}
                                                            name={`no`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        {errors[`no`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`no`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>{inputForm[0].documentType.length > 0 ? inputForm[0].documentTypeLabel : ''} Date<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <span className="float-right" style={{ fontSize: '12px', color: 'black' }}>(Will be filled automatically when selecting {isPPJK ? (<Fragment><b>B/L Number</b></Fragment>) : (<Fragment><b>B/L Date</b></Fragment>)})</span>
                                                        <input
                                                            type="date"
                                                            className="form-control"
                                                            value={inputForm[0].date}
                                                            onChange={event => handleInputChange('date', event)}
                                                            name={`date`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        {errors[`date`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`date`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>{inputForm[0].documentType.length > 0 ? inputForm[0].documentTypeLabel : ''} DO Date<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="date"
                                                            className="form-control"
                                                            value={inputForm[0].doDate}
                                                            onChange={event => handleInputChange('doDate', event)}
                                                            name={`doDate`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        {errors[`doDate`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`doDate`].message}</span>}
                                                    </div>
                                                </div>

                                                <div className="col-6">
                                                    <div className="form-group">
                                                        <label className={dashforgeCss.txBlack}>Terminal Operator<span className={dashforgeCss.txDanger}>*</span></label>
                                                        <input
                                                            type="hidden"
                                                            name={`terminalOperator`}
                                                            ref={register({
                                                                required: {
                                                                    value: true,
                                                                    message: 'This input field is required!'
                                                                }
                                                            })} />
                                                        <Select
                                                            value={{ label: inputForm[0].terminalOperatorLabel, value: inputForm[0].terminalOperator }}
                                                            options={optionTerminalOperator}
                                                            onChange={event => handleInputChange('terminalOperator', event)} />
                                                        {errors[`terminalOperator`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`terminalOperator`].message}</span>}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="text-center">
                                                <button
                                                    type="submit"
                                                    className="btn btn-primary submit-btn"
                                                    disabled={containerList}>
                                                    {containerList && <i className="fa fa-circle-notch fa-spin"></i>}
                                                    {containerList && <span>Please wait...</span>}
                                                    {!containerList && <span>Process</span>}
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </Fragment>
                )
        )
    } else {
        return (
            <Fragment>
                <div className="kt-portlet">
                    <div className="kt-portlet__body">

                        <div className="row">
                            <div className="col-md-12">

                                <label>Oops! You can't directly access this page. Please start from SP 2 Menu</label>

                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }

}

export default Form
