//API CLICKARGO
export const CLIKARGO_DOMAIN = 'https://api.clickargo.com/'

export const CLICKARGO_SCHEDULE_THIS_WEEK = CLIKARGO_DOMAIN + 'api/v1/schedule/week'
export const CLICKARGO_CHECK_NPWP = CLIKARGO_DOMAIN + 'api/v1/auth/check-npwp'
export const CLICKARGO_FIND_SCHEDULE = CLIKARGO_DOMAIN + 'api/v1/find-schedule'
export const CLICKARGO_CHECK_SCHEDULE = CLIKARGO_DOMAIN + 'api/v1/check-schedule'

export const CLICKARGO_COUNTRY_LIST = CLIKARGO_DOMAIN + 'api/v1/helper/country-list'
export const CLICKARGO_STATE_LIST = CLIKARGO_DOMAIN + 'api/v1/helper/state-list'
export const CLICKARGO_INDONESIA_CITY = CLIKARGO_DOMAIN + 'api/v1/helper/indonesia-city'
export const CLICKARGO_INDONESIA_DISTRICT = CLIKARGO_DOMAIN + 'api/v1/helper/indonesia-district'
export const CLICKARGO_INDONESIA_SUB_DISTRICT = CLIKARGO_DOMAIN + 'api/v1/helper/indonesia-sub-district'

export const CLICKARGO_REGISTER = CLIKARGO_DOMAIN + 'api/v1/register'

export const CLICKARGO_FIND_ORDER_BY_JOB_NUMBER = CLIKARGO_DOMAIN + 'api/v1/order/find/job-number'

export const CLICKARGO_FIND_ORDER_BY_UUID = CLIKARGO_DOMAIN + 'api/v1/order/detail'

export const CLICKARGO_CARRIER_SERVICE_LIST = CLIKARGO_DOMAIN + 'api/v1/carrier-service/list'

export const CLICKARGO_CONTAINER_TYPE_LIST = CLIKARGO_DOMAIN + 'api/v1/container-type/list'
export const CLICKARGO_PACKAGE_TYPE_LIST = CLIKARGO_DOMAIN + 'api/v1/package-type/list'
export const CLICKARGO_HS_CODE = CLIKARGO_DOMAIN + 'api/v1/hs-code/find'
export const CLICKARGO_UNLOCS = CLIKARGO_DOMAIN + 'api/v1/unlocs/find'
export const CLICKARGO_CONVERT_CONTAINER_EXCEL = CLIKARGO_DOMAIN + 'api/v1/convert-container-excel'

export const CLICKARGO_STORE_ORDER = CLIKARGO_DOMAIN + 'api/v1/export/freight/store-order'

export const CLICKARGO_ORDER_LIST_BY_NPWP = CLIKARGO_DOMAIN + 'api/v1/order/list/tax-number'

export const CLICKARGO_COMPLETE_SI = CLIKARGO_DOMAIN + 'api/v1/export/freight/shipping-instruction/complete' 

export const CLICKARGO_FIND_COUNTRY = CLIKARGO_DOMAIN + 'api/v1/helper/country/find'

export const CLICKARGO_STORE_DO = CLIKARGO_DOMAIN + 'api/v1/import/do/store-order'

export const CLICKARGO_SP2_DOCS_TRANSACTION_TYPE = CLIKARGO_DOMAIN + 'api/v1/import/sp2/docs-transactions-type'
export const CLICKARGO_SP2_CONTAINER_LIST = CLIKARGO_DOMAIN + 'api/v1/import/sp2/container-list'
export const CLICKARGO_SP2_CONFIRM_TRANSACTION = CLIKARGO_DOMAIN + 'api/v1/import/sp2/confirm-transaction'
export const CLICKARGO_SP2_GENERATE_BILLING = CLIKARGO_DOMAIN + 'api/v1/import/sp2/generate-billing'
export const CLICKARGO_SP2_STORE_ORDER = CLIKARGO_DOMAIN + 'api/v1/import/sp2/store-order'
export const CLICKARGO_SP2_ORDER_GENERATE_BILLING = CLIKARGO_DOMAIN + 'api/v1/order/billing-va'
export const CLICKARGO_SP2_NOTIFICATION_EMAIL_SEND = CLIKARGO_DOMAIN + 'api/v1/import/sp2/notification-email/send'

export const CLICKARGO_SP2_FIND_SPPB = CLIKARGO_DOMAIN + 'api/v1/import/sp2/release'

export const CLICKARGO_ORDER_GET_DETAIL_BILLING = CLIKARGO_DOMAIN + 'api/v1/order/billing/details'

export const CLICKARGO_ORDER_UPDATE_BILLING_VA = CLIKARGO_DOMAIN + 'api/v1/order/billing-va/update'

export const CLICKARGO_UPDATE_ORDER_LINK = CLIKARGO_DOMAIN + 'api/v1/order/update-link'

export const CLICKARGO_UPLOAD_DOCUMENT = CLIKARGO_DOMAIN + 'api/v1/order/document/upload'

//API CLICKARGO

//API NLE FOR CLICKARGO
export const NLE_VESSEL_LIST = 'https://esbbcext01.beacukai.go.id:8089/JobVessel/'
export const NLE_VESSEL_POST = 'https://esbbcext01.beacukai.go.id:8089/JobVessel/'

export const NLE_USER_PROFILE_OSS = 'https://nlehub.kemenkeu.go.id/V1/NLE/Ceisa/ProfilOss'
export const NLE_USER_PROFILE_OSS_API_KEY = '70cf2480-0f82-42eb-a165-34e24d79ad26'
export const NLE_USER_PROFILE = 'https://nlehub.kemenkeu.go.id/V1/NLE/Customer/Profil'
export const NLE_USER_PROFILE_API_KEY = 'b032465e-9cef-44e4-8ef1-faa0a7d13d08'

export const NLE_DATAPELIMPAHAN = 'https://esbbcext01.beacukai.go.id:8085/datapelimpahan'
//API NLE FOR CLICKARGO