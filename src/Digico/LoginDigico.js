import React, {Component} from 'react';
// import Iframe from 'react-iframe'
import {Route, HashRouter} from 'react-router-dom';
import logo_digico_vertical from '../assets/media/digico/digico_vertical.png';
import logo_refresh_image from '../assets/media/digico/refresh_image.png';
import * as Constants from '../Constants.js';
import * as axios from 'axios';
import Swal from 'sweetalert2';

class LoginDigico extends Component{
  constructor(props) {
    super(props);
    this.state  = {
      capchatext: ''
    }
  }
  render(){
    return(
      <HashRouter>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <div>
            <div style={{justifyContent: 'center', display:'flex', marginTop:'50px'}}>
                <img style={{width: '150px', height: '80px'}} src={logo_digico_vertical} />
            </div>

            <div className="row" style={{justifyContent: 'center', display:'flex', marginTop:'30px'}}>
                <div style={{width: '100%', justifyContent: 'center', display:'flex'}}>
                    <label style={{color: 'red'}}>Username : </label>
                    <input id="username" type="text" style={{width: '300px', marginLeft: '10px'}}></input>
                </div>
                <div style={{width: '100%', justifyContent: 'center', display:'flex', marginTop:'15px'}}>
                    <label style={{color: 'red'}}>Password : </label>
                    <input id="password" type="password" style={{width: '300px', marginLeft: '14px'}}></input>
                </div>

                <div style={{width: '100%', height: '60px', justifyContent: 'center', display:'flex', marginTop:'15px'}}>
                    <div style={{float: 'left'}}>
                        <div id="div_captcha">
                        </div> 
                        <input style={{marginTop:'10px'}} type="hidden" className="form-control text-input" id="capchatext" name="capchatext"/>
                    </div>
                    <div style={{float: 'left', height: '0px'}}>
                        <a onClick={this.get_captcha}><img style={{width: '50px', height: '50px'}} className="logo" src={logo_refresh_image}/></a>
                    </div>    
                </div>
                
                <div style={{width: '100%', justifyContent: 'center', display:'flex', marginTop:'20px'}}>
                    <input style={{marginTop:'10px', width:'300px'}} type="text" className="form-control text-input" id="capcha" name="capcha" required autoFocus autoComplete="off"/>
                </div>

                <button onClick={this.login_post.bind(this)} style={{marginTop:'40px'}} type="submit" className="btn btn-login">Login</button>
            </div>
        </div>
    </HashRouter>
    );
  }

  componentDidMount() {
      this.get_captcha();
  }
  
  get_captcha() {
    var url = Constants.GET_CAPCHA;
    console.log(url);
    
    const capcha = document.getElementById("div_captcha");
    console.log(capcha);

    axios.get(url)
        .then((result)=>{
            console.log(result.data)
            capcha.innerHTML = "";
            capcha.innerHTML = result.data.captcha.data;
            var captcha_text = result.data.captcha.text;
            console.log(captcha_text);
            document.getElementById("capchatext").value = captcha_text;
            
        }).catch((error) => {
            console.log(error)
            document.getElementById("div_captcha").innerHTML = "";
            document.getElementById("capchatext").value = "";
        })
    }

    login_post(){
        
        Swal.fire({
            title: 'Please Wait ..',
            onBeforeOpen: () => {
                Swal.showLoading()
            }
        });

        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;
        var capchatext = document.getElementById("capchatext").value;
        var capcha = document.getElementById("capcha").value;

        if (username == '') {
            Swal.fire('Username is required !');
        } else if (password == '') {
            Swal.fire('Password is required !');
        } else if (capcha == '') {
            Swal.fire('Capcha is required !');
        } else {
            let header_config = {
                headers: {
                  "Content-Type": "application/json",
                }
              };
    
              var url = Constants.POST_LOGIN;
              console.log(url);
        
            axios.post(url, 
                {
                    username: username,
                    password: password,
                    capcha: capcha,
                    capchatext: capchatext,
                    check_api: 'get_api',
                },
                header_config
            )
            .then(result => {
                console.log(result);
                if (result.data.status == false) {
                    var message = result.data.message;
                    this.get_captcha();
                    Swal.fire(message);
                } else {
                    // var message = result.data.message;
                    let data_user = result.data.data_user;
                    console.log("data_user");
                    console.log(data_user);
                    localStorage.setItem("id_user", data_user.id);
                    localStorage.setItem("userName", data_user.username);
                    localStorage.setItem("email", data_user.email);
                    localStorage.setItem("companyID", data_user.company_id);
                    localStorage.setItem("compType", data_user.type);
                    localStorage.setItem("roleID", data_user.role_id);
                    Swal.close();
                    if (data_user.role_id == Constants.ROLE_CONSIGNEE || data_user.role_id == Constants.ROLE_CONSIGNEE_FORWARDER ||
                        data_user.role_id == Constants.ROLE_ADMIN || data_user.role_id == Constants.ROLE_ADMIN_OPERATION) {
                        this.props.history.push('/list_bol');
                    } else {
                        this.props.history.push('/list_delivery');
                    }
                    window.location.reload();
                }
            })
            .catch(error => {
                console.log(error);
                this.get_captcha();
                Swal.fire(error);
            });   
        }
    }

}
export default LoginDigico;
