import React, { Component, Button } from 'react';
import { HashRouter } from 'react-router-dom';
import ReactDOM from 'react-dom'
import detail_bol from './detail_bol.js';
// import { constants } from 'fs';
import Modal from 'react-modal';
import Swal from 'sweetalert2';
import * as Constants from '../../Constants.js';
import * as Helper from '../../Helper.js';
import * as axios from 'axios';
import Select from 'react-select';
import uuid from "uuid";

import 'datatables.net-buttons/js/buttons.html5.js';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

// import 'react-select/dist/css/react-select.css';

const customStyles = {
  content : {
      background          : '',
      border              : '',
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginTop              : "2%",
      marginRight           : '-50%',
      width                  : '60%',
      transform             : 'translate(-50%, -50%)'
  }
};

const $ = require('jquery');
$.Datatable = require('datatables.net');
const jzip = require( 'jzip');
window.JSZip = jzip;
let table = null

class list_bol extends Component {
  constructor(props) {
    super(props);
    this.state = {
      npwp: this.props.dataUserPortal.npwp,
      userName: null, 
      email: null, 
      companyID: null, 
      compType: null, 
      roleID: null,
      isFirstLoading: true,
      loading_bl: false,
      modalIsOpen: false,
      modalIsOpenConfirm: false,
      data_container: [],
      option_container: [], loading_container: false,
      option_bl: [], bl_date: [],
      option_ff: [],
      option_sl: [],
      isLoadingExternally: false,
      id_sl: '',
      name_sl: '',
      id_ff: '',
      name_ff: '',
      selectedDocumentType: '',
      bl_number_list: [],
      bl_number: '', bl_date_input: '',
      selected_bl:'',
      selectedContainer: [],
      count_file: 0,
      count_cont: 0,
      option_search_by : [
        {
            value: '0',
            label: 'REQ Number'
        },
        {
            value: '1',
            label: 'BL Number'
        },
        {
            value: '2',
            label: 'Forwarder'
        },
        {
            value: '3',
            label: 'Shipping Line'
        },
        {
            value: '4',
            label: 'Status'
        }
    ],
    search_by: '',
    search_for: ''
      // table: null, 
    }
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.openModalConfirm = this.openModalConfirm.bind(this);
    this.closeModalConfirm = this.closeModalConfirm.bind(this);
    this.autoCompleteDocumentType = this.autoCompleteDocumentType.bind(this);
    this.handleChangeBCSebelas = this.handleChangeBCSebelas.bind(this);
  }

  openModal() {
    this.setState({modalIsOpen: true});
  }

  closeModal() {
    this.setState({modalIsOpen: false});
    this.setState({id_ff:'', name_ff: '', id_sl: '', name_sl: '', count_cont: 0, count_file: 0, selectedContainer: [], bl_number: ''});
  }

  openModalConfirm() {
    console.log("Halo")
    this.setState({modalIsOpenConfirm: true});
  }

  closeModalConfirm() {
    this.setState({modalIsOpenConfirm: false});
  }

  render() {
    const {npwp, isFirstLoading, userName} = this.state;
    if ( !isFirstLoading && (userName === null || userName === "") ) { 
      return(
        <HashRouter>
          <h1 className="page-title"> Your Account is not found in Digico Platform</h1>
          <div className="row">
            <div className="col-md-12 page-404">
              <div className="details">
                <p> You must register <a href="https://digico.id/" target="_blank">here</a> to have an access. </p>
              </div>
            </div>
          </div>
        </HashRouter>
      );
    } else if ( !isFirstLoading ) {
      return (
        <HashRouter>
          {/* <div class="container-fluid cont-content"></div> */}
          <div className="container-fluid" style={{ background: 'white', paddingBottom: '50px' }}>
            <div className="row">
              <div className="col-sm-12">
                <div className="row" style={{ marginTop: '40px', padding: '10px', alignItems: 'center'}}>
                  <h4 style={{ color: '#1E90FF' }}>DIGICO - BILL OF LADING</h4>
                  <button onClick={this.openModal} style={{marginLeft: '100px'}} type="button" className="btn btn-primary">+ Add Request</button>
                  <div className="col-sm-2" style={{marginLeft: '50px'}}>
                      <Select
                          name="form-field-name"
                          options={this.state.option_search_by}
                          onChange={this.OnChangeSearch.bind(this)}
                          placeholder='Search By..'/>
                  </div>
                  <input type="text" className="form-control form-control-sm col-sm-2" id="search_text" name="search_text" onInput={this.onFieldChange('search_for').bind(this)} value={this.state.search_for} placeholder="Search For.."/>
                  <button type="button" style={{marginLeft: '10px'}} className="btn btn-primary btn-sm" onClick={this.SearchData.bind(this)}>Search</button>
                  
                </div>
                <div className="card" style={{ marginTop: '20px' }}>
                  <div className="card-header" style={{ background: '#F5F5F5', color: '#1E90FF'}}>
                    <i className="fa fa-file-alt"></i><strong> LIST OF ORDER</strong>
                  </div>
                  <div className="card-body">
                    <table id="table_bol" ref={el => this.el = el} className="table table-striped table-bordered" cellSpacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>NO</th>
                          <th>REQ NUMBER</th>
                          <th>BL NUMBER</th>
                          <th>FORWARDER </th>
                          <th>SHIPPING LINE</th>
                          <th>STATUS</th>
                          <th>DATE</th>
                          {/* <th>ACTION</th> */}
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <Modal
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.closeModal}
            style={customStyles}
            overlayClassName="Overlay"
            contentLabel="Example Modal">

            <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLongTitle">Add Request</h5>
                  <button type="button" onClick={this.closeModal} className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  <form action='/request/submit' method="POST" id="form-request" encType="multipart/form-data">
                    <div className="form-group row">
                      <label htmlFor="ForSelectedDocumentType" className="col-sm-2 col-form-label">Document Type *</label>
                      <div className="col-sm-6">
                        <Select
                          name="selectedDocumentType"        
                          defaultValue={this.state.selectedDocumentType}
                          options={Constants.DOCUMENT_TYPE_OPTIONS}
                          onChange={(ev) => this.autoCompleteDocumentType(ev)}
                          innerRef={this.state.selectedDocumentType}
                          />
                      </div>
                    </div>
                    {
                      this.state.selectedDocumentType === "BC 2.7" ? (
                        <div className="form-group row">
                        <label htmlFor="ForBLNumber" className="col-sm-2 col-form-label">Document Number (nomor AJU) *</label>
                        <div className="col-sm-6">
                          <input type="text" className="form-control" name="bl_number" value={this.state.bl_number} onChange={(ev) => this.handleChangeBCSebelas(ev)} required/>   
                        </div>
                      </div>
                      ) :
                      this.state.selectedDocumentType === "SP3B"  || this.state.selectedDocumentType === "BC 3.0" ? (
                        <React.Fragment>
                          <div className="form-group row">
                            <label htmlFor="ForBLNumber" className="col-sm-2 col-form-label">BC11 Number *</label>
                            <div className="col-sm-6">
                              <input type="text" className="form-control" name="bl_number" value={this.state.bl_number} onChange={(ev) => this.handleChangeBCSebelas(ev)} required/>   
                            </div>
                          </div>
                          <div className="form-group row">
                            <label htmlFor="ForBLDate" className="col-sm-2 col-form-label">BC11 Date *</label>
                            <div className="col-sm-6">
                              <input type="date" step="1" className="form-control" name="bl_date_input" value={this.state.bl_date_input} onChange={(ev) => this.handleChangeBCSebelas(ev)} required/>
                            </div>
                          </div>
                        </React.Fragment>
                      ): (
                        <React.Fragment>
                        <div className="form-group row">
                          <label htmlFor="ForBLNumber" className="col-sm-2 col-form-label">BL Number *</label>
                          <div className="col-sm-6">
                            <Select
                              name="bl_number"
                              defaultValue={this.state.selected_bl}
                              options={this.state.option_bl}
                              isLoading={this.state.loading_bl}
                              // placeholder="BL Number"
                              onChange={this.OnChangeBL.bind(this)}
                              innerRef={this.state.selected_bl}
                            />
                          </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="ForBLDate" className="col-sm-2 col-form-label">BL Date *</label>
                            <div className="col-sm-6">
                              <input type="datetime" step="1" readOnly={true} 
                              className="form-control" name="bl_date_input" 
                              value={this.state.bl_date_input} onChange={(ev) => this.onChangeBLwithDate(ev)} required/>
                            </div>
                          </div>
                        </React.Fragment> 
                      )
                    }
                    <div className="form-group row">
                      <label htmlFor="ForWarDer" className="col-sm-2 col-form-label">Forwarder *</label>
                      <div className="col-sm-6">
                        <Select
                          name="form-field-name"
                          options={this.state.option_ff}
                          placeholder="Forwarder"
                          onInputChange={this.onInputChangeFF.bind(this)}
                          onChange={this.OnChangeFF.bind(this)}
                          isLoading={this.state.isLoadingExternally}
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label htmlFor="ForWarDer" className="col-sm-2 col-form-label">Shipping *</label>
                      <div className="col-sm-6">
                      <Select
                          name="form-field-name"
                          options={this.state.option_sl}
                          placeholder="Shipping"
                          onInputChange={this.onInputChangeSL.bind(this)}
                          onChange={this.OnChangeSL.bind(this)}
                          isLoading={this.state.isLoadingExternally}
                        />
                      </div>
                    </div>
                    <div id="div-container">
                      <div className="form-group row">
                        <label htmlFor="ForContainer" className="col-sm-2 col-form-label">No. Container</label>
                        <div className="col-sm-6">
                          <Select
                            name="cont_no"
                            isMulti
                            id="no_container"
                            options={this.state.option_container}
                            placeholder="No Container"
                            value={this.state.selectedContainer}
                            onChange={this.OnChangeContainer}
                            isLoading={this.state.loading_container}
                          />
                        </div>
                        <div className="col-sm-1">
                          
                        </div>
                        <div className="col-sm-2">
                          <input type="hidden" id="count" name="count" value="1" />
                        </div>
                      </div>
                    </div>
                    <div id="div-uploadfile">
                      <div className="form-group row">
                        <label htmlFor="Forfile" className="col-sm-2 col-form-label">Upload File</label>
                        <div className="col-sm-6">
                          <input id="fileupload" type="file" name="files" />
                        </div>
                        <div className="col-sm-1">
                          <button onClick={this.add_file_req.bind(this)} id="btnUploadFiles" type="button" className="btn btn-primary btn-sm">Add</button>
                        </div>
                        <div className="col-sm-2">
                          <input type="hidden" id="count_file" name="count_file" value="1" />
                        </div>
                      </div>
                    </div>
                    <div className="modal-footer">
                      <button onClick={this.submit_form.bind(this)} type="button" className="btn btn-primary">Submit</button>
                      <button onClick={this.closeModal} type="button" className="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </Modal>

          <Modal
            isOpen={this.state.modalIsOpenConfirm}
            onRequestClose={this.closeModalConfirm}
            style={customStyles}
            overlayClassName="Overlay"
            contentLabel="Example Modal"
            style={{height: "calc(100% - 60px)", overflow: "auto"}}>

            <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLongTitle">Confirmation</h5>
                  <button type="button" onClick={this.closeModalConfirm} className="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  <form>
                    <div className="form-group">
                      <label htmlFor="recipient-name" className="col-form-label">Are you sure will make a DO request with the following details?</label>
                    </div>
                    <div className="form-group row">
                      <label htmlFor="ForBLNumber" className="col-sm-3 col-form-label">
                        {this.state.selectedDocumentType === "BC 2.0" || this.state.selectedDocumentType === "BC 1.6" || this.state.selectedDocumentType === "BC 2.3"? 'BL Number' : 
                        this.state.selectedDocumentType === "BC 3.0" || this.state.selectedDocumentType ===  "SP3B" ? 'BC11 Number': 'AJU Number'}
                      </label>
                      <div className="col-sm-6">
                        <input type="text" className="form-control col-form-label-sm" value={this.state.bl_number} id="bl_number_conf" readOnly/>
                      </div>
                    </div>
                    <div className="form-group row">
                      <label htmlFor="ForWarDer" className="col-sm-3 col-form-label">Freight Forwarder</label>
                      <div className="col-sm-6">
                        <input type="text" className="form-control col-form-label-sm" value={this.state.name_ff} id="forwarder_conf" readOnly/>
                      </div>
                    </div>
                    <div className="form-group row">
                      <label htmlFor="ForWarDer" className="col-sm-3 col-form-label">Shipping Line</label>
                      <div className="col-sm-6">
                        <input type="text" className="form-control col-form-label-sm" value={this.state.name_sl} id="shipping_conf" readOnly/>
                      </div>
                    </div>
                    <div id="div-container-conf">
                      <div className="form-group row">
                        <label htmlFor="ForContainer" className="col-sm-3 col-form-label">No. Container</label>
                        <div className="col-sm-6">
                          <input type="text" className="form-control col-form-label-sm" value={this.state.selectedContainer? this.state.selectedContainer.length ? this.state.selectedContainer[0].value: null : null} id="no_container_conf" readOnly/>
                        </div>
                      </div>
                    </div>

                    {/* {this.state.data_container.map((element, index) => (
                      <div id="div-container-conf">
                      <div className="form-group row">
                        <label htmlFor="ForContainer" className="col-sm-3 col-form-label">No. Container</label>
                        <div className="col-sm-6">
                          <input type="text" className="form-control col-form-label-sm" value={this.state.container} id="no_container_conf" readOnly/>
                        </div>
                      </div>
                      </div>
                    ))} */}

                    <div className="form-group">
                      <label htmlFor="recipient-name" className="col-form-label">Click "Yes" to continue the process or "No" to correct BL, FF, SL and Container number</label>
                    </div>
                    <div className="modal-footer">
                      <button type="button" className="btn btn-primary" onClick={this.submit_form_conf.bind(this)}>Yes</button>
                      <button type="button" onClick={this.closeModalConfirm} className="btn btn-danger" data-dismiss="modal">No</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </Modal>

        </HashRouter>
      );  
    } else {
      return (<HashRouter><div style={{clear:"both"}}></div><span>Loading ...</span></HashRouter>);
    }
  }
  getUserDigico() {
    const encodedNpwp = encodeURIComponent(Constants.ENC_ID);
    const decryptorURI = Constants.API_USER_DIGICO;
    fetch(`${decryptorURI}${encodedNpwp}`)
      .then((res) => {
        if ( res.status === 200 )
          return res.json();
        else
          this.setState({
            isFirstLoading: false,
            userName: null,
            email: null,
            companyID: null,
            compType: null,
            roleID: null
          })
      })
      .then( (data) => {
        // this.setState({
        //   isFirstLoading: false,
        //   userName: 'User_ff19',//data.digicoUser.userName,
        //   email: 'chalid.harvey@gmail.com',//data.digicoUser.email,
        //   companyID: '1208',//data.digicoUser.companyID,
        //   compType: 'Admin',//data.digicoUser.compType,
        //   roleID: null,//data.digicoUser.roleID 
        // }, () => {
        //   this.ReloadTable();
        // })

        this.setState({
          isFirstLoading: false,
          userName: data.digicoUser.userName,
          email: data.digicoUser.email,
          companyID: data.digicoUser.companyID,
          compType: data.digicoUser.compType,
          roleID: data.digicoUser.roleID 
        }, () => {
          this.ReloadTable();
        })
      })
      .catch(error => {
        this.setState({
          isFirstLoading: false,
          userName: null,
          email: null,
          companyID: null,
          compType: null,
          roleID: null
        })
      });
  }
  componentDidMount() {
    const {npwp, userName} = this.state;
    if ( typeof this.props.dataUserPortal.digicoUser !== "undefined" ) {
      this.setState({
        isFirstLoading: false,
        userName: this.props.dataUserPortal.digicoUser.userName,
        email: this.props.dataUserPortal.digicoUser.email,
        companyID: this.props.dataUserPortal.digicoUser.companyID,
        compType: this.props.dataUserPortal.digicoUser.compType,
        roleID: this.props.dataUserPortal.digicoUser.roleID 
      }, () => {
        this.ReloadTable();
      })
    } else {
      this.getUserDigico();
    }
  }

  loadBlNumber(type) {
    let url = null;
    let resultName = null;
    if ( type === "BC 2.0" ) {
      url = Constants.GET_LIST_BL2_0;
      resultName = "Data BL BC 2.0";
    }else if ( type === "BC 1.6" ) {
      url = Constants.GET_LIST_BL1_6;
      resultName = "Data BL BC 1.6";
    }else if ( type === "BC 2.3" ) {
      url = Constants.GET_LIST_BL2_3
      resultName = "Data BL BC23"
    } else {
      url = null
      resultName = null
    }
    const bl_pelimpahan = Constants.API_DATA_PELIMPAHAN_DOCTYPE + "?ppjk=" + this.state.npwp + "&doctype=" + escape(type)
    if ( url !== null ) {
      const npwp = this.state.npwp;
      let newBLDate = this.state.bl_date;
      fetch(url + npwp)
      .then(Response =>Response.json())
      .then((result) => {
        let newBLOptions = [];
        let blResults = [];
        result[resultName].map((item, index) => {
          let bldata = {value: index, label: item.bl_no}
          newBLDate[item.bl_no] = item.bl_date;
          newBLOptions.push(bldata)
          blResults.push(item)
        });
        fetch(bl_pelimpahan)
          .then(ResponseP =>ResponseP.json())
          .then((resultP) => {
            resultP.map((itemP, indexP) => {
              let bldata = {value: indexP, label: itemP.bl_no}
              newBLOptions.push(bldata)
              newBLDate[itemP.bl_no] = itemP.bl_date;
              blResults.push(itemP)
            })
            this.setState({bl_number_list: blResults, option_bl: newBLOptions, bl_date: newBLDate, loading_bl: false,  data_container: [], option_container: [], selectedContainer: []})
          },
          (error) => {
            this.setState({bl_number_list:[], option_bl: [], loading_bl: false, bl_date: [], data_container: [], option_container: [], selectedContainer: []})
            console.log("Err", error);
          })
        
      }, 
      (error) => {
        this.setState({bl_number_list:[], option_bl: [], bl_date: [],  loading_bl: false, data_container: [], option_container: [], selectedContainer: []})
      })
    }
    else {
      this.setState({bl_number_list:[], bl_number: [], option_bl: [], loading_bL: false, data_container: [], option_container: []})
    }
  }
  autoCompleteDocumentType(selected){
    const docType = selected.value;
    this.setState({
      selectedDocumentType: docType,
      bl_number_list: [],
      bl_number: '',
      bl_date_input: '',
      option_bl: [],
      loading_bl: true,
      selected_bl:'',
      data_container: [],
      option_container: [],
      selectedContainer: []
    }, () => {
      this.loadBlNumber(docType)
    });
  }
  handleChangeBCSebelas = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    let inputValue = val;
    if ( nam === "bl_number" ) {
      const selectedBlData = this.state.bl_number_list[val];
      inputValue = selectedBlData.bl_no;
    }
    
    this.setState({
      [event.target.name]: inputValue
    }, () => {
      if (this.state.selectedDocumentType === 'BC 2.7') {
        if (nam === 'bl_number'){
          this.getContainerByBL(inputValue, null)
        }
      }
      else{
        if ( nam === 'bl_date_input' && this.state.bl_number.length ) {
          this.getContainerByBL(this.state.bl_number, val);
        } else if ( nam === 'bl_number' && this.state.bl_date_input.length ) {
          this.getContainerByBL(inputValue, this.state.bl_date_input);
        }
      }
      this.getContainerByBL(this.state.bl_number, this.state.bl_date_input)
    })
  }

  onChangeBLwithDate = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]:val}, () => {
      this.getContainerByBL(this.state.bl_number, val);
    });
  }

  OnChangeBL(selected) {
    const index = selected.value;
    const selectedBlData = this.state.bl_number_list[index];
    const selectedDate = Helper.formatDate(selectedBlData.bl_date);

    this.setState({selected_bl:index, bl_number: selectedBlData.bl_no, bl_date_input: selectedDate}, () => {
      this.getContainerByBL(selectedBlData.bl_no, selectedDate)
    });
  }

  getContainerByBL(bl_no, bl_date) {
    const {selectedDocumentType} = this.state;
    let urlGetContainer = null;
    let resultName = null;
    let docType = selectedDocumentType;
    if ( docType === "BC 2.0" ) {
      urlGetContainer = Constants.GET_CONTAINER1_2_0
      resultName = "Data Kontainer BC20"
    } else if ( docType === "BC 1.6" ) {
      urlGetContainer = Constants.GET_CONTAINER1_1_6
      resultName = "Data Kontainer BC16";
    } else if ( docType === "SP3B" ) {
      urlGetContainer = Constants.GET_CONTAINER1_SP3B
      resultName = "Data Kontainer SP3B"
    } else if ( docType === "BC 2.3" ) {
      urlGetContainer = Constants.GET_CONTAINER1_2_3
      resultName = "Data Kontainer BC16";
    } else if ( docType === "BC 2.7" ) {
      urlGetContainer = Constants.GET_CONTAINER1_2_7
      resultName = "data status impor"
    } else if ( docType === "BC 3.0" ) {
      urlGetContainer = Constants.GET_CONTAINER1_3_0
      resultName = "Data Kontainer SP3B"
    }
    this.setState({loading_container: true}, () => {
      if ( urlGetContainer !== null ) {
        if (selectedDocumentType === "BC 2.7"){
          bl_date = ''
        } else {
          bl_date =  bl_date.replace(/-/g, "")
        }
        const fullUrlGetContainer = urlGetContainer + bl_no +'/' + bl_date;
        fetch(fullUrlGetContainer)
        .then(Response => Response.json())
        .then((result) => {
          let newContainerOptions = [];
          let newDataContainer = [];
          result[resultName].map((item, index) => {
            let containerdata = {value: item.container_no, label: item.container_no}
            let dataByNo = {
              container_no: item.container_no,
              container_type: item.container_type,
              container_size: item.container_size
            }
            newDataContainer[item.container_no] = dataByNo
            newContainerOptions.push(containerdata)
          });
          this.setState({
            loading_container: false,
            option_container:newContainerOptions,
            data_container: newDataContainer
          });
        },(error) => {
          this.setState({loading_container: false});
          }
        )
      } else {
        this.setState({loading_container: false});
      }
    })
  }
  OnChangeContainer = (selectedContainer) => {
    let containerList = selectedContainer === null ? []: selectedContainer;
    this.setState({selectedContainer: containerList});
  }
  OnChangeFF(val) {
    console.log("Selected: " + val.value);
    this.setState({id_ff: val.value, name_ff: val.label})
  }

  OnChangeSL(val) {
    console.log("Selected: " + val.value);
    this.setState({id_sl: val.value, name_sl: val.label})
  }

  onInputChangeFF(inputValue) {
    console.log("input: " + inputValue);
    this.setState({ isLoadingExternally: true });
    let header_config = {
      headers: {
        "Content-Type": "application/json",
      }
    };

    var url = Constants.GET_FORWARDER;
    console.log(url);

    axios.post(url,
      {
        Search: inputValue
      },
      header_config
    )
      .then(result => {
        console.log(result);
        let data_ff = [];
        for (var i = 0; i < result.data.length; i++) {
          let data_object = {
            value: result.data[i].id,
            label: result.data[i].name
          }
          data_ff.push(data_object);
        }
        console.log(data_ff)
        this.setState({ option_ff: data_ff, isLoadingExternally: false })
      })
      .catch(error => {
        console.log(error);
        this.setState({ option_ff: [], isLoadingExternally: false })
      });
}

onInputChangeSL(inputValue) {
  console.log("input: " + inputValue);
  this.setState({ isLoadingExternally: true });
  let header_config = {
    headers: {
      "Content-Type": "application/json",
    }
  };

  var url = Constants.GET_SHIPPINGLINE;
  console.log(url);

  axios.post(url,
    {
      Search: inputValue
    },
    header_config
  )
    .then(result => {
      console.log(result);
      let data_sl = [];
      for (var i = 0; i < result.data.length; i++) {
        let data_object = {
          value: result.data[i].id,
          label: result.data[i].name
        }
        data_sl.push(data_object);
      }
      console.log(data_sl)
      this.setState({ option_sl: data_sl, isLoadingExternally: false })
    })
    .catch(error => {
      console.log(error);
      this.setState({ option_ff: [], isLoadingExternally: false })
    });
}
  add_new_container() {
    const currentAdditionalContainer = this.state.additional_container;
    var newObjectContainer = { no_container: "" };
    currentAdditionalContainer.push(newObjectContainer);
    this.setState({additional_container: currentAdditionalContainer});
  }

  add_container_req() {
    const self = this;
    console.log('add container');
    const $ = require('jquery')
    // var count = parseInt($('#count').val());
    var count_cont_check = this.state.count_cont + 1;
    console.log(count_cont_check);
    this.setState({count_cont: count_cont_check});

    var count = count_cont_check;

    var input_cont = '<div class="form-group row" id="div-row-cont-' + count + '">' +
      '<label for="ForContainer" class="col-sm-2 col-form-label">No. Container</label>' +
      '<div class="col-sm-6">' +
      '<input type="text" name="cont_no' + count + '" class="form-control col-form-label-sm" id="no_container_add-' + count + '" placeholder="No Container">' +
      '</div>' +
      '<div class="col-sm-2">' +
      '<button id="btnDeleteContainer_'+count+'" type="button" class="btn btn-danger btn-sm">Delete</button>' +
      '</div>' +
      '</div>';
    $('#div-container').append(input_cont);
    // $('#count').val(count + 1);

    $("#btnDeleteContainer_" + count).click(function () {
      self.removeRow_container_req(count);
    });
  }

  add_file_req() {
    const self = this;
    console.log('add file');
    const $ = require('jquery')
    // var count_file = parseInt($('#count_file').val());
    var count_file_check = this.state.count_file + 1;
    console.log(count_file_check);
    this.setState({count_file: count_file_check});

    var count_file = count_file_check;
    // console.log(this.state.count_file);
    var input_file = '<div class="form-group row" id="div-row-file-' + count_file + '">' +
      '<label for="ForContainer" class="col-sm-2 col-form-label">Upload File</label>' +
      '<div class="col-sm-6">' +
      '<input  id="fileupload-' + count_file + '" type="file" name="files' +count_file + '">' +
      '</div>' +
      '<div class="col-sm-2">' +
      '<button id="btnDeleteUploadFile_'+ count_file+'" type="button" class="btn btn-danger btn-sm">Delete</button>' +
      '</div>' +
      '</div>';
    $('#div-uploadfile').append(input_file);
    // console.log(this.state.count_file);
    // $('#count_file').val(count_file_check);

    $("#btnDeleteUploadFile_" + count_file).click(function () {
      console.log('cluciked');
      self.removeRow_file_req(count_file);
    });
  }

  removeRow_container_req(id){
    console.log('remove click ' + id);
    const $ = require('jquery')
    $('#div-row-cont-'+id).remove();
    // var cd = parseInt($('#count').val());
    // $('#count').val(cd-1);
    var count_cont = this.state.count_cont;
    this.setState({count_cont: count_cont - 1});
    console.log(this.state.count_cont);
  }

  removeRow_file_req(id){
    console.log('remove click ' + id);
    const $ = require('jquery');
    $('#div-row-file-'+id).remove();
    // var cd = parseInt($('#count_file').val());
    // $('#count_file').val(cd-1);
    var count_file = this.state.count_file;
    this.setState({count_file: count_file - 1});
    console.log(this.state.count_file);
  }

  submit_form() {
    console.log(' click submit');
    const $ = require('jquery');
    const self = this;
    // var count_cont = $('#count').val();
    var count_cont = this.state.count_cont;
    var count_file = this.state.count_file;
    var bl_number = this.state.bl_number;
    var forwarder = this.state.id_ff;
    var forwarder_name = this.state.name_ff;
    var shipping = this.state.id_sl;
    var shipping_name = this.state.name_sl;
    var cont_no = this.state.selectedContainer.length ? this.state.selectedContainer[0].value: null;
    var files = $('#fileupload').val();
    
    if (bl_number == '' || forwarder == '' || shipping == '' || files == '') {
      Swal.fire("Don't leave empty input!");
      return false;
    }
    var count_file = this.state.count_file;
    for (let i = 1; i <= count_file.length; i++) {
      var files_cek = $('#fileupload-' + i + '').val();
      if ( files_cek == '') {
        Swal.fire("Don't leave empty input!");
        return false;
      }
    }

    this.openModalConfirm();

    Swal.fire({
        title: 'Please Wait ..',
        onBeforeOpen: () => {
            Swal.showLoading()
        }
    });

    setTimeout(function () {
      Swal.close();
      self.setState({bl_number: bl_number});
      // $('#bl_number_conf').val(bl_number);
      // $('#forwarder_conf').val(forwarder_name);
      // $('#shipping_conf').val(shipping_name);
      // $('#no_container_conf').val(cont_no);
      if (self.state.selectedContainer.length > 1) {
        for (var b = 1; b < self.state.selectedContainer.length; b++) {
          var index = b + 1;
          var input_cont = '<div class="form-group row conf" id="div-row-cont-' + index + '">' +
            '<label for="ForContainer" class="col-sm-3 col-form-label">No. Container</label>' +
            '<div class="col-sm-6">' +
            '<input type="text" class="form-control col-form-label-sm" id="no_container_conf' + index + '" readonly>' +
            '</div>' +
            '</div>';
          $('#div-container-conf').append(input_cont);
          var a = self.state.selectedContainer[b].value;
          $('#no_container_conf' + index + '').val(a);
        }
      }
    }, 1000);
  }

  submit_form_conf() {
    const newID = uuid();
    const $ = require('jquery');
    const self = this;
    // var count_cont = $('#count').val();
    var count_cont = this.state.selectedContainer === null ? 0: this.state.selectedContainer.length;
    var count_file = this.state.count_file;
    var bl_number = this.state.bl_number;
    var cont_no = this.state.selectedContainer.length ? this.state.selectedContainer[0].value: null;
    console.log(count_cont);
    console.log(count_file);
    console.log(bl_number);
    console.log(cont_no);
    
    let files_data = new FormData();
  
    if (count_cont > 1) {
      for (var i = 1; i < this.state.selectedContainer.length; i++) {
        console.log('count_container ' + i);
        var container = this.state.selectedContainer[i].value;
        files_data.append("cont_no"+ i, container);
      }
    }

    let dataPostContainer = [];
    this.state.selectedContainer.map((container_item, container_index) => {
      let dataContainerByNo = typeof this.state.data_container[container_item.value] !== "undefined" ? this.state.data_container[container_item.value]: false;
      if ( dataContainerByNo )
        dataPostContainer.push({
          container_no: dataContainerByNo.container_no,
          container_size: dataContainerByNo.container_size,
          container_type: dataContainerByNo.container_type,
          id_request: newID
        });
    });

    files_data.append("check_api", 'get_api');
    files_data.append("bl_number", bl_number);
    files_data.append("username", this.state.userName);
    files_data.append("id_company", this.state.companyID);
    files_data.append("id_forwarder", this.state.id_ff);
    files_data.append("name_forwarder", this.state.name_ff);
    files_data.append("id_shipping", this.state.id_sl);
    files_data.append("name_shipping", this.state.name_sl);
    files_data.append("cont_no", cont_no);
    files_data.append("count", count_cont);
    files_data.append("count_file", this.state.count_file);

    var fileInput = document.getElementById('fileupload');
    if(fileInput.files.length > 0){
      var filename = fileInput.files[0].name;
      let documents = document.getElementById("fileupload").files;
      files_data.append(
          "files",
          documents[0],
          filename
      );        
    }

    if (count_file > 0) {
      console.log('lebih dari 1');
      for (var i = 0; i < count_file; i++) {
        var count = i + 1;
        console.log('count_file ' + count);
        var fileInput = document.getElementById('fileupload-'+count);
        // if(fileInput.files.length > 0){
          // var fileInput = document.getElementById('fileupload-'+count);
          var filename = fileInput.files[0].name;
          console.log(filename);
          let documents = document.getElementById('fileupload-'+count).files;
          console.log(documents)
          files_data.append(
              "files"+ count,
              documents[0],
              filename
          );        
        // }
      }
    }

    files_data.forEach((value,key) => {
      console.log('key ' + key+" : "+value)
    });
      
    Swal.fire({
      title: 'Please Wait ..',
      onBeforeOpen: () => {
          Swal.showLoading();
      }
    });

    let header_config = {
      headers: {
        'Content-Type': 'multipart/form-data',
      }
    };

    var url = Constants.SUBMIT_BOL;
    console.log(url);
    axios.post(url, files_data, header_config)
      .then(result => {
        Swal.close();
        if (result.data.status === false) {
          Swal.fire(result.data.message);
        } else {
          const dataPostToNLE = {
            id_request: newID,
            npwp: this.state.npwp,
            bl_no: bl_number,
            name_forwarder: this.state.name_ff,
            name_shipping: this.state.name_sl,
            jumlah_container: count_cont,
            status: "Add Request",
            digico_request_container: dataPostContainer
          }
          axios(
            {
              method: 'post',
              url: Constants.DIGICO_BOL_REQUEST_NLE,
              data: dataPostToNLE,
              headers: {
                'content-type': `application/json`,
              }
            }
          ).then(resPostData => {
            if ( resPostData.status === 200 ) {
              Swal.fire(result.data.message);
              self.closeModalConfirm();
              self.closeModal();
              self.setState({id_ff:'', name_ff: '', id_sl: '', name_sl: '', count_cont: 0, count_file: 0, selectedContainer: [], bl_number: ''})
              table.ajax.reload();
            }
          })
          .catch(err => {
              Swal.close();
              Swal.fire(err);
          })
        }
      })
      .catch(error => {
        Swal.close();
        Swal.fire(error);
      });
  }

  OnChangeSearch(val) {
    console.log("Selected: " + val.value);
    this.setState({search_by: val.value})
  }

  onFieldChange(fieldName){
    return function (event) {
        const target = event.target;
        this.setState({[fieldName]: event.target.value});
    }
  }

  SearchData(){
    const self = this;
    if (this.state.search_by != '' && this.state.search_for != '') {
        console.log(this.state.search_by);
        console.log(this.state.search_for);
        self.ReloadTable();
      
    }
  }

  ReloadTable(){
    const self = this;
    console.log(this.el);
    this.$el = $(this.el);

    if (table != null) {
      table.destroy();
    }

    var url = Constants.GET_LIST_BOL_SEARCH;
    console.log(url);
    
    table = this.$el.DataTable({
      dom: 'Bfrtip',
      scrollX: true,
      ajax: {
        url: url,
        dataSrc: "data",
        type: "POST",
        data:{
          check_api: 'get_api',
          id_company: this.state.companyID,
          search_by: self.state.search_by,
          search_for: self.state.search_for,
        }
      },
      buttons: [
        {
          extend: 'excelHtml5',
          className: "btn-sm btn-success",
          text: '<span class="fa fa-file-excel"></span> Export Excel',
          filename: 'BILL OF LADING REPORT',
          title: 'BILL OF LADING REPORT',
          exportOptions: {
            columns: [0, 1, 2, 3, 4, 5, 6],
            modifier: {
              page: 'all'
            }
          }
        },
        {
          extend: 'pdfHtml5',
          orientation: 'landscape',
          className: "btn-sm btn-danger ml-2",
          text: '<span class="fa fa-file-pdf"></span> Export Pdf',
          // customize: function (doc) {
          //   console.log(doc);
          //   doc.content.splice(1, 0, {
          //     margin: [0, 0, 0, 15],
          //     alignment: 'left',
          //     image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAAAxCAYAAADzw7RKAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAM6wAADOsB5dZE0gAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABw1SURBVHic7Z15nFTVlce/51V10003NDR700AjiwYXYDQaZSbuY1ySGcMkbhijJmYSJ8mYmMzmTBCN0cnEJVETdyWGGNwS444g7iDigqCCgiDQ0tLQzdJLVVe9M3+c+6hX1VXVDb2ASf0+n/pU1bv33Xvfu/cs99xz7xG6GesqKgb2LRt0U8L332rzkw+s/mT9xmMhBmh311VAAQV0DtLdBTZWVAyMl1c+liByUFz9xW3wcJOfeC7hJT48rLa2ubvrK6CAAjpGpLsL/PeSktJEcemMJLJfQmRMQjgsKd6B+F7ZjJKy7WOadzQsLEj3AgroVfSIRI+VVz4el8jn2oA2VW1F/QRsiSkvxTTxUKvGXxy9aVPtgRDv7voLKKCA9vB6ugIVBJGIigxV4UviRX8qkdJZa6tqpv+xsmrU3B7QKvYQAtwJTN9L9VcA6zvIcwBwdRfruQj43xxpo4DjQ/9fASa5Or+Tp8wpwFz3uRWYQQ8IkU8J+gEb3O9XgYl58v4n8G9dqOvPwN9luX4v8MXwhWgXKtl9iERAR4ky3Rfv8ERJn0VtVTUP39Xc+Nz5jY2NvdqW9jgGmAZMBh7cu03JicHACV0s4yGgJEfawcDXgPnu//nA2k6UOQLYD/gWUAncAPQB7uhKQ/8C8DXgozzpd3ax/EuATZ3J2OMSPStEShEm4MlZIt6tUjbwhtuHVI/fK21J4XxgFiaJJrtrRwGXATcBbwFXAQcC84DXgdNcvrOAF4DXgHuAQe76H4CvAs+7zyHu+qWYtHwNuBYjigD/jEmCPwNVnWz7N4GXgZfcc4AR842urNuAnwKfBY4ATgHKgdnAm8Bi4GzgcozhzQP+FZPiwzLqOhD4LcZ0wtgBLHX33ou9O7D3twhYAlyDCZeLgX8I3Xs48HOMwYbfxZWuvf8TKuNqOqcFCnA/UOT+3471E8CJwHeBB0jRwG+B093v07Bnvz9U3lzgVPd7OnBuKO0gjDkuzLj+L9h76ocR9SL3nP8H7A8cCxzt8p4KLMDe4Wx37QTgWax/ZtJeMJ8HjAWKsXEU9HV55svodUJXEXEfD5Fi9RiKMCPZp+jZX1WNmXX90OpDLmVYGb2r+vXHOv9hjFADYhmKcc0bgSOxwXkF1tEzgJtdvrewTjkMWI51CphadSDWoTcCv3DX52Haw+eAMoxQwSRjMzbwlwD/0Ym2H48N2tNc+34EfB74MVDq6rjRPcdgYCQw3j1jg2vz3wIvAj/BBuuJwPUYYygL1XU0NgivBOoz2lEEDMRU/S9hTAzgUYzoj3LPd65L+1Ho3n/B3uFOUlOnwdhAfhP4k7t/Gja9OKcT70WBvhijqMQY2NkubTqw2V2fijHUo4AzXfpXgY2urknAOOw9fcWln0lKkkYwhnElcBwwIdSGw10bfoq9ryMxBv59bKo2Fqhxbfgl8A3gUEygVGPv+ltYfx6KMcgwpgADXHnDXPk3Aydlvoy9I9Ez4Ii+Gs+71C8qunnQyNLvzRwx6tAfDx7cj94h+DOBxzEi+z3W0cUu7VngXZe2yOXbDryDdXJ/YCvwQ+AWrEMOC5V9E5AEnsTm2LiyZrm0Me4eXLkBNw/nz4dTMWm1FRtMd7trJ2Gd7mNE9ELGfR9ikv3b2IDOp2KCEe/PXNkrs6RPwgb8ExjB/sZdj2FEcCNGUIdhEqoUY4KVGIE84O4JmN55wH3u/gRGLL8ChpP+fsEG+AmkpGOA+e76sZiWUY0xpOOwfl2AMcrjMNvC/hhNHI0xvPku/XiMEKe69KMw7QmMsFtceb5rYyb+HutrxTS7ZRnpp2MSf437/55r09PAKvcObiClQWbiFFdvEniD9n3dJUIXBW8uFL9cXV36bE1NiXaRKAVKVPicilxKJPLzaJ+yiy4ZPnrSyenqXE/gfKwzX8MIeSA2sAGaQvnaMCIN/4+6ezZiatld2CAO0JyRtwx4CmMa12DSKsifWVdnbCil2EAL11eKMaG20PVExn2PAmdghPM0pmLnQ8y1vShH+lvYOxyPqY5fw97jo8Bz2LM+TupZf4MZBs/F1OJWbDpTgTG4b2DENwh4BCOkazAGGH6/kJ/QA0Kej6m2/+TqqiOdkOdjBHgG8DGwLeP+p4H3XfoqUv1aQvr7D/8OECV/X/Qlve+DcsNjrZnctpXMNrTzV9ktY5yCt7hyfHmyjCEa9ape9iIjR6JDkkh/D92wsKbmT1MbGnanyPTyRURtgFYCRyMy2ROOGV89+pGL/MQLDbW16+7P/iK7gkmYpJlKan3/VIz4b+vE/R5miAqkz+n5s+9Szx91/zOl0+7iVYwp3e7+nwL8Dlu6PAsbvKOwacSNofvKMDvD69hc9DZM2gzNUc9T2EB/ArPofpgjXxtmZ/gzpgltcfeCTQWC/puDTXNagC+H7v8NJt02uvqmYkT5dKiMbRl1XpujLcuwvhmITV1GYNrFYy59CWY3GYG9h/mYneIBl/4y9i4/cc8SpM/GGNpg4G1MKxuOqfNfyNKOF7G++AXwGfdMYbwA/ADrnwQ2ppa4ayUYYzoN6+sR2JidH7p/CabBvYVpmNPIMIR2itAVZGnVxEGvRZkiETkiGvEO8ZEaXxmqnleu0EeQ53y/dCE0bO9MmR3CiH4g6MmCTBWv6JWy6uqnvxKLPbtm8+Z1S9M5ZFdwPkakW0PXHsBUpcey3pEOH5Pii7HBuTV/dtYA6zCiitN+rtsRDgJWh/7fgTGYVzAmuRJ7nscxNf414APXvjCTvBiTbhsxCXoFNmAi2ICZm6XueZiq/xjG0LKp8GDz6iXYgNuBSWPFiD5AE8bsDsZU1QBzMM0oUOGXu+db4P6Hy+gIihFZP+xdL8Dm2wGRJLD3E8P6cQH2zoL0VmAFJuHVXb/ZfZ+EzZ3PwOwNL2LTudos7fh3jDmcgfXdW6T3xZ8wRrwM0xYqXdm/x4xztRjBn07KOHlE6P6rsP4+GtO43slsQIeq9goobhs98fBERM5DvCN8oUpFyn0oUhFJGkGC8kyyLfHNqQ0fbws7zMRRYhhVxoC4uG/3iaG7fscJpbl8bUBMfY1DLK5sjuMviylzk9sbHnt4x47d6fSexhBs8HbWzbcKMwh1F8OqcN+Z0g5s6vMONpjCA7EvJsE3YYO6JyCYFPqE9irr/Zil/b7QtVGYhJtIyqEqXxn7Ckqx99zRMnF/rC8OwGwZYZRg46gWm28H5ZZjY6UjBFpFO+SV6Cv6V1fGK8u+puJ9RzzGKEQFI2wNCLw3ICKo9kGoViIjQA9PVlR+4aTyAb+u/zi5dCn7hA99ZzoijGycvyvIJPBxmBT5ADP23ZWlzmY6t07eFWiWemswqV1DSk0G+Dq29HgV6V6T2crY19BC7mnlZ7EltbWYJP4Z7YkcjNlmOk3lKzcTOdfUs0r0meB9sXrcftFI9BL1vLN9ocIXERV74z6CH/wWoccleihPkN6m6seU9a3iP9ySTDy4s7b2zRUmUQt+9ClUYFy+FlOh9xWUYXPnlaQTdA2mQne0AvBpRCUmrTfQ3vDW42jneKAQHVg99tCioqKf+sJ0hHKT3rKLLSiCoL5CDGE7KluBFerFnx7R3NyWLC49JyletY/pH0ms95JAUlLX8n4k47526SIJY0BTETlSBlRUDSkraymLRrc1tLbGeu6VfaoQw+a0+9qegjZMDU9mXG8k+9TjLwEtWF9011Rtt5CmuitEl1VPODQS9f5LkZMQKVZJy6AIbaD1oqwW/HfUl5Xq60cJYa0kE/W0X/roMQgiQImKHCQwQaPR4/uUl99VU1Fxx9q1a3tqzllAAZ86pBH6qqqJ46NRucgXPq+h9VIBFUgqbMJnsZB8BtVX/B1b1zxTX9800wQvAI0VFb1G6GkQaRNfk77PgHgyvk84AhVQwL6CXYT+7siRg/yI/1WInCLQD0mbv7eh/jLP19+Jn3y69aPWD49iQ3evZ+8BVIEEykrV5CPJZHJhrK3t7drNm/d22wRbn32rg3wHYxbYTBU2QJVLq+u+phXw14gomMq+yiv7O9+Tc7Dlll1ELqpJhWW+r1c2bmtbeELDmu2yLxi8VBXVLYg/V+Lx2bHW1lXLt23bwZ4tv6zE/LsfAUZnSb8Hc7gIUI4ZjBowy3Ud8AzmH96KOTwsILW5JRcew9bFc/keXOTScjmEFFBApxAFWF5VNaLIY7rCWBXxdlGx+kmQ5ZpIXCHrP3jixL1kSNgFI+6kiNSj+jIk74i3tL7wUn39TrrGfCow4pzi/h+AeXONyZFfMCeMSnffeGy56GjgZEwKd0Tk0J6pDMcYVeBEMzPLPUMwp5xsWkAlZtDys6QV8FeM6FyIlET6Tk0g0wip8qKoIHXq66/6rf/gqQl7k8gVBfWBRg8Wq+//IRZrnvdUff0m9r524WPeTDMwKT8Z8+b6BCP2X2OeYre4/Odhmyy+7vIfhK1+PIFpA62Y99TFGKEHEn0S5im1Cdv1dBnmvXYY5jb6IebqORbzzV7XQ89bwKcQ0UMGD+7re94xYh5vu1R2hSS+Pt6a2PnHybZM06sQ1YCAFXQ7qq+Lz5xYbOeT9Vu21C3c9zykdmDukpMwQg9wD0aoYUKflXHv5zHX2GAbZTZj4g3YXuzfY5rAEmyDB5hzzMmY085PMCZy+R4/SQF/cYi2eQOGFivTkF3bMgHwlNpEW+KuI2o39rqbqTgjmyiNiv8+vj4VafPnjNi8Yc3MfVstLaa9Sr0I2zO8Pyatx2C7ucJYjKn+T2L+3/fS3pXycOAf3e+PMKk/CWN4b5PyzFuJaQwFFLAL0WiUSYhOVCRkZdck8HB57eqlvdkYUVUBVbQR5A1Rf6GQmB+r3fjWrZ33Id9bCHYVZXtn92DbNluxk0wypxubsF1NR2CHKjxD+11trZgvdOBVVequZW6BVP56z2srIAeikag3DdV+4aEh0JBsS943oZdVdoU4qss85aFEsu3Z5hbev277xn3VuCSYG2cRJq1nYgS9mvYeh/diu5vayHL6B6Z6N2PbIutc3kw8jln+/xubgwebIw7JkreAAtIQFeFwH0mfE6qsapLWVb3RAFFUhDZR/UQ0+YAmdU5rvGllZOvWndf1HoE/T/rGgSayE1uABLb18hZ331qM0B9x6Uq6er4R24pYTPo+7hddWcOxE1TKMOYanDu2hpQEvwTbz7wUYwb/gLm2bsf2Ugeow/ZOF1DALsjKMRPeV/HGux1pKOoremdrbOf39ySySua57vk2tbQpiRi6Me77C1o1efeOj+NLf0RdM3vfkl5AAX9RiAr0z6CqBEptRW3fXN5aXYLNwyUp+PWeMg+/bU5z847F39m2bc+PpimggALyIgqSfh6bkvSQ5jgfdJtUDZbKRCShyHYP/zXP92c3NzU+cc62bY0UJHgBBfQootJ+Hizqa7dtChFFMc19k6ouB12QjLf9qWnzhg9vA/liVdWgJs/r04JZo1qA1h07mj8qMIACCug2RFVpQRgYXFCRKGjFZmq8rhw+4pbKfIF6UX014vtPov7zLXXr3z/FWfO/NHhUFUXRs6KiE4twi9BJTVJevoht24KTQQsooIAuIorQSCgiiKhGfM8bUzmsJdK1PVMaQ3ldfJ2TJLmwONmybsrmzU3hDTGRMm9MQpgOMiU4pkZEWtXzmkg/R6yAAgroAqKivI8wKbjg3GAP9Hb2LWcPjrzxPc+PqLyXUH9hpC354Pb6+LtTqMtWjuepTPTEG4Oo7WE3Yo+h3ifse6eiFFDApxZRX5MviEZOU5FdTh6i7JccFDmcJv68uwUObGjY2cDAn0hDvH4UtS2jc8yzTx85cqCIHIqEd3mpirDN8/1cxwgXUEABewCPpC4WqE/bYy70jXiRC1+jqu/uFiiQrGxY81EVtc159q1LNBL5jCfeNEjzsVdB1yvJXnHWKaCAvxZEm+Jta8qikbcROQ7nI+1CKx0Tqel7oq7lUcl9Asoe4ezRowd4kaJj2yxuVeqQC4jj+28mWlo+7s768qCC9tFYGjE31tnYwf0BvozFaLsY20ByEHaQ/g2k4nBlYha2b/2SHOn5MA0LsDcZY4brsHPQ7yF9596JpIIdZOJuzHU2G36PnZ9+c470MEYC12EHN15EewZ+DunRUbdjh3K87urPPPDx61hEmfOwhZajsOitrdizZLpe/xqL2pJ5AMehwIXue7CrcwXwR8wTMZdnZR9sz0GABNbXy7EIuGFHsSlYHPNcmIm5Is8gFcYLbBytxcJUv5dxz2Dge5gr8zDs0MjnsbG0MZQvgvVTgCR2ou8K106PToam9lrr1zfg6QJgJ6mtoahIeYTIRW9X1aQRY1fxFYiUaOSzUZVTxQIIpKA0qOrCNQ0NvXUcbh9rEgeFrtVgBPwBtmMswCSXN4guOsz9z3YiTYBjyR6ipyNcjrnHTsUG7GyMCG7DTq6pCOUd59pRuZt1/BN23nhncJ6r4xsYA8rEwS490M6GYIN4NjYwL83IP9nlD84/GOX+n4uFK87EaVh8tTBmYVt1T3bft2KRZMZiQSFuJzeirr7goJE+WIy1O7GwR/1DeYe7vNV5ygPbcxB+B2OwaLjLSEVhBWNK72CM7W3XzsVYxKB3XTsCeO7ev3H/i7AxdQcWYWYg7fEF7N2nIfo0xM5I8IpEdA0ik3elCJ4iR/p9Ss59adi4G6bVrf6kgwftFIZUVVUlI96FcSOu9PV64a2k7y+h9/eaPwz8V+h/DdYJ/0PuCJY9hTNcvXdh0i2sTU3HDpu41eUL4z+wAd/dEEwCP4QR2/nk3gdwIekhk6oxafxz7Dmu66CuNdhz3E7+c+gvwDb33IKFjM48FGUq6dI1Fx7HCC7Af2J7DsIhsQNcS3qwiXxtC8JyjcK0mmsxbWwApm3EsJ2K4X0PV2H7Jx7AGOeGUFoQCivApdg7/TIW+TeMd139ade9meBHEztXgD6D0hx4sTn1vUKQc/qWRM97d+TIzhyNlBMzwfu3qv1G9ykuu8zz5GSEvgSagqKo36RJ/3crams35i2od7AW29edT1r3BASTVLXYFCFzyvQgpo5naiE9ib/Fplh3YgEHw1pNR9iAaQ7vYQdilHeQfxYmpX6YJ08U03hWYnHVs5189AZ7dvBGELL64D24NxvWY8EmqzF1/Vvu9/doH6SyDmPsA+h4qrfb7fQAxm/atMVP6IOCLlaR1OASEUSr8eTbzX36fXdp9bjxugehlmdyYDFjxn3O6+Nd5YmcI0h5+JRZsb0vv928c/ujdLM9YA9xEKaqL+rleidgMcf+SO4wPPdhDKG3NI0LsEH4FGYf6Ee6KtoRYhiTqCC72h/GKkyT+QGm/mfDFIxY/kD3a37D3Hd3RrXpi9k0YliU3mZSuxwz8RJ2qEhHfbvb7fQABPyijR+8oerfjfrrxM5nc1JdPIQxInxbi4suf33sxGmv5Y6R3Q7XVldXlta0nRWJRK6JSOR0RFOSHDtlVpBFxFr+d21jY0cB6noKZ2Pq0TxMGizFCOpHvdyOCe57TZ48H2TkDTAbm7cFn/l0Hf0wiTwHI6rlmCp6/m6W87b7HteJvLMwqZ3LABY8d3dsxa3AzhTYD5vX3kLqcJBMXEP6+w0++XA8dirQcowoJ2LaYj5htho7bDQsUPuH2nkMNrWJk5LsHWLXYZATIPZusuVxpO9EEe9bKIMRRAVRC6w4BJGvJvFOYNwB9y3W+G+3NDauKu3fP7557Vq/wVliB3KgUL0tsjNRUp4o5ci4F7nAh2ME+ikImlLXRVGUlerHr3u+rm5vxtv6mNTJMIMwRnYqNh9+MtdNPYDAOJlvJ1+Qlhko41XSVwm640Ser2DqdnhAzcbm2uNJMZ2OEGgnnQnusQG4CZt7X0/7Qy6DMjLdo6eRbjtQOtY+v+4+Aeox4nw7S97V5I4JH8ZSzNrfH1PXN2G2C7C2d7RLswFrdx9SGsvZpM4TDPKcQPo5BHmRFqnlMxs3bnl3+AF3JEq0n4fM8NHK4MBIRUSFKMJQJXKxT8mZAwYOX6boiuHjPlM7TGjy1ZOYaP9W+tWUIJNb8fdPmFTw7IxHV5G6wAvoahH/V41NTc+yd1X2Z0k3xnmYtfs+jIt2FPO8uxAYskblyRPYDTLP8ruR7jfGXYAN8O3YewCzECtGIJd1spxA1ezs+YNXY/PVmbTXHoIyMm1GK0kZoC4i3XqdC/dhUU5LsZDSl7t6X86S91Y6Z4ybjTHZ7RhjeI4Uo9tCx3afUaT2dwWa84NYBNZSbClyFvDP2PJop9AubPIBm95b9/boib/0PGKCnCmq1SrpJ9AoRNSTocDxKt4xnhL3RZMAolIEUgQasWBO7o6024mDLsP372iNtz60eOvWXAEM9hZ8jNCDZZ3HeqneNzCGNzlPnmBJqCO1savYHxtUghF7Js7DDGydYdDBYZWdbfMWjABnYtblMALN6yjS15DrMcs2GJF3htDrQuW9iBHY9Zgm90Qn25qJG8gtGJYCp2P2h2xhtkuxmAKZ72lzRjt3YKsCc7Fx2iHaqTYCeshHqz5MtiR+oZq4GnhTlJigGjqCGbA5vELUF/oq0k+RfipSomi7KK22XVUVaBXV+eLzs8SObfc9sWnT7sYV7y2M2At1NmCGmtOAA7OkF2GGqu3YkmBP4nyMKR+HHVQZ/nwPM4id0IlypmBMYQnpx2B3hOsx4r0y4/oG7PDMM0hpGd2Fm7DpyC/IEmm4G3C3KzfXqsK3MQ34ng7KuRVbi/8/sgjrbMiVSSfXrf7kqWHDZg8pqViL552h8HlEqlCKM+KydQhR9RWaUV1LUp+CxJwd69Ytv3/f2bhSSsr5oD+2pPQDLAjD8x3cO5TsAy4IaF+UI32nKz8TP8QMLo9h67nBvLMKC9QwGZvzdYcW1C9H2xox55WF2LQmE8sx9fECzBofYACmDZViKugp2FJRC7tvwNuJEfkvab+E9l1sCjEfc7CZR2o+W41pI3uChKvzboyRzAmlDSP7u9pM563fj2Fq+I8xb8HrMFtDEdanV2PjrSNCTwJXYF5zM1x78yIvNziprq7pNeqeSY7Z/70oHKWe/D0e0wStFqU4HPAhE6KqouoDTaKs8oSFool5rYn40ps2bmxg3zrZ9RLar12+AHyfjjvxevfJxET3PY7squ+9pA6BDONDLLTT71wbNmEq5RjXlgux5aruwHT3ycRTGGP57xz3xbABO4N0j7xM49xWjFiuIN21s7O4FWO4YzOuv4cx4zsxp5dm7D2VY4x3E3vmdgz23i/Dnv0Poes35sj/TfJ74WXiHEy9vxKzC32MtbkfpopnOknlwlzMseoybCzlXWrstGReAcU7ho4dKEWR/aQ4elQyIseoyEG+MkhFSoBI0g5+bGvxtbkZ/TiW1CVx9Z9tSiRejzc1fLS0oWHn/fvGOnmAYixKShjbMUtv5m78sRjRvoRJqEpSronZ8DK2Fj8gR/om8quyHjYPneLauRaTXJmMpxqb173K7kn548nd/0lMxXyF3FuVq7DnewPThmpCaY2Y2r02x70TMaNUYIQdhjl/LKG9X3yQ9xPMnTQMwTScwzDj3DbMD3wR+UOIRTC7wXrMiJerfYsw6/fUPGW9izGxCaSCc3QmfNkYTHMbjr2r5zF//jAE66cNtPeXB1v5qCG974/EiD7NMPv/mRWghBi+mvUAAAAASUVORK5CYII='
          //   });
          // },
          filename: 'BILL OF LADING REPORT',
          title: ' ',
          exportOptions: {
            columns: [0, 1, 2, 3, 4, 5, 6],
            modifier: {
              page: 'all'
            }
          }
        }
      ],
      columns: [
        {
          data: null,
          render: function (data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
          }, className: "text-center"
        },
        { data: "reqnum" },
        { data: "blnum" },
        { data: "ffname" },
        { data: "slname" },
        {
          data: "status",
          "render": function (data, type, row, meta) {
            var d = '';
            if (row.status == 'CLSE') {
              d = '<h5><span class="badge badge-danger">' + row.status_desc + '</span></h5>';
            }
            if (row.status == 'RQST') {
              d = '<h5><span class="badge badge-warning">' + row.status_desc + '</span></h5>';
            }
            if (row.status == 'BILL') {
              d = '<h5><span class="badge badge-primary">' + row.status_desc + '</span></h5>';
            }
            if (row.status == 'PNDG') {
              d = '<h5><span class="badge badge-success">' + row.status_desc + '</span></h5>';
            }
            if (row.status == 'RJCT') {
              d = '<h5><span class="badge badge-secondary">' + row.status_desc + '</span></h5>';
            }
            if (row.status == 'RJCS') {
              d = '<h5><span class="badge badge-secondary">' + row.status_desc + '</span></h5>';
            }
            if (row.status == 'PAID') {
              d = '<h5><span class="badge badge-primary">' + row.status_desc + '</span></h5>';
            }
            return d;
          }, className: "text-center"
        },
        { data: "create_date" },
        // {
        //   data: "id",
        //   "render": function (data, type, row, meta) {
        //     // var click_event = 
        //     const clicked = self.view();
        //     data = '<button onclick={'+clicked+'} type="button" class="btn btn-info btn-sm">View</button> <span></span>'
        //     // data = '<Button id="bt_view" type="button" onClick={this.view()} className="btn btn-info btn-sm">View</Button>';
        //     return data;
        //   }, className: "text-center"
        // },
      ]
    });
    

    $('#table_bol tbody').on('click', 'tr', function () {
      if ( $(this).hasClass('selected') ) {
          $(this).removeClass('selected');
      }
      else {
          $(this).addClass('selected');
      }
      var data = table.row( this ).data();
      console.log(self.props)
      self.props.history.push({pathname : "/digico_detail_bol", state: {data_bol: data}});
    });
  }
  
}

export default list_bol;
