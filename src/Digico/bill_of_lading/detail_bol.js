import React, { Component } from 'react';
import { HashRouter } from 'react-router-dom';
import * as Constants from '../../Constants.js';
import * as Helper from '../../Helper.js';
import * as axios from 'axios';
import Swal from 'sweetalert2';
import Modal from 'react-modal';
// import ModalEdit from 'react-modal';
// import { NONAME } from 'dns';

const customStyles = {
    content : {
        background          : '',
        border              : '',
        
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        width                  : '60%',
        transform             : 'translate(-50%, -50%)'
    }
  };
  

const $ = require('jquery');
$.Datatable = require('datatables.net');

class detail_bol extends Component {
  constructor(props) {
    super(props);
    this.state = {
        npwp: this.props.dataUserPortal.npwp,
        userName: null, 
        email: null, 
        companyID: null, 
        compType: null, 
        roleID: null,
        isFirstLoading: true,
        data: {},
        data_container: [],
        data_files: [],
    //   isOpenModalAdd: true,
        data_bol: this.props.location.state.data_bol,
        // data_bol: '',
        modalIsOpen: false,
        modalIsOpenEdit: false,
        id_container: '',
        delivery_id: '',
        no_container_after: '',
        no_container_before: '',
        status_do: ''
    }
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.closeModalEdit = this.closeModalEdit.bind(this);

  }

  openModal() {
    this.setState({modalIsOpen: true});
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    // this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  openModalEdit(id, delivery_id, no_container) {
    var statusDo = this.state.status_do;
    console.log('statusDo');
    console.log(statusDo);

    if(statusDo === 'PNDG'){
        console.log(id);
        console.log(delivery_id);
        console.log(no_container);
        this.setState({id_container: id, delivery_id: delivery_id, no_container_before: no_container, no_container_after: no_container});
        this.setState({modalIsOpenEdit: true});
    }
  }

  closeModalEdit() {
    this.setState({modalIsOpenEdit: false});
  }

  render() {
    const {npwp, isFirstLoading, userName} = this.state;
    if ( !isFirstLoading && (userName === null || userName === "") ) { 
      return(
        <HashRouter>
          <h1 className="page-title"> Your Account is not found in Digico Platform</h1>
          <div className="row">
            <div className="col-md-12 page-404">
              <div className="details">
                <p> You must register <a href="https://digico.id/" target="_blank">here</a> to have an access. </p>
              </div>
            </div>
          </div>
        </HashRouter>
      );
    } else if ( !isFirstLoading ) {
      return (
        <HashRouter>
          <div className="container-fluid cont-content">
              <div className="row">
                  <div className="col-sm-12">
                      <div className="text-center" style={{marginTop: '40px'}}>
                          <h6 style={{color: '#1E90FF'}}>DIGICO - BILL OF LADING</h6>    
                      </div>
                      {/* <!-- View Of Request--> */}
                      <div className="card" style={{marginTop: '20px', height: 'auto', overflow: 'hidden'}}>
                          <div className="card-header" style={{background: '#1E90FF', color: 'white'}}>
                              <i className="fa fa-file-alt"></i><strong> VIEW OF REQUEST</strong>
                          </div>
                          <div className="card-body"> 
                              <div className="row">
                                  <div className="col-sm-6">
                                      <div className="form-group row mb-3">
                                          <label htmlFor="name_company" className="col-sm-4 col-form-label">Freight Forwarder</label>
                                          <div className="col-sm-7">
                                              <input type="text" value={this.state.data.ffname == null ? '' : this.state.data.ffname} className="form-control form-control-sm" readOnly/>
                                          </div>
                                      </div>
                                      <div className="form-group row mb-3">
                                          <label htmlFor="name_company" className="col-sm-4 col-form-label">Number Request</label>
                                          <div className="col-sm-7">
                                              <input type="text" value={this.state.data.reqnum == null ? '' : this.state.data.reqnum} className="form-control form-control-sm" readOnly/>
                                          </div>
                                      </div>
                                      <div className="form-group row mb-3">
                                          <label htmlFor="name_company" className="col-sm-4 col-form-label">Number DO</label>
                                          <div className="col-sm-7">
                                              <input type="text" value={this.state.data.donum == null ? '' : this.state.data.donum} className="form-control form-control-sm" readOnly/>
                                          </div>
                                      </div>
                                  </div>
                                  <div className="col-sm-6">
                                      <div className="form-group row mb-3">
                                          <label htmlFor="name_company" className="col-sm-4 col-form-label">Shipping Line</label>
                                          <div className="col-sm-7">
                                              <input type="text" value={this.state.data.slname == null ? '' : this.state.data.slname} className="form-control form-control-sm" readOnly/>
                                          </div>
                                      </div>
                                      <div className="form-group row mb-3">
                                          <label htmlFor="name_company" className="col-sm-4 col-form-label">Number BL</label>
                                          <div className="col-sm-7">
                                              <input type="text" value={this.state.data.blnum == null ? '' : this.state.data.blnum} className="form-control form-control-sm" readOnly/>
                                          </div>
                                      </div>
                                      <div className="form-group row mb-3">
                                          <label htmlFor="name_company" className="col-sm-4 col-form-label">Expired DO</label>
                                          <div className="col-sm-7">
                                              <input type="text" value={this.state.data.duedate == null ? '' : this.state.data.duedate} className="form-control form-control-sm" readOnly/>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      {/* <!-- View Of Detail Container--> */}
                      <div className="card" style={{marginTop: '20px', height: 'auto', overflow: 'hidden'}}>
                          <div className="card-header" style={{background: '#1E90FF', color: 'white'}}>
                              <i className="fa fa-file-alt"></i><strong> VIEW OF DETAIL CONTAINER</strong>
                          </div>
                          <div className="card-body"> 
                              <div className="row mb-3">
                                  {/* <button type="button" className="btn btn-success btn-sm" onClick="add('<%=data[0].req_sl_delivery_id%>')">Add</button> */}
                                  <button id="add_containers" name="add_containers" type="button" className="btn btn-success btn-sm" onClick={this.openModal}>Add</button>
                                  <button style={{marginLeft: '10px'}} type="button" className="btn btn-primary btn-sm" onClick={this.backPage.bind(this)}>Back</button>
                                  {/* <a className="btn btn-primary btn-sm ml-2" href="/dopayment/request/billoflading">Back</a> */}
                              </div>
                              <div className="row">
                                  <table id="detail_container_del" className="table table-striped table-bordered" cellSpacing="0">
                                      <thead>
                                          <tr>
                                              <th>No Container</th>
                                              <th>Tools</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                      {this.state.data_container.map((element, index) => (
                                          <tr key={index}>
                                              <td>{element.no_container}</td>
                                              {/* <td><button type="button" className="btn btn-warning btn-sm" onClick="edit('<%=data_container[i].id%>', '<%=data_container[i].delivery_id%>','<%=data_container[i].no_container%>')">Edit</button> <button type="button" className="btn btn-danger btn-sm" onClick="delete_cont('<%=data_container[i].id%>')">Delete</button></td> */}
                                              <td>
                                                  <button id="edit_container" type="button" className="btn btn-warning btn-sm" onClick={this.openModalEdit.bind(this, element.id, element.delivery_id, element.no_container)}>Edit</button> <button id="delete_container" type="button" className="btn btn-danger btn-sm" onClick={this.delete_cont.bind(this, element.id)}>Delete</button>
                                              </td>
                                          </tr>
                                      ))}
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                      {/* <!-- View Of Detail Files--> */}
                      <div className="card" style={{marginTop: '20px', height: 'auto', overflow: 'hidden'}}>
                          <div className="card-header" style={{background: '#1E90FF', color: 'white'}}>
                              <i className="fa fa-file-alt"></i><strong> VIEW OF DETAIL FILES</strong>
                          </div>
                          <div className="card-body"> 
                              <table id="detail_files" className="table table-striped table-bordered" cellSpacing="0" width="100%">
                                  <thead>
                                      <tr>
                                          <th>File Name</th>
                                          <th>Tools</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      {this.state.data_files.map((element, index) => (
                                          <tr key={index}>
                                              <td>{element.file_name}</td>
                                              <td><a className="btn btn-danger btn-sm" href={Constants.DOWNLOAD_FILE + element.file_name} target="_blank" download>Download</a></td>
                                          </tr>
                                      ))}
                                  </tbody>
                              </table>  
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            style={customStyles}
            overlayClassName="Overlay"
            contentLabel="Example Modal">

            <div className="modal-dialog modal-dialog-centered" role="document">
                  <div className="modal-content">
                      <div className="modal-header">
                      <h5 className="modal-title" id="exampleModalLongTitle">Add Container</h5>
                      <button id="addContainer" onClick={this.closeModal} type="button" className="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      </div>
                      <div className="modal-body">
                          <form action='' method="POST" id="add_container" encType="multipart/form-data">
                              <input type="hidden" name="check_api" id="check_api" value="get_api"/>
                              <input type="hidden" name="id_req" id="id_req"/>
                              <input type="hidden" name="username" id="username" value={this.state.userName}/>
                              <div id="div-container-req">
                                  <div className="form-group row">
                                      <label htmlFor="ForContainer" className="col-sm-4 col-form-label">No. Container</label>
                                      <div className="col-sm-6">
                                          <input type="text" name="cont_no" className="form-control col-form-label-sm" id="no_container" placeholder="No Container"/>
                                      </div>
                                      <div className="col-sm-1">
                                          <button onClick={this.add_container_req.bind(this)} type="button" className="btn btn-primary btn-sm">Add</button>
                                      </div>
                                      <div className="col-sm-2">
                                          <input type="hidden" id="count_req" name="count_req" value="1"/>
                                      </div>
                                  </div>
                              </div>
                              <div className="modal-footer">
                                  <button type="button" className="btn btn-primary" onClick={this.SubmitAdd.bind(this)} >Submit</button>
                                  <button type="button" onClick={this.closeModal} className="btn btn-secondary" data-dismiss="modal">Close</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </Modal>

          <Modal
            isOpen={this.state.modalIsOpenEdit}
            onRequestClose={this.closeModalEdit}
            style={customStyles}
            overlayClassName="Overlay"
            contentLabel="Example Modal">

              <div className="modal-dialog modal-dialog-centered" role="document">
                  <div className="modal-content">
                      <div className="modal-header">
                          <h5 className="modal-title" id="exampleModalLongTitle">Edit Container</h5>
                          <button onClick={this.closeModalEdit} type="button" className="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div className="modal-body">
                          <form action='' method="POST" id="edit_container">
                              <div id="div-container-req">
                                  <div className="form-group row">
                                      <label htmlFor="ForContainer" className="col-sm-4 col-form-label">No. Container</label>
                                      <div className="col-sm-6">
                                          <input type="text" className="form-control col-form-label-sm" onInput={this.onFieldChange('no_container_after').bind(this)} value={this.state.no_container_after}/>
                                      </div>
                                  </div>
                              </div>
                              <div className="modal-footer">
                                  <button onClick={this.submit_edit.bind(this)} type="button" className="btn btn-primary" >Submit</button>
                                  <button onClick={this.closeModalEdit} type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </Modal>
        </HashRouter>
      );
    } else {
        return (<HashRouter>Loading ...</HashRouter>);
    }
  }
  getUserDigico() {
    const encodedNpwp = encodeURIComponent(Constants.ENC_ID);
    const decryptorURI = Constants.API_USER_DIGICO;
    fetch(`${decryptorURI}${encodedNpwp}`)
      .then((res) => {
        if ( res.status === 200 )
          return res.json();
        else
          this.setState({
            isFirstLoading: false,
            userName: null,
            email: null,
            companyID: null,
            compType: null,
            roleID: null
          })
      })
      .then( (data) => {
        // this.setState({
        //   isFirstLoading: false,
        //   userName: 'User_ff19',//data.digicoUser.userName,
        //   email: 'chalid.harvey@gmail.com',//data.digicoUser.email,
        //   companyID: '1208',//data.digicoUser.companyID,
        //   compType: 'Admin',//data.digicoUser.compType,
        //   roleID: null,//data.digicoUser.roleID 
        // }, () => {
        //   this.get_data_detail();
        // })
        this.setState({
          isFirstLoading: false,
          userName: data.digicoUser.userName,
          email: data.digicoUser.email,
          companyID: data.digicoUser.companyID,
          compType: data.digicoUser.compType,
          roleID: data.digicoUser.roleID 
        }, () => {
          this.get_data_detail();
        })
      })
      .catch(error => {
        this.setState({
          isFirstLoading: false,
          userName: null,
          email: null,
          companyID: null,
          compType: null,
          roleID: null
        })
      });
  }
  componentDidMount() {
    const {npwp, userName} = this.state;
    if ( typeof this.props.dataUserPortal.digicoUser !== "undefined" ) {
      this.setState({
        isFirstLoading: false,
        userName: this.props.dataUserPortal.digicoUser.userName,
        email: this.props.dataUserPortal.digicoUser.email,
        companyID: this.props.dataUserPortal.digicoUser.companyID,
        compType: this.props.dataUserPortal.digicoUser.compType,
        roleID: this.props.dataUserPortal.digicoUser.roleID 
      }, () => {
        this.get_data_detail();
      })
    } else {
      this.getUserDigico();
    }
  }

  get_data_detail() {
    const self = this;
    let header_config = {
        headers: {
          "Content-Type": "application/json",
        }
    };

    var url = Constants.VIEW_DETAIL_BOL;
    console.log(url);

    axios.post(url, 
        {
            id: this.state.data_bol.id,
            // id: '40',
            check_api: 'get_api',
        },
        header_config
    )
    .then(result => {
        console.log(result);
        if (result.data.status == false) {
            var message = result.data.message;
            Swal.fire(message);
        } else {
            console.log(result.data.data);
            console.log(result.data.data_container);
            console.log(result.data.data_files);

            self.setState({data: result.data.data[0], data_container: result.data.data_container, data_files: result.data.data_files})

            const $ = require('jquery');

            var statusDo = result.data.data[0].status_do;
            console.log('statusDo');
            console.log(statusDo);
            this.setState({status_do: statusDo});

            if(statusDo === 'PNDG'){
                $('#add_containers').prop('disabled', false);
            } else if(statusDo === 'RJCT'){
                $('#add_containers').prop('disabled', true);
            } else if(statusDo === 'RQST'){
                $('#add_containers').prop('disabled', true);
            } else if(statusDo === 'RJCS'){
                $('#add_containers').prop('disabled', true);
            } else if(statusDo === 'CLSE'){
                $('#add_containers').prop('disabled', true);
            } else if(statusDo === 'BILL'){
                $('#add_containers').prop('disabled', true);;
            } else if(statusDo === 'PAID'){
                $('#add_containers').prop('disabled', true);;
            }
        }
    })
    .catch(error => {
        console.log(error);
        Swal.fire(error);
    });
  }

  add_container_req(){
        const self = this;
        console.log('add container');
        const $ = require('jquery')
        var count = parseInt($('#count_req').val());
        var input_cont = '<div class="form-group row req" id="div-row-cont-'+count+'">'+
                '<label for="ForContainer" class="col-sm-4 col-form-label">No. Container</label>'+
                '<div class="col-sm-6">'+
                    '<input type="text" name="cont_no'+count+'" class="form-control col-form-label-sm" id="no_container_add-'+count+'" placeholder="No Container">'+
                '</div>'+
                '<div class="col-sm-1">'+
                    '<button id="btnDeleteContainer_'+count+'" type="button" class="btn btn-danger btn-sm">Delete</button>'+
                '</div>'+
                '</div>';
        $('#div-container-req').append(input_cont);
        $('#count_req').val(count+1);

        $( "#btnDeleteContainer_"+count).click(function() {
            self.removeRow_container_req(count);
        });
  }

  removeRow_container_req(id){
        console.log('remove click ' + id);
        const $ = require('jquery')
        $('#div-row-cont-'+id).remove();
        var cd = parseInt($('#count_req').val());
        $('#count_req').val(cd-1);
}

  SubmitAdd(){
    console.log('submit container');
    const self = this;
    const $ = require('jquery')
    $('#id_req').val(this.state.data.req_sl_delivery_id);

    var url = Constants.POST_ADD_CONTAINER;
    console.log(url);

    Swal.fire({
        title: "Add Container",
        text: "Submit to Add Container",
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }).then((result) => {
        if (result.value) {
            axios.post(url, $( "#add_container" ).serialize())
            .then(result => {
                console.log(result);
                if (result.data.status == false) {
                    var message = result.data.message;
                    Swal.fire(message);
                } else {
                    
                    Swal.fire({title: "Done", text: "'"+result.data.message+"'", type: "success"})
                    .then((result) => {
                        self.closeModal();
                        self.get_data_detail()
                    });
                }
            })
            .catch(error => {
                console.log(error);
                Swal.fire(error);
                self.closeModal();
            });   
        }
    });
  }

  submit_edit(){
        const self = this;
        let input_data = {
            id: this.state.id_container,
            delivery_id: this.state.delivery_id,
            container_before: this.state.no_container_before,
            container_after: this.state.no_container_after,
            check_api: 'get_api',
            username: this.state.userName
        }
        console.log(input_data)

        var url = Constants.POST_EDIT_CONTAINER;
        console.log(url);

        Swal.fire({
            title: "Edit Container Number",
            text: "Submit to Edit Container Number",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }).then((result) => {
            if (result.value) {
                let header_config = {
                    headers: {
                    "Content-Type": "application/json",
                    }
                };
                axios.post(url, input_data, header_config).then(result => {
                    console.log(result);
                    if (result.data.status == 'F') {
                        var message = result.data.message;
                        Swal.fire(message);
                    } else {
                        Swal.fire({title: "Done", text: "'"+result.data.message+"'", type: "success"})
                        .then((result) => {
                            self.closeModalEdit();
                            self.get_data_detail()
                        });
                    }
                }).catch(error => {
                    console.log(error);
                    Swal.fire(error);
                    self.closeModalEdit();
                });
            }
        });
    }

    onFieldChange(fieldName){
        return function (event) {
            const target = event.target;
            this.setState({[fieldName]: event.target.value});
        }
    }

    backPage() {
        this.props.history.goBack();
    }

    delete_cont(id) {
        var statusDo = this.state.status_do;
        console.log('statusDo');
        console.log(statusDo);

        if(statusDo === 'PNDG'){
            const self = this;
            Swal.fire({
                title: "Delete Container",
                text: "Submit to Delete Container",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }).then((result) => {
                if (result.value) {
                    let header_config = {
                        headers: {
                        "Content-Type": "application/json",
                        }
                    };

                    var url = Constants.DELETE_CONTAINER;
                    axios.post(url, {
                        id: id,
                        check_api: 'get_api',
                    }, header_config)
                    .then(result => {
                        console.log(result);
                        if (result.data.status == false) {
                            var message = result.data.message;
                            Swal.fire(message);
                        } else {                        
                            Swal.fire({title: "Done", text: "'"+result.data.message+"'", type: "success"});
                            self.get_data_detail()
                        }
                    })
                    .catch(error => {
                        console.log(error);
                        Swal.fire(error);
                    });   
                }
            });
        }
    }

}
export default detail_bol;
