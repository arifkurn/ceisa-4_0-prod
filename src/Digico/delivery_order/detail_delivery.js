import React, { Component } from 'react';
import { HashRouter } from 'react-router-dom';
import * as Constants from '../../Constants.js';
import * as Helper from '../../Helper.js';
import * as axios from 'axios';
import Swal from 'sweetalert2';
import Modal from 'react-modal';
import Select from 'react-select';

const customStyles = {
    content : {
        background          : '',
        border              : '',
        
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        width                  : '60%',
        transform             : 'translate(-50%, -50%)'
    }
  };
  

const $ = require('jquery');
$.Datatable = require('datatables.net');

class detail_delivery extends Component {
  constructor(props) {
    super(props);
    this.state = {
        npwp: this.props.dataUserPortal.npwp,
        userName: null, 
        email: null, 
        companyID: null, 
        compType: null, 
        roleID: null,
        isFirstLoading: true,
        id : '',
        blnum: '',
        name_company: '',
        slname: '',
        id_do: '',
        status: '',
        amount: '',
        amount2: '',
        transfee: '',
        note_request: '',
        fileproformainv: '',
        fileinv: '',
        filedo: '',
        ffname: '',
        donum: '',
        due_date: '',
        total: '',
        refid: '',
        vessel: '',
        voyage: '',
        pkk: '',
        reqnum: '',
        service_type: '',
        no_proforma: '',
        id_consigne: '',
        data_delivery: this.props.location.state.data_delivery,
        data_container: [],
        data_files: [],
        modalIsOpen: false,
        modalIsOpenEdit: false,
        modalIsOpenReject: false,
        no_container_after: '',
        no_container_before: '',
        is_method_payment: false,
        id_payment_method: '',
        tag: this.props.location.state.data_delivery.tag,
        option_payment : [ 
            {
                value: '4',
                label: 'Credit'
            },
            {
                value: '2',
                label: 'Direct Payment'
            },
        ]
    }
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.closeModalEdit = this.closeModalEdit.bind(this);

    this.openModalReject = this.openModalReject.bind(this);
    this.closeModalReject = this.closeModalReject.bind(this);
  }

  openModal() {
    this.setState({modalIsOpen: true});
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    // this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  openModalEdit(id, delivery_id, no_container) {
    if(this.state.status == 'PNDG' || this.state.status == 'RQST') {
        console.log(id);
        console.log(delivery_id);
        console.log(no_container);
        this.setState({id_container: id, delivery_id: delivery_id, no_container_before: no_container, no_container_after: no_container});
        this.setState({modalIsOpenEdit: true});
    }
  }

  closeModalEdit() {
    this.setState({modalIsOpenEdit: false});
  }

  openModalReject() {
    this.setState({modalIsOpenReject: true});
  }

  closeModalReject() {
    this.setState({modalIsOpenReject: false});
  }

  render() {
    const {npwp, isFirstLoading, userName} = this.state;
    if ( !isFirstLoading && (userName === null || userName === "") ) { 
      return(
        <HashRouter>
          <h1 className="page-title"> Your Account is not found in Digico Platform</h1>
          <div className="row">
            <div className="col-md-12 page-404">
              <div className="details">
                <p> You must register <a href="https://digico.id/" target="_blank">here</a> to have an access. </p>
              </div>
            </div>
          </div>
        </HashRouter>
      );
    } else if ( !isFirstLoading ) {
      return (
        <HashRouter>
            <div className="container-fluid cont-content">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="text-center" style={{marginTop: '40px'}}>
                            <h6 style={{color: '#1E90FF'}}>DELIVERY ORDER</h6>    
                        </div>
                        <button type="button" className="btn btn-danger btn-sm" id="rejectDO" onClick={this.openModalReject}>
                            Reject</button>
                        <button type="button" className="btn btn-info btn-sm ml-1" id="approveDO" onClick={this.Approve.bind(this)}>
                            Approve</button>
                        <button type="button" className="btn btn-info btn-sm ml-1" id="payment_button" onClick={this.payment.bind(this)}>
                            Payment</button>
                        {/* <a id="payment_button" href="#" className="btn btn-info btn-sm ml-1" onClick={this.payment.bind(this)}>Payment</a> */}
                        
                        {/* View Of Request */}
                        <div className="card" style={{marginTop: '20px', height: 'auto', overflow: 'hidden'}}>
                            <div className="card-header" style={{background: '#1E90FF', color: 'white'}}>
                                <i className="fa fa-file-alt"></i><strong> VIEW OF REQUEST</strong>
                            </div>
                            <div className="card-body"> 
                                <div className="row">
                                    <div className="col-sm-6">
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">Shipping Line</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="shipping_line" name="shipping_line" value={this.state.slname == null ? '-' : this.state.slname} readOnly/>
                                                <input type="hidden" className="form-control form-control-sm" id="id_do" name="id_do" value={this.state.id_do == null ? '' : this.state.id_do} readOnly/>
                                                <input type="hidden" className="form-control form-control-sm" id="status_do" name="status_do" value={this.state.status == null ? '' : this.state.status} readOnly/>
                                                <input type="hidden" className="form-control form-control-sm" id="id_request" name="id_request" value={this.state.id == null ? '' : this.state.id} readOnly/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">Nomor Request</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="no_req" name="no_req" value={this.state.reqnum == null ? '-' : this.state.reqnum} readOnly/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">Payment Method *</label>
                                            <div className="col-sm-7">
                                                <Select
                                                    name="form-field-name"
                                                    options={this.state.option_payment}
                                                    defaultValue={this.state.tag == '4' ? this.state.option_payment[0] : this.state.tag == "2" ? this.state.option_payment[1] : ''}
                                                    isDisabled={this.state.is_method_payment}
                                                    onChange={this.OnChangePayment.bind(this)}
                                                    placeholder='-- Choose Payment --'
                                                />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">Amount</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="Amount" name="Amount" value={this.state.amount == null ? '-' : this.state.amount} readOnly/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">Warranty Amount</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="warranty_amount" name="warranty_amount" value={this.state.amount2 == null ? '-' : this.state.amount2} readOnly/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">Transaction Fee</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="transaction_fee" name="transaction_fee" value={this.state.transfee == null ? '-' : this.state.transfee} readOnly/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">Note Request</label>
                                            <div className="col-sm-7">
                                            <textarea className="form-control form-control-sm" id="nota_req" name="nota_req" rows="5" onInput={this.onFieldChange('note_request').bind(this)} defaultValue={this.state.note_request == null ? '' : this.state.note_request}></textarea>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3" id='view_proforma'>
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">File Proforma Invoice</label>
                                            <div className="col-sm-5">
                                                <input type="text" className="form-control form-control-sm" id="file_pInvoice" name="file_pInvoice" value={this.state.fileproformainv == null ? '-' : this.state.fileproformainv} autoComplete="off" readOnly/>
                                            </div>
                                            <div className="col-sm-3">
                                                <a href={this.state.fileproformainv == null || this.state.fileproformainv == '' ? '#' : Constants.DOWNLOAD_FILE + this.state.fileproformainv} className="btn btn-primary btn-sm" target="_blank">Download</a>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3" id='view_invoice'>
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">File  Invoice</label>
                                            <div className="col-sm-5">
                                                <input type="text" className="form-control form-control-sm" id="file_invoice" name="file_invoice" value={this.state.fileinv == null ? '-' : this.state.fileinv} readOnly/>
                                            </div>
                                            <div className="col-sm-3">
                                                <a href={this.state.fileinv == null || this.state.fileinv == '' ? '#' : Constants.DOWNLOAD_FILE + this.state.fileinv} className="btn btn-primary btn-sm" target="_blank">Download</a>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3" id='view_DO'>
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">File DO</label>
                                            <div className="col-sm-5">
                                                <input type="text" className="form-control form-control-sm" id="file_do" name="file_do" value={this.state.filedo == null ? '-' : this.state.filedo}  readOnly/>
                                            </div>
                                            <div className="col-sm-3">
                                                <a href={this.state.filedo == null || this.state.filedo == '' ? '#' : Constants.DOWNLOAD_FILE + this.state.filedo} className="btn btn-primary btn-sm" target="_blank">Download</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">Cargo Owner</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="cargo_owner" value={this.state.ffname == null ? '-' : this.state.ffname} name="cargo_owner" readOnly/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">No BL</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="no_bl" name="no_bl" value={this.state.blnum == null ? '-' : this.state.blnum} readOnly/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">No DO</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="no_do" name="no_do" value={this.state.donum == null ? '-' : this.state.donum} readOnly/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">Expired DO</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="exp_do" name="exp_do" value={this.state.due_date == null ? '-' : this.state.due_date} readOnly/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">Total</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="total" name="total" value={this.state.total == null ? '-' : this.state.total} readOnly/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">Reference ID</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="ref_id" name="ref_id" value={this.state.refid == null ? '-' : this.state.refid} readOnly/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">Vessel</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="vessel" name="vessel" value={this.state.vessel == null ? '-' : this.state.vessel} readOnly/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">Voyage</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="voyage" name="voyage" value={this.state.voyage == null ? '-' : this.state.voyage} readOnly/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label htmlFor="name_company" className="col-sm-4 col-form-label">PKK</label>
                                            <div className="col-sm-7">
                                                <input type="text" className="form-control form-control-sm" id="pkk" name="pkk" value={this.state.pkk == null ? '-' : this.state.pkk} readOnly/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* <!-- View Of Detail Container--> */}
                        <div className="card" style={{marginTop: '20px', height: 'auto', overflow: 'hidden'}}>
                            <div className="card-header" style={{background: '#1E90FF', color: 'white'}}>
                                <i className="fa fa-file-alt"></i><strong> VIEW OF DETAIL CONTAINER</strong>
                            </div>
                            <div className="card-body"> 
                                <div className="row mb-3">
                                    {/* <button type="button" className="btn btn-success btn-sm" onClick="add('<%=data[0].req_sl_delivery_id%>')">Add</button> */}
                                    <button id="addContainer" type="button" className="btn btn-success btn-sm" onClick={this.openModal}>Add</button>
                                    <button style={{marginLeft: '10px'}} type="button" className="btn btn-primary btn-sm" onClick={this.backPage.bind(this)}>Back</button>
                                    {/* <a className="btn btn-primary btn-sm ml-2" href="/dopayment/request/billoflading">Back</a> */}
                                </div>
                                <div className="row">
                                    <table id="detail_container_del" className="table table-striped table-bordered" cellSpacing="0">
                                        <thead>
                                            <tr>
                                                <th>No Container</th>
                                                <th>Tools</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {this.state.data_container.map((element, index) => (
                                            <tr key={index}>
                                                <td>{element.no_container}</td>
                                                {/* <td><button type="button" className="btn btn-warning btn-sm" onClick="edit('<%=data_container[i].id%>', '<%=data_container[i].delivery_id%>','<%=data_container[i].no_container%>')">Edit</button> <button type="button" className="btn btn-danger btn-sm" onClick="delete_cont('<%=data_container[i].id%>')">Delete</button></td> */}
                                                <td>
                                                    <button type="button" id="edit_container" className="btn btn-warning btn-sm" onClick={this.openModalEdit.bind(this, element.id, element.delivery_id, element.no_container)}>Edit</button> <button id="delete_container" type="button" className="btn btn-danger btn-sm" onClick={this.delete_cont.bind(this, element.id)}>Delete</button>
                                                </td>
                                            </tr>
                                        ))}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        {/* <!-- View Of Detail Files--> */}
                        <div className="card" style={{marginTop: '20px', height: 'auto', overflow: 'hidden'}}>
                            <div className="card-header" style={{background: '#1E90FF', color: 'white'}}>
                                <i className="fa fa-file-alt"></i><strong> VIEW OF DETAIL FILES</strong>
                            </div>
                            <div className="card-body"> 
                                <table id="detail_files" className="table table-striped table-bordered" cellSpacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>File Name</th>
                                            <th>Tools</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.data_files.map((element, index) => (
                                            <tr key={index}>
                                                <td>{element.file_name}</td>
                                                <td><a className="btn btn-danger btn-sm" href={Constants.DOWNLOAD_FILE + element.file_name} target="_blank" download>Download</a></td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>

          <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            style={customStyles}
            overlayClassName="Overlay"
            contentLabel="Example Modal">

            <div className="modal-dialog modal-dialog-centered" role="document">
                  <div className="modal-content">
                      <div className="modal-header">
                      <h5 className="modal-title" id="exampleModalLongTitle">Add Container</h5>
                      <button onClick={this.closeModal} type="button" className="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      </div>
                      <div className="modal-body">
                          <form action='' method="POST" id="add_container" encType="multipart/form-data">
                              <input type="hidden" name="check_api" id="check_api" value="get_api"/>
                              <input type="hidden" name="id_req" id="id_req"/>
                              <input type="hidden" name="username" id="username" value={this.state.userName}/>
                              <div id="div-container-req">
                                  <div className="form-group row">
                                      <label htmlFor="ForContainer" className="col-sm-4 col-form-label">No. Container</label>
                                      <div className="col-sm-6">
                                          <input type="text" name="cont_no" className="form-control col-form-label-sm" id="no_container" placeholder="No Container"/>
                                      </div>
                                      <div className="col-sm-1">
                                          <button onClick={this.add_container_req.bind(this)} type="button" className="btn btn-primary btn-sm">Add</button>
                                      </div>
                                      <div className="col-sm-2">
                                          <input type="hidden" id="count_req" name="count_req" value="1"/>
                                      </div>
                                  </div>
                              </div>
                              <div className="modal-footer">
                                  <button type="button" className="btn btn-primary" onClick={this.SubmitAdd.bind(this)} >Submit</button>
                                  <button type="button" onClick={this.closeModal} className="btn btn-secondary" data-dismiss="modal">Close</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </Modal>

          <Modal
            isOpen={this.state.modalIsOpenEdit}
            onRequestClose={this.closeModalEdit}
            style={customStyles}
            overlayClassName="Overlay"
            contentLabel="Example Modal">

              <div className="modal-dialog modal-dialog-centered" role="document">
                  <div className="modal-content">
                      <div className="modal-header">
                          <h5 className="modal-title" id="exampleModalLongTitle">Edit Container</h5>
                          <button onClick={this.closeModalEdit} type="button" className="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div className="modal-body">
                          <form action='' method="POST" id="edit_container">
                              <div id="div-container-req">
                                  <div className="form-group row">
                                      <label htmlFor="ForContainer" className="col-sm-4 col-form-label">No. Container</label>
                                      <div className="col-sm-6">
                                          <input type="text" className="form-control col-form-label-sm" onInput={this.onFieldChange('no_container_after').bind(this)} value={this.state.no_container_after}/>
                                      </div>
                                  </div>
                              </div>
                              <div className="modal-footer">
                                  <button onClick={this.submit_edit.bind(this)} type="button" className="btn btn-primary" >Submit</button>
                                  <button onClick={this.closeModalEdit} type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </Modal>

          <Modal
              isOpen={this.state.modalIsOpenReject}
              onRequestClose={this.closeModalReject}
              style={customStyles}
              overlayClassName="Overlay"
              contentLabel="Example Modal">

              <div className="modal-dialog modal-dialog-centered" role="document">
                  <div className="modal-content">
                      <div className="modal-header">
                          <h5 className="modal-title" id="exampleModalLongTitle">Reject Request DO</h5>
                          <button onClick={this.closeModalReject} type="button" className="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div className="modal-body">
                          <p style={{fontSize: '15px'}}>Anda yakin akan melakukan "REJECT" pada request DO tersebut ?</p>
                          <form action='' method="POST" id="edt_rjt">
                              <input type="hidden" name="id_rjtcont_edit" id="id_rjtcont_edit" value={this.state.id} readOnly/>
                              <div id="div-reject-req">
                                  <div className="form-group row">
                                      <label htmlFor="" className="col-sm-4 col-form-label">BL Number</label>
                                      <div className="col-sm-8">
                                          <input type="text" className="form-control form-control-sm" value={this.state.blnum} name="rjtblnumber" id="rjtblnumber" readOnly />
                                      </div>
                                  </div>

                                  <div className="form-group row">
                                      <label htmlFor="" className="col-sm-4 col-form-label">Cargo Owner</label>
                                      <div className="col-sm-8">
                                          <input type="text" value={this.state.name_company} className="form-control form-control-sm" name="rjtcargonum" id="rjtcargonum" readOnly />
                                      </div>
                                  </div>

                                  <div className="form-group row mb-3">
                                      <label htmlFor="" className="col-sm-4 col-form-label">Shipping Line</label>
                                      <div className="col-sm-8">
                                          <input type="text" value={this.state.slname} className="form-control form-control-sm" name="rjtsl" id="rjtsl" readOnly />
                                      </div>
                                  </div>

                                  <p style={{fontSize: '15px'}}>Silahkan isi alasan REJECT dan klik "Yes" untuk melanjutkan proses atau "No" untuk cancel.</p>

                                  <div className="form-group row mb-3">
                                      <label htmlFor="" className="col-sm-4 col-form-label">Keterangan Reject</label>
                                      <div className="col-sm-8">
                                          <input type="text" className="form-control form-control-sm" name="ketrjt" id="ketrjt" />
                                      </div>
                                  </div>
                              </div>
                              <div className="modal-footer">
                                  <button type="button" className="btn btn-primary btn-sm" onClick={this.Reject.bind(this)} >Yes</button>
                                  <button onClick={this.closeModalReject} type="button" className="btn btn-danger btn-sm" data-dismiss="modal">No</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </Modal>
        </HashRouter>
      );
    } else {
        return (<HashRouter>Loading ...</HashRouter>);
    }
  }
  getUserDigico() {
    const encodedNpwp = encodeURIComponent(Constants.ENC_ID);
    const decryptorURI = Constants.API_USER_DIGICO;
    fetch(`${decryptorURI}${encodedNpwp}`)
      .then((res) => {
        if ( res.status === 200 )
          return res.json();
        else
          this.setState({
            isFirstLoading: false,
            userName: null,
            email: null,
            companyID: null,
            compType: null,
            roleID: null
          })
      })
      .then( (data) => {
        this.setState({
          isFirstLoading: false,
          userName: data.digicoUser.userName,
          email: data.digicoUser.email,
          companyID: data.digicoUser.companyID,
          compType: data.digicoUser.compType,
          roleID: data.digicoUser.roleID 
        }, () => {
          this.get_data_detail();
        })
      })
      .catch(error => {
        this.setState({
          isFirstLoading: false,
          userName: null,
          email: null,
          companyID: null,
          compType: null,
          roleID: null
        })
      });
  }
  componentDidMount() {
    const {npwp, userName} = this.state;
    if ( typeof this.props.dataUserPortal.digicoUser !== "undefined" ) {
      this.setState({
        isFirstLoading: false,
        userName: this.props.dataUserPortal.digicoUser.userName,
        email: this.props.dataUserPortal.digicoUser.email,
        companyID: this.props.dataUserPortal.digicoUser.companyID,
        compType: this.props.dataUserPortal.digicoUser.compType,
        roleID: this.props.dataUserPortal.digicoUser.roleID 
      }, () => {
        this.get_data_detail();
      })
    } else {
      this.getUserDigico();
    }
  }

  get_data_detail() {
    const self = this;

    Swal.fire({
        title: 'Please Wait ..',
        onBeforeOpen: () => {
            Swal.showLoading()
        }
    });

    let header_config = {
        headers: {
          "Content-Type": "application/json",
        }
    };

    var url = Constants.DETAIL_DELIVERY;
    console.log(url);
    console.log(this.state.data_delivery);
    console.log(this.state.data_delivery.id_req);

    axios.post(url, 
        {
            id: this.state.data_delivery.id_req,
            check_api: 'get_api',
        },
        header_config
    )
    .then(result => {
        console.log(result);
        Swal.close();
        if (result.data.status == false) {
            var message = result.data.message;
            Swal.fire(message);
        } else {
            console.log(result.data);
            console.log(result.data.data);
            console.log(result.data.data_container);
            console.log(result.data.data_files);

            self.setState({
                id : result.data.data[0].id,
                blnum: result.data.data[0].blnum,
                name_company: result.data.data[0].name_company,
                slname: result.data.data[0].slname,
                id_do: result.data.data[0].id_do,
                status: result.data.data[0].status,
                amount: result.data.data[0].amount,
                amount2: result.data.data[0].amount2,
                transfee: result.data.data[0].transfee,
                note_request: result.data.data[0].note_request,
                fileproformainv: result.data.data[0].fileproformainv,
                fileinv: result.data.data[0].fileinv,
                filedo: result.data.data[0].filedo,
                ffname: result.data.data[0].ffname,
                donum: result.data.data[0].donum,
                due_date: result.data.data[0].due_date,
                total: result.data.data[0].total,
                refid: result.data.data[0].refid,
                vessel: result.data.data[0].vessel,
                voyage: result.data.data[0].voyage,
                pkk: result.data.data[0].pkk,
                reqnum: result.data.data[0].reqnum,
                service_type: result.data.data[0].service_type,
                no_proforma: result.data.data[0].no_proforma,
                id_consigne: result.data.data[0].id_consigne,
                data_container: result.data.data_container, 
                data_files: result.data.data_files,
            });

            const $ = require('jquery');

            var statusDo = $('#status_do').val();
            console.log('statusDo');
            console.log(statusDo);

            // $('#Amount').inputmask({
            //     'alias': 'decimal',
            //     rightAlign: true,
            //     'groupSeparator': '.',
            //     'autoGroup': true
            // });
            // $('warranty_amount').inputmask({
            //     'alias': 'decimal',
            //     rightAlign: true,
            //     'groupSeparator': '.',
            //     'autoGroup': true
            // });
            // $('#total').inputmask({
            //     'alias': 'decimal',
            //     rightAlign: true,
            //     'groupSeparator': '.',
            //     'autoGroup': true
            // }); 

            if(statusDo === 'PNDG'){
                $('#addContainer').prop('disabled', false);
                $('#rejectDO').show();
                $('#approveDO').show();
                $('#payment_button').hide();
                $('#view_proforma').hide();
                $('#view_invoice').hide();
                $('#view_DO').hide();
            }else if(statusDo === 'RJCT'){
                $('#addContainer').prop('disabled', true);
                $('#rejectDO').hide();
                $('#approveDO').hide();
                $('#payment_button').hide();
                document.getElementById("nota_req").readOnly = true;
                $('#view_proforma').hide();
                $('#view_invoice').hide();
                $('#view_DO').hide();
                self.setState({is_method_payment: true});
            }else if(statusDo === 'RQST'){
                // $('#addContainer').prop('disabled', true);
                $('#addContainer').prop('disabled', false);
                $('#rejectDO').hide();
                $('#approveDO').hide();
                $('#payment_button').hide();
                document.getElementById("nota_req").readOnly = true;
                self.setState({is_method_payment: true});
                $('#view_proforma').hide();
                $('#view_invoice').hide();
                $('#view_DO').hide();
            }else if(statusDo === 'RJCS'){
                $('#addContainer').prop('disabled', true);
                $('#rejectDO').hide();
                $('#approveDO').hide();
                $('#payment_button').hide();
                document.getElementById("nota_req").readOnly = true;
                self.setState({is_method_payment: true});
                $('#view_proforma').hide();
                $('#view_invoice').hide();
                $('#view_DO').hide();
            }else if(statusDo === 'CLSE'){
                $('#addContainer').prop('disabled', true);
                self.setState({is_method_payment: true});
                $('#rejectDO').hide();
                $('#approveDO').hide();
                $('#payment_button').hide();
                document.getElementById("nota_req").readOnly = true;
                console.log("tag");
                console.log(self.state.tag);
                if (self.state.tag == '2') {
                    $('#view_invoice').show();
                    $('#view_DO').show();
                    $('#view_proforma').show();
                } else {
                    $('#view_proforma').hide();
                    $('#view_invoice').show();
                    $('#view_DO').show();
                }
            }else if(statusDo === 'BILL'){
                $('#addContainer').prop('disabled', true);
                $('#rejectDO').hide();
                $('#approveDO').hide();
                if (self.state.tag == '2') {
                    $('#payment_button').show();
                } else {
                    $('#payment_button').hide();
                }
                document.getElementById("nota_req").readOnly = true;
                self.setState({is_method_payment: true});
                $('#view_proforma').show();
                $('#view_DO').hide();
                $('#view_invoice').hide();
            } else if(statusDo === 'PAID'){
                $('#addContainer').prop('disabled', true);
                $('#rejectDO').hide();
                $('#approveDO').hide();
                $('#payment_button').hide();
                document.getElementById("nota_req").readOnly = true;
                self.setState({is_method_payment: true});
                $('#view_proforma').show();
                $('#view_DO').hide();
                $('#view_invoice').hide();
            }

        }
    })
    .catch(error => {
        Swal.close();
        console.log(error);
        Swal.fire(error);
    });
  }

  OnChangePayment(val) {
    console.log("Selected: " + val.value);
    this.setState({id_payment_method: val.value})
  }

  Reject(){
    const self = this;
    const $ = require('jquery');

    console.log('Reject');
    var id_req = $('#id_rjtcont_edit').val();
    var ket = $('#ketrjt').val();
    console.log(id_req);
    console.log(ket);

    if(ket === ''){
        Swal.fire('Keterangan Reject tidak boleh kosong');
        return false;
    }

    Swal.fire({
        title: 'Please Wait ..',
        onBeforeOpen: () => {
            Swal.showLoading()
        }
    });

    let header_config = {
        headers: {
            "Content-Type": "application/json",
        }
    };

    var url = Constants.REJECT_DELIVERY;
    console.log(url);

    axios.post(url, 
        {
            check_api: 'get_api',
            id_company: this.state.companyID,
            id: id_req,
            ket: ket
        },
        header_config
    )
    .then(result => {
        console.log(result);
        Swal.close();
        if (result.data.status == false) {
            var message = result.data.message;
            Swal.fire(message);
        } else {
            var message = result.data.message;

            Swal.fire({title: "Done", text: message, type: "success"}).then(() => {
                console.log('swal success')
                self.props.history.goBack();
            });
        }
    })
    .catch(error => {
        Swal.close();
        console.log(error);
        Swal.fire(error);
    });
        


        // $.ajax({
        // type: "POST",
        // url: "/dopayment/requests/rejectDO",
        // dataType: "json",
        // data: { 
        //         id:id_req,
        //         ket:ket
        //       },
        // cache: false,
        //     success: function(result){         
        //         if(result.status == 'S'){
        //             setTimeout(function () {
        //                 swal({title: "Done", text: "'"+result.message+"'", type: "success"},
        //                     function(){ 
        //                         window.location.href = "/dopayment/requests/delivery";
        //                     }
        //                 );
        //             }, 3000);
        //         }else{
        //             setTimeout(function () {
        //                     swal({title: "Failed", text: "Something wrong", type: "error"},
        //                         function(){ 
        //                             location.reload();
        //                         }
        //                     );
        //                 }, 3000);
        //         }
        //     }
        // });
  }

  Approve(){
    const $ = require('jquery');
    const self = this;
      
    console.log("approve");

    if (this.state.id_payment_method == '') {
        Swal.fire("Payment method is required !");
        return false;
    } else {
        var id_req = $('#id_request').val();
        var payment = this.state.id_payment_method;
        var note_request = $('#nota_req').val();
        console.log(note_request);
        console.log(id_req)
        Swal.fire({
            title: "Approve Delivery Order",
            text: "Submit to Approve Delivery Order",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
        }).then(() => {
                Swal.fire({
                    title: 'Please Wait ..',
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    }
                });
            
                let header_config = {
                    headers: {
                      "Content-Type": "application/json",
                    }
                };
            
                var url = Constants.APPROVE_DELIVERY;
                console.log(url);
            
                axios.post(url, 
                    {
                        check_api: 'get_api',
                        id_company: this.state.companyID,
                        id: id_req,
                        payment: payment,
                        note_request: note_request
                    },
                    header_config
                )
                .then(result => {
                    console.log(result);
                    Swal.close();
                    if (result.data.status == false) {
                        var message = result.data.message;
                        Swal.fire(message);
                    } else {
                        var message = result.data.message;

                        Swal.fire({title: "Done", text: message, type: "success"}).then(() => {
                            console.log('swal success')
                            self.props.history.goBack();
                        });
                    }
                })
                .catch(error => {
                    Swal.close();
                    console.log(error);
                    Swal.fire(error);
                });
            });
    }
  }

  add_container_req(){
        const self = this;
        console.log('add container');
        const $ = require('jquery')
        var count = parseInt($('#count_req').val());
        var input_cont = '<div class="form-group row req" id="div-row-cont-'+count+'">'+
                '<label for="ForContainer" class="col-sm-4 col-form-label">No. Container</label>'+
                '<div class="col-sm-6">'+
                    '<input type="text" name="cont_no'+count+'" class="form-control col-form-label-sm" id="no_container_add-'+count+'" placeholder="No Container">'+
                '</div>'+
                '<div class="col-sm-1">'+
                    '<button id="btnDeleteContainer_'+count+'" type="button" class="btn btn-danger btn-sm">Delete</button>'+
                '</div>'+
                '</div>';
        $('#div-container-req').append(input_cont);
        $('#count_req').val(count+1);

        $( "#btnDeleteContainer_"+count).click(function() {
            self.removeRow_container_req(count);
        });
  }

  removeRow_container_req(id){
        console.log('remove click ' + id);
        const $ = require('jquery')
        $('#div-row-cont-'+id).remove();
        var cd = parseInt($('#count_req').val());
        $('#count_req').val(cd-1);
}

  SubmitAdd(){
    console.log('submit container');
    console.log(this.state.id_do);
    const self = this;
    const $ = require('jquery')
    $('#id_req').val(this.state.id_do);

    var url = Constants.POST_ADD_CONTAINER;
    console.log(url);

    Swal.fire({
        title: "Add Container",
        text: "Submit to Add Container",
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }).then((result) => {
        if (result.value) {
            axios.post(url, $( "#add_container" ).serialize())
            .then(result => {
                console.log(result);
                if (result.data.status == false) {
                    var message = result.data.message;
                    Swal.fire(message);
                } else {
                    Swal.fire({title: "Done", text: "'"+result.data.message+"'", type: "success"})
                    .then((result) => {
                        self.closeModal();
                        self.get_data_detail()
                    });
                }
            })
            .catch(error => {
                console.log(error);
                Swal.fire(error);
                self.closeModal();
            });   
        }
    });
  }

  submit_edit(){
        const self = this;
        let input_data = {
            id: this.state.id_container,
            delivery_id: this.state.delivery_id,
            container_before: this.state.no_container_before,
            container_after: this.state.no_container_after,
            check_api: 'get_api',
            username: this.state.userName
        }
        console.log(input_data)

        var url = Constants.POST_EDIT_CONTAINER;
        console.log(url);

        Swal.fire({
            title: "Edit Container Number",
            text: "Submit to Edit Container Number",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }).then((result) => {
            if (result.value) {
                let header_config = {
                    headers: {
                    "Content-Type": "application/json",
                    }
                };
                axios.post(url, input_data, header_config).then(result => {
                    console.log(result);
                    if (result.data.status == 'F') {
                        var message = result.data.message;
                        Swal.fire(message);
                    } else {
                        Swal.fire({title: "Done", text: "'"+result.data.message+"'", type: "success"})
                        .then((result) => {
                            self.closeModalEdit();
                            self.get_data_detail()
                        });
                    }
                }).catch(error => {
                    console.log(error);
                    Swal.fire(error);
                    self.closeModalEdit();
                });
            }
        });
    }

    onFieldChange(fieldName){
        return function (event) {
            const target = event.target;
            this.setState({[fieldName]: event.target.value});
        }
    }

    backPage() {
        this.props.history.goBack();
    }

    delete_cont(id) {
        const self = this;
        if(this.state.status == 'PNDG' || this.state.status == 'RQST') {
            Swal.fire({
                title: "Delete Container",
                text: "Submit to Delete Container",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }).then((result) => {
                if (result.value) {
                    let header_config = {
                        headers: {
                        "Content-Type": "application/json",
                        }
                    };

                    var url = Constants.DELETE_CONTAINER;
                    axios.post(url, {
                        id: id,
                        check_api: 'get_api',
                    }, header_config)
                    .then(result => {
                        console.log(result);
                        if (result.data.status == false) {
                            var message = result.data.message;
                            Swal.fire(message);
                        } else {                        
                            Swal.fire({title: "Done", text: "'"+result.data.message+"'", type: "success"});
                            self.get_data_detail()
                        }
                    })
                    .catch(error => {
                        console.log(error);
                        Swal.fire(error);
                    });   
                }
            });
        }
    }

    payment(){
        console.log('click payment');
        console.log(this.state.data_delivery);

        var no_proforma = this.state.no_proforma;
        var service_type = this.state.service_type;
        var shipping_line = this.state.slname;
        var amount = this.state.total;
        var id_consigne = this.state.id_consigne;
        var no_request = this.state.reqnum;

        var service_type_desc = '';
        if (service_type == '1') {
            service_type_desc = 'PETIKEMAS';
        } else {
            service_type_desc = 'OTHER';
        }

        console.log(shipping_line);
        console.log(service_type_desc);
        console.log(no_proforma);
        console.log(amount);
        console.log(id_consigne);
        console.log(no_request);

        let input_data = {
            no_proforma: no_proforma,
            amount: amount,
            service_type: service_type_desc,
            shipping_line: shipping_line,
            id_consigne: id_consigne,
            no_request: no_request
        }

        console.log(input_data);

        var url = Constants.POST_INQUIRY_PAYMENT;
        console.log(url);

        Swal.fire({
            title: "Payment",
            text: "Are you sure you will make a payment",
            showCancelButton: false,
            // showLoaderOnConfirm: true,
        }).then((results) => {
            // preConfirm: () => {
            Swal.fire({
                title: 'Please Wait ..',
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            });
            return axios.post(url, input_data)
            .then(result => {
                console.log(result);
                if(result.data.status == '01'){
                    console.log('masuk sukses');
                    console.log(result.data);
                    var pid = result.data.pid;
                    var encrypt = result.data.encrypt;
                    var cust_id = result.data.cust_id;
                    
                    console.log('data');
                    console.log(pid);
                    console.log(encrypt);
                    console.log(cust_id);

                    Swal.close();                        
                    Swal.fire({
                        title: "Done", 
                        text: "'"+result.data.message+"'", 
                        showCancelButton: false,
                    }).then((results) => {
                        var url_payment = Constants.IP_PAYMENT + '0_1_0/invoice?app_id='+Constants.APP_ID_DIGICO+'&user='+Constants.USERNAME_DIGICO+'&pass='+Constants.PASS_DIGICO+'&pid='+pid+'&key='+encrypt+'&no_rek&kode_bank&cust_id='+cust_id;
                        console.log(url_payment);
                        window.location.href = url_payment;
                    });
                } else {
                    console.log('masuk gagal')
                    var message = result.data.message;
                    Swal.fire(message);
                }
            })
            .catch(error => {
                console.log(error);
                Swal.fire(error);
            });   
        // }
        });
    }
}
export default detail_delivery;
