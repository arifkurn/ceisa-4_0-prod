import React, { Component, Fragment } from 'react';
import * as Constants from '../Constants.js';
import * as Helper from '../Helper.js';
import { HashRouter } from 'react-router-dom'
import axios from 'axios'
import Swal from 'sweetalert2'
import { useToasts } from 'react-toast-notifications'
import * as ApiClickargo from '../Clickargo/ApiList.js'

function withToast(Component) {
  return function WrappedComponent(props) {
    const toastFuncs = useToasts()
    return <Component {...props} {...toastFuncs} />;
  }
}

class Sp2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      //CLICKARGO STATE
      clickargoInputForm: [{
        moveType: '',
        moveTypeLabel: 'Please Select Move Type',
        actionType: 'import',
        actionTypeLabel: 'Import',
        transactionType: '',
        transactionTypeLabel: 'Please Select Transaction Type',
        documentType: '',
        documentTypeLabel: 'Please Select Document Type',
        blNumber: '',
        blNumberLabel: 'Please Select BL Number',
        blDate: '',
        no: '',
        date: '',
        doDate: '',
        terminalOperator: '',
        terminalOperatorLabel: 'Select terminal operator',
      }],
      cursor: 'pointer'
      //CLICKARGO STATE
    }
  }

  contentSp2Header = (breadcrump) => {
    return (
      <div className="kt-portlet__head">

        <div className="kt-portlet__head-label">
          <h3 className="kt-portlet__head-title">
            {
              breadcrump.map((bcp, i) => {
                return (
                  <span key={i}>
                    {
                      bcp.link === null
                        ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                        : (<a href="/#/warehouse/" className="kt-link kt-font-transform-u" onClick={() => { this.setActiveView(bcp.link) }} key={i}>{bcp.label}</a>)
                    }
                    {
                      i !== (breadcrump.length - 1)
                        ? " > "
                        : null
                    }
                  </span>
                )
              })
            }
          </h3>
        </div>
      </div>
    )
  }

  //CLICKARGO DEV
  clickargoSP2 = () => {
    //clear session
    localStorage.removeItem("ckSP2Docs")
    localStorage.removeItem("ckSP2Form")
    localStorage.removeItem("ckSP2ContainerSelected")
    localStorage.removeItem("ckSP2ConfirmTrxId")
    localStorage.removeItem("ckSP2AdditionalForm")
    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }

    setTimeout(() => {
      this.setState({
        cursor: 'not-allowed'
      })
      this.props.addToast('Clickargo service clicked, please wait!', {
        appearance: 'info',
        autoDismiss: true,
      })
      //auth check
      axios
          .post(ApiClickargo.CLICKARGO_CHECK_NPWP, {
              tax_number: this.props.dataUserPortal.npwp,
              email: this.props.dataUserPortal.email,
              // tax_number: '541267799887999',
              // email: 'kusnle5@yopmail.com'
          }, {
              headers: headers
          })
          .then(responseNPWP => {
              if (responseNPWP.data.success === true) {
                  this.setState({
                    cursor: 'pointer'
                  })
                  //document transaction type
                  const headerSP2 = {
                    'Secret': responseNPWP.data.data.secret,
                    'Key': responseNPWP.data.data.key,
                    'Content-Type': 'application/json',
                    'Accept-Language': 'application/json'
                  }
                  axios
                    .post(ApiClickargo.CLICKARGO_SP2_DOCS_TRANSACTION_TYPE, {
                      category_id: 'I',
                      terminal_id: 'KOJA'
                    }, {
                      headers: headerSP2
                    })
                    .then(reponseSP2Docs => {
                      console.log(reponseSP2Docs.data.data)
                      //save header token
                      localStorage.setItem('ckHeaderKey', responseNPWP.data.data.key)
                      localStorage.setItem('ckHeaderSecret', responseNPWP.data.data.secret)
                      localStorage.setItem('ckHeaderToken', responseNPWP.data.data.token)
                      //save cksp2 documents transaction type
                      localStorage.setItem('ckSP2Docs', JSON.stringify(reponseSP2Docs.data.data, null, 2))
                      localStorage.setItem('ckSP2Form', JSON.stringify(this.state.clickargoInputForm[0], null, 2))
                      this.props.history.push('/clickargo_sp2/form')
                    })
                    .catch(error => {
                      if(error.response) {
                        if(error.response.data.success === false) {
                            this.props.addToast(error.response.data.message, {
                              appearance: 'error',
                              autoDismiss: true,
                            })
                        }
                      }
                    })
                  
                  
              }
          })
          .catch(error => {
            this.setState({
              cursor: 'pointer'
            })
              if (error.response) {
                  if(error.response.data.success === false) {
                      Swal.fire({
                          title: 'Sorry',
                          text: "You need to register first before using the platform. Just a simple step! Lets go :)",
                          icon: 'error',
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Yes!'
                      }).then((result) => {
                          if (result.value) {
                              localStorage.setItem('ckServiceSelected', 'SP2')
                              localStorage.setItem('ckSP2Form', JSON.stringify(this.state.clickargoInputForm[0], null, 2))
                              
                              this.props.history.push("/clickargo_register")
                          }
                      })
                  }
              }
          })
    }, 1200);
  }
  //CLICKARGO DEV

  render() {
    const breadcrump = [
      {
        label: 'SP 2',
        link: null
      }
    ];
    return (
      <HashRouter>
        <div className="row">
          <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12 mt-3" style={{"height": "200px"}}>
            <div onClick={() => { this.clickargoSP2(); }} className="card" style={{ justifyContent: 'center', display: 'flex', cursor: this.state.cursor }}>
              <div className="row" style={{ justifyContent: 'center', display: 'flex' }}>
                <div>
                  <img style={{ width: '150px', height: '60px' }} src={window.location.origin + '/assets/images/Clickargo5.png'} />
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12 mt-3" style={{"height": "200px"}}>
            <div className="card" style={{ justifyContent: 'center', display: 'flex' }}>
              <div className="row" style={{ justifyContent: 'center', display: 'flex' }}>
                <div>
                  <label>Principle</label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </HashRouter>
    )
  }

}

export default withToast(Sp2);