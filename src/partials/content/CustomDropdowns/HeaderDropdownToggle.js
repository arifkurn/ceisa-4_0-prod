import React from "react";

class HeaderDropdownToggle extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    this.props.onClick(e);
  }

  render() {
    return (
      <div
        className="kt-header__topbar-wrapper"
        onClick={this.handleClick}
        ref={this.props.innerRef}
      >
        {this.props.children}
      </div>
    );
  }
}

const AsHeaderDropdownToggle = React.forwardRef((props, ref) => (
  <HeaderDropdownToggle innerRef={ref} {...props} />
))

export default AsHeaderDropdownToggle
