export const popupWindow = function(url, title, win, w, h) {
    const y = win.top.outerHeight / 2 + win.top.screenY - ( h / 2);
    const x = win.top.outerWidth / 2 + win.top.screenX - ( w / 2);
    return win.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+y+', left='+x);
}
export const getUrlVars = function() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
  function(m,key,value) {
    vars[key] = value;
  });
  return vars;
}
export const getUrlParameter = function(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(window.location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}
export const getNPWP = async () => {
  const queryString = getUrlVars();
  const encodedNpwp = typeof queryString["npwp"] === "undefined"? null: queryString["npwp"];
  const decryptorURI = 'https://esbbcext01.beacukai.go.id:8087/decodenpwp?npwp=';
  const response = await fetch(`${decryptorURI}${encodedNpwp}`);
  const currentStatus = await response.status;
  const currentNPWP = await response.text();
  return currentStatus === "200" ? currentNPWP: null;
}
export const randomColor = function() {
  let hue = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';
  return hue;
}
export const formatPrintDate = function(input_date) {
  if ( input_date !== '' && input_date !== null && typeof input_date !== "undefined" ) {
    const d = new Date(input_date);
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const string_month = months[d.getMonth()];

    const year = d.getFullYear();
    let day = d.getDate();
    day = day >= 10 ? day: '0'+day;
    return day + " " + string_month + " " + year;
  } else {
    return "";
  }
}
export const formatDate = function(input_date) {
  // console.log(string_date);
  if ( input_date !== '' && input_date !== null && typeof input_date !== "undefined" ) {
    let string_date = '';
    let split_date = input_date.split('-');
    let string_year = split_date[0] ? split_date[0]: '1900';
    if ( string_year.substring(0, 2) === '00' && string_year.length === 4 ) {
      string_year = '20'+string_year.substring(2, 4);
    }
    let string_month = split_date[1] ? parseInt(split_date[1]): 1;
    string_month = string_month < 10 ? '0'+string_month: string_month;
    let string_day = split_date[2] ? split_date[2]: '01';
    string_date = string_year+'-'+string_month+'-'+string_day;

    var dateObject = new Date(string_date);
    var year = dateObject.getFullYear();
    var month = parseInt(dateObject.getMonth()+1);
    month = month < 10 ? '0'+month: month;
    var day = dateObject.getDate();
    day = day >= 10 ? day: '0'+day;
    return year+'-'+month+'-'+day;
  } else {
    return "";
  }
}
export const convertToThousand = function(angka) {
  if ( !isNaN(angka) && angka !== null ) {
    var rupiah = '';    
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) {
      if(i%3 === 0) {
        rupiah += angkarev.substr(i,3)+'.';
      }
    }
    return rupiah.split('',rupiah.length-1).reverse().join('');
  } else {
    return 0;
  }
}

export const convertToRupiah = function(angka) {
  if ( !isNaN(angka) && angka !== null ) {
    var rupiah = '';    
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) {
      if(i%3 === 0) {
        rupiah += angkarev.substr(i,3)+'.';
      }
    }
    return rupiah.split('',rupiah.length-1).reverse().join('');
  } else {
    return 0;
  }
}

export const getDateTimeNow = function(){
  var tempDate = new Date();
  var year = tempDate.getFullYear();
  var month = (tempDate.getMonth()+1);
  var day = tempDate.getDate();
  if(day<10){
    day='0'+day
  } 
  if(month<10){
    month='0'+month
  } 
  var hour = tempDate.getHours();
  var minutes = tempDate.getMinutes();
  var seconds = tempDate.getSeconds();
  return year + '-' + month + '-' + day + ' ' + hour + ':' + minutes + ':' + seconds;
}
export const getDateNow = function(){
  var tempDate = new Date();
  var year = tempDate.getFullYear();
  var month = (tempDate.getMonth()+1);
  var day = tempDate.getDate();
  if(day<10){
    day='0'+day
  } 
  if(month<10){
    month='0'+month
  } 
  return year + '-' + month + '-' + day;
}
export const getTimeNow = function() {
  var date = new Date();
  var seconds = date.getSeconds();
  var minutes = date.getMinutes();
  var hour = date.getHours();
  return hour+':'+minutes+':'+seconds;
}

export const in_array = function(needle, haystack, argStrict) { 
  var key = ''
  var strict = !!argStrict

  if (strict) {
    for (key in haystack) {
      if (haystack[key] === needle) {
        return true
      }
    }
  } else {
    for (key in haystack) {
      if (haystack[key] == needle) { // eslint-disable-line eqeqeq
        return true
      }
    }
  }

  return false
}
export const array_search = function(needle, haystack, argStrict) {
    var strict = !!argStrict
    var key = ''

    if (typeof needle === 'object' && needle.exec) {
        // Duck-type for RegExp
        if (!strict) {
            // Let's consider case sensitive searches as strict
            var flags = 'i' + (needle.global ? 'g' : '') +
                (needle.multiline ? 'm' : '') +
                // sticky is FF only
                (needle.sticky ? 'y' : '')
            needle = new RegExp(needle.source, flags)
        }
        for (key in haystack) {
            if (haystack.hasOwnProperty(key)) {
                if (needle.test(haystack[key])) {
                    return key
                }
            }
        }
        return false
    }

    for (key in haystack) {
        if (haystack.hasOwnProperty(key)) {
            if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) { // eslint-disable-line eqeqeq
                return key
            }
        }
    }
    return false
}