import React, { Component } from 'react';
import { withRouter } from 'react-router'
import { Route, NavLink, HashRouter, Switch } from 'react-router-dom';
import Customs from "./Customs/Customs";
import Dashboard from "./Dashboard/Dashboard";
import Trucking from "./Trucking/Trucking";
import BookTruck from "./Trucking/BookTruck";
import DigicoListBOL from "./Digico/bill_of_lading/list_bol";
import DigicoDetailBOL from "./Digico/bill_of_lading/detail_bol";
import DigicoListDelivery from "./Digico/delivery_order/list_delivery";
import DigicoDetailDelivery from "./Digico/delivery_order/detail_delivery";
import ChoosePrinciple from "./E-DO/ChoosePrinciple";
import LoginDigico from "./Digico/LoginDigico";
import Sp2 from "./SP2/Sp2";
import Vessel from "./Vessel/Vessel";
import ClikargoRegister from "./Vessel/Register/Register";
import ClickargoDOForm from "./Clickargo/DO/Form"
import ClickargoDOReview from "./Clickargo/DO/Review"
import ClickargoDOMyTransaction from "./Clickargo/DO/MyTransaction"
import ClickargoDOMyTransactionDetail from "./Clickargo/DO/MyTransactionDetail"
import ClickargoDOInvoice from "./Clickargo/DO/TransactionDetailTabs/PaymentDetail/Invoice"
import ClickargoSP2Form from "./Clickargo/SP2/Form"
import ClickargoSP2ChooseContainer from "./Clickargo/SP2/ChooseContainer"
import ClickargoSP2MyTransaction from "./Clickargo/SP2/MyTransaction"
import ClickargoSP2MyTransactionDetail from "./Clickargo/SP2/MyTransactionDetail"
import ClickargoRedirectOrders from "./Clickargo/Redirection/RedirectOrders"
import BookingRequest from "./Vessel/BookingRequest/BookingRequest";
import BookingRequestBack from "./Vessel/BookingRequest/BookingRequestBack";
import ReviewFreight from "./Vessel/BookingRequest/ReviewFreight";
import ShipmentManagement from "./Vessel/ShipmentManagement/Index"
import ShipmentManagementDetail from "./Vessel/ShipmentManagement/Detail"
import Railway from "./Railway/Railway";
import Pelimpahan from "./Pelimpahan/Pelimpahan";
import Warehouse from "./Warehouse/Warehouse";
import Analytics from "./Analytics/Analytics";
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import {in_array, getUrlVars} from './Helper.js';
import {API_DECRYPTOR, API_USER_DIGICO, ENC_ID} from './Constants.js';
import RestrictAccess from './RestrictAccess.js';
import { ToastProvider } from 'react-toast-notifications'
import VesselList from "./Vessel/Dashboard/List"
import VesselDetail from "./Vessel/Dashboard/Detail"
import * as Constants from '../src/Constants';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      showMobileSideMenu: false,
      activeEdo: null,
      doRedirectEDO: false,
      activeHash: this.props.history.location.hash,
      dataUserPortal: {
        npwp: null,
        nama_perusahaan: null,
        alamat: null,
        telp: null,
        nama_kontak_person: null,
        email: null
      },
      isFirstLoading: true,
      isLoadingContent: false
    }
    const {activeHash} = this.state;
    this.unlisten = this.props.history.listen((location, action) => {
      var newLocation = location.hash;
      this.setState({activeHash: newLocation});        
    });
    const self = this;
    if ( in_array(activeHash, ["#/digico_list_bol", "#/digico_detail_bol", "#/digico_list_delivery", "#/digico_detail_delivery"]) ) 
      this.getUserDigico();
    else
      this.getUserPerusahaan();
  }
  handleLogoutEDO = () => {
    this.setState({activeEdo: null});
  }
  handleChoosePrinciple = (principle) => {
    this.setState({activeEdo: principle});
    // this.props.history.push('/'+principle+'_list_bol')
  }
  handleHeaderMobileToggler = (ev) => {
    this.setState({showMobileSideMenu: true});
  }
  handleCloseHeaderMobileToggler = (ev) => {
    this.setState({showMobileSideMenu: false});
  }
  logout =()=>{
    window.location.href=(Constants.LOGOUT)
  }
  render() {
    const {activeHash, dataUserPortal, isFirstLoading, isLoadingContent} = this.state;
    console.log(dataUserPortal);
    if ( isFirstLoading ) {
      return (<HashRouter>Loading ...</HashRouter>);
    } else if ( dataUserPortal.npwp === null || dataUserPortal.npwp === "" || dataUserPortal.npwp === undefined ) {
      return <RestrictAccess />;
    } else {
      return (
        <HashRouter>
          <div className={"kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading" + (this.state.showMobileSideMenu ? " kt-header-menu-wrapper--on": null)}>
            <div id="kt_header_mobile" className="kt-header-mobile  kt-header-mobile--fixed" style={{backgroundColor:"#051a92"}}>
              <div className="kt-header-mobile__logo">
                <img width="240px" height="70px" src="assets/images/logo-icon-transparent.png"></img>                    
              </div>
              <div className="kt-header-mobile__toolbar">
                <button className="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler" onClick={(ev) => this.handleHeaderMobileToggler(ev)}><span></span></button>
              </div>
            </div>
            <div className="kt-grid kt-grid--hor kt-grid--root">
              <div className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

                {/*<!-- begin:: Aside -->*/}
                <button className="kt-aside-close " id="kt_aside_close_btn" onClick={(ev) => this.handleCloseHeaderMobileToggler(ev)}><i className="la la-close"></i></button>
                <div className={this.state.showMobileSideMenu ? "kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid kt-header-menu-wrapper--on": "kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop"} id="kt_aside">

                  {/*<!-- begin:: Brand -->*/}
                  <div className="kt-aside__brand kt-grid__item  " id="kt_aside_brand" style={{backgroundColor:"#051a92"}}>
                      <img width="130%" src="assets/images/logo-icon-transparent.png" style={{marginLeft:"-22px"}}></img>                    
                  </div>
                  {/*<!-- end:: Brand -->*/}

                  {/*<!-- begin:: Aside Menu -->*/}
                  <div className="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
                    <div id="kt_aside_menu" className="kt-aside-menu  kt-aside-menu--dropdown " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
                      <ul className="kt-menu__nav ">
                        <li className="kt-menu__item" aria-haspopup="true">
                          <NavLink to="/dashboard" className="kt-menu__link " onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                            <i className="kt-menu__link-icon flaticon2-protection"></i>
                            <span className="kt-menu__link-text">Dashboard</span>
                          </NavLink>
                        </li>
                        <li className="kt-menu__item" aria-haspopup="true">
                          <NavLink to="/pelimpahan" className="kt-menu__link" onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                            <i className="kt-menu__link-icon flaticon-multimedia-2"></i>
                            <span className="kt-menu__link-text">Pelimpahan</span>
                          </NavLink>
                        </li>
                        <li className="kt-menu__item  kt-menu__item--submenu kt-menu__item--open" aria-haspopup="true" data-ktmenu-submenu-toggle="hover" data-ktmenu-submenu-mode="accordion">
                          <div className="kt-menu__link ">
                            <i className="kt-menu__link-icon fa fa-ship"></i>
                            <span className="kt-menu__link-text">Shipping</span>
                          </div>
                          
                          <div className="kt-menu__submenu ">
                            <ul className="kt-menu__subnav">
                              <li className="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">
                                <span className="kt-menu__link"><span className="kt-menu__link-text">Shipping</span></span>
                              </li>
                              <li className="kt-menu__item  kt-menu__item--submenu kt-menu__item--open" aria-haspopup="true" data-ktmenu-submenu-toggle="hover" data-ktmenu-submenu-mode="accordion">
                                <NavLink to="/chooseprinciple" className="kt-menu__link kt-menu__toggle" onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                                  <span className="kt-menu__link-text" onClick={() => this.handleLogoutEDO}>E-DO</span>
                                </NavLink>
                                {
                                  this.state.activeEdo === "digico" || in_array(activeHash, ["#/digico_list_bol", "#/digico_detail_bol", "#/digico_list_delivery", "#/digico_detail_delivery"]) ? (
                                    <div className="kt-menu__submenu " id="sub_menu_digico">
                                      <ul className="kt-menu__subnav">
                                        <li className="kt-menu__item " aria-haspopup="true">
                                          <NavLink to="/digico_list_bol" className="kt-menu__link " onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                                            <i className="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                            <span className="kt-menu__link-text">Bill Of Lading</span>
                                          </NavLink>
                                        </li>
                                        <li className="kt-menu__item " aria-haspopup="true">
                                          <NavLink to="/digico_list_delivery" className="kt-menu__link " onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                                            <i className="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                            <span className="kt-menu__link-text">Delivery Order</span>
                                          </NavLink>
                                        </li>
                                      </ul>
                                    </div>
                                  ): null
                                }
                                {
                                  this.state.activeEdo === "clickargo_do" || in_array(activeHash, ["#/clickargo_do/form", "#/clickargo_do/review", "#/clickargo_do/my_transaction", "#/clickargo_do/my_transaction_detail"]) ? (
                                    <div className="kt-menu__submenu " id="sub_menu_clickargo">
                                      <ul className="kt-menu__subnav">
                                          <li className="kt-menu__item " aria-haspopup="true">
                                            <NavLink to="/clickargo_do/my_transaction" className="kt-menu__link " onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                                              <i className="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                              <span className="kt-menu__link-text">My Transaction</span>
                                            </NavLink>
                                          </li>
                                      </ul>
                                    </div>
                                  ): null
                                }
                              </li>
                              <li className="kt-menu__item  kt-menu__item--submenu kt-menu__item--open" aria-haspopup="true" data-ktmenu-submenu-toggle="hover" data-ktmenu-submenu-mode="accordion">
                                <NavLink to="/sp2" className="kt-menu__link " onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                                  <span className="kt-menu__link-text" onClick={() => this.handleLogoutEDO}>SP 2</span>
                                </NavLink>
                                {
                                  in_array(activeHash, ["#/clickargo_sp2/form", "#/clickargo_sp2/choose_container", "#/clickargo_sp2/my_transaction"]) ? (
                                    <div className="kt-menu__submenu " id="sub_menu_clickargo">
                                      <ul className="kt-menu__subnav">
                                          <li className="kt-menu__item " aria-haspopup="true">
                                            <NavLink to="/clickargo_sp2/my_transaction" className="kt-menu__link " onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                                              <i className="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                              <span className="kt-menu__link-text">My Transaction</span>
                                            </NavLink>
                                          </li>
                                      </ul>
                                    </div>
                                  ) : null
                                }
                              </li>
                              <li className="kt-menu__item" aria-haspopup="true">
                                <NavLink to="/vessel" className="kt-menu__link " onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                                  <span className="kt-menu__link-text">Vessel</span>
                                </NavLink>
                              </li>
                            </ul>
                          </div>
                        </li>
                        <li className="kt-menu__item" aria-haspopup="true">
                          <NavLink to="/customs" className="kt-menu__link " onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                            <i className="kt-menu__link-icon flaticon2-analytics-2"></i>
                            <span className="kt-menu__link-text">Customs</span>
                          </NavLink>
                        </li>
                        <li className="kt-menu__item  kt-menu__item--submenu kt-menu__item--open" aria-haspopup="true" data-ktmenu-submenu-toggle="hover" data-ktmenu-submenu-mode="accordion">
                          <NavLink to="/" className="kt-menu__link" onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                            <i className="kt-menu__link-icon fa fa-truck"></i>
                            <span className="kt-menu__link-text">Transport</span>
                          </NavLink>
                          <div className="kt-menu__submenu ">
                            <ul className="kt-menu__subnav">
                              <li className="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">
                                <span className="kt-menu__link"><span className="kt-menu__link-text">Transport</span></span>
                              </li>
                              <li className="kt-menu__item  kt-menu__item--submenu kt-menu__item--open" aria-haspopup="true" data-ktmenu-submenu-toggle="hover" data-ktmenu-submenu-mode="accordion">
                                <NavLink to="/trucking" className="kt-menu__link kt-menu__toggle" onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                                  <span className="kt-menu__link-text">Trucking</span>
                                </NavLink>
                                {
                                  /*
                                  <NavLink to="/railway" className="kt-menu__link kt-menu__toggle" onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                                  <span className="kt-menu__link-text">Railway</span>
                                </NavLink>
                                  */
                                }
                              </li>
                            </ul>
                          </div>
                        </li>
                        {
                          /*
                          <li className="kt-menu__item" aria-haspopup="true">
                          <NavLink to="/warehouse" className="kt-menu__link " onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                            <i className="kt-menu__link-icon fa fa-users"></i>
                            <span className="kt-menu__link-text">Warehouse</span>
                          </NavLink>
                        </li>
                        <li className="kt-menu__item" aria-haspopup="true">
                          <NavLink to="/analytics" className="kt-menu__link " onClick={(e) => this.handleCloseHeaderMobileToggler(e)}>
                            <i className="kt-menu__link-icon fa fa-chart-bar"></i>
                            <span className="kt-menu__link-text">Analytics</span>
                          </NavLink>
                        </li>
                          */
                        }
                        <li className="kt-menu__item kt-menu__item--submenu kt-menu__item--bottom-2" aria-haspopup="true">
                          <span className="kt-menu__link-text-bottom"></span>
                        </li>
                      </ul>
                    </div>
                  </div>

                  {/*<!-- end:: Aside Menu -->*/}
                </div>
                <div className="kt-aside-menu-overlay"></div>

                {/*<!-- end:: Aside -->*/}
                <div className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                  {/*<!-- begin:: Header -->*/}
                  <div id="kt_header" className="kt-header kt-grid kt-grid--ver  kt-header--fixed ">

                    {/*<!-- begin: Header Menu -->*/}
                    <button className="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn" onClick={(ev) => this.handleCloseHeaderMobileToggler(ev)}><i className="la la-close"></i></button>
                    <div className="kt-header__topbar"></div>

                    {/*<!-- end: Header Menu -->*/}

                    {/*<!-- begin:: Header Topbar -->*/}

                    <div className="kt-header__topbar">

                      {/*<!--begin: Notifications -->*/}
                      <div className="kt-header__topbar-item kt-header__topbar-item--user">
                        {/* <div className="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                          <span className="kt-header__topbar-icon kt-header__topbar-icon--success"><i className="flaticon2-bell-alarm-symbol"></i></span>
                          <span className="kt-hidden kt-badge kt-badge--danger"></span>
                        </div> */}
                        <Dropdown className="kt-header__topbar-wrapper">
                          <Dropdown.Toggle 
                            variant="success" 
                            id="dropdown-basic" 
                            className="btn btn-outline-brand btn-icon btn-circle"
                            style={{marginTop:"10px",backgroundColor:"white", borderColor:""}}
                            >
                              <i style={{color:"#051a92",width:"10px"}} className="flaticon-bell"></i>
                          </Dropdown.Toggle>

                          <Dropdown.Menu style={{marginTop:"15px"}} className="kt-menu__nav ">
                            {/* href="#/action-1" */}
                            <Dropdown.Item className="kt-nav__item" >Action</Dropdown.Item>
                            <Dropdown.Item className="kt-nav__item">Another action</Dropdown.Item>
                            <Dropdown.Item className="kt-nav__item">Something else</Dropdown.Item>
                          </Dropdown.Menu>
                        </Dropdown>
                      </div>
                      {/*<!--end: Notifications -->*/}

                      {/*<!--begin: User bar -->*/}
                      <div className="kt-header__topbar-item kt-header__topbar-item--user">

                        <Dropdown className="kt-header__topbar-wrapper">
                          <Dropdown.Toggle 
                            variant="success" 
                            id="dropdown-basic" 
                            className="btn btn-outline-brand btn-icon btn-circle"
                            style={{marginTop:"10px",backgroundColor:"white", borderColor:"none"}}
                            >
                              <i style={{color:"#051a92",width:"10px"}} className="flaticon2-user-outline-symbol"></i>
                          </Dropdown.Toggle>

                          <Dropdown.Menu style={{marginTop:"15px"}} className="kt-menu__nav ">
                            {/* href="#/action-1" */}
                            <Dropdown.Item className="kt-nav__item" >My Profile</Dropdown.Item>
                            <Dropdown.Item className="kt-nav__item">Account settings and more</Dropdown.Item>
                            <Dropdown.Item className="kt-nav__item" onClick={this.logout}>Sign Out</Dropdown.Item>
                          </Dropdown.Menu>
                        </Dropdown>
                      </div>

                      {/*<!--end: User bar -->*/}

                    </div>
                    {/*<!-- end:: Header Topbar -->*/}
                  </div>
                  {/*<!-- end:: Header -->*/}

                  <div className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
                    {/*<!-- begin:: Content -->*/}

                    {/*<!-- begin:: Content Dashboard -->*/}
                    <div className="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                      <div style={{marginBottom: "10px"}}>
                        <h3 style={{display: "inline"}}>{dataUserPortal.nama_perusahaan}</h3>
                        <div style={{display: "inline", paddingLeft: "5px"}}>{dataUserPortal.npwp}</div>
                      </div>
                      {/* <div className="row">     */}
                      {/* <div className="col-xl-6"> */}
                      {
                        isLoadingContent ? (
                          <span>Loadings ...</span>
                        ): (
                          <Switch>
                            <ToastProvider>
                                  <Route exact path="/" component={props => <Dashboard dataUserPortal={dataUserPortal} {...props}/>}/>
                                  <Route exact path="/dashboard" component={props => <Dashboard dataUserPortal={dataUserPortal} {...props}/>} />
                                  <Route exact path="/customs" component={props => <Customs dataUserPortal={dataUserPortal}/>} />
                                  <Route exact path="/pelimpahan" component={props => <Pelimpahan dataUserPortal={dataUserPortal}/>} />
                                  <Route exact path="/trucking" component={props => <Trucking dataUserPortal={dataUserPortal} {...props}/>} />
                                  <Route exact path="/trucking/booktruck" component={props => <BookTruck dataUserPortal={dataUserPortal} {...props}/>}/>
                                  <Route exact path="/digico_list_bol" component={props => <DigicoListBOL dataUserPortal={dataUserPortal} {...props}/>} />
                                  <Route exact path="/digico_list_delivery" component={props => <DigicoListDelivery dataUserPortal={dataUserPortal} {...props}/>} />
                                  <Route exact path="/digico_detail_delivery" component={props => <DigicoDetailDelivery dataUserPortal={dataUserPortal} {...props}/>} />
                                  <Route exact path="/digico_detail_bol" component={props => <DigicoDetailBOL dataUserPortal={dataUserPortal} {...props}/>} />
                                  <Route exact path="/vessel/list" component={props => <VesselList dataUserPortal={dataUserPortal} {...props} /> } />
                                  <Route exact path="/vessel/detail" component={props => <VesselDetail dataUserPortal={dataUserPortal} {...props} /> } />
                                  <Route exact path="/vessel" component={props => <Vessel dataUserPortal={dataUserPortal} {...props}/>} />
                                  <Route exact path="/chooseprinciple" component={props => <ChoosePrinciple dataUserPortal={dataUserPortal} handleChoosePrinciple={this.handleChoosePrinciple.bind(this)} {...props} />} />
                                  <Route exact path="/sp2" component={props => <Sp2 dataUserPortal={dataUserPortal} handleChoosePrinciple={this.handleChoosePrinciple.bind(this)} {...props}/>} />
                                  
                                  <Route exact path="/booking_request" component={props => <BookingRequest dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/booking_request_back" component={props => <BookingRequestBack dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/clickargo_register" component={props => <ClikargoRegister dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/booking_request/review" component={props => <ReviewFreight dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/shipment_management" component={props => <ShipmentManagement dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/shipment_management_detail" component={props => <ShipmentManagementDetail dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/clickargo_do/form" component={props => <ClickargoDOForm dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/clickargo_do/review" component={props => <ClickargoDOReview dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/clickargo_do/my_transaction" component={props => <ClickargoDOMyTransaction dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/clickargo_do/my_transaction_detail" component={props => <ClickargoDOMyTransactionDetail dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/clickargo_do/invoice" component={props => <ClickargoDOInvoice dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/clickargo_sp2/form" component={props => <ClickargoSP2Form dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/clickargo_sp2/choose_container" component={props => <ClickargoSP2ChooseContainer dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/clickargo_sp2/my_transaction" component={props => <ClickargoSP2MyTransaction dataUserPortal={dataUserPortal} {...props}/>} />
                                    <Route exact path="/clickargo_sp2/my_transaction_detail" component={props => <ClickargoSP2MyTransactionDetail dataUserPortal={dataUserPortal} {...props}/>} />
                                    
                                    <Route exact path="/clickargo/redirect_orders/:id" component={props => <ClickargoRedirectOrders dataUserPortal={dataUserPortal} {...props}/>} />
                            </ToastProvider>
                            
                            <Route exact path="/railway" component={props => <Railway dataUserPortal={dataUserPortal}/>} />
                            <Route exact path="/warehouse" component={props => <Warehouse dataUserPortal={dataUserPortal}/>} />
                            <Route exact path="/analytics" component={props => <Analytics dataUserPortal={dataUserPortal}/>} />
                          </Switch>
                        )
                      }
                      
                      {/* </div> */}
                      {/* </div> */}
                    </div>
                    {/*<!-- end:: Content Dashboard -->*/}

                    {/*<!-- end:: Content -->*/}

                  </div>

                </div>
              </div>
            </div>
          </div>
        </HashRouter>
      );
    }
  }
  getUserDigico() {
    const encodedNpwp = encodeURIComponent(ENC_ID);
    const decryptorURI = API_USER_DIGICO;
    if ( ENC_ID === "" ) {
      document.location = "/portal/"
    } else {
      fetch(`${decryptorURI}${encodedNpwp}`)
      .then((res) => {
        if ( res.status === 200 )
          return res.json();
        else
          if ( ENC_ID === "" )
            document.location = "/portal/";
          else {
            this.setState({
              isFirstLoading: false, 
              isLoadingContent: false,
              dataUserPortal: {
                npwp: null, 
                nama_perusahaan: null,
                alamat: null,
                telp: null,
                nama_kontak_person: null,
                email: null,
                digicoUser: {
                  userName: null,
                  email: null,
                  companyID: null,
                  compType: null,
                  roleID: null
                }
              } 
            })
          }
      })
      .then( (data) => {
        this.setState({
          isFirstLoading: false, 
          isLoadingContent: false,
          dataUserPortal: {
            npwp: data.npwp, 
            nama_perusahaan: data.nama_perusahaan,
            alamat: data.alamat,
            telp: data.telp,
            nama_kontak_person: data.nama_kontak_person,
            email: data.email,
            digicoUser: {
              userName: data.digicoUser.userName,
              email: data.digicoUser.email,
              companyID: data.digicoUser.companyID,
              compType: data.digicoUser.compType,
              roleID: data.digicoUser.roleID
            }
          } 
        })
      })
      .catch(error => {
        this.setState({
          isFirstLoading: false, 
          isLoadingContent: false,
          dataUserPortal: {
            npwp: null, 
            nama_perusahaan: null,
            alamat: null,
            telp: null,
            nama_kontak_person: null,
            email: null,
            digicoUser: {
              userName: null,
              email: null,
              companyID: null,
              compType: null,
              roleID: null
            }
          } 
        })
      });
    }
  }
  getUserPerusahaan() {
    const encodedNpwp = encodeURIComponent(ENC_ID);
    const decryptorURI = API_DECRYPTOR;
    if ( ENC_ID === "" ) {
      document.location = "/portal/"
    } else {
      fetch(`${decryptorURI}${encodedNpwp}`)
        .then((res) => {
          if ( res.status === 200 )
            return res.json();
          else {
            this.setState({
              isFirstLoading: false, 
              isLoadingContent: false,
              dataUserPortal: {
                npwp: null, 
                nama_perusahaan: null,
                alamat: null,
                telp: null,
                nama_kontak_person: null,
                email: null
              } 
            })
          }
        })
        .then( (data) => {
          console.log("success", data);
          this.setState({
            isFirstLoading: false, 
            isLoadingContent: false,
            dataUserPortal: {
              npwp: data.npwp, 
              nama_perusahaan: data.nama_perusahaan,
              alamat: data.alamat,
              telp: data.telp,
              nama_kontak_person: data.nama_kontak_person,
              email: data.email
            } 
          })
        })
        .catch(error => {
            this.setState({
              isFirstLoading: false, 
              isLoadingContent: false,
              dataUserPortal: {
                npwp: null, 
                nama_perusahaan: null,
                alamat: null,
                telp: null,
                nama_kontak_person: null,
                email: null
              } 
            })
        });
    }
  }
  componentDidMount() {
    // const {activeHash} = this.state;
    // const self = this;
    // if ( in_array(activeHash, ["#/digico_list_bol", "#/digico_detail_bol", "#/digico_list_delivery", "#/digico_detail_delivery"]) ) 
    //   this.getUserDigico();
    // else
    //   this.getUserPerusahaan();
  }

  // componentWillMount() {
  //   this.unlisten = this.props.history.listen((location, action) => {
  //     var newLocation = location.hash;
  //     this.setState({activeHash: newLocation});        
  //   });
  // }
  // componentWillUnmount() {
  //      this.unlisten();
  // }
}
export default withRouter(App);
