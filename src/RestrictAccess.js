import React,{Component, Fragment} from 'react';

class RestrictAccess extends Component{
  render(){
    return(
      <Fragment>
        <h1 className="page-title"> Restrict Access</h1>
        <div className="row">
          <div className="col-md-12 page-404">
            <div className="details">
              <h3>Oops! You dont have access to NLE</h3>
              <p> You must login <a href="https://customer.beacukai.go.id/">here</a> to have an access. </p>
            </div>
          </div>
        </div>
      </Fragment>
    )
  }

}

export default RestrictAccess;