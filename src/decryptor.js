var http = require('http');
var url = require('url');
const crypto = require('crypto');

function encrypt(inputString) {
    var iv = new Buffer.from('');
    var key = new Buffer.from('6d7956657279546f705365637265744b', 'hex');
    let cipher = crypto.createCipheriv('aes-128-ecb',key, iv);
    var chunks = [];
    chunks.push(
        cipher.update(new Buffer.from(inputString, 'utf8'), 'buffer', 'base64')
    );
    chunks.push(cipher.final('base64'));
    var encryptedString = chunks.join('');
    return encryptedString;
}
function decrypt(inputString) {
    try {
        var iv = new Buffer.from('');
        var key = new Buffer.from('6d7956657279546f705365637265744b', 'hex');
        var decipher = crypto.createDecipheriv('aes-128-ecb',key, iv);
        var chunks = [];
        var encryptedBuffer = new Buffer.from(inputString, 'utf8').toString('binary');
        chunks.push(decipher.update(encryptedBuffer, 'base64', 'buffer'));
        chunks.push(decipher.final('buffer'));
        var decryptedBuffer = new Buffer.from(chunks.join(''));
        return decryptedBuffer;
    } catch(err) {
        return "";
    }
}

http.createServer(function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
    res.setHeader('Access-Control-Allow-Headers', '*');

    var q = url.parse(req.url, true);

    if( (q.pathname === "/decodenpwp/" || q.pathname === "/decodenpwp") && req.method === "GET"){
        // ambil parameter dari URL
        var npwp = q.query.npwp;
        
        if( npwp && npwp != null && npwp != "" && npwp != "null" ){
            if ( npwp.length == 0 || npwp.length < 24 ) {
                res.writeHead(200, {'Content-Type': 'text/html'});
                res.end();
            } else {
                var encValue = npwp.substring(15,22) + npwp.substring(0,15) + npwp.substring(22,24);
                var decrypted = decrypt(encValue);
                res.writeHead(200, {'Content-Type': 'text/html'});
                res.write("Acak Value: " + npwp+ "<br/>");
                res.write("Encrypted Value: " + encValue+ "<br/>");
                res.write(decrypted);
                res.end();
            }
        } else {
            res.writeHead(404, {'Content-Type': 'text/html'});
            return res.end("404 Not Found");
        }
    } else if( (q.pathname === "/getuserdigico/" || q.pathname === "/getuserdigico") && req.method === "GET"){
        // ambil parameter dari URL
        var npwp = q.query.npwp;
        
        if( npwp && npwp != null && npwp != "" && npwp != "null" ){
            var result = {npwp: null};
            if ( npwp.length == 0 || npwp.length < 24 ) {
                res.writeHead(200, {'Content-Type': 'application/json'});
                result = {
                    npwp: null
                };
            } else {
                var encValue = npwp.substring(15,22) + npwp.substring(0,15) + npwp.substring(22,24);
                var decrypted = decrypt(encValue);
                res.writeHead(200, {'Content-Type': 'application/json'});
                result = {
                    npwp: decrypted
                };
            }
            res.json(result);
            res.end();
        } else {
            res.writeHead(404, {'Content-Type': 'text/html'});
            return res.end("404 Not Found");
        }
    } else {
        var encrypted = encrypt("021942578058000");
        var acak = encodeURIComponent(encrypted.substring(7,22) + encrypted.substring(0,7) + encrypted.substring(22,24));
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write("Original Encrypted: "+encrypted+" <br/>");
        res.write("Acak: "+acak+" <br/>");
        res.write(acak);
        res.end();
    }
}).listen(8086);
console.log('server is running on http://localhost:8086');