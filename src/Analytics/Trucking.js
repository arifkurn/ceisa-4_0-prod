import React,{Component, Fragment} from 'react';
import {NavLink, HashRouter} from 'react-router-dom';
import {Map,GoogleApiWrapper,Marker} from 'google-maps-react';
import * as Constants from '../Constants.js';
import * as Helper from '../Helper.js';
import Highcharts from "highcharts";
import highcharts3d from 'highcharts/highcharts-3d'
import HighchartsReact from "highcharts-react-official";

class Trucking extends Component{
  constructor(props){
    super(props)
    this.state = {
      // Map Trucking
      markers: [
        {
          icon: 'assets/images/truck-icon.png',
          lat: '-6.1023223',
          lng: '106.9019725'
        },
        {
          icon: 'assets/images/truck-icon.png',
          lat: '-6.307923199999999',
          lng: '107.17208499999992'
        },
        {
          icon: 'assets/images/truck-icon.png',
          lat: '-6.1404077',
          lng: '106.9370861'
        }
      ],
      // Trucking
      truck_statistic: {
        joined_truck: 39000,
        empty_trip: 4510,
        occupied_trip: 7535,
        truck_utilized: 58,
      },
      trip_util_statistic: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun'],
      occupied_trip_data: [1,4,5,10,12,20],
      empty_trip_data: [18,15,5,7,12,1],
      trip_ratio: {
        roundTrip: 15145,
        nonRoundTrip: 45673
      },
    }
  }
  getTotalTripTruckStat() {
    const {truck_statistic} = this.state
    let totalTrip = truck_statistic.empty_trip + truck_statistic.occupied_trip
    return Helper.convertToThousand(totalTrip) + ' km'
  }
  render(){
    const contentTruckingMap = () => {
      return (
        <div className='col-xl-6 col-lg-6 order-lg-3 order-xl-1'>
            <div className='kt-portlet kt-portlet--tabs kt-portlet--height-fluid'>
              <div className='kt-portlet__body'>
                <div className='kt-widget15'>
                  <div className='kt-widget15__map'>
                    <div className="form-group row">
                      <div className="col-lg-5 mt-2" style={{marginTop:"10px", paddingRight:"0px"}}>
                        <div className="kt-font-boldest">LIVE Occupied Truck Empty</div>
                      </div>
                      <div className='col-auto mt-2' style={{marginTop:"10px", paddingRight:"0px"}}>
                        <div className="kt-font-boldest">Area : </div>
                      </div>
                      <div className="col-3">
                        <div className="kt-input-icon kt-input-icon--right">
                          <input type="text" className="form-control form-control-sm"></input>
                          <span className="kt-input-icon__icon kt-input-icon__icon--right">
                            <span><i className="flaticon-placeholder-2"></i></span>
                          </span>
                        </div>
                      </div>
                      <div className="col-lg-2">
                        <button type="submit" className="btn btn-primary btn-sm">Search</button>
                      </div>
                      
                    </div>
                  {/*MAPS*/}
                    <div style={{height: "450px", position: "relative", overflow: "hidden"}}>
                      <div style={{height: "100%", width: "100%", position: "absolute", top: "0px", left: "0px"}}>
                        <div className="gm-err-container">
                          <Map
                            google={this.props.google} 
                            zoom={13} 
                            initialCenter={{lat: '-6.1066044', lng: '106.8942988'}}
                            bounds={new this.props.google.maps.LatLngBounds()}
                          >
                          {
                            this.state.markers.map((store, index) => {
                              return <Marker key={index} id={index}
                                icon={store.icon ? store.icon: null}
                                position={{lat: parseFloat(store.lat),lng: parseFloat(store.lng)}}
                                />
                            })
                          }
                          </Map>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      )
    }

    const contentTruckingRoundTrip = () => {
      const chart = {
      chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: -5
        },
        height: '270px'
      },
      title: {
        text: 'Round Trip Ratio'
      },
      accessibility: {
        point: {
        valueSuffix: '%'
        }
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          colors: ['#efea5f',  '#006ea3'],
          allowPointSelect: true,
          cursor: 'pointer',
          startAngle: 270,
          slicedOffset: 15,
          depth: 45,
          dataLabels: {
            enabled: false,
          },
        }
      },
      series: [{
        type: 'pie',
        name: 'Trip Ratio',
        data: [
          {
            name: 'Round Trip',
            y: this.state.trip_ratio.roundTrip,
            sliced: true,
            selected: true,
          },
          {
            name: 'Non Round Trip',
            y: this.state.trip_ratio.nonRoundTrip,
            sliced: true,
            selected: true,
          },
        ],
      }],
      credits: {
        enabled: false
      }
    }
    return (
      <div className='col-xl-6 col-lg-6 order-lg-3 order-xl-1'>
        <div className='kt-portlet kt-portlet--tabs kt-portlet--height-fluid'>
          <div className="kt-portlet__head-label">
                  
            </div>
          <div className='kt-portlet__body'>
            <HighchartsReact highcharts={highcharts3d(Highcharts)} options={chart}/>
              <div className='row justify-content-center mt-5 mb-4'>
                <div className='col-lg-6 col-md-6 col-xs-12 text-center'>
                  <div className='mr-2 mt-2' style={{width: '10px', height: '10px', borderRadius: '50%', backgroundColor:'#efea5f', display:"inline-flex" }}></div>
                  <span>Round Trip</span> <br/>
                  <span>{Helper.convertToThousand(this.state.trip_ratio.roundTrip)}</span>
                </div>
                <div className='col-lg-6 col-md-6 col-xs-12 text-center'>
                  <div className='mr-2 mt-2' style={{width: '10px', height: '10px', borderRadius: '50%', backgroundColor:  '#006ea3', display:"inline-flex" }}></div>
                  <span>Non Round Trip</span> <br/>
                  <span>{Helper.convertToThousand(this.state.trip_ratio.nonRoundTrip)}</span>
                </div>
            </div>
            
            
            <div className='row justify-content-center mt-5'>
              <div className='col-lg-6 col-md-6 col-xs-12 text-center'>
                <span>Total Trip</span> <br/>
                <span>{Helper.convertToThousand(this.state.trip_ratio.roundTrip + this.state.trip_ratio.nonRoundTrip)}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      )
    }
    

  const contentTripUtilization = () => {
    const chart = {
      chart: {
        type: 'areaspline',
        height: '250px'
      },
      title: {
          text: ''
      },
      legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 170,
        y: 10,
        floating: true,
        borderWidth: 1,
        backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
      },
      xAxis: {
        categories: this.state.trip_util_statistic,
        lineColor: 'transparent'
      },
      yAxis: {
        title: {
           text: 'km'
        }
      },
      tooltip: {
        shared: true,
        valueSuffix: ' units'
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        areaspline: {
          fillOpacity: 0.3
        }
      },
      series: [{
        name: 'Occupied Trip',
        data: this.state.occupied_trip_data,
        color: '#9698d0'
      }, {
        name: 'Empty Trip',
        data: this.state.empty_trip_data,
        color: '#66dcff'
      }]
    }

    
    return (
      <div className='col-xl-6 col-lg-6 order-lg-3 order-xl-1'>
        <div className="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
          <div className="kt-portlet__body">
            <div className="row">
              <div className="col-8">
                <h4 className='text-center mt-2'>Trip Utilization Statistic</h4>
              </div>
              <div className="col-lg-4">
                <div className="input-group-sm mb-3">
                  <select className="custom-select">
                    <option selected>Last 6 Month</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </select>
                </div>
              </div>
                  
            </div>
            <div className='row mt-4'>
              <div className='col-12'>
                <HighchartsReact highcharts={Highcharts} options={chart} />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  const contentTruckingStatistic = () => {
    const chart = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
        height: '250px'
      },
      title: {
        text: '58%', //this.getPercentageTruckStat()
        align: 'center',
        verticalAlign: 'middle',
        y: 60,
      },
      subtitle: {
        text: 'Truck utilized',
        y: 80,
        align: 'center',
        verticalAlign: 'middle'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          colors: ['#56fefe', '#46d2fa'],
          dataLabels: {
          enabled: true,
          distance: -50,
          style: {
            fontWeight: 'bold',
            color: 'white'
          }
        },
        startAngle: -90,
        endAngle: 90,
        center: ['50%', '75%'],
        size: '110%'
        }
      },
      series: [{
        type: 'pie',
        name: '',
        innerSize: '50%',
        data: [
          ['', this.state.truck_statistic.truck_utilized],
          ['', 100-(this.state.truck_statistic.truck_utilized)],
        ]
      }],
      credits: {
        enabled: false
      }
    }
    return (
      <div className='col-xl-6 col-lg-6 order-lg-3 order-xl-1'>
        <div className='kt-portlet kt-portlet--tabs kt-portlet--height-fluid'>
          <div className='kt-portlet__body'>
            <div className="row">
              <div className="col-8">
                <h4 className='text-center mt-2'>Truck Statistic</h4>
              </div>
              <div className="col-lg-4">
                <div className="input-group-sm mb-3">
                  <select className="custom-select">
                    <option selected>June 2020</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </select>
                </div>
              </div> 
            </div>
            <div className='row mt-4'>
              <div className='col-12'>
                <div className='row'>
                  <div className='col-6'>
                    <div className='row'>
                      <div className='col-6 my-4'>Joined Truck</div>
                      <div className='col-6 my-4'>{(this.state.truck_statistic.joined_truck)/1000} k</div>
                    </div>
                    <div className='row'>
                      <div className='col-6 my-4'>Empty Trip</div>
                      <div className='col-6 my-4'>{Helper.convertToThousand(this.state.truck_statistic.empty_trip)} km</div>
                    </div>
                    <div className='row'>
                      <div className='col-6 my-4'>Occupied Trip</div>
                      <div className='col-6 my-4'>{Helper.convertToThousand(this.state.truck_statistic.occupied_trip)} km</div>
                    </div>
                    <div className='row'>
                      <div className='col-6 my-4'>Total Trip</div>
                      <div className='col-6 my-4'>{this.getTotalTripTruckStat()}</div>
                    </div>
                  </div>
                  <div className='col-6'>
                    <HighchartsReact highcharts={Highcharts} options={chart} />
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>        
      </div>
    )
  }
   
    return (
      <span>
        <div className='row'>
          {contentTruckingMap()}
          {contentTruckingRoundTrip()}
        </div>
        <div className='row'>
          {contentTruckingStatistic()}
          {contentTripUtilization()}
        </div>

      </span>
    );
  }

}

export default GoogleApiWrapper({
  apiKey: (Constants.googleMapAPI)
})(Trucking);